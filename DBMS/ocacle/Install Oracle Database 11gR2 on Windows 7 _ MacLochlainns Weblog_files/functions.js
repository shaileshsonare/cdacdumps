// Dropdown control for inline sidebars.
$(document).ready(
	function(){
	  var definition = $("#definitions").find("tr.uniqueKey").find(".definition").text();
	  $("body").addClass("script");
	  $(".dropdownclick").toggle(
		  function(){
		    $(this).siblings("div").slideDown("fast");
		    $(this).children("span").html("&uarr;");
		    $(this).attr("title","Click to hide content ...");
		  },
		  function(){
			  $(this).siblings("div").slideUp("fast");
			  $(this).children("span").html("&darr;");
		    $(this).attr("title","Click to show content ...");
		});
		$("#bubble").toggle(
			function(){
				$("#loginslider").slideDown("medium",function(){$("#user_login").focus()});
				$("#sidebar").animate({top:"+=65"},"medium");
			},
			function(){
				$("#loginslider").slideUp("medium");
				$("#sidebar").animate({top:"-=65"},"medium");
			});
	  $("abbr").hover(
		  function(){
			  $(this).css("cursor","help");
			  var currentClass = $(this).attr("class");
			  var definition = $("#definitions").find("tr." + currentClass).find(".definition").text();
        var word = $("table").find("tr." + currentClass).find(".word").text();
				//$(this).attr("title",word + ": " + definition);
			  $(this).tooltip({
				  bodyHandler: function() { 
				    return word + ": " + definition;
				  },
				delay: 0
				});
			},
			function(){}
		);
});