<%-- 
    Document   : header.jsp
    Created on : Mar 6, 2012, 11:30:01 PM
   
--%>

<div id="header">
    <h1>
        <jsp:setProperty name="links" property="linkName" value="APPLICATION_NAME" />
        <jsp:getProperty name="links" property="linkLabel" />
    </h1>
    <%
        if (session.getAttribute("loggedin") != null) {
    %>
    <div id="header_bottom">
        <a href="LogOut.web">
            <strong>Logout</strong>
        </a>
    </div>
    <%        }
    %>

</div>
<div id="extra_top">
    &nbsp;&nbsp;&nbsp;&nbsp;
</div>
