/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function isNumeric(number)
{
    var numericExpression = /^[0-9]+$/;
    if(number.match(numericExpression)){
          return true;
    }else{
          return false;
    }
}

function validateAmount(amount)
{
    if(amount=="")
    {
        alert("Please enter amount");
        document.withdrawal.amount.focus();
    }
    else
    {
        if(isNaN(amount))
        {
            alert("Amount should be Numeric");
            document.withdrawal.amount.focus();
        }
        else
        {
            if((amount%100)!=0)
            {
                alert("Amount should be multiple of 100");
                document.withdrawal.amount.focus();
            }
        }
    }
}

function validateChangePin()
{
    var newPin=document.changepin.new_pin.value;
    var reNewPin=document.changepin.re_new_pin.value;
    
    //alert(atmNumber +" "+ pin );
    if(newPin=="" && reNewPin =="")
    {
        alert("All fields must be filled out");
        return false;
    }
    if(newPin=="")
    {
        alert("Please Enter Pin Number");
        return false;
    }
    if(!isNumeric(newPin))
    {
        alert("Pin Number should be numeric");
        return false;
    }
    if(newPin.length!=4)
    {
        alert("Pin Number should be 4 digit");
        return false;
    }
    if(reNewPin=="")
    {
        alert("Please Re-Enter Pin Number");
        return false;
    }
    if(!isNumeric(reNewPin))
    {
        alert("Pin Number should be numeric again");
        return false;
    }
    if(reNewPin.length!=4)
    {
        alert("Pin Number should be 4 digit again");
        return false;
    }
    return true;
}
function validateUserLogin()
{
    var atmNumber=document.login.atmnumber.value;
    var pin=document.login.pin.value;
    //alert(atmNumber +" "+ pin );
    if(atmNumber=="" && pin =="")
    {
        alert("All fields must be filled out");
        return false;
    }
    if(atmNumber=="")
    {
        alert("Please Enter ATM Number");
        return false;
    }
    if(!isNumeric(atmNumber))
    {
        alert("ATM Number should be numeric");
        return false;
    }
    if(atmNumber.length!=16)
    {
        alert("ATM Number should be 16 digit");
        return false;
    }
    if(pin=="")
    {
        alert("Please Enter Pin Number");
        return false;
    }
    if(!isNumeric(pin))
    {
        alert("Pin Number should be numeric");
        return false;
    }
    if(pin.length!=4)
    {
        alert("Pin Number should be 4 digit");
        return false;
    }
    return true;
}