/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.user;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.web.connection.DBConnectionBean;
import java.sql.*;
import org.apache.log4j.Logger;
import com.web.locale.ResourceBundleBean;

/**
 *
 * 
 */
public class CheckBalance extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Logger userInfoLogger = Logger.getLogger("com.web.User");
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        try {
            Connection con = new DBConnectionBean().getCon();
            if (con != null) {
                userInfoLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_ESTABLISHED"));
                CheckBalanceBean obj = new CheckBalanceBean(con);
                HttpSession session = request.getSession();
                ResultSet rs = obj.getBalance(Integer.parseInt(session.getAttribute("usrId").toString()));
                if (rs != null && rs.next()) {
                    out.print("Base 3 ");
                    request.setAttribute("accounttype", rs.getString("hmm_account.AccountType"));
                    request.setAttribute("atmnumber", rs.getString("hmm_account.ATMNumber"));
                    request.setAttribute("balance", rs.getString("hmm_account.Balance"));
                    request.getRequestDispatcher("checkbalance.jsp").forward(request, response);
                } else {
                    request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_FETCH_USERINFO"));
                    request.getRequestDispatcher("checkbalance.jsp").forward(request, response);
                }
            } else {
                userInfoLogger.error(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
                request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
            }
        } catch (Exception ex) {
            userInfoLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_CHECK_BALANCE"));
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
