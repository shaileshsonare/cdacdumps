/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.user;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;
import com.web.hmm.CheckForFraudulentBehaviorBean;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.smsmodule.SMSClient;
/**
 *
 * 
 */
public class WithdrawalBean {

    Connection con;
    Logger withdrawalBeanLogger;

    public WithdrawalBean(Connection con) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        this.con = con;
        withdrawalBeanLogger = Logger.getLogger("com.web.User.WithdrawalBean");
        if (con == null) {
            con = (Connection) new DBConnectionBean().getCon();
            withdrawalBeanLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_REESTABLISHED"));
        } else {
            withdrawalBeanLogger.debug(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
        }
    }

    public int withdrawAmmount(String ATMNumber, String mobileNumber, int userId, int accountId, float ammount, java.util.Date loginTime, String pageVisitSeq) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        try {

              Calendar currentDate = Calendar.getInstance();
              SimpleDateFormat formatter=
              new SimpleDateFormat("yyyy-MM-dd");
              String dateNow = formatter.format(currentDate.getTime());

            String query="SELECT sum(Amount) FROM hmm_transaction WHERE DATE(Date)='"+dateNow+"' and AccountId="+accountId+" and UserId="+userId+" and Type=2";
            Statement stmt = con.createStatement();
            ResultSet rs0 = stmt.executeQuery(query);

            if(rs0.next())
            {

                float wAmount=rs0.getFloat(1);
                System.out.print(wAmount+"----"+ammount);

                float tAmount=wAmount+ammount;
                System.out.print(tAmount);
                if(tAmount<25000)
                {
                    query = "SELECT hmm_account.Balance,hmm_account.AccountType,hmm_accounttype.MinAmmountRequired FROM hmm_account INNER JOIN hmm_accounttype WHERE hmm_account.ATMNumber = '" + ATMNumber + "' AND hmm_account.AccountType=hmm_accounttype.id" + ";";
                    withdrawalBeanLogger.debug(query);
                    stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(query);
                    if (rs != null && rs.next())
                    {
                        float balance = rs.getFloat("hmm_account.Balance");
                        float minReqAmmount = rs.getInt("hmm_accounttype.MinAmmountRequired");
                        float finalAmmount = (balance - ammount);
                        if (finalAmmount > minReqAmmount)
                        {
                            java.util.Date date = new java.util.Date();
                            long withdrawalTime = (date.getTime() - loginTime.getTime())/(1000);
                            CheckForFraudulentBehaviorBean chkFraud = new CheckForFraudulentBehaviorBean(con);
                            if (!chkFraud.isFraudulentBehavior(accountId, 10, ammount, withdrawalTime, pageVisitSeq)) {
                                con.setAutoCommit(false);
                                withdrawalBeanLogger.info(rsBundle.getErrorMessage("NO_FRAUDLANT_BEHAVIOR"));
                                String query2 = "UPDATE hmm_account set hmm_account.Balance = ? WHERE hmm_account.ATMNumber = ?";
                                withdrawalBeanLogger.debug(query2);
                                PreparedStatement pstmt = con.prepareStatement(query2);
                                withdrawalBeanLogger.info("Ammount:" + (balance - ammount));
                                pstmt.setFloat(1, (balance - ammount));
                                pstmt.setString(2, ATMNumber);
                                if (pstmt.executeUpdate() != 0)
                                {
                                    Statement stmt2 = con.createStatement();
                                    String query3 = "INSERT INTO hmm_transaction (AccountId, UserId, Date, Type, Amount, WithdrawalTime, PageVisitSeqence)" +
                                        "VALUES (" + accountId + " , " + userId + " , " + ResourceBundleBean.toMysqlDateStr(new Date(date.getTime())) + ", 2 , " + ammount + " , " +withdrawalTime+ " ,'"+ pageVisitSeq+"');";
                                    withdrawalBeanLogger.debug(query3);
                                    stmt2.executeUpdate(query3, Statement.RETURN_GENERATED_KEYS);
                                    ResultSet rs2 = stmt2.getGeneratedKeys();
                                    if (rs2 != null && rs2.next()) {
                                        withdrawalBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                                        withdrawalBeanLogger.info(rsBundle.getErrorMessage("WITHDRAWAL_SUCCESS") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                                        con.commit();
                                        return 1;
                                    }
                                    else
                                    {
                                        con.rollback();
                                        withdrawalBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                                        withdrawalBeanLogger.info(rsBundle.getErrorMessage("WITHDRAWAL_FAILED") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                                    }
                                }
                                else
                                {
                                    con.rollback();
                                    withdrawalBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                                    withdrawalBeanLogger.info(rsBundle.getErrorMessage("WITHDRAWAL_FAILED") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                                }
                            }
                            else
                            {
                                //sms code integrate here
                                int x=(int)(Math.random()*8999)+1000;
                                System.out.println(x+"");
                                System.out.println(mobileNumber);
                                SMSClient s = new SMSClient(0);
                                String mobstr=""+mobileNumber;
                                s.sendMessage(mobstr, "Fraudulent behaviour found Varify by this OTPassword"+x);
                                return x;
                            }
                        }
                    }
                }
                else
                {
                    System.out.print("Daily amount out of reach.....");
                    withdrawalBeanLogger.debug("DAILY AMOUNT OUT OF REACH !...");
                    return -1;
                }
            }
            else
            {
                query = "SELECT hmm_account.Balance,hmm_account.AccountType,hmm_accounttype.MinAmmountRequired FROM hmm_account INNER JOIN hmm_accounttype WHERE hmm_account.ATMNumber = '" + ATMNumber + "' AND hmm_account.AccountType=hmm_accounttype.id" + ";";
                withdrawalBeanLogger.debug(query);
                stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);
                if (rs != null && rs.next())
                {
                    float balance = rs.getFloat("hmm_account.Balance");
                    float minReqAmmount = rs.getInt("hmm_accounttype.MinAmmountRequired");
                    float finalAmmount = (balance - ammount);
                    if (finalAmmount > minReqAmmount)
                    {
                        java.util.Date date = new java.util.Date();
                        long withdrawalTime = (date.getTime() - loginTime.getTime())/(1000);
                        CheckForFraudulentBehaviorBean chkFraud = new CheckForFraudulentBehaviorBean(con);
                        if (!chkFraud.isFraudulentBehavior(accountId, 10, ammount, withdrawalTime, pageVisitSeq)) {
                            con.setAutoCommit(false);
                            withdrawalBeanLogger.info(rsBundle.getErrorMessage("NO_FRAUDLANT_BEHAVIOR"));
                            String query2 = "UPDATE hmm_account set hmm_account.Balance = ? WHERE hmm_account.ATMNumber = ?";
                            withdrawalBeanLogger.debug(query2);
                            PreparedStatement pstmt = con.prepareStatement(query2);
                            withdrawalBeanLogger.info("Ammount:" + (balance - ammount));
                            pstmt.setFloat(1, (balance - ammount));
                            pstmt.setString(2, ATMNumber);
                            if (pstmt.executeUpdate() != 0)
                            {
                                Statement stmt2 = con.createStatement();
                                String query3 = "INSERT INTO hmm_transaction (AccountId, UserId, Date, Type, Amount, WithdrawalTime, PageVisitSeqence)" +
                                    "VALUES (" + accountId + " , " + userId + " , " + ResourceBundleBean.toMysqlDateStr(new Date(date.getTime())) + ", 2 , " + ammount + " , " +withdrawalTime+ " ,'"+ pageVisitSeq+"');";
                                withdrawalBeanLogger.debug(query3);
                                stmt2.executeUpdate(query3, Statement.RETURN_GENERATED_KEYS);
                                ResultSet rs2 = stmt2.getGeneratedKeys();
                                if (rs2 != null && rs2.next()) {
                                    withdrawalBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                                    withdrawalBeanLogger.info(rsBundle.getErrorMessage("WITHDRAWAL_SUCCESS") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                                    con.commit();
                                    return 1;
                                }
                                else
                                {
                                    con.rollback();
                                    withdrawalBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                                    withdrawalBeanLogger.info(rsBundle.getErrorMessage("WITHDRAWAL_FAILED") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                                }
                            }
                            else
                            {
                                con.rollback();
                                withdrawalBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                                withdrawalBeanLogger.info(rsBundle.getErrorMessage("WITHDRAWAL_FAILED") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                            }
                        }
                        else
                        {
                            //sms code integrate here
                            //sms code integrate here
                            int x=(int)(Math.random()*8999)+1000;
                            System.out.println(x+"");
                            System.out.println(mobileNumber);
                            SMSClient s = new SMSClient(0);
                            String mobstr=""+mobileNumber;
                            s.sendMessage(mobstr, "Fraudulent behaviour found Varify by this OTPassword"+x);
                            return x;
                        }
                    }
                }
            }
            
            /*String query = "SELECT hmm_account.Balance,hmm_account.AccountType,hmm_accounttype.MinAmmountRequired FROM hmm_account INNER JOIN hmm_accounttype WHERE hmm_account.ATMNumber = '" + ATMNumber + "' AND hmm_account.AccountType=hmm_accounttype.id" + ";";
            withdrawalBeanLogger.debug(query);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if (rs != null && rs.next()) {
                float balance = rs.getFloat("hmm_account.Balance");
                float minReqAmmount = rs.getInt("hmm_accounttype.MinAmmountRequired");
                float finalAmmount = (balance - ammount);
                if (finalAmmount > minReqAmmount) {
                    con.setAutoCommit(false);
                    String query2 = "UPDATE hmm_account set hmm_account.Balance = ? WHERE hmm_account.ATMNumber = ?";
                    withdrawalBeanLogger.debug(query2);
                    PreparedStatement pstmt = con.prepareStatement(query2);
                    withdrawalBeanLogger.info("Ammount:" + (balance - ammount));
                    pstmt.setFloat(1, (balance - ammount));
                    pstmt.setString(2, ATMNumber);
                    if (pstmt.executeUpdate() != 0) {
                        Statement stmt2 = con.createStatement();
                        java.util.Date date = new java.util.Date();
                        long withdrawalTime = (date.getTime() - loginTime.getTime())/(1000);
                        String query3 = "INSERT INTO hmm_transaction (AccountId, UserId, Date, Type, Amount, WithdrawalTime, PageVisitSeqence)" +
                                "VALUES (" + accountId + " , " + userId + " , " + ResourceBundleBean.toMysqlDateStr(new Date(date.getTime())) + ", 2 , " + ammount + " , " +withdrawalTime+ " ,'"+ pageVisitSeq+"');";
                        withdrawalBeanLogger.debug(query3);
                        stmt2.executeUpdate(query3, Statement.RETURN_GENERATED_KEYS);
                        ResultSet rs2 = stmt2.getGeneratedKeys();
                        if (rs2 != null && rs2.next()) {
                            withdrawalBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                            withdrawalBeanLogger.info(rsBundle.getErrorMessage("WITHDRAWAL_SUCCESS") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                            //Check if transaction is fraudulent in behavior
                            CheckForFraudulentBehaviorBean chkFraud = new CheckForFraudulentBehaviorBean(con);
                            if (!chkFraud.isFraudulentBehavior(accountId, 10, ammount, withdrawalTime, pageVisitSeq)) {
                                con.commit();
                                withdrawalBeanLogger.info(rsBundle.getErrorMessage("NO_FRAUDLANT_BEHAVIOR"));
                                return true;
                            } else {
                                
                                // Insert code for SMS integration
                                con.rollback();
                                withdrawalBeanLogger.info(rsBundle.getErrorMessage("OBSERVED_FRAUDLANT_BEHAVIOR"));
                            }
                        } else {
                            con.rollback();
                            withdrawalBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                            withdrawalBeanLogger.info(rsBundle.getErrorMessage("WITHDRAWAL_FAILED") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                        }
                    } else {
                        con.rollback();
                        withdrawalBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                        withdrawalBeanLogger.info(rsBundle.getErrorMessage("WITHDRAWAL_FAILED") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                    }
                }
            } else {
                withdrawalBeanLogger.warn(rsBundle.getErrorMessage("WITHDRAWAL_FAILED") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
            }*/
        } catch (SQLException ex) {
            withdrawalBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_WITHDRAWAL_BEAN") + " isValidUser");
            ex.printStackTrace();
        } catch (Exception ex) {
            withdrawalBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_WITHDRAWAL_BEAN") + " isValidUser");
            ex.printStackTrace();
        }
        return 0;
    }
}
