/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.web.user;

import com.web.connection.DBConnectionBean;
import com.web.locale.ResourceBundleBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * 
 */
public class FradulentVarify extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        try {
            Connection con = new DBConnectionBean().getCon();
            if (con != null) {
                String otPasswordClient=request.getParameter("otpassword");
                String otPassword=(String)session.getAttribute("otpassword");
                System.out.print(otPassword +"    "+otPasswordClient);
                if(!otPassword.equals(otPasswordClient))
                {
                    request.setAttribute("errorMessage", "Fraduolent Behaviour Confirm");
                    request.getRequestDispatcher("fraudulentvarify.jsp").forward(request, response);
                }
                else
                {
                    String ATMNumber = session.getAttribute("usrATMNumber").toString();
                    float ammount = (Float)session.getAttribute("amount");
                    int userId = Integer.parseInt(session.getAttribute("usrId").toString());
                    int accountId = Integer.parseInt(session.getAttribute("accountId").toString());
                    java.util.Date loginTime = (java.util.Date)session.getAttribute("logintime");
                    String pageVisitSeq = session.getAttribute("pageVisitSeq").toString();
                    
                    String query = "SELECT hmm_account.Balance,hmm_account.AccountType,hmm_accounttype.MinAmmountRequired FROM hmm_account INNER JOIN hmm_accounttype WHERE hmm_account.ATMNumber = '" + ATMNumber + "' AND hmm_account.AccountType=hmm_accounttype.id" + ";";
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(query);
                    if (rs != null && rs.next())
                    {
                        float balance = rs.getFloat("hmm_account.Balance");
                        java.util.Date date = new java.util.Date();
                        long withdrawalTime = (date.getTime() - loginTime.getTime())/(1000);
                        con.setAutoCommit(false);

                        String query2 = "UPDATE hmm_account set hmm_account.Balance = ? WHERE hmm_account.ATMNumber = ?";
                        PreparedStatement pstmt = con.prepareStatement(query2);
                        pstmt.setFloat(1, (balance - ammount));
                        pstmt.setString(2, ATMNumber);
                        if (pstmt.executeUpdate() != 0)
                        {
                            Statement stmt2 = con.createStatement();
                            String query3 = "INSERT INTO hmm_transaction (AccountId, UserId, Date, Type, Amount, WithdrawalTime, PageVisitSeqence)" +
                                "VALUES (" + accountId + " , " + userId + " , " + ResourceBundleBean.toMysqlDateStr(new Date(date.getTime())) + ", 2 , " + ammount + " , " +withdrawalTime+ " ,'"+ pageVisitSeq+"');";
                            stmt2.executeUpdate(query3, Statement.RETURN_GENERATED_KEYS);
                            ResultSet rs2 = stmt2.getGeneratedKeys();
                            if (rs2 != null && rs2.next())
                            {
                                con.commit();
                                request.setAttribute("successMessage", "Please Collect Cash");
                                request.getRequestDispatcher("LogOut.web").forward(request, response);
                                //request.getRequestDispatcher("withdrawal.jsp").forward(request, response);
                            }
                            else
                            {
                                con.rollback();
                            }
                        }
                        else
                        {
                            con.rollback();
                        }
                    }
                }
            }
        } catch(Exception e)
        {
            System.out.print(e);
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
