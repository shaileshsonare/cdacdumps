/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.user;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import javax.servlet.http.*;
import com.web.locale.ResourceBundleBean;
import java.sql.Connection;
import com.web.connection.DBConnectionBean;

/**
 *
 * 
 */
public class DepositAmount extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Logger depositAmountLogger = Logger.getLogger("com.web.User");
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        HttpSession session = request.getSession();
        try {
            Connection con = new DBConnectionBean().getCon();
            if (con != null) {
                String ATMNumber = session.getAttribute("usrATMNumber").toString();
                float depositAmmount = Integer.parseInt(request.getParameter("depositammount"));
                int userId = Integer.parseInt(session.getAttribute("usrId").toString());
                int accountId = Integer.parseInt(session.getAttribute("accountId").toString());
                DepositAmountBean obj = new DepositAmountBean(con);
                request.setAttribute("depositammount", request.getParameter("depositammount"));
                java.util.Date loginTime = (java.util.Date) session.getAttribute("logintime");
                String pageVisitSeq = session.getAttribute("pageVisitSeq").toString();
                if (obj.depositAmmount(ATMNumber, userId, accountId, depositAmmount, loginTime, pageVisitSeq)) {
                    request.setAttribute("successMessage", rsBundle.getText("AMMOUNT_CREDITED_SUCCESSFULLY"));
                    request.getRequestDispatcher("depositamount.jsp").forward(request, response);
                } else {
                    request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_PROCESS_TRANSACTION"));
                    request.getRequestDispatcher("depositamount.jsp").forward(request, response);
                }
            } else {
                depositAmountLogger.error(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
                request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
