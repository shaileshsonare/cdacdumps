/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.user;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;

/**
 *
 * 
 */
public class GetLastNTransactionBean {

    private String date;
    private int transId,  accountType;
    private Connection con;
    private float ammount;
    private Logger getLastNTransactionBeanLogger;
    private ResourceBundleBean rsBundle;

    public GetLastNTransactionBean() {
        rsBundle = new ResourceBundleBean();
        getLastNTransactionBeanLogger = Logger.getLogger("com.web.User.GetLastNTransactionBeanLogger");
    }
//select * from hmm_transaction where hmm_transaction.AccountId=2 order by hmm_transaction.`Date` desc limit 0,5;

    public void setConection() {
        con = new DBConnectionBean().getCon();
    }

    public int getAccountType() {
        return accountType;
    }

    public float getAmmount() {
        return ammount;
    }

    public String getDate() {
        return date;
    }

    public int getTransId() {
        return transId;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public void setAmmount(float ammount) {
        this.ammount = ammount;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTransId(int transId) {
        this.transId = transId;
    }

    public ResultSet getLastNTransactions(int accountId, int n) {
        ResultSet rs = null;
        try {
            String query = "SELECT hmm_transaction.Id, hmm_transaction.`Date`, hmm_transaction.`Type`,hmm_transaction.Amount FROM hmm_transaction WHERE hmm_transaction.AccountId="+accountId+" ORDER BY hmm_transaction.`Date` DESC LIMIT 0,"+n+";";
            getLastNTransactionBeanLogger.debug(query);
            Statement stmt = con.createStatement();
            rs = stmt.executeQuery(query);
        } catch (SQLException ex) {
            getLastNTransactionBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_GET_LAST_N_TRANSACTION_BEAN") + " getLastNTransactions");
            ex.printStackTrace();
        } catch (Exception ex) {
            getLastNTransactionBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_GET_LAST_N_TRANSACTION_BEAN") + " getLastNTransactions");
            ex.printStackTrace();
        }
        return rs;
    }
}
