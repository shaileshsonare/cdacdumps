/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.user;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;

/**
 *
 * 
 */
public class CheckBalanceBean {

    Connection con;
    Logger checkBalanceBeanLogger;

    public CheckBalanceBean(Connection con) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        this.con = con;
        checkBalanceBeanLogger = Logger.getLogger("com.web.User.CheckBalanceBean");
        if (con == null) {
            con = (Connection) new DBConnectionBean().getCon();
            checkBalanceBeanLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_REESTABLISHED"));
        } else {
            checkBalanceBeanLogger.debug(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
        }
    }

    public ResultSet getBalance(int userId) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        ResultSet rs = null;
        try {
            String query = "SELECT hmm_account.AccountType, hmm_account.Balance, hmm_account.ATMNumber FROM hmm_account WHERE hmm_account.UserId = "+userId +";";
            checkBalanceBeanLogger.debug(query);
            Statement stmt = con.createStatement();
            rs = stmt.executeQuery(query);
        } catch (SQLException ex) {
            checkBalanceBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_FORGOT_PASSWORD_BEAN") + " isValidUser");
            ex.printStackTrace();
        } catch (Exception ex) {
            checkBalanceBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_FORGOT_PASSWORD_BEAN") + " isValidUser");
            ex.printStackTrace();
        }
        return rs;
    }
}
