/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.user;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;
import com.web.connection.EncDec;

/**
 *
 * 
 */
public class ValidateUserBean {

    Connection con;
    Logger validateUserBeanLogger;
    int usrId, accountType, accountId;
    String usrFirstName,
            usrLastName,
            ATMNumber,
            mobileNumber;

    public ValidateUserBean(Connection con) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        this.con = con;
        validateUserBeanLogger = Logger.getLogger("com.web.User.ValidateUserBean");
        if (con == null) {
            con = (Connection) new DBConnectionBean().getCon();
            validateUserBeanLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_REESTABLISHED"));
        } else {
            validateUserBeanLogger.debug(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
        }
    }

    public String getUsrFirstName() {
        return usrFirstName;
    }

    public int getUsrId() {
        return usrId;
    }

    public String getUsrLastName() {
        return usrLastName;
    }

    public String getATMNumber() {
        return ATMNumber;
    }
    
    public String getMobileNumber() {
        return mobileNumber;
    }

    public int getAccountType() {
        return accountType;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public void setUsrFirstName(String usrFirstName) {
        this.usrFirstName = usrFirstName;
    }

    public void setUsrId(int usrId) {
        this.usrId = usrId;
    }

    public void setUsrLastName(String usrLastName) {
        this.usrLastName = usrLastName;
    }

    public void setATMNumber(String ATMNumber) {
        this.ATMNumber = ATMNumber;
    }
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    boolean isValidUser(String ATMNumber, String pin) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        try {
            EncDec ed=EncDec.getEDObject();
            String encPin=ed.meth(pin);
            Statement stmt = con.createStatement();
            String query = "SELECT Id, UserId, AccountType, Pin, ATMNumber FROM hmm_account WHERE ATMNumber = '" + ATMNumber + "'";
            validateUserBeanLogger.debug(query);
            ResultSet rs = stmt.executeQuery(query);
            validateUserBeanLogger.debug(query);
            if (rs != null && rs.next()) {
                if (encPin.equals(rs.getString("Pin"))) {
                    setUsrId(Integer.parseInt(rs.getString("UserId")));
                    query = "SELECT FirstName, LastName, MobileNumber FROM hmm_user WHERE Id = " + usrId + "";
                    validateUserBeanLogger.debug(query);
                    Statement stmt2 = con.createStatement();
                    ResultSet rs2 = stmt2.executeQuery(query);
                    if (rs2 != null && rs2.next()) {
                        setUsrId(Integer.parseInt(rs.getString("UserId")));
                        setATMNumber(rs.getString("ATMNumber"));
                        setAccountId(rs.getInt("Id"));
                        setATMNumber(ATMNumber);
                        setUsrFirstName(rs2.getString("FirstName"));
                        setUsrLastName(rs2.getString("LastName"));
                        setMobileNumber(rs2.getString("MobileNumber"));
                        validateUserBeanLogger.info(rsBundle.getErrorMessage("LOGIN_SUCCESSFULL") + "ATMNumber: " + ATMNumber);
                        return true;
                    }
                } else {
                    validateUserBeanLogger.warn(rsBundle.getErrorMessage("LOGIN_FAILED") + "ATMNumber: " + ATMNumber);
                }
            } else {
                validateUserBeanLogger.warn(rsBundle.getErrorMessage("LOGIN_FAILED") + "ATMNumber: " + ATMNumber);
            }
        } catch (SQLException ex) {
            validateUserBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_VALIDATE_USER_BEAN") + " isValidUser");
            ex.printStackTrace();
        } catch (Exception ex) {
            validateUserBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_VALIDATE_USER_BEAN") + " isValidUser");
            ex.printStackTrace();
        }
        return false;
    }
}
