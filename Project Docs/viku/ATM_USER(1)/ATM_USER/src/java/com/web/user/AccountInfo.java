/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.user;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.web.connection.DBConnectionBean;
import java.sql.*;
import org.apache.log4j.Logger;
import com.web.locale.ResourceBundleBean;
import javax.servlet.http.*;

/**
 *
 * 
 */
public class AccountInfo extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Logger accountInfoLogger = Logger.getLogger("com.web.User");
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        HttpSession session = request.getSession();
        try {
            Connection con = new DBConnectionBean().getCon();
            if (con != null) {
                accountInfoLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_ESTABLISHED"));
                AccountInfoBean accInfoBean = new AccountInfoBean(con);
                ResultSet rs = accInfoBean.getAccountInfo(Integer.parseInt(session.getAttribute("usrId").toString()));
                if (rs != null && rs.next()) {
                    request.setAttribute("firstname", rs.getString("hmm_user.FirstName"));
                    request.setAttribute("lastname", rs.getString("hmm_user.LastName"));
                    request.setAttribute("mobilenumber", rs.getString("hmm_user.MobileNumber"));
                    request.setAttribute("address", rs.getString("hmm_user.Address"));
                    request.setAttribute("emailaddress", rs.getString("hmm_user.EmailAddress"));
                    request.setAttribute("pancardnumber", rs.getString("hmm_user.PanCardNumber"));
                    request.setAttribute("accountid", rs.getString("hmm_account.Id"));
                    request.setAttribute("accounttype", rs.getString("hmm_account.AccountType"));
                    request.setAttribute("balance", rs.getString("hmm_account.Balance"));
                    request.setAttribute("atmnumber", rs.getString("hmm_account.ATMNumber"));
                    request.setAttribute("userid", request.getParameter("userId"));
                    request.getRequestDispatcher("accountinfo.jsp").forward(request, response);
                } else {
                    request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_FETCH_ACCOUNTINFO"));
                    request.getRequestDispatcher("accountinfo.jsp").forward(request, response);
                }
            } else {
                accountInfoLogger.error(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
                request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
            }
        } catch (Exception ex) {
            accountInfoLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_ACCOUNT_INFO"));
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
