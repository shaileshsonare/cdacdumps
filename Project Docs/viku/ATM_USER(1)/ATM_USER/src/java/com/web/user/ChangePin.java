/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.web.user;

import com.web.connection.DBConnectionBean;
import com.web.connection.EncDec;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * 
 */
public class ChangePin extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String newPin=request.getParameter("new_pin");
        String reNewPin=request.getParameter("re_new_pin");

        HttpSession session = request.getSession();
        String accountId = session.getAttribute("accountId").toString();
        System.out.print(accountId);
        Connection con = new DBConnectionBean().getCon();
        try {
            if(newPin.equals(reNewPin))
            {
                EncDec ed=EncDec.getEDObject();
                String encNewPin=ed.meth(newPin);
                if (con != null) {
                    Statement stmt=con.createStatement();
                    String query = "UPDATE hmm_account set Pin='"+encNewPin+"' WHERE Id='"+accountId+"'";
                    if(stmt.executeUpdate(query)==1)
                    {
                        request.setAttribute("successMessage","Pin changed successfully...!");
                        request.getRequestDispatcher("changepin.jsp").forward(request, response);
                    }
                    else
                    {
                        request.setAttribute("errorMessage", "Pin change failed...!");
                        request.getRequestDispatcher("changepin.jsp").forward(request, response);
                    }
                }
                else
                {
                    request.setAttribute("errorMessage", "Error in DB Connection!");
                    request.getRequestDispatcher("changepin.jsp").forward(request, response);
                }
            }
            else
            {
                request.setAttribute("errorMessage", "Type miss-match in old and new password");
                request.getRequestDispatcher("changepin.jsp").forward(request, response);
            }
            
        } catch(Exception e){
            System.out.print(e);
        }finally  {
            out.close();
        }
        
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
