/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.user;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;

/**
 *
 *
 */
public class DepositAmountBean {

    Connection con;
    Logger depositAmountBeanLogger;

    public DepositAmountBean(Connection con) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        this.con = con;
        depositAmountBeanLogger = Logger.getLogger("com.web.User.WithdrawalBean");
        if (con == null) {
            con = (Connection) new DBConnectionBean().getCon();
            depositAmountBeanLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_REESTABLISHED"));
        } else {
            depositAmountBeanLogger.debug(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
        }
    }

    public boolean depositAmmount(String ATMNumber, int userId, int accountId, float ammount, java.util.Date loginTime, String pageVisitSeq) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        try {
            String query = "SELECT hmm_account.Balance,hmm_account.AccountType,hmm_accounttype.MinAmmountRequired FROM hmm_account INNER JOIN hmm_accounttype WHERE hmm_account.ATMNumber = '" + ATMNumber + "' AND hmm_account.AccountType=hmm_accounttype.id" + ";";
            depositAmountBeanLogger.debug(query);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if (rs != null && rs.next()) {
                float balance = rs.getFloat("hmm_account.Balance");
                float minReqAmmount = rs.getInt("hmm_accounttype.MinAmmountRequired");
                float finalAmmount = (balance + ammount);
                if (finalAmmount > minReqAmmount) {
                    con.setAutoCommit(false);
                    String query2 = "UPDATE hmm_account set hmm_account.Balance = ? WHERE hmm_account.ATMNumber = ?";
                    depositAmountBeanLogger.debug(query2);
                    PreparedStatement pstmt = con.prepareStatement(query2);
                    depositAmountBeanLogger.info("Ammount:" + finalAmmount);
                    pstmt.setFloat(1, finalAmmount);
                    pstmt.setString(2, ATMNumber);
                    if (pstmt.executeUpdate() != 0) {
                        Statement stmt2 = con.createStatement();
                        java.util.Date date = new java.util.Date();
                        long depositeTime = (date.getTime() - loginTime.getTime()) / (1000);
                        // type = 1 means credit
                        String query3 = "INSERT INTO hmm_transaction (AccountId, UserId, Date, Type, Amount, WithdrawalTime, PageVisitSeqence)"
                                + "VALUES (" + accountId + " , " + userId + " , " + ResourceBundleBean.toMysqlDateStr(new Date(date.getTime())) + ", 1 , " + ammount + " , " + depositeTime + " ,'"+ pageVisitSeq+"');";
                        depositAmountBeanLogger.debug(query3);
                        stmt2.executeUpdate(query3, Statement.RETURN_GENERATED_KEYS);
                        ResultSet rs2 = stmt2.getGeneratedKeys();
                        if (rs2 != null && rs2.next()) {
                            depositAmountBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                            depositAmountBeanLogger.info(rsBundle.getErrorMessage("DEPOSIT_SUCCESS") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                            con.commit();
                            return true;
                        } else {
                            con.rollback();
                            depositAmountBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                            depositAmountBeanLogger.info(rsBundle.getErrorMessage("DEPOSIT_FAILED") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                        }
                    } else {
                        con.rollback();
                        depositAmountBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                        depositAmountBeanLogger.info(rsBundle.getErrorMessage("DEPOSIT_FAILED") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
                    }
                }
            } else {
                depositAmountBeanLogger.warn(rsBundle.getErrorMessage("DEPOSIT_FAILED") + " ATMNumber: " + ATMNumber + " Ammount:" + ammount);
            }
        } catch (SQLException ex) {
            depositAmountBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_WITHDRAWAL_BEAN") + " isValidUser");
            ex.printStackTrace();
        } catch (Exception ex) {
            depositAmountBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_WITHDRAWAL_BEAN") + " isValidUser");
            ex.printStackTrace();
        }
        return false;
    }
}
