/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.user;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;

/**
 *
 *
 */
public class AccountInfoBean {
     Connection con;
    Logger accountInfoBeanLogger;

    public AccountInfoBean(Connection con) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        this.con = con;
        accountInfoBeanLogger = Logger.getLogger("com.web.User.UserInfoBean");
        if (con == null) {
            con = (Connection) new DBConnectionBean().getCon();
            accountInfoBeanLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_REESTABLISHED"));
        } else {
            accountInfoBeanLogger.debug(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
        }
    }

     public ResultSet getAccountInfo(int userId) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        ResultSet rs = null;
        try {
            Statement stmt = con.createStatement();
            String query = "SELECT hmm_user.Id, hmm_user.FirstName, hmm_user.LastName, hmm_user.MobileNumber,hmm_user.Address, hmm_user.EmailAddress,hmm_user.PanCardNumber, hmm_account.Id,hmm_account.AccountType,hmm_account.Balance, hmm_account.ATMNumber FROM hmm_user INNER JOIN hmm_account WHERE hmm_user.Id = "+userId+" AND hmm_user.Id = hmm_account.UserId;";
            rs = stmt.executeQuery(query);
        } catch (SQLException ex) {
            accountInfoBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_ACCOUNT_INFO_BEAN") + " getAccountInfo");
            ex.printStackTrace();
        } catch (Exception ex) {
            accountInfoBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_ACCOUNT_INFO_BEAN") + " getAccountInfo");
            ex.printStackTrace();
        }
        return rs;
    }
}
