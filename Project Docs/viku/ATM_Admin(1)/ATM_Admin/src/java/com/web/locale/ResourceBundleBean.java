/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.web.locale;

import java.util.*;
/**
 *
 * 
 */
public class ResourceBundleBean {
    private String linkName, linkLabel;
    private String errorLabel;
    
    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public String getLinkLabel() {
        linkLabel = ResourceBundle.getBundle("com/web/locale/enUS/labelStrings").getString(linkName);
        return linkLabel;
    }

    public String getErrorMessage(String errorMessage) {
        return ResourceBundle.getBundle("com/web/locale/enUS/errorStrings").getString(errorMessage);
    }

    public void setErrorLabel(String errorLabel) {
        this.errorLabel = errorLabel;
    }
    
    public String getText(String key){
        return ResourceBundle.getBundle("com/web/locale/enUS/labelStrings").getString(key);
    }
}
