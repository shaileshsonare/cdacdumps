/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.admin;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;
import com.web.connection.EncDec;

/**
 *
 * 
 */
public class AddUserBean {

    private String firstName,  lastName,  panCardNumber,  mobileNumber,  address,  atmNumber,  pinNumber,  emailAddress;
    int accountType, balance;
    private Connection con;
    private Logger addUserBeanLogger;
    private int userId;

    public AddUserBean(Connection con) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        this.con = con;
        addUserBeanLogger = Logger.getLogger("com.web.User.AddUserBean");
        if (con == null) {
            con = (Connection) new DBConnectionBean().getCon();
            addUserBeanLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_REESTABLISHED"));
        } else {
            addUserBeanLogger.debug(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
        }
    }

    public String getAddress() {
        return address;
    }

    public Connection getCon() {
        return con;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getPanCardNumber() {
        return panCardNumber;
    }

    public String getPinNumber() {
        return pinNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public int getUserId() {
        return userId;
    }

    public int getAccountType() {
        return accountType;
    }

    public int getBalance() {
        return balance;
    }

    public String getAtmNumber() {
        return atmNumber;
    }

    public void setAtmNumber(String atmNumber) {
        this.atmNumber = atmNumber;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPinNumber(String pinNumber) {
        this.pinNumber = pinNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setPanCardNumber(String panCardNumber) {
        this.panCardNumber = panCardNumber;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    boolean isUserExist(String panCardNumber) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        try {
            Statement stmt = con.createStatement();
            String query = "SELECT Id, panCardNumber FROM hmm_user WHERE PanCardNumber = '" + panCardNumber + "'";
            ResultSet rs = stmt.executeQuery(query);
            addUserBeanLogger.debug(query);
            if (rs != null && rs.next()) {
                addUserBeanLogger.debug(rsBundle.getErrorMessage("QUERY_EXECUTED_SUCCESSFULLY"));
                addUserBeanLogger.info(rsBundle.getErrorMessage("USERNAME_ALLREADY_EXIST") + "PanCardNumber: " + panCardNumber);
                return true;
            } else {
                 addUserBeanLogger.debug(rsBundle.getErrorMessage("QUERY_EXECUTED_SUCCESSFULLY"));
                addUserBeanLogger.warn(rsBundle.getErrorMessage("USERNAME_DOES_NOT_EXIST") + "PanCardNumber: " + panCardNumber);
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            addUserBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_ADD_USER_BEAN") + " isUserExist");
            ex.printStackTrace();
        } catch (Exception ex) {
            addUserBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_ADD_USER_BEAN") + " isUserExist");
            ex.printStackTrace();
        }
        return false;
    }

    public boolean insertRecord() {
        boolean result = false;
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        EncDec ed=EncDec.getEDObject();
        String encPin=ed.meth(pinNumber);
        try {
            con.setAutoCommit(false);
            String query = "INSERT INTO hmm_user" +
                    "(FirstName, LastName, PanCardNumber, MobileNumber, Address, EmailAddress)" +
                    "values ('" + firstName + "','" + lastName + "','" + panCardNumber + "','" + mobileNumber + "','" + address + "','" + emailAddress + "')";
            addUserBeanLogger.debug(query);

            Statement stmt = (Statement) con.createStatement();
            stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = stmt.getGeneratedKeys();
            balance = 1000;
            if (rs.next()) {
                addUserBeanLogger.debug(rsBundle.getErrorMessage("QUERY_EXECUTED_SUCCESSFULLY"));
                setUserId(rs.getInt(1));
                query = "INSERT INTO hmm_account" +
                        "(UserId, AccountType, ATMNumber, Pin, Balance)" +
                        "values (" + getUserId() + "," + accountType + ",'" + atmNumber + "','" + encPin + "'," + balance + ")";
                addUserBeanLogger.debug(query);
                Statement stmt2 = (Statement) con.createStatement();
                stmt2.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
                ResultSet rs1 = stmt2.getGeneratedKeys();
                if (rs1.next()) {
                    addUserBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                    result = true;
                    con.commit();
                } else {
                    addUserBeanLogger.debug("Query executed failed");
                    result = false;
                    con.rollback();
                }
            }
        } catch (SQLException ex) {
            addUserBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_CREATE_ACCOUNT_BEAN") + " insertRecord : SQLException");
            ex.printStackTrace();
        } catch (Exception ex) {
            addUserBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_CREATE_ACCOUNT_BEAN") + " insertRecord : Exception");
            ex.printStackTrace();
        }
        return result;
    }
}
