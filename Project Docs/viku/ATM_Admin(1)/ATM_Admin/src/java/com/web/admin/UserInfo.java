/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.admin;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.web.connection.DBConnectionBean;
import java.sql.*;
import org.apache.log4j.Logger;
import com.web.locale.ResourceBundleBean;

/**
 *
 * 
 */
public class UserInfo extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Logger userInfoLogger = Logger.getLogger("com.web.Admin");
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        try {
            Connection con = new DBConnectionBean().getCon();
            if (con != null) {
                userInfoLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_ESTABLISHED"));
                UserInfoBean userInfoBean = new UserInfoBean(con);
                ResultSet rs = userInfoBean.getUserInfo(Integer.parseInt(request.getParameter("userId").toString()));
                if (rs != null && rs.next()) {
                    request.setAttribute("firstname", rs.getString("hmm_user.FirstName"));
                    request.setAttribute("lastname", rs.getString("hmm_user.LastName"));
                    request.setAttribute("mobilenumber", rs.getString("hmm_user.MobileNumber"));
                    request.setAttribute("address", rs.getString("hmm_user.Address"));
                    request.setAttribute("emailaddress", rs.getString("hmm_user.EmailAddress"));
                    request.setAttribute("pancardnumber", rs.getString("hmm_user.PanCardNumber"));
                    request.setAttribute("accountid", rs.getString("hmm_account.Id"));
                    request.setAttribute("accounttype", rs.getString("hmm_account.AccountType"));
                    request.setAttribute("balance", rs.getString("hmm_account.Balance"));
                    request.setAttribute("atmnumber", rs.getString("hmm_account.ATMNumber"));
                    request.setAttribute("userid", request.getParameter("userId"));
                    request.getRequestDispatcher("userinfo.jsp").forward(request, response);
                } else {
                    request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_FETCH_USERINFO"));
                    request.getRequestDispatcher("userinfo.jsp").forward(request, response);
                }
            } else {
                userInfoLogger.error(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
                request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
            }
        } catch (Exception ex) {
            userInfoLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_USER_INFO"));
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
     // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
