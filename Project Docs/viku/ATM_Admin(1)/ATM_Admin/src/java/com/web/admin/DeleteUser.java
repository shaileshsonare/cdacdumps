/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.web.admin;

import com.web.connection.DBConnectionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * 
 */
public class DeleteUser extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String accountId=request.getParameter("accountId");
        String userId=request.getParameter("userId");
        out.print(userId+"  "+accountId);
        try {
            Connection con = new DBConnectionBean().getCon();
            if (con != null)
            {
                String query="delete from hmm_user where Id="+userId;
                Statement stmt=con.createStatement();
                int i = stmt.executeUpdate(query);

                query = "delete from hmm_account where Id="+accountId;
                stmt=con.createStatement();
                int j = stmt.executeUpdate(query);
                
                if(i==1 && j==1)
                {
                    request.setAttribute("successMessage", "Successfully Deleted");
                }
                else
                {
                    request.setAttribute("errorMessage", "Not Done");
                }
            }
            else
            {
                request.setAttribute("errorMessage", "Not Done");
            }
        }catch(Exception e)
        {
            System.out.print(e);
        }

        ServletContext ctx = this.getServletContext();
        RequestDispatcher rd = ctx.getRequestDispatcher("/main.jsp");
        rd.include(request, response);
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
