package com.web.connection;

public class EncDec
{
    private static EncDec edobj;
    final String Key="Encrypt";
    private EncDec()
    {

    }
    public String meth(String str)
    {
    	StringBuffer sb = new StringBuffer (str);

          int lenStr = str.length();
          int lenKey = Key.length();

          //
          // For each character in our string, encrypt it...
          for ( int i = 0, j = 0; i < lenStr; i++, j++ )
          {
             if ( j >= lenKey ) j = 0;  // Wrap 'round to beginning of key string.

             //
             // XOR the chars together. Must cast back to char to avoid compile error.
             //
             sb.setCharAt(i, (char)(str.charAt(i) ^ Key.charAt(j)));
          }
          return sb.toString();
    }
    public static synchronized EncDec getEDObject()
    {
        if(edobj==null)
        {
            edobj=new EncDec();
        }
        return edobj;
    }
}
