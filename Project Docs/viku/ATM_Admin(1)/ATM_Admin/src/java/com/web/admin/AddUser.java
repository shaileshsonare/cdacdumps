/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.admin;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import org.apache.log4j.Logger;
import com.web.locale.ResourceBundleBean;
import java.sql.Connection;
import com.web.connection.DBConnectionBean;

/**
 *
 * 
 */
public class AddUser extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Logger addUserLogger = Logger.getLogger("com.web.User");
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        try {
            Connection con = new DBConnectionBean().getCon();
            if (con != null) {
                addUserLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_ESTABLISHED"));
                AddUserBean addUserBean = new AddUserBean(con);
                HttpSession session = request.getSession();
                request.setAttribute("atmnumber", request.getParameter("atmnumber"));
                request.setAttribute("pin", request.getParameter("pin"));
                request.setAttribute("firstname", request.getParameter("firstname"));
                request.setAttribute("lastname", request.getParameter("lastname"));
                request.setAttribute("mobilenumber", request.getParameter("mobilenumber"));
                request.setAttribute("pancardnumber", request.getParameter("pancardnumber"));
                request.setAttribute("accounttype", request.getParameter("accounttype"));
                request.setAttribute("address", request.getParameter("address"));
                request.setAttribute("emailaddress", request.getParameter("emailaddress"));
                out.print("Base 1");
                if (!addUserBean.isUserExist(request.getParameter("pancardnumber"))) {
                    out.print("Base 3");
                    //firstName,  lastName,  creditCardNumber,  panCardNumber,  mobileNumber,  address, pinNumber
                    addUserBean.setAtmNumber(request.getParameter("atmnumber"));
                    addUserBean.setPinNumber(request.getParameter("pin"));
                    addUserBean.setFirstName(request.getParameter("firstname"));
                    addUserBean.setLastName(request.getParameter("lastname"));
                    addUserBean.setMobileNumber(request.getParameter("mobilenumber"));
                    addUserBean.setPanCardNumber(request.getParameter("pancardnumber"));
                    addUserBean.setAccountType(Integer.parseInt(request.getParameter("accounttype")));
                    addUserBean.setAddress(request.getParameter("address"));
                    addUserBean.setEmailAddress(request.getParameter("emailaddress"));
                    if (addUserBean.insertRecord()) {
                        request.setAttribute("successMessage", rsBundle.getText("ACCOUNT_CREATED_SUCCESSFULLY"));
                        addUserLogger.info(rsBundle.getErrorMessage("ACCOUNT_CREATED_SUCCESSFULLY") + " " + request.getParameter("atmcardnumber") + " " + addUserBean.getUserId());
                        request.getRequestDispatcher("adduser.jsp").forward(request, response);
                        session.setAttribute("loggedin", true);
                    } else {
                        request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_CREATE_ACCOUNT"));
                        addUserLogger.info(rsBundle.getErrorMessage("UNABLE_TO_CREATE_ACCOUNT") + " " + request.getParameter("username"));
                    }
                } else {
                    addUserLogger.info(rsBundle.getErrorMessage("PAN_NUMBER_ALLREADY_EXIST"));
                    request.setAttribute("errorMessage", rsBundle.getText("PAN_NUMBER_ALLREADY_EXIST"));
                }
            } else {
                request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
                addUserLogger.info(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
            }
            request.getRequestDispatcher("adduser.jsp").forward(request, response);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
