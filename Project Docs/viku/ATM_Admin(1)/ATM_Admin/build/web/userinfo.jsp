<%-- 
    Document   : userinfo
    Created on : Mar 13, 2012, 1:56:06 AM
    
--%>

<jsp:useBean class="com.web.locale.ResourceBundleBean" id="links" scope="application" />
<jsp:useBean class="com.web.admin.GetLastNTransactionBean" id="transactions" scope="page" />
<%@ page import="java.sql.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css"/>
        <title>
            <jsp:setProperty name="links" property="linkName" value="APPLICATION_WINDOW_TITLE" />
            <jsp:getProperty name="links" property="linkLabel" />
            -
            ${sessionScope.admUserName}
            :
            <jsp:setProperty name="links" property="linkName" value="USER_INFO" />
            <jsp:getProperty name="links" property="linkLabel" />
        </title>
    </head>
    <body>
        <%
        if (session.getAttribute("loggedin") == null) {
            response.sendRedirect("index.jsp");
        }
        %>
        <!--style="background-color:#B9CAFF"-->
        <div id="container">
            <%@include file="header.jsp" %>
            <div id="wrapper">
                <div id="content">
                    <div id="context">
                        <strong>
                            <jsp:setProperty name="links" property="linkName" value="USER_INFO" />
                            <jsp:getProperty name="links" property="linkLabel" />
                        </strong>
                        <table id="newspaper-a">
                            <tbody>
                                <c:if test="${requestScope.errorMessage != null && requestScope.errorMessage != ''}">
                                    <tr>
                                        <td colspan="2" align="center">
                                            <span id="errorMessage" style="color:red;font-size:14px;font-weight:bold">${requestScope.errorMessage}</span>
                                        </td>
                                    </tr>
                                </c:if>
                                <tr>
                                    <td>
                                        <jsp:setProperty name="links" property="linkName" value="USERID" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </td>
                                    <td>
                                        ${requestScope.userid}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <jsp:setProperty name="links" property="linkName" value="ACCOUNT_ID" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </td>
                                    <td>
                                        ${requestScope.accountid}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <jsp:setProperty name="links" property="linkName" value="NAME" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </td>
                                    <td>
                                        ${requestScope.firstname}&nbsp;${requestScope.lastname}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <jsp:setProperty name="links" property="linkName" value="MOBILE_NUMBER" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </td>
                                    <td>
                                        ${requestScope.mobilenumber}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <jsp:setProperty name="links" property="linkName" value="PAN_CARD_NUMBER" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </td>
                                    <td>
                                        ${requestScope.pancardnumber}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <jsp:setProperty name="links" property="linkName" value="ADDRESS" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </td>
                                    <td>
                                        ${requestScope.address}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <jsp:setProperty name="links" property="linkName" value="EMAIL_ADDRESS" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </td>
                                    <td>
                                        ${requestScope.emailaddress}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <jsp:setProperty name="links" property="linkName" value="ACCOUNT_TYPE" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </td>
                                    <td>
                                        ${requestScope.accounttype}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <jsp:setProperty name="links" property="linkName" value="ATM_NUMBER" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </td>
                                    <td>
                                        ${requestScope.atmnumber}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <jsp:setProperty name="links" property="linkName" value="BALANCE" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </td>
                                    <td>
                                        ${requestScope.balance}
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <br><br><br>
                        <strong>
                            <jsp:setProperty name="links" property="linkName" value="USER_TRANSACTION" />
                            <jsp:getProperty name="links" property="linkLabel" />
                        </strong>
                        <table id="newspaper-a">
                            <thead>
                                <tr>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="SR_NO" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="TRANSACTION_ID" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="DATE" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="ACCOUNT_TYPE" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="AMMOUNT" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
        transactions.setConection();
        ResultSet rs = transactions.getLastNTransactions(Integer.parseInt(request.getParameter("userId").toString()), 5);
        rs.beforeFirst();
        int count = 1;
        while (rs.next()) {
                                %>
                                <!--hmm_transaction.Id, hmm_transaction.`Date`, hmm_transaction.`Type`,hmm_transaction.Amount-->
                                <tr>
                                    <td><%=count%></td>
                                    <td><%=rs.getInt("hmm_transaction.Id")%></td>
                                    <td><%=rs.getInt("hmm_transaction.Date")%></td>
                                    <td>
                                        <%
                                    if (rs.getInt("hmm_transaction.Type") == 1) {
                                        %>
                                        <jsp:setProperty name="links" property="linkName" value="CREDIT" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                        <%                                        } else {
                                        %>
                                        <jsp:setProperty name="links" property="linkName" value="DEBIT" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                        <%                                    }
                                        %>
                                    </td>
                                    <td><%=rs.getString("hmm_transaction.Amount")%></td>
                                </tr>
                                <%
            count++;
        }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="navigation">
                <strong>
                    <ul>

                        <li>
                            <a href="main.jsp">
                                <jsp:setProperty name="links" property="linkName" value="LIST_USERS" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="adduser.jsp">
                                <jsp:setProperty name="links" property="linkName" value="ADD_USER" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="update_user.jsp">
                                Update User
                            </a>
                        </li>
                        <!--<li>
                            <a href="myaccount.jsp">
                                <jsp:setProperty name="links" property="linkName" value="MY_ACCOUNT" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>-->
                    </ul>
                </strong>
            </div>
            <div id="extra">
                <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
            </div>
            <%@include file="footer.jsp" %>
        </div>
    </body>
</html>

