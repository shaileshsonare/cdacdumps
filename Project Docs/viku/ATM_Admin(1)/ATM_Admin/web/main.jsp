<%-- 
    Document   : main
    Created on : Mar 6, 2012, 10:53:45 PM
    
--%>

<jsp:useBean class="com.web.locale.ResourceBundleBean" id="links" scope="application" />
<jsp:useBean class="com.web.admin.ListUserBean" id="users" scope="page" />
<%@ page import="java.sql.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css"/>
        <title>
            <jsp:setProperty name="links" property="linkName" value="APPLICATION_WINDOW_TITLE" />
            <jsp:getProperty name="links" property="linkLabel" />
            -
            ${sessionScope.admUserName}
            :
            <jsp:setProperty name="links" property="linkName" value="DASH_BOARD" />
            <jsp:getProperty name="links" property="linkLabel" />
        </title>
    </head>
    <body>
        <%
        if (session.getAttribute("loggedin") == null) {
            response.sendRedirect("index.jsp");
        }
        %>
        <!--style="background-color:#B9CAFF"-->
        <div id="container">
            <%@include file="header.jsp" %>
            <div id="wrapper">
                <div id="content">
                    <div id="context">
                        <strong>List Users</strong>
                        <table id="newspaper-a">
                            <thead>
                                <tr>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="USERID" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="ACCOUNT_ID" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="NAME" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="ACCOUNT_TYPE" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="ATM_NUMBER" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="BALANCE" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                    <th scope="col">
                                        <jsp:setProperty name="links" property="linkName" value="ACTION" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
        users.setConection();
        ResultSet rs = users.getUsersList();
        rs.beforeFirst();
        while (rs.next()) {
                                %>
                                <tr>
                                    <td><%=rs.getInt("hmm_user.Id")%></td>
                                    <td><%=rs.getInt("hmm_account.Id")%></td>
                                    <td>
                                        <a href="UserInfo.web?userId=<%=rs.getInt("hmm_user.Id")%>">
                                            <%out.print(rs.getString("hmm_user.FirstName") + " " + rs.getString("hmm_user.LastName"));%>
                                        </a>
                                    </td>
                                    <td>
                                        <%
                                    if (rs.getInt("hmm_account.AccountType") == 1) {
                                        %>
                                        <jsp:setProperty name="links" property="linkName" value="SAVING_ACCOUNT" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                        <%                                        } else {
                                        %>
                                        <jsp:setProperty name="links" property="linkName" value="CURRENT_ACCOUNT" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                        <%                                    }
                                        %>
                                    </td>
                                    <td><%=rs.getString("hmm_account.ATMNumber")%></td>
                                    <td><%=rs.getFloat("hmm_account.Balance")%></td>
                                    <td>
                                        <a href="DeleteUser.web?userId=<%=rs.getInt("hmm_user.Id")%>&accountId=<%=rs.getInt("hmm_account.Id")%>">
                                            Delete User
                                        </a>
                                    </td>
                                </tr>
                                <%
        }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="navigation">
                <strong>
                    <ul>
                        
                        <li>
                            <a href="main.jsp">
                                <jsp:setProperty name="links" property="linkName" value="LIST_USERS" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="adduser.jsp">
                                <jsp:setProperty name="links" property="linkName" value="ADD_USER" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="update_user.jsp">
                                <jsp:setProperty name="links" property="linkName" value="UPDATE_USER" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <!--<li>
                            <a href="myaccount.jsp">
                                <jsp:setProperty name="links" property="linkName" value="MY_ACCOUNT" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>-->
                    </ul>
                </strong>
            </div>
            <div id="extra">
                <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
            </div>
            <%@include file="footer.jsp" %>
        </div>
    </body>
</html>
