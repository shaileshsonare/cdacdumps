<%--
    Document   : adduser
    Created on : Mar 9, 2012, 7:46:50 PM
    
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.web.connection.DBConnectionBean"%>
<jsp:useBean class="com.web.locale.ResourceBundleBean" id="links" scope="application" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css"/>
        <script type="text/javascript" src="js/java_script.js"></script>
        <title>
            <jsp:setProperty name="links" property="linkName" value="APPLICATION_WINDOW_TITLE" />
            <jsp:getProperty name="links" property="linkLabel" />
            -
            ${sessionScope.admUserName}
            :
            <jsp:setProperty name="links" property="linkName" value="ADD_USER" />
            <jsp:getProperty name="links" property="linkLabel" />
        </title>
    </head>
    <body>
        <%
        if (session.getAttribute("loggedin") == null) {
            response.sendRedirect("index.jsp");
        }
        %>
        <!--style="background-color:#B9CAFF"-->
        <div id="container">
            <%@include file="header.jsp" %>
            <div id="wrapper">
                <div id="content">
                    <div id="context">
                        <%
                        if(request.getMethod().equals("GET"))
                        {
                        %>
                        <strong>
                            <jsp:setProperty name="links" property="linkName" value="UPDATE_USER" />
                            <jsp:getProperty name="links" property="linkLabel" />
                        </strong>
                        <form class="adduserform" id="updateuser1" name="updateuser1" action="update_user.jsp" method="post" onsubmit="return validateATMNumber();">
                            <table id="newspaper-a">
                                <tbody>
                                    <c:if test="${requestScope.successMessage != null && requestScope.successMessage != ''}">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <span id="successMessage" style="color:blue;font-size:14px;font-weight:bold">${requestScope.successMessage}</span>
                                            </td>
                                        </tr>
                                    </c:if>
                                    <c:if test="${requestScope.errorMessage != null && requestScope.errorMessage != ''}">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <span id="errorMessage" style="color:red;font-size:14px;font-weight:bold">${requestScope.errorMessage}</span>
                                            </td>
                                        </tr>
                                    </c:if>
                                    <tr>
                                        <td>
                                            <jsp:setProperty name="links" property="linkName" value="ENTER_ATM_NUMBER" />
                                            <jsp:getProperty name="links" property="linkLabel" />
                                        </td>
                                        <td>
                                            <input type="text" name="atm_number"   maxlength="16" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="reset"
                                                   value=<jsp:setProperty name="links" property="linkName" value="RESET" />
                                                   <jsp:getProperty name="links" property="linkLabel" />
                                                   name="Reset" />
                                            &nbsp;&nbsp;
                                            <input type="submit"
                                                   value=<jsp:setProperty name="links" property="linkName" value="SUBMIT" />
                                                   <jsp:getProperty name="links" property="linkLabel" />
                                                   name="Submit" />
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>

                        <%
                        }
                        if(request.getMethod().equals("POST"))
                        {
                            String atmNumber= request.getParameter("atm_number");
                            if(atmNumber==null)
                            {
                                atmNumber=(String)request.getAttribute("atm_number");
                            }
                            System.out.print(atmNumber);
                            DBConnectionBean db=new DBConnectionBean();
                            Connection con=db.getCon();
                            Statement stmt=con.createStatement();
                            ResultSet rs=stmt.executeQuery("SELECT * FROM hmm.hmm_user where Id=(select UserId from hmm_account where ATMNumber='"+atmNumber+"')");
                            if(rs.next())
                            {
                            
                        %>
                            <strong>
                                <jsp:setProperty name="links" property="linkName" value="UPDATE_USER" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </strong>
                            <form class="adduserform" id="updateuser2" name="updateuser2" action="UpdateUser.web" method="post" onsubmit="return validateUpdateUser();">
                                <table id="newspaper-a">
                                    <tbody>
                                        <c:if test="${requestScope.successMessage != null && requestScope.successMessage != ''}">
                                            <tr>
                                                <td colspan="2" align="left">
                                                    <span id="successMessage" style="color:blue;font-size:14px;font-weight:bold">${requestScope.successMessage}</span>
                                                </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${requestScope.errorMessage != null && requestScope.errorMessage != ''}">
                                            <tr>
                                                <td colspan="2" align="left">
                                                    <span id="errorMessage" style="color:red;font-size:14px;font-weight:bold">${requestScope.errorMessage}</span>
                                                </td>
                                            </tr>
                                        </c:if>
                                                    <input type="hidden" value="<%= rs.getString(1)%>" name="user_id">
                                                    <input type="hidden" value="<%= atmNumber %>" name="atm_number">
                                        <tr>
                                            <td>
                                                <jsp:setProperty name="links" property="linkName" value="FIRST_NAME" />
                                                <jsp:getProperty name="links" property="linkLabel" />
                                            </td>
                                            <td>
                                                <input type="text" name="firstname"  value ="<% if (request.getAttribute("firstname") != null) {%><%= request.getAttribute("firstname")%><%}else{%><%= rs.getString(3) %><%}%>" size="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <jsp:setProperty name="links" property="linkName" value="LAST_NAME" />
                                                <jsp:getProperty name="links" property="linkLabel" />
                                            </td>
                                            <td>
                                                <input type="text" name="lastname"  value ="<% if (request.getAttribute("lastname") != null) {%><%= request.getAttribute("lastname")%><%}else{%><%= rs.getString(4) %><%}%>" size="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <jsp:setProperty name="links" property="linkName" value="PAN_CARD_NUMBER" />
                                                <jsp:getProperty name="links" property="linkLabel" />
                                            </td>
                                            <td>
                                                <input type="text" name="pancardnumber"  value ="<% if (request.getAttribute("pancardnumber") != null) {%><%= request.getAttribute("pancardnumber")%><%}else{%><%= rs.getString(2) %><%}%>" size="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <jsp:setProperty name="links" property="linkName" value="MOBILE_NUMBER" />
                                                <jsp:getProperty name="links" property="linkLabel" />
                                            </td>
                                            <td>
                                                <input type="text" name="mobilenumber"  value ="<% if (request.getAttribute("mobilenumber") != null) {%><%= request.getAttribute("mobilenumber")%><%}else{%><%= rs.getString(5) %><%}%>" size="25" />
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                                <jsp:setProperty name="links" property="linkName" value="EMAIL_ADDRESS" />
                                                <jsp:getProperty name="links" property="linkLabel" />
                                            </td>
                                            <td>
                                                <input type="text" name="emailaddress"  value ="<% if (request.getAttribute("emailaddress") != null) {%><%= request.getAttribute("emailaddress")%><%}else{%><%= rs.getString(7) %><%}%>" size="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <jsp:setProperty name="links" property="linkName" value="ADDRESS" />
                                                <jsp:getProperty name="links" property="linkLabel" />
                                            </td>
                                            <td>
                                                <input type="text" name="address"  value ="<% if (request.getAttribute("address") != null) {%><%= request.getAttribute("address")%><%}else{%><%= rs.getString(6) %><%}%>" size="70" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="reset"
                                                       value=<jsp:setProperty name="links" property="linkName" value="RESET" />
                                                       <jsp:getProperty name="links" property="linkLabel" />
                                                       name="Reset" />
                                                &nbsp;&nbsp;
                                                <input type="submit"
                                                       value=<jsp:setProperty name="links" property="linkName" value="SUBMIT" />
                                                       <jsp:getProperty name="links" property="linkLabel" />
                                                       name="Submit" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                            <%
                            }
                        }
                        %>
                    </div>
                </div>
            </div>
            <div id="navigation">
                <strong>
                    <ul>
                        <li>
                            <a href="main.jsp">
                                <jsp:setProperty name="links" property="linkName" value="LIST_USERS" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="adduser.jsp">
                                <jsp:setProperty name="links" property="linkName" value="ADD_USER" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="update_user.jsp">
                                <jsp:setProperty name="links" property="linkName" value="UPDATE_USER" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <!--<li>
                            <a href="myaccount.jsp">
                                <jsp:setProperty name="links" property="linkName" value="MY_ACCOUNT" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>-->
                    </ul>
                </strong>
            </div>
            <div id="extra">
                <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
            </div>
            <%@include file="footer.jsp" %>
        </div>
    </body>
</html>
