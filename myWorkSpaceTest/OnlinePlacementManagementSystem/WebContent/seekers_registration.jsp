<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Job Seekers Registration</title>
</head>
<body>
<a href="seekers_registration.jsp">Register Seekers</a>
<a href="provider_registration.jsp">Register Provider</a>
<a href="providers_login.jsp">Register Provider</a>
<a href="post_ads.jsp">Post Ad</a>
<a href="show_ads.jsp">Show Ads</a>


<h1>Job Seekers Registration</h1>
	<form action="seekers_reg">
		<s:textfield name="username" label="Username"/>
		<s:password name="password" label="Password"/>
		<s:textfield name="qualification" label="Qualification"/>
		<s:submit value="Register Me"/>
	</form>
</body>
</html>