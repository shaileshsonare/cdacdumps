package com.bslogics;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.connections.DBConnection;
import com.pojos.ProvidersPojo;

public class LoginBsLogic 
{

	boolean status=false;
	Connection conn=null;
	String sql=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int rows;
	ProvidersPojo prpo=null;
	
	
	public void getConnect()
	{
		conn=new DBConnection().getConnection();
	}
	
	
	public boolean loginSeekers(String username,String password)
	{
		getConnect();
		sql="select * from seekers_reg_tb where username=? and password=?";
		try 
		{
				
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, username);
			pstmt.setString(2, password);
			
			
			rs=pstmt.executeQuery();
			
			while(rs.next())
			{
				System.out.println(rs.getInt("seekers_id"));
				System.out.println(rs.getString("username"));
				System.out.println(rs.getString("password"));
				System.out.println(rs.getString("qualification"));
			}
			
			status=true;
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			try 
			{
				conn.close();
			} 
			catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		return status;
		}
		
		
		
	public boolean loginProviders(String username,String password)
		{
			prpo=new ProvidersPojo();
			getConnect();
			sql="select * from providers_reg_tb where username=? and password=?";
			try 
			{
					
				pstmt=conn.prepareStatement(sql);
				pstmt.setString(1, username);
				pstmt.setString(2, password);
				
				
				rs=pstmt.executeQuery();
				
				while(rs.next())
				{
					prpo.setProviders_id(rs.getInt("providers_id"));
					prpo.setUsername(rs.getString("username"));
					prpo.setPassword(rs.getString("password"));
				}
				
				status=true;
			} 
			catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally
			{
				try 
				{
					conn.close();
				} 
				catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
		return status;
	}


	public ProvidersPojo getProvider()
	{
		return prpo;
	}
		
	
}
