package com.actions;

import com.bslogics.RegisterBsLogic;
import com.connections.DBConnection;

public class SeekersRegistrationAction 
{
	private String username;
	private String password;
	private String qualification;
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the qualification
	 */
	public String getQualification() {
		return qualification;
	}
	/**
	 * @param qualification the qualification to set
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	
	public String execute()
	{
		System.out.println(username+"\n"+password+"\n"+qualification);
		
		if(new RegisterBsLogic().saveSeekers(username, password, qualification))
		return "success";
		else
			return "failure";
	}
	
}
