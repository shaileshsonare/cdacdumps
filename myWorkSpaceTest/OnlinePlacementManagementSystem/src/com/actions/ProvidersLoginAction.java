package com.actions;

import com.bslogics.LoginBsLogic;
import com.bslogics.RegisterBsLogic;

public class ProvidersLoginAction 
{
	private int providers_id;
	private String username;
	private String password;
	
	
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the providers_id
	 */
	public int getProviders_id() {
		return providers_id;
	}

	/**
	 * @param providers_id the providers_id to set
	 */
	public void setProviders_id(int providers_id) {
		this.providers_id = providers_id;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}



	public String execute()
	{
		LoginBsLogic lbl=new LoginBsLogic();
		if(lbl.loginProviders(username, password))
		{
			setProviders_id(lbl.getProvider().getProviders_id());
			return "success";
		}
			else
				return "failure";
	}
	
}
