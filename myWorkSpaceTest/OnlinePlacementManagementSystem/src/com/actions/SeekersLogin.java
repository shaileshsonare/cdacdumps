package com.actions;

import com.bslogics.LoginBsLogic;
import com.bslogics.RegisterBsLogic;

public class SeekersLogin 
{
	private int seekers_id;
	private String username;
	private String password;
	private String qualification;
	
	
	
	
	/**
	 * @return the seekers_id
	 */
	public int getSeekers_id() {
		return seekers_id;
	}


	/**
	 * @param seekers_id the seekers_id to set
	 */
	public void setSeekers_id(int seekers_id) {
		this.seekers_id = seekers_id;
	}


	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}


	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the qualification
	 */
	public String getQualification() {
		return qualification;
	}


	/**
	 * @param qualification the qualification to set
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}


	public String execute()
	{
		
		if(new LoginBsLogic().loginSeekers(username, password))
		{
			
			return "success";
		}
		else
			return "failure";
	}
}
