
public class LoginAction 
{
	
	private String username;
	private String password;
	
	public LoginAction() {
		super();
	}

	public LoginAction(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String execute()
	{
		BsLogic bsl=new BsLogic();
		System.out.println(username);
		System.out.println(password);
		
		if(bsl.validate(username, password))
			return "sss";
		else
			return "vvv";
	}
}
