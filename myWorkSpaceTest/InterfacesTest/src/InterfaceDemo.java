
interface I1
{
	
}

interface I2
{
}

interface I3 extends I1,I2
{
	
}

class MyStaticClass
{
	public MyStaticClass()
	{
		System.out.println("hello");
		this.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MyStaticClass [getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	
}

public class InterfaceDemo {

	public static void main(String[] args) {
	
		MyStaticClass msc=new MyStaticClass();
		MyStaticClass msc2=msc;
		msc.toString();
		System.out.println(msc.hashCode());
		System.out.println(msc2.hashCode());
		System.out.println(msc.getClass().getSimpleName());
		
		String s1=new String("shailesh");
		String s2="shailesh";
		System.out.println(s1.equals(s2));
		System.out.println(s1.hashCode());
		System.out.println(s2.hashCode());
		
	}

}
