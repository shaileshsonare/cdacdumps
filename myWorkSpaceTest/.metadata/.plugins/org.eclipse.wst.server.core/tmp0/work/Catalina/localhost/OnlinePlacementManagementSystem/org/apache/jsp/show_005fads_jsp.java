package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.io.PrintWriter;
import java.sql.ResultSet;
import com.connections.DBConnection;

public final class show_005fads_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>Ads Page</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("<a href=\"seekers_registration.jsp\">Register Seekers</a>\r\n");
      out.write("<a href=\"provider_registration.jsp\">Register Provider</a>\r\n");
      out.write("<a href=\"providers_login.jsp\">Register Provider</a>\r\n");
      out.write("<a href=\"post_ads.jsp\">Post Ad</a>\r\n");
      out.write("<a href=\"show_ads.jsp\">Show Ads</a>\r\n");
      out.write("<h1>Ads Page</h1>\r\n");
      out.write("\r\n");


Connection conn;
String sql=null;
PreparedStatement pstmt=null;
ResultSet rs;
PrintWriter pw;
int providers_id;

try 
{
	pw=response.getWriter();
	conn=new DBConnection().getConnection();
	
	System.out.println("Connectedddd....");
	
		//sql="select * from ads_tb order by TO_DATE(posting_date,'YYYY-MM-DD') desc";
		sql="select job_title,job_category,eligibility,no_of_seats,to_char(posting_date,'dd-Mon-yy') posting_date,providers_id from ads_tb order by posting_date desc";
		
		pstmt=conn.prepareStatement(sql);
		
		rs=pstmt.executeQuery();
		
		System.out.println(rs);
		
		pw.println("<table border='1'><th>Job Title<th>Job Catetory<th>Eligibility<th>No. of Seats<th>Posting Date");
		
		while(rs.next())
		{
			pw.println("<tr>");
			 pw.println("<td><h3>"+rs.getString("job_title")+"</h3>");
			 pw.println("<td><h3>"+rs.getString("job_category")+"</h3>");
			pw.println("<td><h3>"+rs.getString("eligibility")+"</h3>");
			pw.println("<td><h3>"+rs.getString("no_of_seats")+"</h3>");
			 pw.println("<td><h3>"+rs.getString("posting_date")+"</h3>");
			 pw.println("<td><h3>"+rs.getInt("providers_id")+"</h3>");
			 pw.println("<td><h3>"+"<input type='button' value='Show Details'/>");
			 providers_id=rs.getInt("providers_id"); 
		}
		
		pw.println("<tr><td colspan='4'>");
		pw.println("</table>");
}catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}



      out.write("\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
