  #include<iostream>
using namespace std;

struct Node
{
	int data;
	Node *left,*right;
};

class Tree
{
	Node *root;
	public:
		Tree()
		{
			root=NULL;
		}

		void add_node(int x)
		{
			Node *newnode=new Node;
			if(!newnode)
			{
				cout<<"No space in ram"<<endl;
				return;
			}
			newnode->data=x;
			newnode->left=NULL;
			newnode->right=NULL;

			if(root==NULL)
			{
				root=newnode;
				cout<<x<<"added to tree";
				return;
			}

			Node *p,*q;
			p=q=root;

			if(x==p->data)
			{
				cout<<"Duplicate"<<endl;
				return;
			}

			while(q!=NULL)
			{
				p=q;

				if(x>p->data)
				{
					q=q->right;
				}
				if(x<p->data)
				{
					q=q->left;
				}
			}

			if(q==NULL)
			{
				if(x>p->data)
				p->right=newnode;
				else
				p->left=newnode;
			}

			cout<<x<<"inserted";
		}

		void inorder(Node *root)
		{
			if(root!=NULL)
			{
				inorder(root->left);
				cout<<root->data;
				inorder(root->right);
			}
		}
/*
		void inorder(Node *root)
		{
			if(root!=NULL)
			{
				inorder(root->left);
				cout<<root->data<<" ";
				inorder(root->right);
			}
		}
*/
		void display()
		{
			inorder(root);
		}

};

int main(void)
{
	Tree t1;
	t1.add_node(10);
	t1.add_node(20);
	t1.add_node(5);
	t1.display();

}

