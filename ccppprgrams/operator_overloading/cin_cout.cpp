#include<iostream>
using namespace std;

class num
{
	int x;
	int y;

	public:
		friend istream& operator>>(istream&,num&);
		friend ostream& operator<<(ostream&,num&);
};

istream& operator>>(istream &scan,num &no)
{
	scan>>no.x>>no.y;
	return scan;
}

ostream& operator<<(ostream &print,num &no)
{
	print<<no.x<<" "<<no.y<<endl;
	return print;
}

int main(void)
{
	num n1,n2;
	cin>>n1>>n2;
	cout<<n1<<n2;
}
