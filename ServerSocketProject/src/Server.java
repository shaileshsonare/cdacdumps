import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {

	public static void  getMessage()
	{
			
		ServerSocket socket;
		
		try 
		{
			socket = new ServerSocket(8765);
			
			Socket csocket=socket.accept();
			
			BufferedReader in=new BufferedReader(new InputStreamReader(csocket.getInputStream()));
										
			System.out.println(in.readLine());
			
			csocket.close();
		
		} 
		catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}

	public static void main(String[] args) {
		getMessage();
	}
}
