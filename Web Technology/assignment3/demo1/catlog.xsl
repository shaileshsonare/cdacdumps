<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
	<html>
		<body>
			<table border="1">
			<xsl:for-each select="/college/students">
				<tr><td><xsl:value-of select="name"/></td></tr>
			</xsl:for-each>
			</table>
		</body>
	</html>

</xsl:template>
</xsl:stylesheet>