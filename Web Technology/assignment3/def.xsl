<?xml version="1.0" encoding="UTF-16"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<html>
		<body>
			<table border="1">
				<xsl:for-each select="EMPLOYEES/employee">
				<tr>
				<td><xsl:value-of select="name"/></td>
				<td><xsl:value-of select="basic"/></td>
				</tr>
				</xsl:for-each>
			</table>
		</body>
	</html>
</xsl:template>
</xsl:stylesheet>

