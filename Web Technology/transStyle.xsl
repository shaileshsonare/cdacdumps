<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <table border="1">
	<tr><td>Empno</td><td>name</td><td>basic</td></tr>
      <xsl:for-each select="Employees/Employee[basic>5000]">
      <tr>
        <td><xsl:value-of select="empNo"/></td>
        <td><xsl:value-of select="name"/></td>
        <td><xsl:value-of select="basic"/></td>
		</tr>
	</xsl:for-each>
	 
  </table>
	  
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>
