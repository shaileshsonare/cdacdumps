<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
	<html>
		<table border="1">
			<xsl:for-each select="/acts/dac4[basic>2000]">
			<tr>
				<td><xsl:value-of select="name"/></td>
				<td><xsl:value-of select="dob"/></td>
				<td><xsl:value-of select="basic"/></td>
			</tr>
			</xsl:for-each>
		</table>
	</html>
</xsl:template>
</xsl:stylesheet>
