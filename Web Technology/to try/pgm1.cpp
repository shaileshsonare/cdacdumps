#include<iostream>

using namespace std;

struct node
{  
	public:
	int data;
	node* left,*right;;
};	

	 
class Tree
{
	 node * root,*p,*q;
	public:

	   Tree()
	   {
		   root=NULL; 
	   }   

	   void insert(int data)
	   {
		   node* n= new node;
		   n->data=data;
                   n->left=n->right=NULL;
		   if(root==NULL)
		   {
		       root=n;
	    	       return;
	 	   }	       
                 p=q=root;

	while(p->data!=data && q!=NULL)
	{
  		p=q;

		if(p->data<data)
		q=q->right;

		else
		q=q->left;
	}

	if(p->data==data)
		{
			cout<<"\n data duplication not allowed...";
			return;
		}
	if(p->data<data)
		p->right=n;
	
  	else
      		p->left=n;
	}	      


	   
	   void display()
	   {
		   cout<<"\n in order tree::"; 
		   inorder(root);
	   }	    
	   
	   
	   void inorder(node * proot)

	   {
	   if(proot!=NULL)
	   {
	        inorder(proot->left);
		
		cout<<"\t"<<proot->data;

		inorder(proot->right);
	    }	
			 
   
	   } 

        void displaypost()
	{
		cout<<"\n post order tree::";
		postorder(root);
	}   
        
	void postorder (node *proot)
	{
		if(proot!=NULL)
		{	postorder(proot->left);
	        	postorder(proot->right);
			cout<<"\t"<<proot->data;
		}
	}



        void displaypre()
	{
		cout<<"\n post order tree::";
		preorder(root);
	}   
        
	void preorder (node *proot)
	{
		if(proot!=NULL)
		{	preorder(proot->right);
	        	preorder(proot->left);
			cout<<"\t"<<proot->data;
		}
          }
};




int main()
{ 
     Tree t;
       t.insert(89);
       t.insert(61);
       t.insert(25); 
       t.insert(31);
       t.insert(41);
       t.insert(77);
       t.insert(66);
       t.display();
       t.displaypost();
       t.displaypre();
     cout<<endl<<"\n";
     return 0;

}
