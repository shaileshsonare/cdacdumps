<?php
// file type
header("Content-type: image/png");
// create image
$img = imagecreatetruecolor(300, 350);
// set colors
$blue = imagecolorallocate($img, 0, 0, 255);
$green = imagecolorallocate($img, 0, 255, 0);
// draw two rectangles, one is filled
imagerectangle($img, 20,20,120,120, $blue);
imagefilledrectangle($img, 150,150,250,320, $green);
// display image
imagepng($img);
// release image from memory
imagedestroy($img)
?>
