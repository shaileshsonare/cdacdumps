<?php
// file type
header("Content-type: image/png");
// create image
$img = imagecreatetruecolor(400, 800);
// set background to white
imagefill($img,0,0,imagecolorallocate($img, 255, 255, 255));
//cycle through colors while drawing lines
for ($i=0;$i<=255;$i++) {
  imageline($img, 10, $i+10, 300, $i+10, imagecolorallocate($img,$i, 0, 0));
} 
for ($i=0;$i<=255;$i++) {
  imageline($img, 10, $i+266, 300, $i+266, imagecolorallocate($img, 255, 0, $i));
} 
// display image
imagepng($img);
// release image from memory
imagedestroy($img);
?>