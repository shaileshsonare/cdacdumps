<?php
// file type
header ("Content-type: image/png");
// create image
$img = imagecreatetruecolor(350, 300);
// define colors
$red = imagecolorallocate($img, 255, 0, 0);
$blue = imagecolorallocate($img, 0, 0, 255);
$green = imagecolorallocate($img, 0, 255, 0);
$white = imagecolorallocate($img, 255, 255, 255);
$black = imagecolorallocate($img, 0, 0, 0);
// set background color
imagefill($img, 0, 0, $red );
// draw filled rectangles
imagefilledrectangle($img, 10,10,160,110, $blue);
imagefilledrectangle($img, 40,40,190,140, $green);
imagefilledrectangle($img, 70,70,220,170, $white);
imagefilledrectangle($img, 100,100,250,200, $black);
// display the image
imagepng($img);
// release image from memory
imagedestroy($img);
?>
