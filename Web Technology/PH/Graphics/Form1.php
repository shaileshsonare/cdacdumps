<html>
<head><title>GD and Photos</title></head>
<body>
<table Width="85%">
<tr>
<td width="5%"></td>
<td><h3>Upload a photo:</h3>
Use the Browse button to find your picture file on your computer. 
<br />Then click the Upload button.
<br />The file must be either a JPEG (.jpg) or a GIF (.gif). 
<br />The file must be less than 2,000,000 bytes.<br /><br /></td>
</tr>
<?php
if ($_REQUEST['Error']=="true") {
echo '<tr>';
echo '<td width="5%"></td>';
echo '<td><b>The file you just uploaded was either ';
echo 'too large or the wrong type of file. The file must be a ';
echo '.jpg or a .gif; and be smaller than 2,000,000 bytes.</b>';
echo '</td></tr>';
}
?>
<tr><td> </td>
<td>
<form enctype="multipart/form-data" action="pixupload.php" method="POST">
   <!-- MAX_FILE_SIZE must precede the file input field -->
   <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
   <!-- Name of input element determines name in $_FILES array -->
   Upload this file:<br /><input size="40" id="userfile" name="userfile" 
   type="file" accept="image/gif, image/jpeg" />
   <input type="submit" value="Upload" />
</form>
<hr />
<h3>Change a photo:</h3>
</td></tr>
<tr><td> </td><td><img src="myphoto.jpg" /></td></tr>
<tr><td> </td><td>
<form name="process"  id="process" action="processphoto.php">
Rotation:  
<select id="rotation" name="rotation">
<option value="0">None</option>
<option value="90">90 degrees</option>
<option value="180">180 degrees</option>
<option value="270">270 degrees</option>
</select>
<br />
Resizing:  
<select id="resize" name="resize">
<option value="100">100px</option>
<option value="200">200px</option>
<option value="300">300px</option>
</select>
<br /><br />
<input type="submit" value="Process Photo" /></form>
</td></tr>
</table>
</body>
</html>