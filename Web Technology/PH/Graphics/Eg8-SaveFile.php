<?php
// file type
header("Content-type: image/jpeg");
// create image
$img = imagecreatetruecolor(400, 400);
// set background to white
imagefill($img,0,0,imagecolorallocate($img, 255, 255, 255));
//cycle through colors while drawing lines
for ($i=0;$i<=255;$i++) {
  imageline($img, 10, $i+10, 300, $i+10, imagecolorallocate($img,255-$i, 255-$i, 255-$i));
} 
// cycle colors while placing text
for ($i=0;$i<=240;$i=$i + 20) {
  imagestring($img, 4,100,$i,"Good Afternoon",imagecolorallocate($img, $i,$i,$i));
}
// remove file if it is already there
if (file_exists("myartwork.jpg")) {
 unlink ("myartwork.jpg");
}
// save the image as a jpeg
imagejpeg($img, "myartwork.jpg");
// release image from memory
imagedestroy($img);
?>