Numeric array - An array with a numeric index
Associative array - An array where each ID key is associated with a value

$cars=array("Saab","Volvo","BMW","Toyota");

$cars[0]="Saab";
$cars[1]="Volvo";
$cars[2]="BMW";
$cars[3]="Toyota";

$ages['Peter'] = "32";
$ages['Quagmire'] = "30";
$ages['Joe'] = "34";

--------------------------------------------------------


foreach ($array as $value)
  {
  code to be executed;
  }

$x=array("one","two","three");
foreach ($x as $value)
  {
  echo $value . "<br />";
  }
