<%@page import="com.pojos.PostJobPojo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.SQLException,java.sql.DriverManager,java.sql.PreparedStatement,java.sql.Connection,java.io.PrintWriter,java.sql.ResultSet,com.connections.DBConnection" %>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Show jobs</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
</head>
<body>
<jsp:include page="header.html"/>

	
<div id="content" style="height:400px">


<%

String sid=null;
int seeker_id;
try
{
	sid=session.getAttribute("seeker_session_id").toString();
	
}
	catch(Exception e){}
	if(sid==null)
	{
		response.sendRedirect("login_seeker.jsp");
	}
%>

<%

Connection conn;
String sql=null;
PreparedStatement pstmt=null;
ResultSet rs;
seeker_id=Integer.parseInt(sid.toString());
try 
{
	
		conn=new DBConnection().getConnection();
	
		sql="select job_id,job_title,company_name,position,location,job_description,eligibility,contact_email,contact_number,package,to_char(posting_date,'dd-Mon-yy') posting_date,recruiter_id rec_id from jobs_tb order by posting_date desc";
		
		pstmt=conn.prepareStatement(sql);
		
		rs=pstmt.executeQuery();
		
		System.out.println(rs);
		
		out.println("<table border='1'><th>Job Id<th>Posting Date<th>Job Title<th>Company Name<th>Position<th>Location<th>Description<th>Eligibility<th>Email<th>Contact Number<th>Package<th>Recruiter Id");
		
		while(rs.next())
		{
			out.println("<tr><td>"+rs.getString("job_id")+
						"<td>"+rs.getString("posting_date")+
						"<td>"+rs.getString("job_title")+
						"<td>"+rs.getString("company_name")+
						"<td>"+rs.getString("position")+
						"<td>"+rs.getString("location")+
						"<td>"+rs.getString("job_description")+
						"<td>"+rs.getString("eligibility")+
						"<td>"+rs.getString("contact_email")+
						"<td>"+rs.getString("contact_number")+
						"<td>"+rs.getString("package")+
						"<td>"+rs.getString("rec_id")+
						"<td><form action='apply_job.jsp'>"+
						"<input type='hidden' name='job_id' value='"+rs.getString("job_id")+"'/>"+
						"<input type='hidden' name='recruiter_id' value='"+rs.getString("rec_id")+"'/>"+
						"<input type='hidden' name='seeker_id' value='"+seeker_id+"'/>"+
						"<input type='hidden' name='status' value=''/>"+
						"<input type='submit' value='Apply'>"+
						"</form>"
					);
			
			

		}
		out.println("</table>");
		conn.close();
}catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

%>

 </div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.html"/>
</body>
</html>