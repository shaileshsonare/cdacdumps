<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Seeker</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
</head>
<body>

<jsp:include page="header.html"/>

	
<div id="content" style="height:400px">


		Your id id ${seeker_session_id}
		another way <s:property value="#session['seeker_session_id']" />
		
		Seekers home page
		
		<a href="show_jobs.jsp">Show Jobs</a>  
		<a href="show_applied_jobs.jsp">Show Applied Jobs</a>
		<a href="logout.jsp">Logout</a>
		
		</div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.html"/>
		
</body>
</html>