<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home recruiter</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
</head>
<body>
    <jsp:include page="header.html"/>
    
    <div id="content" style="height:400px">
		Your id id ${session_id}
		another way <s:property value="#session['session_id']" />
		
		home page
		<a href="post_job.jsp">post job</a>
		<a href="posted_jobs.jsp">Show Posted Ads</a>  
		<a href="logout.jsp">Logout</a>
		<a href="view_applications.jsp">view applications</a>
		</div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.html"/>
		
</body>
</html>