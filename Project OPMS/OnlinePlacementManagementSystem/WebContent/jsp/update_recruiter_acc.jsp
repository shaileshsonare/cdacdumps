<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.SQLException,java.sql.DriverManager,java.sql.PreparedStatement,java.sql.Connection,java.io.PrintWriter,java.sql.ResultSet,com.connections.DBConnection" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Seekers Account</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body id="home">
<jsp:include page="header_menu.jsp"/>

	
<div id="templatemo_main" style="height:400px">
<a href="show_recruiters.jsp">Back</a>

<%

int recruiter_id=Integer.parseInt(request.getParameter("recruiter_id"));
int status=Integer.parseInt(request.getParameter("status"));

Connection conn=null;
String sql=null;
PreparedStatement stmt;
int rows;

try
{
	conn=new DBConnection().getConnection();
	
	sql="update recruiters_tb set status=? where recruiter_id=?";
	
	stmt=conn.prepareStatement(sql);
	stmt.setInt(1, status);
	stmt.setInt(2,recruiter_id);
	
	rows=stmt.executeUpdate();
	

	if(rows==1)
	{
		if(status==1)
		out.println("<h3>Recruiter Activated Successfully.</h3>");
		if(status==0)
			out.println("<h3>Recruiter Deactivated Successfully.</h3>");
	}
	else
		out.println("<h3 style='color:red'>Error occured during updation.</h3>");
	

}
finally
{
conn.close();
}
 
%>


</div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.jsp"/>
</body>
</html>