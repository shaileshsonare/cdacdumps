<%@page import="com.pojos.PostJobPojo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.SQLException,java.sql.DriverManager,java.sql.PreparedStatement,java.sql.Connection,java.io.PrintWriter,java.sql.ResultSet,com.connections.DBConnection" %>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Posted Jobs</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body id="home">
<jsp:include page="header_menu.jsp"/>

	
<div id="templatemo_main" style="height:400px">

<a href="home_recruiter.jsp">Back</a>
<a href="post_job.jsp">Post Job</a>
<a href="logout.jsp">Logout</a><br><br>

<%

String rid=null;

int recruiter_id;
	try
{
	rid=session.getAttribute("session_id").toString();
	
}
	catch(Exception e){}
	if(rid==null)
	{
		response.sendRedirect("login_recruiter.jsp");
	}
%>

<%

Connection conn;
String sql=null;
PreparedStatement pstmt=null;
ResultSet rs;
recruiter_id=Integer.parseInt(rid);
try 
{
	
	conn=new DBConnection().getConnection();
	
		sql="select job_id,job_title,company_name,position,location,job_description,eligibility,contact_email,contact_number,package,to_char(posting_date,'dd-Mon-yy') posting_date,recruiter_id rec_id from jobs_tb where recruiter_id="+recruiter_id+"order by posting_date desc";
		
		pstmt=conn.prepareStatement(sql);
		
		rs=pstmt.executeQuery();
		
		System.out.println(rs);
		
		out.println("<table border='1'><th>Job Id<th>Posting Date<th>Job Title<th>Company Name<th>Position<th>Location<th>Description<th>Eligibility<th>Email<th>Contact Number<th>Package<th>Recruiter Id");
		
		while(rs.next())
		{
			out.println("<tr><td>"+rs.getString("job_id")+
						"<td>"+rs.getString("posting_date")+
						"<td>"+rs.getString("job_title")+
						"<td>"+rs.getString("company_name")+
						"<td>"+rs.getString("position")+
						"<td>"+rs.getString("location")+
						"<td>"+rs.getString("job_description")+
						"<td>"+rs.getString("eligibility")+
						"<td>"+rs.getString("contact_email")+
						"<td>"+rs.getString("contact_number")+
						"<td>"+rs.getString("package")+
						"<td>"+rs.getString("rec_id")+
						"<td><form action='view_applications.jsp'>"+
						"<input type='hidden' name='job_id' value='"+rs.getString("job_id")+"'/>"+		
						"<input type='hidden' name='status' value=''/>"+
						"<input type='submit' value='view'>"+
						"</form>"
					);
		}
		out.println("</table>");
}catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

%>
</div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.jsp"/>

</body>
</html>