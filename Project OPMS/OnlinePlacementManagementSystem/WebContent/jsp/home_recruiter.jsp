<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home recruiter</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body id="home">
    <jsp:include page="header_menu.jsp"/>
    
    <div id="templatemo_main" style="height:400px">
		<%-- Your id id ${session_id}
		another way <s:property value="#session['session_id']" />
		 --%>
		Recruiter Home<br/>
		<a href="post_job.jsp">Post job</a>
		<a href="posted_jobs.jsp">Show Posted Ads</a>  
		<a href="logout.jsp">Logout</a>
		</div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.jsp"/>
		
</body>
</html>