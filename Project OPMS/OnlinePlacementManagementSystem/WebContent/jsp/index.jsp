<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>India Jobs</title>
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<script type="text/javascript" src="js/jquery-1-4-2.min.js"></script> 
<!--script type="text/javascript" src="/jqueryui/js/jquery-ui-1.7.2.custom.min.js"></script--> 
<script type="text/javascript" src="js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="js/showhide.js"></script> 
<script type="text/JavaScript" src="js/jquery.mousewheel.js"></script> 
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">
</script>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script> 

<!-- Load the CloudCarousel JavaScript file -->
<script type="text/JavaScript" src="js/cloud-carousel.1.0.5.js"></script>
 
<script type="text/javascript">
$(document).ready(function(){
						   
	// This initialises carousels on the container elements specified, in this case, carousel1.
	$("#carousel1").CloudCarousel(		
		{			
			reflHeight: 40,
			reflGap:2,
			titleBox: $('#da-vinci-title'),
			altBox: $('#da-vinci-alt'),
			buttonLeft: $('#slider-left-but'),
			buttonRight: $('#slider-right-but'),
			yRadius:30,
			xPos: 480,
			yPos: 32,
			speed:0.15,
		}
	);
});
 
</script>

</head>

<body id="home">

<jsp:include page="header_menu.jsp" />

<div id="templatemo_slider">
	<!-- This is the container for the carousel. -->
    <div id = "carousel1" style="width:960px; height:280px;background:none;overflow:scroll; margin-top: 20px">            
        <!-- All images with class of "cloudcarousel" will be turned into carousel items -->
        <!-- You can place links around these images -->
        <a href="show_all_jobs.jsp" rel="lightbox"><img class="cloudcarousel" src="../images/slider/10.jpg" alt="CSS Templates 2" title="Website Templates 2" /></a>
        <a href="register_seeker.jsp" rel="lightbox"><img class="cloudcarousel" src="../images/slider/09.jpg" alt="CSS Templates 3" title="Website Templates 3" /></a>
         <a href="index.jsp" rel="lightbox"><img class="cloudcarousel" src="../images/slider/11.jpg" alt="CSS Templates 4" title="Website Templates 4" /></a>
        <a href="" rel="lightbox"><img class="cloudcarousel" src="../images/slider/12.jpg" alt="Flash Templates 1" title="Flash Templates 1" /></a>
        <a href="" rel="lightbox"><img class="cloudcarousel" src="../images/slider/13.jpg" alt="Flash Templates 2" title="Flash Templates 2" /></a>
    </div>
    
    <!-- Define left and right buttons. -->
    <center>
    <input id="slider-left-but" type="button" value="" />
    <input id="slider-right-but" type="button" value="" />
    </center>
</div>


<div id="templatemo_main">
	
    <div class="col one_third fp_services">
   	ITES/BPO/.COM<br/>
   	Elico Healthcare Services<br/>
   	Ienergizer<br/>
   	SourceHOV<br/>
   	Presentation Design<br/>
    </div>
    <div class="col one_third fp_services">
    hello world column 2
    </div>
    <div class="col one_third no_margin_right fp_services">
    hello world column 3
    </div>
    <div class="cleaner"></div>
</div> <!-- END of templatemo_main -->

<jsp:include page="footer_menu.jsp"></jsp:include>

<jsp:include page="footer.jsp"></jsp:include>


</body>
</html>