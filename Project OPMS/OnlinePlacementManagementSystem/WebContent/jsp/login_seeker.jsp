<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Seeker</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />
</head>
<body id="home">


<jsp:include page="header_menu.jsp" />

<div id="templatemo_main">

<table>
	<tr>
	
		<td style="width:300px"></td>
		<td align="center" width="400">
			<div >
				<s:form action="login_seeker" id="login_form">
					
					<div>
					<s:label><h3 style="text-align: center;">Job Seeker Login</h3></s:label>
					<s:textfield name="email_id" label="Email "></s:textfield>
					<s:password name="password" label="Password "></s:password> 
					<s:submit value="Login"/><s:reset value="Clear"/>
				<s:actionerror/>
					</div>
				</s:form>
				</div><!-- END OF LOGIN FORM HERE-->
		</td>
		<td style="width:300px"></td>
	</tr>
</table>

</div><!-- END OF CONTENT HERE -->

<jsp:include page="footer.jsp"/>

</body>
</html>
