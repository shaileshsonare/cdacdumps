<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.SQLException,java.sql.DriverManager,java.sql.PreparedStatement,java.sql.Connection,java.io.PrintWriter,java.sql.ResultSet,com.connections.DBConnection" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Show applied jobs</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />
</head>
<%
String sid=null;
int seeker_id;
try
{
	sid=session.getAttribute("seeker_session_id").toString();
}
	catch(Exception e){}
	if(sid==null)
	{
		response.sendRedirect("login_seeker.jsp");
	}
%>
<body id="home">
<jsp:include page="header_menu.jsp"/>

	
<div id="templatemo_main" style="height:400px">
<a href="home_seeker.jsp">Back</a>
<a href="show_jobs.jsp">Show Jobs</a> 
<a href="logout.jsp">Logout</a><br><br>


<%

Connection conn;
String sql=null;
PreparedStatement pstmt=null;
ResultSet rs;
seeker_id=Integer.parseInt(sid.toString());
try 
{
	
		conn=new DBConnection().getConnection();

		sql="select j.job_id,j.job_title,j.company_name,j.position,j.location,j.job_description,j.eligibility,j.contact_email,j.contact_number,j.package,to_char(posting_date,'dd-Mon-yy') posting_date,j.recruiter_id rec_id from jobs_tb j join applied_jobs_tb aj on (j.job_id=aj.job_id) and seeker_id="+seeker_id;
		
		pstmt=conn.prepareStatement(sql);
		
		rs=pstmt.executeQuery();
		
		System.out.println(rs);
		
		out.println("<table border='1'><th>Job Id<th>Posting Date<th>Job Title<th>Company Name<th>Position<th>Location<th>Description<th>Eligibility<th>Email<th>Contact Number<th>Package<th>Recruiter Id");
		
		while(rs.next())
		{
			out.println("<tr><td>"+rs.getString("job_id")+
					"<td>"+rs.getString("posting_date")+
					"<td>"+rs.getString("job_title")+
					"<td>"+rs.getString("company_name")+
					"<td>"+rs.getString("position")+
					"<td>"+rs.getString("location")+
					"<td>"+rs.getString("job_description")+
					"<td>"+rs.getString("eligibility")+
					"<td>"+rs.getString("contact_email")+
					"<td>"+rs.getString("contact_number")+
					"<td>"+rs.getString("package")+
					"<td>"+rs.getString("rec_id")
			);
		}
		out.println("</table>");
		conn.close();
}catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

%>

</div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.jsp"/>

</body>
</html>