<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/ddsmoothmenu.css" />
</head>
<body>
<div id="templatemo_bottom_wrapper">
    <div id="templatemo_bottom">
    	<div class="col one_third">
        	<h4><span></span>Blogroll</h4>
            <div class="bottom_box">
                <ul class="footer_list">
                    <li><a href="login_seeker.jsp">Seekers Login</a></li>
                    <li><a href="register_seeker.jsp">Seekers Signup</a></li>
                    <li><a href="login_recruiter.jsp">Recruiters Login</a></li>
                    <li><a href="register_recruiter.jsp">Recruiters Signup</a></li>
                    <li><a href="login_admin.jsp">Admin</a></li>
                    <li><a href="show_all_jobs.jsp">Show Jobs</a></li>
                </ul>  
			</div>
        </div>
        <div class="col one_third">
        	<h4><span></span>Updates</h4>
            <div class="bottom_box">
                <ul class="twitter_post">
                    <li>Curabitur nec dui venenatis sem consectetur facilisis. Donec velit nisi, lacinia sed et. <a href="index.jsp">www.indiajobs.com</a></li>
                    <li>Proin dignissim, diam necenim lorem tempus orci, ac imperdiet ante purus in justo.</li>
                </ul>
			</div>
        </div>
        <div class="col one_third no_margin_right">
        	<h4><span></span>About Us</h4>
            <div class="bottom_box">
                <p><em>Duis sem nisl, dignissim ac feugiat in, mattis sit amet est. Duis leo leo, suscipit cursus vehicula sit amet. Sed varius pellentesque massa vel rutrum.</em></p><br />              
                <div class="footer_social_button">
                    <a href="#"><img src="../images/facebook.png" title="facebook" alt="facebook" /></a>
                    <a href="#"><img src="../images/flickr.png" title="flickr" alt="flickr" /></a>
                    <a href="#"><img src="../images/twitter.png" title="twitter" alt="twitter" /></a>
                    <a href="#"><img src="../images/youtube.png" title="youtube" alt="youtube" /></a>
                    <a href="#"><img src="../images/feed.png" title="rss" alt="rss" /></a>
                </div>
            </div>
        </div>
        
    	<div class="cleaner"></div>
    </div> <!-- END of tempatemo_bottom -->
</div> <!-- END of tempatemo_bottom_wrapper -->


</body>
</html>