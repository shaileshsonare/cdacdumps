package com.actions;

import com.bslogics.RegisterBsLogic;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;




public class AdminLoginAction extends ActionSupport
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int admin_id;
	String username;
	String password;
		
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public int getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(int admin_id) {
		this.admin_id = admin_id;
	}

	public String execute() throws Exception
	{	
		RegisterBsLogic rbl=new RegisterBsLogic();
		if(rbl.loginAdmin(this))
		{
			setAdmin_id(rbl.getAdmin_id());
			ActionContext.getContext().getSession().put("admin_session_id", getAdmin_id());
			return "success";
		}
		else
			return "failure";
		
	}
}
