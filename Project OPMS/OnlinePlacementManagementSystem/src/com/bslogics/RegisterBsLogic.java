package com.bslogics;


import java.util.Iterator;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.mapping.List;

import com.actions.AdminLoginAction;
import com.actions.PostJobAction;
import com.actions.RecruiterLoginAction;
import com.actions.RegisterRecruiterAction;
import com.actions.RegisterSeekerAction;
import com.actions.SeekerLoginAction;
import com.pojos.AdminPojo;
import com.pojos.PostJobPojo;
import com.pojos.RecruiterPojo;
import com.pojos.SeekerPojo;

import freemarker.template.SimpleDate;


public class RegisterBsLogic 
{
	
	boolean status=false;
	SessionFactory sessionFactory;
	Session session;
	Transaction t;
	int recruiter_id;
	int seeker_id;
	int admin_id;
	int acc_status;
	
	public void createSession()
	{
		sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
	}
	
	public boolean saveSeeker(RegisterSeekerAction rsa)
	{
		createSession();
		t=session.beginTransaction();
	
		SeekerPojo sp=new SeekerPojo(rsa.getSeeker_id(),rsa.getEmail_id(),rsa.getPassword(),rsa.getFirst_name(),rsa.getLast_name(),rsa.getGender(),rsa.getDob(),rsa.getQualification(),rsa.getStream(),rsa.getPercentage(),rsa.getExperience(),rsa.getAddress(),rsa.getCity(),rsa.getCountry(),rsa.getPin(),rsa.getContact_number(),1,null);
		
		session.save(sp);
		t.commit();
		status=true;
		session.close();
		return status;
	}

	public boolean saveRecruiter(RegisterRecruiterAction rra)
	{
		createSession();
		t=session.beginTransaction();
		
		RecruiterPojo rp=new RecruiterPojo(rra.getRecruiter_id(),rra.getRecruiter_name(),rra.getEmail_id(),rra.getPassword(),rra.getAddress(),rra.getCity(),rra.getCountry(),rra.getPin(),rra.getContact_number(),1);
		
		session.save(rp);
		t.commit();
		status=true;
		session.close();
		return status;
	}
	
	public boolean loginSeeker(SeekerLoginAction sla)
	{
		createSession();
		
		Query query=session.createQuery("from SeekerPojo sp where sp.email_id='"+sla.getEmail_id()+"'");
		
		Iterator iterator=query.iterate(); 
		
		while(iterator.hasNext())
		{
			SeekerPojo sp=(SeekerPojo)iterator.next();
			this.seeker_id=sp.getSeeker_id();
			this.acc_status=sp.getStatus();
			System.out.println(sp.getEmail_id());
			System.out.println(sp.getPassword());
			System.out.println(sp.getFirst_name());
			
			status=true;
		}
		return status;
	}
	
	public boolean loginRecruiter(RecruiterLoginAction rla)
	{
		createSession();
		
		Query query=session.createQuery("from RecruiterPojo rp where rp.email_id='"+rla.getEmail_id()+"'");
		
		Iterator iterator=query.iterate(); 
		
		while(iterator.hasNext())
		{
			RecruiterPojo rp=(RecruiterPojo)iterator.next();
			this.recruiter_id=rp.getRecruiter_id();
			this.acc_status=rp.getStatus();
			System.out.println(rp.getEmail_id());
			System.out.println(rp.getPassword());
			
			status=true;
		}
		return status;
	}

	public boolean post_job(PostJobAction pja)
	{
		createSession();
		t=session.beginTransaction();
		
		PostJobPojo pjp=new PostJobPojo(pja.getJob_id(),pja.getJob_title(),pja.getCompany_name(),pja.getLocation(),pja.getPosition(),pja.getJob_description(),pja.getEligibility(),pja.getContact_email(),pja.getContact_number(),pja.getSal_package(),pja.getRecruiter_id());
		session.save(pjp);
		
		t.commit();
		status=true;
		session.close();
		
		return status;
	}
	
	
	public boolean loginAdmin(AdminLoginAction ala)
	{
		
		createSession();
		
		Query query=session.createQuery("from AdminPojo ap where ap.username='"+ala.getUsername()+"'");
		
		Iterator iterator=query.iterate(); 
		
		while(iterator.hasNext())
		{
			AdminPojo ap=(AdminPojo)iterator.next();
			if(ap.getUsername().equals(ala.getUsername()) && ap.getPassword().equals(ala.getPassword()))
			{
				admin_id=ap.getAdmin_id();
				status=true;
			}
		}
		return status;
	}
	
	public boolean uploadResume(String id,String resume_path)
	{
		
		int seeker_id=Integer.parseInt(id.toString());
		
		createSession();
		t=session.beginTransaction();
		
		String sql="update SeekerPojo set resume_path = :newResumePath where seeker_id = :new_seeker_id";
		
		Query query=session.createQuery(sql);
		
		query.setString("newResumePath",resume_path);
		query.setInteger("new_seeker_id", seeker_id);
		
		int rows=query.executeUpdate();
		t.commit();
		
		System.out.println(rows+" updated...");
		status=true;
				return status;	
	
	}
	

	public int getRecruiter_id() {
		return recruiter_id;
	}
	
	public int getSeeker_id() {
		return seeker_id;
	}

	public int getAdmin_id() {
		return admin_id;
	}

	public int getAccStatus() {	
		return acc_status;
	}
	
}
