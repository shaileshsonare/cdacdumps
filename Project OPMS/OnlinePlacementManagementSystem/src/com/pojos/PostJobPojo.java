package com.pojos;

public class PostJobPojo 
{
	int job_id;
	String job_title;
	String company_name;
	String location;
	String position;
	String job_description;
	String eligibility;
	String contact_email;
	String contact_number;
	String sal_package;
	int recruiter_id;
	public PostJobPojo(int job_id, String job_title, String company_name,
			String location, String position, String job_description,
			String eligibility, String contact_email, String contact_number,
			String sal_package, int recruiter_id) {
		super();
		this.job_id = job_id;
		this.job_title = job_title;
		this.company_name = company_name;
		this.location = location;
		this.position = position;
		this.job_description = job_description;
		this.eligibility = eligibility;
		this.contact_email = contact_email;
		this.contact_number = contact_number;
		this.sal_package = sal_package;
		this.recruiter_id = recruiter_id;
	}
	public int getJob_id() {
		return job_id;
	}
	public void setJob_id(int job_id) {
		this.job_id = job_id;
	}
	public String getJob_title() {
		return job_title;
	}
	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getJob_description() {
		return job_description;
	}
	public void setJob_description(String job_description) {
		this.job_description = job_description;
	}
	public String getEligibility() {
		return eligibility;
	}
	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}
	public String getContact_email() {
		return contact_email;
	}
	public void setContact_email(String contact_email) {
		this.contact_email = contact_email;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	public String getSal_package() {
		return sal_package;
	}
	public void setSal_package(String sal_package) {
		this.sal_package = sal_package;
	}
	public int getRecruiter_id() {
		return recruiter_id;
	}
	public void setRecruiter_id(int recruiter_id) {
		this.recruiter_id = recruiter_id;
	}
		
}
