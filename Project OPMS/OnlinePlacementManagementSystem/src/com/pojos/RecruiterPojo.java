package com.pojos;

public class RecruiterPojo 
{
	int recruiter_id;
	String recruiter_name;
	String email_id;
	String password;
	String address;
	String city;
	String country;
	int pin;
	String contact_number;
	int status;
	public RecruiterPojo() {
		super();
	}
	
	public RecruiterPojo(int recruiter_id, String recruiter_name,
			String email_id, String password, String address, String city,
			String country, int pin, String contact_number, int status) {
		super();
		this.recruiter_id = recruiter_id;
		this.recruiter_name = recruiter_name;
		this.email_id = email_id;
		this.password = password;
		this.address = address;
		this.city = city;
		this.country = country;
		this.pin = pin;
		this.contact_number = contact_number;
		this.status = status;
	}
	public int getRecruiter_id() {
		return recruiter_id;
	}
	public void setRecruiter_id(int recruiter_id) {
		this.recruiter_id = recruiter_id;
	}
	public String getRecruiter_name() {
		return recruiter_name;
	}
	public void setRecruiter_name(String recruiter_name) {
		this.recruiter_name = recruiter_name;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getPin() {
		return pin;
	}
	public void setPin(int pin) {
		this.pin = pin;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
