1. Table Seekers Registration



<s:textfield name="username" label="Username"/>
		<s:password name="password" label="Password"/>
		<s:textfield name="qualification" label="Qualification"/>
		<s:submit value="Register Me"/>

create table seekers_reg_tb(seekers_id number(10) primary key,username varchar2(20),password varchar2(20),qualification varchar2(20))
/


create sequence seekers_id_sequence start with 1
increment by 1
minvalue 1
maxvalue 9999999999
/


create or replace trigger seekers_id_trigger
before insert
on seekers_reg_tb
referencing new as new
for each row
begin
select seekers_id_sequence.nextval into:new.seekers_id from dual;
end;
/


2. Table Providers Registration

<s:textfield name="username" label="Username"/>
		<s:password name="password" label="Password"/>



create table providers_reg_tb(providers_id number(10) primary key, username varchar2(20),password varchar2(20))
/


create sequence providers_id_sequence
start with 1000
increment by 1
/


create or replace trigger providers_id_trigger
before insert
on providers_reg_tb
referencing new as new
for each row
begin
select providers_id_sequence.nextval into:new.providers_id from dual;
end;
/



3. Table Ads

private String job_title;
	private String qualification;
	private String no_of_seats;

create table ads_tb(ad_id number(10) primary key, job_title varchar2(50),job_category varchar2(20),eligibility varchar2(50), no_of_seats number(10), providers_id number(10),posting_date date default sysdate)
/


create sequence ad_id_sequence
start with 1
increment by 1
/


create or replace trigger ad_id_trigger
before insert
on ads_tb
referencing new as new
for each row
begin
select ad_id_sequence.nextval into:new.ad_id from dual;
end;
/



4.

pstmt.setInt(1, 0);
pstmt.setString(2, request.getParameter("job_title"));
pstmt.setString(3, request.getParameter("job_category"));
pstmt.setString(4, request.getParameter("eligibility"));
pstmt.setString(5, request.getParameter("no_of_seats"));
pstmt.setString(6, request.getParameter("providers_id"));

create table ads_tb(ad_id number(10) primary key, job_title varchar2(50),qualification varchar2(50), no_of_seats number(10))
/

CONSTRAINT TO ADS TB
alter table ads_tb
add constraint providers_id_fk foreign key (providers_id)
references providers_reg_tb(providers_id)
/



5. Table Application 

create table ads_tb(ad_id number(10) primary key, job_title varchar2(50),qualification varchar2(50), no_of_seats number(10))
/