#include<dirent.h>
#include<stdio.h>

int main(int argc,char *argv[])
{
	char path[100];
	if(argc==2)
		strcpy(path,argv[1]);
	else
		strcpy(path,".");

	DIR* pd=opendir(path);
	struct dirent* ps;

	while(1)
	{
		ps=readdir(pd);
		if(ps==NULL) break;
		puts(ps->d_name);
	}
	closedir(pd);
	return 0;
}
