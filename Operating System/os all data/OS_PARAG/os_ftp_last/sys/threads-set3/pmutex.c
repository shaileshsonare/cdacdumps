#include<pthread.h>

const int max=10;
int i;
pthread_mutex_t m1;

void* efun1(void* pv)
{
	printf("Thread--A\n");

	pthread_mutex_lock(&m1);
	for(i=1;i<=max;i++) {
		printf("thread--A--%d\n",i);
		sleep(1);
		//access shared resource
	}
	pthread_mutex_unlock(&m1);
}
void* efun2(void* pv)
{
	printf("Thread--B\n");

	pthread_mutex_lock(&m1);
	for(i=1;i<=max;i++){
		printf("thread--B--%d\n",i);
		sleep(1);
		//access shared resource
	}
	pthread_mutex_unlock(&m1);
}

int main()
{
	int i;
	pthread_t pt1,pt2,ptm;
	pthread_mutex_init(&m1,NULL);
	
	pthread_create(&pt1,NULL,efun1,NULL);
	pthread_create(&pt2,NULL,efun2,NULL);

	pthread_join(pt1,NULL);
	pthread_join(pt2,NULL);
	pthread_mutex_destroy(&m1);

	printf("main--thank you\n");
	return 0;
}
