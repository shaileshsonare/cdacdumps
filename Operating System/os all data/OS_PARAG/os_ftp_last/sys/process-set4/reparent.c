#include<unistd.h>

int main()
{
	int ret,i;
	printf("welcome\n");
	ret=fork();
	if(ret<0) {
		perror("fork failed");
		exit(0);
	}
	if(ret==0) { //child
		sleep(2);
		for(i=1;i<=5;i++)
		printf("child-pid=%d,ppid=%d\n",
				getpid(),getppid());
		exit(0);
	}
	else{  //ret>0,parent
		printf("parent-pid=%d,ppid=%d,ret=%d\n",
				getpid(),getppid(),ret);
	}
	return 0;
}
