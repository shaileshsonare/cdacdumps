
vsum()
{
	sum=0
	for x in $*
	do
		let sum=$sum+$x
	done
	echo "sum of $* is $sum"
}

#vsum 12 15
#vsum 10 20 18
#vsum 10 20 30 40
