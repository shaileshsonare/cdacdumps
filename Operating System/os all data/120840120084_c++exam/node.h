class node
{
	int data;
	node *left;
	node *right;
	public:
	node();
	node(int data);
	void setdata(int data);
	int getdata();
	void setleft(node *left);
	node* getleft();
	node* getright();
	void setright(node *right);
};
