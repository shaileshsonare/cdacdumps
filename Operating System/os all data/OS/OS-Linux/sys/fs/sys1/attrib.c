#include<unistd.h>
#include<sys/stat.h>

int main(int argc,char*argv[])
{
	int k;
	struct stat sb;
	printf("file name is %s\n",argv[1]);
	k=lstat(argv[1],&sb);
	if(k<0) {
		perror("lstat");
		exit(0);
	}
	printf("Inode no: %u\n",sb.st_ino);
	printf("No.of links:%u\n",sb.st_nlink);
	printf("Mode:%o\n",sb.st_mode);
	printf("uid=%u,gid=%u\n",sb.st_uid,sb.st_gid);
	printf("Actual size=%u bytes\n",sb.st_size);
	printf("No.of blocks=%u, block size=%u\n",
			sb.st_blocks,sb.st_blksize);
	printf("Last mod time is %s\n",
			ctime(&sb.st_mtime));
	char buf[100];
	if(S_ISREG(sb.st_mode))
		printf("regular file\n");
	else if(S_ISDIR(sb.st_mode))
		printf("dir file\n");
	else if(S_ISLNK(sb.st_mode))
	{
		readlink(argv[1],buf,100);
		printf("sym link,target=%s\n",buf);
	}

	return 0;
}
