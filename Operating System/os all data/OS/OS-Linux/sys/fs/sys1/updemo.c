#include<unistd.h>
#include<fcntl.h>

int main()
{
	int fds[2],k,ret;
	open("sample.dat",O_RDONLY);
	k=pipe(fds);
	if(k<0) { perror("pipe"); exit(0); }

	ret=fork();
	if(ret==0)//child, reading
	{
		char buf[100];
		close(fds[1]); //closing write end
		k=read(fds[0],buf,20);
		if(k<0) { perror("read"); exit(0); }
		printf("child:buf=%s\n",buf);
		close(fds[0]);

	}
	else //parent, writing
	{
		close(fds[0]);//close read end of pipe
		sleep(10);
		write(fds[1],"Hello FIFO\n",12);
		close(fds[1]);
	}
	return 0;	
}
