#include<dirent.h>
#include<stdio.h>
//these are lib calls

int main(int argc,char* argv[])
{
	DIR* pd;
	struct dirent* sd;
	pd=opendir(argv[1]);
	if(pd==NULL) {
		printf("opendir fails\n");
		exit(0);
	}
	while(1) {
		sd=readdir(pd);
		if(sd==NULL) break;
		printf("%s\n",sd->d_name);
	}
	closedir(pd);
	return 0;
}

