#include<unistd.h>
#include<sys/shm.h>

#define KEY 1234

int main()
{
	int shmid;
	int *pi;
	char *ps;
	char buf[64];
	shmid=shmget(KEY,1024,IPC_CREAT|0666);
	if(shmid<0) {
		perror("shmget");
		exit(0);
	}	
	void* pv;
	pv=shmat(shmid,0,0);
	if(pv<0) {
		perror("shmat"); exit(0);
	}
	//access shared memory
	pi=pv;
	printf("val=%d\n",*pi);
	ps=pv+4;
	strcpy(buf,ps);
	printf("buf=%s\n",buf);

	shmdt(pv);
	return 0;
}

