#include<unistd.h>
#include<sys/wait.h>

int main()
{
	int ret,k,st;
	ret=fork();
	if(ret==0)
	{
		printf("child process\n");
		//k=execl("/bin/date","date","+%d-%m-%Y",NULL);
		//k=execl("/usr/bin/cal","cal","14","2015",NULL);
		//k=execl("/usr/bin/bc","abc",NULL);
		//k=execl("./s.out","abc.out",NULL);
		k=execl("./fsample","fsample",NULL);
		if(k<0) {
			perror("execl");
			exit(0);
		}
		printf("child-thank you\n");
	}
	else
	{
		printf("parent process\n");
		waitpid(-1,&st,0);
		printf("termination status:%d\n",WEXITSTATUS(st));
		printf("parent-thank you\n");

	}
	return 0;
	
}
