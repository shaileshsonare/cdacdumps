#include<unistd.h>
#include<sys/wait.h>

int main()
{
	int ret,i,st;
	int *ptr=NULL;
	printf("welcome\n");
	ret=fork();
	if(ret<0) {
		perror("fork failed");
		exit(0);
	}
	if(ret==0) { //child
		//sleep(2);
		for(i=1;i<=10;i++)
			printf("child-pid=%d,ppid=%d\n",
				getpid(),getppid());
		//*ptr=10;
		//some code
		exit(5);
	}
	else{  //ret>0,parent
		printf("parent-pid=%d,ppid=%d,ret=%d\n",
				getpid(),getppid(),ret);
		waitpid(-1,&st,0); //blocking call
		printf("thank you\n");
		if(WIFEXITED(st))
			printf("normal exit,status is %d\n",
				WEXITSTATUS(st));
		else
			printf("abnormal termination\n");
	}
	return 0;
}







