#include<pthread.h>

void* efun(void* ptr)
{
	int i;
	char* pc=ptr;
	printf("I am thread-%c\n",*pc);
	for(i=1;i<=10;i++){
		printf("%c--%d\n",*pc,i);
		sleep(1);
	}
}
int main()
{
	int i;
	pthread_t pt1,pt2,pt3,ptm;
	char c1='A',c2='B',c3='X';
	ptm=pthread_self();
	//efun1(NULL);
	//efun2(NULL);
	pthread_create(&pt1,NULL,efun,&c1);
	pthread_create(&pt2,NULL,efun,&c2);
	pthread_create(&pt3,NULL,efun,&c3);

	pthread_join(pt1,NULL);
	pthread_join(pt2,NULL);
	pthread_join(pt3,NULL);
	printf("main--thank you\n");
	return 0;
}
