#include<unistd.h>
#include<sys/stat.h>

int main(int argc,char* argv[])
{
	int k;
	struct stat sb;
	k=lstat(argv[1],&sb);
	if(k<0) {
		perror("lstat");
		exit(0);
	}
	printf("file name is %s\n",argv[1]);
	printf("mode is %o\n",sb.st_mode);
	printf("inode no. is %d\n",sb.st_ino);
	printf("no.of links  %d\n",sb.st_nlink);
	printf("uid=%d,gid=%d\n",sb.st_uid,sb.st_gid);
	printf("file size=%d bytes\n",sb.st_size);
	printf("last mod time :%s\n",ctime(&sb.st_mtime));
	if(S_ISREG(sb.st_mode))
		printf("regular file\n");
	else if(S_ISDIR(sb.st_mode))
		printf("dir file\n");
	//some more

	return 0;
}

