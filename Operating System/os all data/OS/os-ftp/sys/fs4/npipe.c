#include<unistd.h>
#include<fcntl.h>

int main()
{
	int k,ret,fd;
	char buf[20];
	k=mkfifo("/tmp/ff1",0666);

	ret=fork();
	if(ret==0) //child,reading
	{
		fd=open("/tmp/ff1",O_RDONLY);

		k=read(fd,buf,20);
		//write(1,buf,k);
		printf("child,buf=%s\n",buf);

		close(fd);

	}
	else	   //parent,writing
	{
		fd=open("/tmp/ff1",O_WRONLY);
		sleep(5);
		write(fd,"Hello FIFO\n",12);
		
		close(fd);
	}
	return 0;
}
