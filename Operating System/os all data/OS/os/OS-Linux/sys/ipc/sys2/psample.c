#include<pthread.h>

const int max=10;
void* efun(void* pv)
{
	int i;
	char *ps=pv;
	printf("Hello, i am thread:%s\n",ps);
	for(i=1;i<=max;i++)
		printf("thread %s--%d\n",ps,i);
}
int main()
{
	pthread_t t1,t2,t3;

	pthread_create(&t1,NULL,efun,"abc");
	pthread_create(&t2,NULL,efun,"xyz");
	pthread_create(&t3,NULL,efun,"pqr");

	//sleep(5);
	pthread_join(t1,NULL);
	pthread_join(t2,NULL);
	printf("main--thank you\n");
	return 0;
}


