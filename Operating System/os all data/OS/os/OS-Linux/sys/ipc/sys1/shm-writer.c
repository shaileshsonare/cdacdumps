#include<unistd.h>
#include<sys/shm.h>

#define KEY 1234

int main()
{
	int shmid;
	int *pi;
	char *ps;
	shmid=shmget(KEY,1024,IPC_CREAT|0666);
	
	void* pv;
	pv=shmat(shmid,0,0);

	//access shared memory
	pi=pv;
	*pi=20;
	ps=pv+4;
	strcpy(ps,"Hello Shm...\n");

	shmdt(pv);
	return 0;
}

