#include<unistd.h>
#include<fcntl.h>

int main()
{
	int fd;
	fd=open("sample.dat",O_RDONLY);
	if(fd<0)
	{
		perror("open");
		exit(0);
	}
	char buf[50];
	int k;
	k=read(fd,buf,50);
	if(k<0) {
		perror("read");
		exit(0);
	}
	write(1,buf,k);
	close(fd);
	return 0;
}
