#include<unistd.h>
#include<sched.h>
#include<sys/time.h>
#include<sys/resource.h>

int main()
{
	int pid=getpid();
	struct timespec tm;
	//nice(5);
	setpriority(PRIO_PROCESS,PRIO_PROCESS,6);
	printf("current priority=%d\n",
			getpriority(PRIO_PROCESS,PRIO_PROCESS));
	int sch=sched_getscheduler(pid);
	if(sch==SCHED_FIFO) {
		printf("FIFO\n");
		printf("min=%d,max=%d\n",
			  sched_get_priority_min(SCHED_FIFO),
			  sched_get_priority_max(SCHED_FIFO));
	}
	else if(sch==SCHED_RR)
	{
		printf("round robin\n");
		sched_rr_get_interval(pid,&tm);
		printf("secs=%u,ns=%ld\n",tm.tv_sec,tm.tv_nsec);
	}
	else if(sch==SCHED_OTHER)
		printf("time sharing\n");
	else
		printf("unknown\n");
	//getchar();

	return 0;
}

