#include<unistd.h>
const int max=60;
int main()
{
	int ret,i;
	printf("welcome\n");
	ret=fork();
	if(ret<0) {
		perror("fork");
		exit(0);
	}
	if(ret==0) {
		for(i=1;i<=max;i++)
			printf("child--%d\n",i);
		exit(0);
	}
	else {//ret>0
		for(i=1;i<=max;i++)
			printf("parent--%d\n",i);
		printf("parent-thank you\n");
	}
	return 0;
}

