#include<unistd.h>

int main()
{
	int ret,i;
	printf("welcome\n");
	ret=fork();
	if(ret<0) {
		perror("fork");
		exit(0);
	}
	if(ret==0) {
		//sleep(2);
		for(i=1;i<=10;i++)
		printf("child-pid=%d,ppid=%d\n",
			getpid(),getppid());
		exit(0);
	}
	else {//ret>0
		printf("parent-pid=%d,ppid=%d,ret=%d\n",
			getpid(),getppid(),ret);
		printf("parent-thank you\n");
	}
	return 0;
}

