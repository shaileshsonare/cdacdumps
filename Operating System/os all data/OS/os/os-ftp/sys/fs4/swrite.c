#include<unistd.h>
#include<fcntl.h>

int main()
{
	int fd,k;
	fd=open("simple.dat",O_WRONLY|O_CREAT,0666);
	if(fd<0) {
		perror("open");
		exit(0);
	}
	char buf[]="ABCDEFGHIJKLMNOPQRST12345\n";
	k=write(fd,buf,sizeof(buf));
	if(k<0)
	{
		perror("write");
		exit(0);
	}
	printf("written data,k=%d\n",k);
	return 0;
}

