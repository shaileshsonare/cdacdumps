#include<unistd.h>
const int max=50;
int main()
{
	int ret,i;
	printf("welcome\n");
	ret=fork();
	if(ret==0) { //child
		for(i=1;i<=max;i++)
			printf("child--%d\n",i);
	}
	else{  //ret>0,parent
		for(i=1;i<=max;i++)
			printf("parent--%d\n",i);
	}
	return 0;
}
