rsum()
{
	num=$1
	sum=0
	while [ $num -ne 0 ]
	do
		let rem=$num%10
		let sum=$sum+$rem
		let num=$num/10
	done
	let temp1=$sum/10
	if [ $temp1 -eq 0 ]
	then
		return $sum
	else
		rsum $sum
	fi
}

echo "Enter a number :"
read a
rsum $a
echo "The recursive sum of $a is :$?"
	
