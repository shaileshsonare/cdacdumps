#!/bin/sh

test $# -eq 3
if [ $? -ne 0 ]
then
	exit
else
	let sum=$1+$2+$3
	avg=`echo "scale=3;$sum/3" | bc`
	echo "The average is :$avg"
fi

