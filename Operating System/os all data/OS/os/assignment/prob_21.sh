#!/bin/sh

echo "Enter the no. of terms :"
read n
let temp1=$n-1
sum=0
for i in `seq 0 $temp1`
do
	let temp=$i+1
	echo "Enter number $temp :"
	read arr[i]
	let sum=sum+${arr[i]}
done
avg=`echo "scale=3;$sum/$n" | bc`
echo "The sum is :$sum"
echo "The average is :$avg"
