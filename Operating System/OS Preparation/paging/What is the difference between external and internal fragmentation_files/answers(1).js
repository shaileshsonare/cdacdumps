var ANSW = (function($){
	var options = {
		storage_key: 'answ-storage'
	};
	
	var module_storage = {};
	
	var user = {
		logged_in: false
	,	name: ''
	,	locale: ''
	,	photo: ''
	,	unread_messages: 0
	,	account:{
			is_super: false
		}
	};
	
	var page = {
		lmi: 15001
	,	controller: 'index'
	,	action: 'index'
	,	type: ''
	};
	
	var question = {
		id: null
	,	url: '/'
	,	answered: false
	,	category: ''
	};
	
	var _debug = {
		log: function(message){
			console && console.info(message);
		}
	};
	
	var store = function(engine, key, value){
		if( value !== undefined ){
			window[engine] && window[engine].setItem(key, JSON.stringify(value));
			return this;
		} else {
			var val = window[engine] && window[engine].getItem(key);
			if( val !== null ){
				val = JSON.parse(val);
			}
			return val;
		}
	};

	
	var util = {
		is: {
			array: function(item){
				return typeof o === "object" && typeof o.splice === "function";
			}
		,	object: function(item){
				return (o !== null && typeof o === "object" && typeof o.splice !== "function");
			}
		,	callable: function(item){
				return typeof fn !== "undefined" && typeof fn === "function";	
			}
		}
	,	load_lib: function(src, callback){
			(function(source){
				var script = document.createElement('script');
				script.async = true; script.type = 'text/javascript'; script.src = source;
				script.onload = callback || function(e){};
				(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
			})(src);
		}
	};
	
	(function core_enhancements(){
		String.prototype.supplant = function(o){
			o = o || {};
			return this.replace(/{([^{}]*)}/g,
				function (bracketed, clean) {
					var object_value = o[clean];
					return ['string', 'number'].indexOf((typeof object_value)) > -1 ? object_value : bracketed;
				}
			);
		}
	})();
	
	return {
		debug: function(message){
			_debug(message);
		},
		extend:function(nm, method){
			this[nm] = method;
			return this;
		},
		store:function(key, val){
			typeof this[options.storage_key] === "object" || (this[options.storage_key] = {})
			if( typeof val !== 'undefined' ){
				this[options.storage_key][key] = val;
				return this;
			} else {
				return this[options.storage_key][key] || null;
			}
			return this;
		},
		persist: function(key, value){
			return store('localStorage', key, value);
		},
		session: function(key, value){
			return store('sessionStorage', key, value);
		},
		register_module:function(module, data){
			module_storage[module] = module_storage[module] || [];
			module_storage[module].push(data);
			return this;
		},
		templatize:function(template, data){
			return template.supplant(data);
		},
		loadScript: function(src, cb){
			return util.load_lib(src, cb);
		}
	};
})(jQuery);

var _qevents = _qevents || [];

(function() { var elem = document.createElement('script'); elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js"; elem.async = true; elem.type = "text/javascript"; var scpt = document.getElementsByTagName('script')[0]; scpt.parentNode.insertBefore(elem, scpt); })();

_qevents.push( { qacct:"p-72V4-XKpaKDrE"} );
