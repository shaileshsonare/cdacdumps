package com.actions;

import java.io.File;
 
import org.apache.commons.io.FileUtils;
 
import com.bslogics.RegisterBsLogic;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
 
public class ResumeUploadAction extends ActionSupport{
    private static final long serialVersionUID = 1L;
 
    private File uploadFile;
    private String uploadFileContentType;
    private String uploadFileFileName; 
 
    public File getUploadFile() {
        return uploadFile;
    }
    public void setUploadFile(File uploadFile) {
        this.uploadFile = uploadFile;
    }
 
    public String getUploadFileContentType() {
        return uploadFileContentType;
    }
    public void setUploadFileContentType(String uploadFileContentType) {
        this.uploadFileContentType = uploadFileContentType;
    }
 
    public String getUploadFileFileName() {
        return uploadFileFileName;
    }
    public void setUploadFileFileName(String uploadFileFileName) {
        this.uploadFileFileName = uploadFileFileName;
    }
 
    public String execute()
    {
    	/*System.out.println("alo");
    	System.out.println(ActionContext.getContext().getSession().get("seeker_session_id"));
    	System.out.println(id);
    	*/
    	
    	String id=ActionContext.getContext().getSession().get("seeker_session_id").toString();
        try{
        	File dir = new File("H:/DAC/Project OPMS/OnlinePlacementManagementSystem/Resumes/"+id);  
        	dir.mkdir();
       // String filePath = "c:/Myuploads";  // Path where uploaded file will be stored
       // System.out.println("Server path:" + filePath); // check your path in console
        	System.out.println(dir.getAbsolutePath());
        File fileToCreate = new File(dir.getPath(), id+"_resume.docx");// Create file name  same as original
        FileUtils.copyFile(uploadFile, fileToCreate); // Just copy temp file content tos this file     
 
        String resume_path=dir.getPath()+"\\"+id+"_resume.docx";
        
        RegisterBsLogic rbl=new RegisterBsLogic();
        
        rbl.uploadResume(id, resume_path);
     
        
        }catch(Exception e)
        {
            e.printStackTrace();
            addActionError(e.getMessage());
            return INPUT;
 
        }
        return SUCCESS;
    }
 
}
// Context pirnt.....
 
//private HttpServletRequest servletRequest;
//String filePath = servletRequest.getSession().getServletContext().getRealPath("/");
//System.out.println("Server path:" + filePath);
//File fileN = new File(filePath, this.uploadFileFileName);
//FileUtils.copyFile(this.userImage, fileToCreate);