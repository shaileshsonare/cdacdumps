<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Job-Seeker Registration Page</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />


</head>
<body id="home">
<jsp:include page="header_menu.jsp"/>

<div id="templatemo_main">
<table>
	<tr>
		<td style="width:300px"></td>
		<td align="center" width="400">
			<div>
				<s:form action="register_seeker" id="registration_form">
					<div>
					<s:label><h3 style="text-align: center;">Get Started With IndiaJobs</h3></s:label>
					<s:label><h3 style="text-align: center;">Job Seekers</h3></s:label>
					<s:hidden name="seeker_id" value="0"></s:hidden>
					<s:textfield name="first_name" label="*First name "></s:textfield>
					<s:textfield name="last_name" label="Last name "></s:textfield>
					<s:radio list="{'Male','Female'}" name="gender" label="*gender"></s:radio>
					<s:textfield name="email_id" label="*Email address "></s:textfield>	
					<s:password name="password" label="*Choose a password "></s:password>
					<s:password name="passwd_conf" label="*Re-enter password"></s:password> 
					<%-- <s:date name="dob" id="bDateId" format="MM-DD-YYYY"/>  
					<s:textfield name="dob" id="%{bDateId}" label="Birth Date (MM-DD-YYYY)"/> --%>
					<s:textfield name="dob" label="*Date"/>
					<s:textfield name="qualification" label="*Qualification"></s:textfield>
					<s:textfield name="stream" label="*Stream"></s:textfield>
					<s:textfield name="percentage" label="*Aggregate"></s:textfield>
					<s:textfield name="experience" label="*Experience"></s:textfield>
					<s:textfield name="address" label="Street Address"></s:textfield>
					<s:textfield name="city" label="*City"></s:textfield>
					<s:textfield name="pin" label="PIN"></s:textfield>
					<s:textfield name="contact_number" label="*Contact Number"></s:textfield>
					</div>
					<s:submit value="Register"/><s:reset value="Clear"/>
					<s:actionerror/>

				</s:form>
				</div><!-- END OF REGISTRATION FORM HERE-->
		</td>
		<td style="width:300px"></td>
	</tr>
</table>

</div><!-- END OF CONTENT HERE -->



<jsp:include page="footer.jsp"/>
</body>
</html>