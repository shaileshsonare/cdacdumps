<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.SQLException,java.sql.DriverManager,java.sql.PreparedStatement,java.sql.Connection,java.io.PrintWriter,java.sql.ResultSet,com.connections.DBConnection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View applications</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />


</head>
<body id="home">
<jsp:include page="header_menu.jsp"/>

	
<div id="templatemo_main" style="height:400px">
<a href="posted_jobs.jsp">Back</a>
<a href="logout.jsp">Logout</a><br><br>

<%
String sid=null;
int recruiter_id=0;
try
{
	sid=session.getAttribute("session_id").toString();
	recruiter_id=Integer.parseInt(sid);
}
	catch(Exception e){}
	if(sid==null)
	{
		response.sendRedirect("login_recruiter.jsp");
	}
%>

<%

Connection conn;
String sql=null;
PreparedStatement pstmt=null;
ResultSet rs;

try 
{
	
		conn=new DBConnection().getConnection();
		
		
	//	sql="select s.seeker_id, s.email_id,s.password,s.first_name,s.last_name,to_char(s.dob,'dd-Mon-yy') Dob,s.qualification,s.stream,s.percentage,s.experience,s.address,s.city,s.country,s.pin,s.contact_number from seekers_tb s";
		sql="select a.recruiter_id, a.job_id, s.seeker_id, s.email_id,s.password,s.first_name,s.last_name,to_char(s.dob,'dd-Mon-yy') Dob,s.qualification,s.stream,s.percentage,s.experience,s.address,s.city,s.country,s.pin,s.contact_number from seekers_tb s join applied_jobs_tb a on (s.seeker_id = a.seeker_id) and a.recruiter_id="+recruiter_id;
		
		
		pstmt=conn.prepareStatement(sql);
		
		rs=pstmt.executeQuery();
		
		out.println("<table border='1'><th>recruiter id<th>job id<th>Email id<th>First Name<th>Last Name<th>Date of Birth<th>Qualification<th>Stream<th>Percentage<th>Experience<th>Address<th>city<th>Country<th>Pin<th>Contact Number<th>Resume");
		
		while(rs.next())
		{
			out.println("<tr><td>"+rs.getInt("recruiter_id")+
					"<td>"+rs.getString("job_id")+
					"<td>"+rs.getString("email_id")+
					"<td>"+rs.getString("first_name")+
					"<td>"+rs.getString("last_name")+
					"<td>"+rs.getString("Dob")+
					"<td>"+rs.getString("qualification")+
					"<td>"+rs.getString("stream")+
					"<td>"+rs.getInt("percentage")+
					"<td>"+rs.getString("experience")+
					"<td>"+rs.getString("address")+
					"<td>"+rs.getString("city")+
					"<td>"+rs.getString("country")+
					"<td>"+rs.getInt("pin")+
					"<td>"+rs.getString("contact_number")+
					"<td><form action='resume_download.jsp'>"+
					"<input type='hidden' name='seeker_id' value='"+rs.getString("seeker_id")+"'/>"+		
					"<input type='submit' value='Download'>"+
					"</form>"
			);
		}
		out.println("</table>");
		conn.close();
}catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

%>
 </div><!-- END OF CONTENT HERE -->
<jsp:include page="footer.jsp"/>

</body>
</html>