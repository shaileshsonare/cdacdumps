<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<title>Insert title here</title>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/ddsmoothmenu.css" />

<script type="text/javascript" src="../js/jquery-1-4-2.min.js"></script>  
<script type="text/javascript" src="../js/ddsmoothmenu.js"></script>
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script> 

<!-- Load the CloudCarousel JavaScript file -->
<script type="text/JavaScript" src="../js/cloud-carousel.1.0.5.js"></script>

<script type="text/javascript">
$(document).ready(function(){
						   
	// This initialises carousels on the container elements specified, in this case, carousel1.
	$("#carousel1").CloudCarousel(		
		{			
			reflHeight: 40,
			reflGap:2,
			titleBox: $('#da-vinci-title'),
			altBox: $('#da-vinci-alt'),
			buttonLeft: $('#slider-left-but'),
			buttonRight: $('#slider-right-but'),
			yRadius:30,
			xPos: 480,
			yPos: 32,
			speed:0.15,
		}
	);
});
 
</script>

</head>
<body>
<div id="templatemo_header_wrapper">
	<div id="site_title"><h1><a href="http://www.templatemo.com">Free CSS Templates</a></h1></div>
     <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li><a href="index.jsp" class="selected">Home</a></li>
            <li><a href>Seeker</a>
                <ul>
                    <li><span class="top"></span><span class="bottom"></span></li>
                    <li><a href="login_seeker.jsp">Login</a></li>
                    <li><a href="register_seeker.jsp">Sign up</a></li>
              	</ul>
            </li>
            <li><a href>Recruiter</a>
                <ul>
                    <li><span class="top"></span><span class="bottom"></span></li>
                    <li><a href="login_recruiter.jsp">Login</a></li>
                    <li><a href="register_recruiter.jsp">Sign up</a></li>       
              	</ul>
            </li>
             <li><a href>Admin</a>
                <ul>
                    <li><span class="top"></span><span class="bottom"></span></li>
                    <li><a href="login_admin.jsp">Login</a></li>       
              	</ul>
            </li>
        </ul>
        <br style="clear: left" />
    </div> <!-- end of templatemo_menu -->
    <div class="cleaner"></div>
</div>	<!-- END of templatemo_header_wrapper -->

</body>
</html>