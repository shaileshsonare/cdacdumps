<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Job Provider-Registration Page</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body id="home">
<jsp:include page="header_menu.jsp"/>
	
	<div id="templatemo_main">
<table>
	<tr>
		<td style="width:300px"></td>
		<td align="center" width="400">
			<div>
				<s:form action="register_recruiter" id="registration_form">
					<div>
					<s:label><h3 style="text-align: center;">Recruiter Registration Form</h3></s:label>
					<s:hidden name="recruiter_id" value="0"></s:hidden>
					<s:textfield name="recruiter_name" label="*Full Name "></s:textfield>
					<s:textfield name="email_id" label="*Emailid "></s:textfield>
					<s:password name="password" label="*Password "></s:password>
					<s:password name="passwd_conf" label="*ConfirmPassword "></s:password>
					<s:textfield name="address" label="*Address "></s:textfield>
					<s:textfield name="city" label="*City "></s:textfield>
					<s:textfield name="country" label="*Country "></s:textfield>
					<s:textfield name="pin" label="Pin"></s:textfield>
					<s:textfield name="contact_number" label="*Mobile No."></s:textfield>
					
					</div>
					<s:submit value="Register"/><s:reset value="Clear"/>
					<s:actionerror/>
				</s:form>
				</div><!-- END OF REGISTRATION FORM HERE-->
		</td>
		<td style="width:300px"></td>
	</tr>
</table>

</div><!-- END OF CONTENT HERE -->



<jsp:include page="footer.jsp"/>
</body>
</html>