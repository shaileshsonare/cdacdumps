<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Seeker</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />
</head>
<%
String sid=null;
int seeker_id=0;
try
{
	sid=session.getAttribute("seeker_session_id").toString();
}
	catch(Exception e){}
	if(sid==null)
	{
		response.sendRedirect("login_seeker.jsp");
	}
%>
<body id="home">

<jsp:include page="header_menu.jsp"/>

	
<div id="templatemo_main" style="height:400px">


		<%-- Your id id ${seeker_session_id}
		another way <s:property value="#session['seeker_session_id']" />
		 --%>
		Seekers Home<br>
		<a href="show_jobs.jsp">Show Jobs</a>  
		<a href="show_applied_jobs.jsp">Show Applied Jobs</a>
		<a href="resume_upload.jsp">Upload Resume</a>
		<a href="logout.jsp">Logout</a>

		
</div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.jsp"/>
		
</body>
</html>