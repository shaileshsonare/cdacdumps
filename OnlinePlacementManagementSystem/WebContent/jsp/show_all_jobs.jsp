<%@page import="com.pojos.PostJobPojo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.SQLException,java.sql.DriverManager,java.sql.PreparedStatement,java.sql.Connection,java.io.PrintWriter,java.sql.ResultSet,com.connections.DBConnection" %>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Show jobs</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />
</head>
<body id="home">
<jsp:include page="header_menu.jsp"/>

	
<div id="templatemo_main" style="height:400px">


<%

Connection conn;
String sql=null;
PreparedStatement pstmt=null;
ResultSet rs;

try 
{
	
		conn=new DBConnection().getConnection();
	
		sql="select job_id,job_title,company_name,position,location,job_description,eligibility,package,to_char(posting_date,'dd-Mon-yy') posting_date from jobs_tb order by posting_date desc";
		
		pstmt=conn.prepareStatement(sql);
		
		rs=pstmt.executeQuery();
		
		System.out.println(rs);
		
		out.println("<center><h1>List of Jobs Available</h1></center>");
		
		out.println("<table border='1' align='center'><th>Job Id<th>Posting Date<th>Job Title<th>Company Name<th>Position<th>Location<th>Description<th>Eligibility<th>Package");
		
		while(rs.next())
		{
			out.println("<tr><td>"+rs.getString("job_id")+
						"<td>"+rs.getString("posting_date")+
						"<td>"+rs.getString("job_title")+
						"<td>"+rs.getString("company_name")+
						"<td>"+rs.getString("position")+
						"<td>"+rs.getString("location")+
						"<td>"+rs.getString("job_description")+
						"<td>"+rs.getString("eligibility")+
						"<td>"+rs.getString("package")
						);
			
			

		}
		out.println("</table>");
		conn.close();
}catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

%>

 </div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.jsp"/>
</body>
</html>