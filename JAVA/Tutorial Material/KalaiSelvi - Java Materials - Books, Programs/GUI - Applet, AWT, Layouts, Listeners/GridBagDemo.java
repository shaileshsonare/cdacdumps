package layouts.demo;

//<applet code=gbg1.class height=300 width=300></applet>
import java.awt.*;
import java.applet.*;
public class GridBagDemo extends Applet
{
protected void but (String name,GridBagLayout gbg,GridBagConstraints gbc)
{
Button button=new Button(name);
gbg.setConstraints(button,gbc);
add(button);
}
public void init()
{
GridBagLayout gbg=new GridBagLayout();
GridBagConstraints gbc=new GridBagConstraints();
setLayout(gbg);
gbc.fill=GridBagConstraints.BOTH;
gbc.weightx=1.0;
but("one",gbg,gbc);
but("two",gbg,gbc);
but("three",gbg,gbc);
gbc.gridwidth=GridBagConstraints.REMAINDER;
but("four",gbg,gbc);
but("five",gbg,gbc);
gbc.gridwidth=GridBagConstraints.RELATIVE;
but("six",gbg,gbc);
gbc.gridwidth=GridBagConstraints.REMAINDER;
but("seven",gbg,gbc);
gbc.gridheight=2;
gbc.weighty=1.0;
gbc.weightx=0.0;
gbc.gridwidth=GridBagConstraints.RELATIVE;
but("eight",gbg,gbc);
gbc.weighty=0.0;
gbc.weightx=1.0;
gbc.gridwidth=GridBagConstraints.REMAINDER;
gbc.gridheight=1;
but("nine",gbg,gbc);
but("ten",gbg,gbc);
resize(300,150);

}

public static void main(String sam[])
{
Frame f=new Frame("my grid bag layout");
GridBagDemo g1=new GridBagDemo();
g1.init();
f.add("Center",g1);
f.pack();
f.resize(f.preferredSize());
f.setVisible(true);

}

}
