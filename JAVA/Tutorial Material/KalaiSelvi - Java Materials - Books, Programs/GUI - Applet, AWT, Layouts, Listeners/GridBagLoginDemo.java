package layouts.demo;
import java.awt.*;
import java.applet.*;
import java.awt.event.*;
import java.util.Enumeration;
import java.util.Hashtable;

public class GridBagLoginDemo extends Applet implements ActionListener
{
	Label l1,l2,l3;
	TextField t1,t2,t3;
	Button b1,b2,b3;
	int ano;
	int bal;
	String str;
	Hashtable balance = new Hashtable();
	Enumeration bals;
	
	public void init()
	{
		
		balance.put(new Integer(1010),new Double(30000));
		balance.put(new Integer(1011),new Double(40000));
		GridBagLayout gbl=new GridBagLayout();
		GridBagConstraints gbc=new GridBagConstraints();
		bals=balance.keys();
		
		l1=new Label("Account no:");
		l2=new Label("Amount:");
		l3=new Label("Balance");

		t1=new TextField(30);
		t2=new TextField(30);
		t3=new TextField(30);

		b1=new Button("Balance");
		b2=new Button("Deposit");
		b3=new Button("Withdrawal");
		//gbc.fill=GridBagConstraints.BOTH;
		gbc.gridx=10;
		gbc.gridy=10;

		gbl.setConstraints(l1,gbc);
		
		gbc.gridx=30;
		gbc.gridy=10;
	
		gbl.setConstraints(t1,gbc);


		gbc.gridx=10;
		gbc.gridy=20;
		gbl.setConstraints(l2,gbc);
		
		gbc.gridx=30;
		gbc.gridy=20;
		gbl.setConstraints(t2,gbc);
		
		gbc.gridx=10;
		gbc.gridy=30;
		gbl.setConstraints(l3,gbc);
		
		gbc.gridx=30;
		gbc.gridy=30;
		gbl.setConstraints(t3,gbc);
		
		gbc.gridx=10;
		gbc.gridy=40;
		gbl.setConstraints(b1,gbc);
		

		gbc.gridx=30;
		gbc.gridy=40;
		gbl.setConstraints(b2,gbc);

		gbc.gridx=50;
		gbc.gridy=40;
		gbl.setConstraints(b3,gbc);

		setLayout(gbl);
		add(l1);
		add(l2);
		add(l3);
		add(t1);
		add(t2);
		add(t3);
		add(b1);
		add(b2);
		add(b3);
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		
	}


	public void actionPerformed(ActionEvent a)
	{
		if(a.getSource()==b2)
		{
			String s1=t2.getText();
			int num1=Integer.parseInt(s1);
			bal=bal+num1;
			
			t3.setText("Balance= "+bal);
		}
		else if(a.getSource()==b1)
		{
			t3.setText(""+bal);
			//int num2=t1.getInt();
		}
		else if(a.getSource()==b3)
		{
			String s2=t2.getText();
			int num2=Integer.parseInt(s2);
			bal=bal-num2;
			t3.setText(""+bal);
		}
	}
}
