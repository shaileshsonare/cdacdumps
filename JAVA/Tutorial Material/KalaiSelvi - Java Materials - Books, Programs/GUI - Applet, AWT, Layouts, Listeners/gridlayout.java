package layouts.demo;

import java.awt.*;
import java.applet.*;
public class gridlayout extends Applet
{
 GridLayout gl;
 Label l1,l2,l3;
 TextField t1,t2,t3;
 public void init()
 {
   gl=new GridLayout(3,2,10,10);
  
   setLayout(gl);
   l1=new Label("Emp no");
   l2=new Label("Emp Name");
   l3=new Label("Designation");
   t1=new TextField(20);
   t2=new TextField(20);
   t3=new TextField(20);
   add(l1);
   add(t1);
   add(l2);
   add(t2);
   add(l3);
   add(t3);
   }
 }
