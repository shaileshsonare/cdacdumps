import java.applet.Applet;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;



public class FocusAdaptarExample extends Applet // implements FocusListener
{
   Label lblV1, lblV2, lblRes;
   TextField txtV1, txtV2, txtRes;
   
   public void init()
   {
	   lblV1 = new Label("Enter Value 1");
	   lblV2 = new Label("Enter Value 2");
	   lblRes = new Label("Result");
	   txtV1 = new TextField(20);
	   txtV2 = new TextField(20);
	   txtRes = new TextField(20);
	   
   }
   
   public void start()
   {
	   add(lblV1);
	   add(txtV1);
	   add(lblV2);
	   txtV2.addFocusListener(new FocusAdapter() {
		   
		   //implement the methods
		   
		   public void focusLost(FocusEvent fe)
		   {
			   System.out.println("I am in Anonymous Inner Class");
			   
			   //bs logic
			   
			   int i1 = Integer.parseInt(txtV1.getText());
			   int i2 = Integer.parseInt(txtV2.getText());
			   
			   Integer r = i1+i2; //Autoboxing
			   
			   txtRes.setText(r.toString());
			   
		   }
		   
		   
		   
	    });  //end of the statement
	   
	   add(txtV2);
	   add(lblRes);
	   add(txtRes);
	   
   }

/*public void focusGained(FocusEvent arg0) {
	// TODO Auto-generated method stub
	
}

public void focusLost(FocusEvent arg0) {
	// TODO Auto-generated method stub
	
}
*/   
 
}
