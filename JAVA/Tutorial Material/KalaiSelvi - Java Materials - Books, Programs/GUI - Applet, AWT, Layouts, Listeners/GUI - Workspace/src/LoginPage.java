import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Button;
import java.awt.TextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginPage extends Applet implements ActionListener 
{
	
	Label lblUid, lblPwd;
	TextField txtUid, txtPwd;
	Button btnSubmit, btnClear;
	String status;
	
	public void init()
	{
		lblPwd = new Label("Password");
		lblUid = new Label("UserId");
		txtUid = new TextField(20);
		txtPwd = new TextField(20);
		btnSubmit = new Button("Submit");
		btnClear = new Button("Reset");
		status = "Login Here";
		
	}
	
	public void start()
	{
		add(lblUid);
		add(txtUid);
		add(lblPwd);
		add(txtPwd);
		btnSubmit.addActionListener(this);
		add(btnSubmit);
		add(btnClear);
		
		
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		
		if (ae.getSource().equals(btnSubmit))
		{
			String uid = txtUid.getText();
			String pwd = txtPwd.getText();
			
			if (uid.equalsIgnoreCase("dac4") && (pwd.equals("gem")))
			{
				status = "Welcome "+uid;
			}
			else
			{
				status = "Incorrect UserId / Password";
				
			}
			repaint();
		}		
	}
	
	
	
	public void paint(Graphics g)
	{
		g.drawString(status, 100, 100);
	}
	
	

}
