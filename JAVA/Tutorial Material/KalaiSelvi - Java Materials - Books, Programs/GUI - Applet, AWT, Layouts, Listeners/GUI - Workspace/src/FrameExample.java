import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class FrameExample //implements WindowListener
{
	Frame frame = new Frame();
	 void display()
	{
		frame.setTitle("WindowListener");
		frame.setLayout(new FlowLayout());
		frame.add(new Button("I am Here"));
		frame.setSize(700, 700);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
			{
				frame.setVisible(false);
				frame.dispose();
			}
			public void windowOpened(WindowEvent we)
			{
				frame.setTitle("Welcome");
				
			}
			
			public void windowDeiconified(WindowEvent we)
			{
				frame.setTitle("Hello");
				
			}
			
		});
		frame.setVisible(true);
	}
	
	
	public static void main(String[] args) {
		new FrameExample().display();

	}


	/*public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	public void windowClosed(WindowEvent arg0) {
		if (arg0.getSource()==frame)
		{
			frame.dispose();
		}
		
	}


	public void windowClosing(WindowEvent arg0) {
		frame.setVisible(false);
		frame.dispose();
		System.exit(0);
		
	}


	public void windowDeactivated(WindowEvent arg0) {
		System.out.println("drgtfhjkl");
			
			frame.setVisible(false);
			frame.dispose();
			
		
		
	}


	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
*/
}
