package components.demo;
import java.awt.*;
import java.applet.*;

public class paneldemo extends Applet
{
Panel p1,p2;
Label l1,l2,l3;
TextField t1,t2,t3;
Button b1,b2,b3;
TextArea ta;
BorderLayout bl;
GridLayout gl;
public void init()
{
p1=new Panel();
gl=new GridLayout(3,2,10,10);
l1=new Label("Empno");
l2=new Label("Ename");
l3=new Label("Designation");
p1.setLayout(gl);
t1=new TextField(20);
t2=new TextField(20);
t3=new TextField(20);
p1.add(l1);
p1.add(t1);
p1.add(l2);
p1.add(t2);
p1.add(l3);
p1.add(t3);
p2=new Panel();
b1=new Button("add");
b2=new Button("modify");
b3=new Button("delete");
p2.add(b1);
p2.add(b2);
p2.add(b3);
bl=new BorderLayout();
setLayout(bl);
add(p1);
add(p2,"South");
}
}

