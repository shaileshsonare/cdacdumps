package components.demo;

 import java.awt.*;
import java.awt.event.*;
import java.applet.*;
public class itemlis extends Applet implements ItemListener
{
 List l;
 public void init()
 {
  setBackground(Color.green);
  l=new List();
  l.add("white");
  l.add("blue");
  l.add("yellow");
  l.add("cyan");
  add(l);
  l.addItemListener(this);
  }
public void itemStateChanged(ItemEvent ei)
{
 String s=l.getSelectedItem();
 if(s.equals("white"))
 {
  setBackground(Color.white);
  }
 else if(s.equals("blue"))
 {
  setBackground(Color.blue);
  }
 else if(s.equals("yellow"))
 {
  setBackground(Color.yellow);
  }
 else if(s.equals("cyan"))
 {
  setBackground(Color.cyan);
  }
 }
}