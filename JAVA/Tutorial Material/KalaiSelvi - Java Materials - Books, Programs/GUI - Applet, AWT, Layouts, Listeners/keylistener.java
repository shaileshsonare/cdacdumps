package components.demo;

import java.awt.*;
import java.awt.event.*;
import java.applet.*;
public class keylistener extends Frame
{
	Frame f;
	TextField t1,t2;
	public keylistener()
	{
		f = new Frame();
		setSize(300,350);
		setLayout(new FlowLayout());
		t1=new TextField(20);
		t2=new TextField(20);
		t1.requestFocus();
		t2.setEditable(false);
		t1.addKeyListener(new mykey());
		add(t1);add(t2);
		setVisible(true);
		f.addWindowListener(new win());
	}
	class mykey extends KeyAdapter
	{
		public void keyTyped(KeyEvent e)
		{
			char ch = e.getKeyChar();
			if(Character.isDigit(ch))
			{
				t2.setText(t1.getText()+ch);
			}
			else
			{
				Toolkit.getDefaultToolkit().beep();
			}
		}
		public void keyPressed(KeyEvent e)
		{
			int a = e.getKeyCode();
			t2.setText("Code Is :" +a);
		}
	}

	class win extends WindowAdapter
	{
		public void windowClosing(WindowEvent e)
		{
			System.exit(0);
		}
	}

	public static void main(String arg[])
	{
		new keylistener();
	}
}