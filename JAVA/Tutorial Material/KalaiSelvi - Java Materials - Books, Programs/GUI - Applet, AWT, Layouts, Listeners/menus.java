package components.demo;

import java.awt.*;
import java.awt.event.*;
public class menus extends WindowAdapter
{
 Frame f;
 MenuBar mbar;
 Menu file;
 Menu item5;
 MenuItem item1,item2,item3,item4,item6,sub1,sub2;
 Menu edit;
 Menu help;

 public menus()
 {
  f = new Frame(); 
  mbar = new MenuBar();
  file = new Menu("File");
  item1 = new MenuItem("Open...");
  item2 = new MenuItem("Save");
  item3 = new MenuItem("Close");
  item4 = new MenuItem("--------");
  item5 = new Menu("Supopt");
  sub1 = new MenuItem("One");
  sub2 = new MenuItem("Two");
  item6 = new MenuItem("Exit");
  file.add(item1);
  file.add(item2);
  file.add(item3);
  file.add(item4);
  file.add(item5);
  item5.add(sub1);
  item5.add(sub2);

  file.add(item6);

  edit = new Menu("Edit");
  help = new Menu("Help");

  f.setMenuBar(mbar);
  mbar.add(file);
  mbar.add(edit);
  mbar.add(help);
  f.setSize(300,300);
  f.addWindowListener(this);
  f.setVisible(true);
  }

public void windowClosing(WindowEvent e)
{
System.exit(0);
}
/*public void windowClosed(WindowEvent e)
{}
public void windowOpened(WindowEvent e)
{}
public void windowIconified(WindowEvent e)
{}
public void windowDeiconified(WindowEvent e)
{}
public void windowActivated(WindowEvent e)
{}
public void windowDeactivated(WindowEvent e)
{}*/

 public static void main(String s[])
 {
  new menus();
  }
}