package components.demo;

import java.awt.*;
public class mnusample extends Frame {

	public static void main(String[] args) {
    Frame f=new Frame("Editor");
	MenuBar mb=new MenuBar ();
	Menu mfile=new Menu("File");
	Menu medit=new Menu("Edit");
	Menu mhelp=new Menu("Help");
	Menu mexit=new Menu("Exit");
	MenuItem minew=new MenuItem("New");
	MenuItem miopen=new MenuItem("Open");
	MenuItem misave=new MenuItem("Save");
	MenuItem miexit=new MenuItem("Exit");
	mfile.add(minew);
	mfile.add(miopen);
	mfile.add(misave);
	mfile.add(miexit);
	mb.add(mfile);
	mb.add(medit);
	mb.add(mhelp);
	mb.add(mexit);
	f.setMenuBar(mb);
	f.setSize(500,500);
	f.setVisible(true);
}
}
