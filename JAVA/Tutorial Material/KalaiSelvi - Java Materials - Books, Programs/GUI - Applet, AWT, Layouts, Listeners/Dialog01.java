package components.demo;
import java.awt.*;
import java.awt.event.*;
//=======================================================//

public class Dialog01 extends Frame{
  public static void main(String[] args){
    new Dialog01();//instantiate an object of this type
  }//end main

  public Dialog01(){//constructor
    setSize(250,150);
    setVisible(true);
     Dialog myDialog = new Dialog(this,"Dialog");
    myDialog.setSize(125,75);
    myDialog.show();//make the Dialog object appear
  }//end constructor
  
}//end class Dialog01
