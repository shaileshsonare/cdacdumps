import java.awt.*;
import java.applet.*;

public class Face extends Applet
{
  Color c1;
  public void init()
  {
    c1 = new Color(0,134,134);
  }

  public void paint(Graphics g)
  {
    g.setColor(Color.black);
    g.drawOval(100,100,180,180);
    g.setColor(c1);
    g.fillOval(100,100,180,180);
    g.setColor(Color.black);
    g.drawArc(120,140,50,20,0,180);
    g.drawArc(120,140,50,20,0,-180);
    g.drawArc(200,140,50,20,0,180);
    g.drawArc(200,140,50,20,0,-180);
    g.drawArc(160,220,50,20,0,-180);
    g.drawLine(185,165,195,215);
    g.drawLine(185,165,175,215);
    g.drawLine(175,215,195,215);
/*    g.setColor(Color.white);
    g.fillArc(120,140,50,20,0,180);
    g.fillArc(120,140,50,20,0,-180);
    g.setColor(Color.white);
    g.fillArc(200,140,50,20,0,180);
    g.fillArc(200,140,50,20,0,-180); */
    g.setColor(Color.black);
    g.drawOval(215,140,20,20);
    g.fillOval(215,140,20,20);
    g.drawOval(135,140,20,20);
    g.fillOval(135,140,20,20);
  }
}


