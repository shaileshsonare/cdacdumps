/*<APPLET CODE = "AppletLifeCycle.class" height =500 width = 500></APPLET>*/
import java.applet.Applet;
import java.awt.Graphics;

public class AppletLifeCycle extends Applet
{
     String str = "Hello !";
    public void init()
 {
    str = str + "<init()>";
}
 public void start()
 {
    str = str + "<start()>";
}
/* public void stop()
 {
    str = str + "<stop()>";
}
 public void destroy()
 {
   str = str + "<destroy()>";
}*/
public void paint(Graphics g)
{
   
    str = str + "<paint()>";
    g.drawString(str,30,30);

}
}