import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;

public class GenericWildUpper
{
    public static void printList (List<?> list)
    {
        for (Object element : list) {
            System.out.println(element);
        }
    }
    
    public static void main(String[] args) {
        List<String> list1 = new ArrayList<String>();

        list1.add ("Hello");
        list1.add ("World");
        printList (list1);

        List<Integer> list2 = new LinkedList<Integer>();
        list2.add(100);
        list2.add(200);
        printList(list2);
    }
}