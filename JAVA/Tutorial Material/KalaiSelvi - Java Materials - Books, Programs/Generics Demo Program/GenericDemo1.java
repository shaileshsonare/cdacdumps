//Generic for type safety
class Gen<T>
{
	T obj;
	
	Gen(T ob)
	{
	  obj=ob;	
	}
	
	T getObj()
	{
		return obj;
	}
	
	void showType()
	{
		System.out.println("Type of T : "+obj.getClass().getName());
	}
	
}

public class GenericDemo1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Gen<Integer> iobj;
		
		iobj = new Gen<Integer>(100);
		
		iobj.showType();
		
		int val = iobj.getObj();
		
		System.out.println("Value : "+val);
		
        Gen<String> sobj = new Gen<String>("Hello");
		
        sobj.showType();
        
        String str = sobj.getObj();
        
        System.out.println("Value : "+str);
		      
        //The process of removing generic type is called erasure
        Gen g = new Gen(12);
        
        g.showType();
        
        System.out.println(g.getObj());
        
        //iobj = new Gen<Double>(45.00);
        
        //Generics work only with objects
      
        //Gen<int> intv = new Gen<int>(90);
        
        Gen obj = sobj; //compiles, but conceptually wrong
        
        int v = (Integer)obj.getObj();//ClassCastException - runtime
		System.out.println(v);

	}

}
