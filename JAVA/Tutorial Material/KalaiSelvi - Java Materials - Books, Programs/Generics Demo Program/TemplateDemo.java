class Account
{
	String obj;
	
	Account(String s)
	{
		obj =s;
	}
}

class Template<T>
{
	T tobj; //instance var
	
	Template(T tobj)
	{
		this.tobj=tobj;
	}
	
	public T getData()
	{
		return tobj;
	}
}

public class TemplateDemo {

	
	public static void main(String[] args) {
		//Using Integer Object
		Template<Integer> obj = new Template<Integer>(123);
		System.out.println("Data : "+obj.getData());
		
		//Using String Object
		Template<String> sobj = new Template<String>("abc");
		System.out.println("String : "+sobj.getData());
		
		//Using Account -user defined Object
		Template<Account> accobj = new Template<Account>(new Account("1001"));
		
		Account acc = accobj.tobj;//returning userdefined object
		//System.out.println(accobj.getData());
		System.out.println("Account Data : "+acc.obj);
		
		//erasure
		Template tt1 =new Template(new String("1212"));
		Template tt2 =new Template(1212);
	}

}
