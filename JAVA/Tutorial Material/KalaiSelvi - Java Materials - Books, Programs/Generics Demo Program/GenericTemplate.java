

public class GenericTemplate<T, X> {
	T one;
	X two;

	GenericTemplate(T one, X two) {
		this.one = one;
		this.two = two;
	}

	T getT() {
		return one;
	}

	X getX() {
		return two;
	}

	public static void main(String[] args) {
		GenericTemplate<String, Integer> twos = 
			new GenericTemplate<String, Integer>("java", 42);
		String theT = twos.getT();
		int theX = twos.getX();
		
		System.out.println(theX);
	}
}