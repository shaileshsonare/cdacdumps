public class GenericsMethods { 
	static <T> void printType(T anyType) 
	{ 
		System.out.println(anyType.getClass().getName()); 
	}
	 public static void main(String[] args) { 
		GenericsMethods.printType(String.class);
		 GenericsMethods.printType(new String(""));
		 
	 } 
} 
