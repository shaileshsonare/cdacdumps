class Generics<T,V>
{
	T o1;
	V o2;
	
	Generics(T o1,V o2)
	{
	  this.o1=o1;
	  this.o2=o2;
	}
	
	T getO1()
	{
		return o1;
	}
	
	V getO2()
	{
		return o2;
	}
	
	void showType()
	{
		System.out.println("Type of T : "+o1.getClass().getName());
		System.out.println("Type of T : "+o1.getClass().getName());
	}
	
}

public class GenericDemo2TwoTypeParameters {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
