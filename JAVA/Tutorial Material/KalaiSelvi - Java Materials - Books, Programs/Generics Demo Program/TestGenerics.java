
//creating generics class
public class TestGenerics<T> { // as the class type
	T anInstance; // as an instance variable type
	T [ ] anArrayOfTs; // as an array type
	TestGenerics(T anInstance){ // as an argument type		this.anInstance = anInstance;
   }
   T getT( ) { // as a return type
	return anInstance;
}
}


//making generics method
public class CreateAnArrayList {
	public <T> void makeArrayList(T t) { // take an object
					// of an unknown type and use a
					// "T" to represent the type
		List<T> list = new ArrayList<T>(); // now we can 					// create the list using "T"
		list.add(t);
	}
}
