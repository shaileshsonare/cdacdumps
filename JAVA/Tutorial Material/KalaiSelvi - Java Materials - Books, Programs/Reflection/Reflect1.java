package com.exs;

import java.lang.reflect.*;

class CModifiers
{
	public void m1(){}
	private void m2(){}
	protected void m3(){}
}

public class Reflect {

	public static void main(String[] args) {

		try{
		
			Class cls=Class.forName("java.lang.Object");
			System.out.println("Constructors, Methods, Fields in java.Object Class \n");
			System.out.println("\n\t\tConstructors");
			
			Constructor c[]=cls.getConstructors() ;
			
			for(int i=0;i<c.length; i++)
				System.out.println(c[i]);
			
				Method m[]=cls.getMethods() ;
				System.out.println("\n\t\tMethods");
			
				for(int j=0;j<m.length; j++)
					System.out.println(m[j]);
				
					Field f[]=cls.getFields() ;
					System.out.println("\n\t\tFields");
					
					for(int k=0;k<f.length; k++)
						System.out.println(f[k]);

			CModifiers cm=new CModifiers();
			Class Ccls=cm.getClass();
			Constructor cc[] = Ccls.getConstructors();
			System.out.println("\n Total Constructors : "+cc.length);
			Method clsm[]=Ccls.getDeclaredMethods();

			System.out.println("\n\t\tMethods of Userdefined Class - CModifiers");
				
			for(int p=0;p<clsm.length; p++)
					System.out.println(clsm[p]);
					System.out.println("\n\t\tModifiers - Methods of Class CModifiers");
					
					for(int a=0;a<clsm.length; a++){
						int modifiers = clsm[a].getModifiers();
						
						if (Modifier.isPublic(modifiers))
						System.out.println("\n\tPublic Method : "+clsm[a].getName());
							
						if (Modifier.isPrivate(modifiers))
								System.out.println("\n\tPrivate Method : "+clsm[a].getName());
								if (Modifier.isProtected(modifiers))
									System.out.println("\n\tProtected Method : "+clsm[a].getName());
				}
			
		}catch(Exception e){System.out.println("Error");}
	}
}


