/*
 * Created on Dec 25, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author Acer
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
import java.lang.reflect.*;
class CModifiers
{public void m1(){}
private void m2(){}
protected void m3(){}
}

public class Reflect {

	public static void main(String[] args) {
		try{
			Class cls=Class.forName("java.awt.Dimension");
			System.out.println("Constructors, Methods, Fields in awt.Dimension Class \n");
			System.out.println("Constructors ");
			Constructor c[]=cls.getConstructors() ;
			for(int i=0;i<c.length; i++)
				System.out.println(c[i]);
			Method m[]=cls.getMethods() ;
			System.out.println("Methods ");
			for(int j=0;j<m.length; j++)
				System.out.println(m[j]);
			Field f[]=cls.getFields() ;
			System.out.println("Fields ");
			for(int k=0;k<f.length; k++)
				System.out.println(f[k]);

			CModifiers cm=new CModifiers();
			Class Ccls=cm.getClass();
			Method clsm[]=Ccls.getDeclaredMethods();
			System.out.println("Methods of Class CModifiers");
			for(int p=0;p<clsm.length; p++)
			System.out.println(clsm[p]);
			System.out.println("Modifiers - Methods of Class CModifiers");
			for(int a=0;a<clsm.length; a++){
				int modifiers = clsm[a].getModifiers();
				if (Modifier.isPublic(modifiers))
					System.out.println("Public Method "+clsm[a].getName());
				if (Modifier.isPrivate(modifiers))
						System.out.println("Private Method "+clsm[a].getName());
				if (Modifier.isProtected(modifiers))
							System.out.println("Protected Method "+clsm[a].getName());
				}
			
		}catch(Exception e){System.out.println("Error");}
	}
}


