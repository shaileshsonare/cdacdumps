package enmpkg;

public class CoinTest {

    public static void main(String[] args) {
        for (Coin c : Coin.values())
            System.out.println(c + ":   \t" + c.value() +"� \t" + color(c));
    }

    private enum CoinColor { copper, nickel, silver }

    private static CoinColor color(Coin c) {
    	//System.out.println(c);
        switch(c) {
          case penny: return CoinColor.copper;
          case nickel: return CoinColor.nickel;
          case dime: // deliberate fall through to next case
          case quarter:  return CoinColor.silver;
          default: assert false:"Invalid Data"; return CoinColor.copper; 
        	  //throw new AssertionError("Unknown Coin: " + c); 
        }
        
    }
}
