package enmpkg;

public enum Coin{
 	penny(1), nickel(5), dime(10), quarter(25), copper(23); // values
 	Coin(int value) { this.value = value; }          // constructor
 	private final int value;                               // instance variable
 	public int value() { return value; }             // method
 	}