class Resource
{

	int n;
	boolean value=false;

	synchronized int consume()
	{
		if(!value)
		try
		{
		wait();
		}
		catch(InterruptedException e)
		{
		System.out.println("interrupted exception has accured");
		}
		
		System.out.println("Got:"+n);
		value=false;
		notify();
		return n;
	}
	synchronized void produce(int m)
	{
		if(value)
		try
		{
		wait();
		}
		catch(InterruptedException e)
		{
		System.out.println("interrupted exception has accured");
		}
		n=m;
		value=true;
		System.out.println("Put :"+n);
		notify();
	}
}
class Producer implements Runnable{

	Resource resource;
	Producer(Resource resource)
	{
		this.resource=resource;
		new Thread(this, "Producer").start();
	}
	public void run()
	{
		int i=0;
		while(true)
		{
			resource.produce(i++);
		}
	}
}

class Consumer implements Runnable
{
	Resource resource;
		
	Consumer(Resource resource)
	{
		this.resource = resource;
		new Thread(this, "Consumer").start();
	}
	public void run()
	{
		while(true)
		{
			resource.consume();
		}
	}
}

public class ProducerConsumerDemo
{
	public static void main(String[] arg){
		Resource resource=new Resource();
		new Producer(resource);
		new Consumer(resource);
		System.out.println("press Ctrl+C to quit");
	}
}

	

