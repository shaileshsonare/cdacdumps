class UserThread implements Runnable 
{
   Thread t;

UserThread()
{
   t = new Thread(this,"User Thread");
   System.out.println("Child Thread: "+t);
   t.start();
}

public void run() {
   try {
	 for(int i=5; i>0; i--)
	  {
            System.out.println("Child Thread: "+i);
	    Thread.sleep(500);
          }
       }

       catch(InterruptedException e)
	  { 
            System.out.println("Child Interrupted");
          }
            System.out.println("Exiting Child Thread");
  }
  }

  class Imprun 
  {
    public static void main(String args[])
    {
      new UserThread();
      try
	{
	 for(int i=5; i>0; i--)
	  {
            System.out.println("Main Thread: "+i);
	    Thread.sleep(1000);
          }
	}  catch(InterruptedException e)
	   {
	     System.out.println("Main thread interrupted");
           }
	     System.out.println("Main thread exiting");
}
}




