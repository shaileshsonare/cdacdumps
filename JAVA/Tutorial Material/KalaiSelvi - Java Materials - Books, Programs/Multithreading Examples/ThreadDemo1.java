class Tread implements Runnable
{
String name;
Thread t;

Tread(String title)
 {
 name=title;
 t=new Thread(this,name);
 System.out.println("New Thread " +t);
 t.start();
 }

public void run()
{
try
 {
 //t.resume();
 for(int i=5;i>0;i--)
 {
 System.out.println(name+" "+i);
 //t.interrupt();
Thread.sleep(1000);
 //Thread.yield();
 }
 }
 catch(Exception e)
 {
 System.out.println(name+"Interrupted");
 }
 System.out.println(name+"exiting");
 }
}

class ThreadDemo1
{
 public static void main(String args[])
  {
   Tread t1=new Tread("First");
   Tread t2=new Tread("Second");
   Tread t3=new Tread("Third");
   System.out.println("First alive is "+t1.t.isAlive());
   System.out.println("Second alive is "+t2.t.isAlive());
   System.out.println("Third alive is "+t3.t.isAlive());
    try
     {
      System.out.println("Waiting for Threads to finish.....");
      t1.t.join();
     }
     catch(InterruptedException e)
     {
     System.out.println("Main thread interrupted..");
     }
   System.out.println("First alive is "+t1.t.isAlive());
   System.out.println("Second alive is "+t2.t.isAlive());
   System.out.println("Third alive is "+t3.t.isAlive());
    System.out.println("Main thread Exiting..");
  }
}
