class NewThread implements Runnable
{
	Thread t;
	NewThread()
	{
		t = new Thread(this,"ChildThread");
		System.out.println("Chile Thread:" +t);
		t.start();
	}
	public void run()
	{
		   System.out.println("Child Thread Started");
		   System.out.println("Exiting the Child Thread");
		   
	}
}
public class ThreadClass {

		public static void main(String[] args) 
		{
			new NewThread();
			System.out.println("Main Thread Started");
			try
			{
				Thread.sleep(5000);
			}
			catch (Exception e)
			{
				System.out.println("Main Thread Interrupted");
				System.out.println("Exiting Main Thread");
				   
				   
			}
			   

	    }

}
