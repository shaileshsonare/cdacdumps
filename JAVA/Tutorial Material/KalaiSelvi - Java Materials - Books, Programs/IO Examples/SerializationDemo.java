import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;


public class SerializationDemo
{
	public void serialize()
	{
	        Product proobj = new Product();
	        
		    Scanner s = new Scanner(System.in);
			System.out.println("Enter the Product Details");
			System.out.println("Product Id : ");
			proobj.pid = s.nextInt();
			System.out.println("Product Name: ");
			proobj.pname = s.next();
			System.out.println("Product Quantity : ");
			proobj.qty = s.nextInt();
			System.out.println("Product Price : ");
			proobj.price = s.nextFloat();
			System.out.println("Before Serialization..........");
			System.out.println("Product Id :"+proobj.pid);
		    //Writing the product object into file	
			System.out.println("Serialization Process...............");
			try
			{
				File f = new File("Product.data");
				FileOutputStream fos = new FileOutputStream(f);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(proobj);
				oos.close();
				fos.close();
				 
			}catch(FileNotFoundException fn)
			{
				System.out.println("File Not Found");
			}
			catch(IOException ioe)
			{
				System.out.println(ioe.getMessage());
			}
	}
	
	public void deserialize()
	{
		Product pro;
		try
		{
			FileInputStream fis = new FileInputStream(new File("product.data"));
			ObjectInputStream ois = new ObjectInputStream(fis);
			pro = (Product)ois.readObject();
			System.out.println("Deserialization Process...............");
			System.out.println(pro);
			System.out.println("Product Id : "+pro.pid);
			 
		}catch(FileNotFoundException fn)
		{
			System.out.println("File Not Found");
		}
		catch(ClassNotFoundException cn)
		{
			System.out.println(cn.getMessage());
		}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
		
	}
	
	public static void main(String[] args) {
	    SerializationDemo obj = new SerializationDemo();
	    obj.serialize();
	    obj.deserialize();
	    
	}

}
