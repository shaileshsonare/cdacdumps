import java.io.FileInputStream;
import java.io.FileOutputStream;

public class LowLevelStreamDemo {

	public static void main(String[] args) {

		try{
			String str ="he is good boy\n welcome to all";
			FileOutputStream fos=new FileOutputStream("vamlow.txt");
			fos.write(str.getBytes());
			System.out.println("file created successfully");
		}catch(Exception ex){
			
		}
		
		try{
			byte b;
			byte bytes[] = new byte[100];
			
			FileInputStream fis = new FileInputStream("vamlow.txt");
			b = (byte) fis.read(); // Single byte
			//fis.read(bytes); // Fill the array
			for(int i = 0; i<30; i++){
				//System.out.print((char)b[i]);
				System.out.print((char)b);
				b = (byte) fis.read();
			}
			System.out.println();
			fis.close();
			
		}catch(Exception ex1){
			
		}
	}

}
