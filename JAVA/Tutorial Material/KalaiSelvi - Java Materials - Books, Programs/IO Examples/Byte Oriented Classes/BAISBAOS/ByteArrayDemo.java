
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ByteArrayDemo 
{

	public static void main(String[] args) throws IOException 
	{
		ByteArrayOutputStream  d = new ByteArrayOutputStream( );
		String s = "hello how are u";
		byte buf[] = s.getBytes();
		d.write(buf);
		System.out.println(d);
		System.out.println("in to bytearray");
		byte c[] = d.toByteArray();
		for(int i = 0;i<c.length;i++)
		{
			System.out.print((char)c[i]);
		}
		FileOutputStream  f=  new FileOutputStream("b5.txt");
		d.writeTo(f);
		f.close();
	}

}
