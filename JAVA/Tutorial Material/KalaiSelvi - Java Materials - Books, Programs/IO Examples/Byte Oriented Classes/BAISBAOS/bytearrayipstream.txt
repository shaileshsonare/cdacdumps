import java.io.*;
public class ByteArrayInputStreamDemo {
	
	public static void main(String[] args) throws FileNotFoundException,IOException
	{
		//byte buffer[]=new byte[100];
	File f=new File("sample");
	FileInputStream fi=new FileInputStream(f);
	int n=(int)f.length();
	
	byte buffer[]=new byte[n];
	fi.read(buffer);
	ByteArrayInputStream fis=new ByteArrayInputStream(buffer);
	int i;
	while ((i=fis.read())!=-1)
			System.out.print((char)i);
	        fis.reset();
	while(fis.available()>0){
		System.out.print((char)fis.read());
	}
	fis.reset();
	}
	}
