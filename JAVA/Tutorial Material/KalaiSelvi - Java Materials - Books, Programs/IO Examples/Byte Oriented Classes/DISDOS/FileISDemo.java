package Byte.file.stream;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class FileReaderDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try
		{
			FileInputStream fis = new FileInputStream("vam.txt");
			BufferedInputStream bis = new BufferedInputStream(fis);
			DataInputStream dis =new DataInputStream(fis);
			
			System.out.println(dis.readInt());
			System.out.println(dis.readChar());
			System.out.println(dis.readDouble());
			dis.close();
		}
		   catch(IOException e)
	                	{
				System.out.println(e);
			}
	}

}
