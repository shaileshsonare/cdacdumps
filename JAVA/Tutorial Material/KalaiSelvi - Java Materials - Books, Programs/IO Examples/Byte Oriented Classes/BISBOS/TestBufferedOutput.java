import java.io.*;

public class TestBufferedOutput
{
    public static void main(String args[])
     {
         String str = "Creating a program using the BufferedOutputStream. ";
         byte buffer[] = str.getBytes();
         BufferedOutputStream br = new BufferedInputStream(System.out);
         try
         {
	br.write(buffer,0,50);	
	br.flush();

         }
         catch(IOException ioe)
          {
             System.out.println("IOException: "+ioe.toString());
           }
          
        }
}
	
                       