import java.io.FileInputStream;
import java.io.FileOutputStream;

public class LowLevelStreamDemo {

	public static void main(String[] args) {

		try{
			String str ="he is good boy\n welcome to all";
			FileOutputStream fos=new FileOutputStream("vamlow.txt");
			fos.write(str.getBytes());
			System.out.println("file created successfully");
		}catch(Exception ex){
			
		}
		
		try{
			byte b;
			byte bytes[] = new byte[100];
			
			FileInputStream fis = new FileInputStream("vamlow.txt");
			b = (byte) fis.read(); // Single byte
			fis.read(bytes); // Fill the array
			for(int i = 0; i< 5; i++){
				System.out.print((char)bytes[i]);
			}
			System.out.println();
			fis.close();
			System.out.println((char)b);
			
		}catch(Exception ex1){			
		}
	}
}