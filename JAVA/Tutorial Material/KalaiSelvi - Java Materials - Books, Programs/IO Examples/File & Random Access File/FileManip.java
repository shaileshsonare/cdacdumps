import mypack.FileOp;
class FileManip
{
 public static void main(String args[])
  {

   FileOp f= new FileOp("FILE MANIPULATION");
   f.setBounds(100,100,600,600);
   f.setVisible(true);
  }
}


interface FilenameFilter {
	boolean accept(File dir, String name);
	}