import java.io.*;
public class RandomAccessFileUsage {


	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		try
		{
			RandomAccessFile raf=new RandomAccessFile("myfile.dat","rw");
			DataInputStream dis=null;
			BufferedInputStream bis=new BufferedInputStream(System.in);
			dis=new DataInputStream(bis);
			String myName="";
			int myAge=0;
			System.out.print("Please enter your name : ");
			myName=dis.readLine().toString();
			System.out.print("Please enter your age : ");
			myAge=new Integer(dis.readLine()).intValue();

			raf.writeBytes(myName + "\n");
			raf.writeInt(myAge);
			raf.close();
			raf=new RandomAccessFile("myfile.dat","r");
			String accessedName="";
			accessedName = raf.readLine();


			int accessedAge=raf.readInt();
			raf.close();
			System.out.println("Your Name is " + accessedName);
			System.out.println("Your age is " + Integer.toString(accessedAge));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		

	}

}
