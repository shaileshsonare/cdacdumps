import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StreamTokenizer;


public class StreamTokens {

	public static void main(String[] args) {
        FileInputStream fis;
		try {
			fis = new FileInputStream(new File("E:/Java Materials/Core, Advanced, JEE/Advanced Java - PPTS, Programs/Input Output/Examples/InputOutput/src/ex.txt"));
		
		StreamTokenizer tokens = new StreamTokenizer(fis);
		int res = tokens.nextToken();
		
		while(tokens.TT_EOF!=-1)
		{
			res = tokens.nextToken();
			System.out.println(res);
		}
		
		
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		catch(IOException ioe)
		{
			
		}

	}

}
