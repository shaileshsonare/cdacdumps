import java.io.*;
public class FileReading {

	public static void main(String[] args) {
		
		try
		{
			DataInputStream dis = null;
			File f=new File("divisibleByFive.java");
			FileInputStream fis=new FileInputStream(f);
			BufferedInputStream bis=new BufferedInputStream(fis);
			LineNumberInputStream lnis=new LineNumberInputStream(bis);
			dis=new DataInputStream(lnis);
			String record="";
			
			while((record=dis.readLine())!=null)
			{
				System.out.print(lnis.getLineNumber());
				System.out.println(record);
			}
				
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}


	}

}
