import java.io.*;

public class TestBufferedInput
{
    public static void main(String args[])
     {
         //Allocates a buffer of 100 bytes
         byte buffer[] = new byte[100];
         BufferedInputStream bis = new BufferedInputStream(System.in);
         try
         {
	bis.read(buffer,0,50);	

         }
         catch(Exception e)
          {
             System.out.println("Exception: "+e.toString());
           }
           String str = new String(buffer);
            System.out.println(str);
        }
}
	
                       