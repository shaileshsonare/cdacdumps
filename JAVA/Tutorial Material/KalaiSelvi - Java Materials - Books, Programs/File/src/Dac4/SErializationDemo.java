package Dac4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SErializationDemo {
	
	 int eid;
	String name;
	String desgn;
	File file = new File("abc.txt");
	BufferedReader br=null;
	FileOutputStream fos=null;
	ObjectOutputStream oos=null;
	FileInputStream fis = null;
	ObjectInputStream ois = null;
	
	
	public void serialized() throws FileNotFoundException,IOException
	{
		
		br=new BufferedReader(new InputStreamReader(System.in));
		fos=new FileOutputStream(file);
	

		EmployeeData e;
		
		System.out.println("Enter Employee Details :");
		System.out.println("Enter Employee id :");
		eid=Integer.parseInt(br.readLine());
		System.out.println("Enter Employee Name :");
		name=br.readLine();
		System.out.println("Enter Employee Designation :");
		desgn=br.readLine();
		
		oos=new ObjectOutputStream(fos);
		
		e=new EmployeeData(eid, name, desgn);
		oos.writeObject(e);
		System.out.println("Serialization Succeed. .!!");
		
	}
	public void Deserialization() throws IOException, FileNotFoundException
	{
		fis= new FileInputStream(file);
		ois = new ObjectInputStream(fis);
		System.out.println("employee Id is:"+eid);
		System.out.println("employee Name is:"+name);
		System.out.println("employee Designation is:"+desgn);
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SErializationDemo s = new SErializationDemo();
		try {
			s.serialized();
			s.Deserialization();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
