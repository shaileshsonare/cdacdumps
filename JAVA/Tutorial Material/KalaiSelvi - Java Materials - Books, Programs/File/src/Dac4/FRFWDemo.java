package Dac4; 

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;

public class FRFWDemo {

	File file = new File("Test.txt");
	
	
	public void Write()
	{
		
		FileWriter fw = null;
		char arr[] = {'H','E','L','L','O'};
		//String data = "Java Session";
		try
		{
			fw = new FileWriter(file);
			fw.write(101);
			//fw.write(arr);
			fw.write(arr,0,arr.length-2);
			fw.write("To");
		
			
		} catch(IOException e)
		{
		    e.printStackTrace();	
		}
		finally
		  {
			try 
			{
				fw.close();
			}catch(IOException e)
			{
				e.printStackTrace();
			}
			
		  }
	}
	
		public void Read() throws IOException
		{
			FileReader fr = null;
			
			try{
				fr = new FileReader(file);
				int ch;
				while((ch = fr.read())!= -1)
				{
					System.out.println((char)ch);
				}
			}catch(FileNotFoundException e)
			{
				e.printStackTrace();
			}
			finally
			  {
				try 
				{
					fr.close();
				}catch(IOException e)
				{
					e.printStackTrace();
				}
				
			  }
     	}
	  
	
	public static void main(String[] args) throws IOException {
		FRFWDemo obj = new  FRFWDemo();
		obj.Write();
		obj.Read();
		

	}

}
