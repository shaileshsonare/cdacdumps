import java.io.IOException;
import java.sql.SQLException;

 class SuperClass 
      {
public int doIt(String str, Integer... data)throws SQLException{
String signature = "(String, Integer[])";
System.out.println(str + " " + signature);
return 1;
                                          }
}

class SubClass extends SuperClass{
public int doIt(String str, Integer... data)
{
   String signature = "(String, Integer[])";
   System.out.println("Overridden: " + str + " " +signature);
   return 0;
}
public static void main(String... args)
{
SuperClass sb1 = new SuperClass();
SuperClass sb2 = new SuperClass();

   if (sb1==sb2)
	   System.out.println("eq"+sb1.hashCode());
   else
	   System.out.println("unequal"+sb2.hashCode());
   
	//sb1.doIt("hello", 3);

}
}
