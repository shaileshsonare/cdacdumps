import java.util.*;

class Employee {

	String empname;
	int empno;

	Employee( int eno, String ename )
	{
		empno=eno;
		empname=ename;
	}


public String  toString()
{
	return empno+"  "+empname;
}

}

class Book {

	private String bookName;
	private String authorName;

	public  Book(String bookName,String authorName)
	{
		this.bookName= bookName;
		this.authorName=authorName;
	}


	public String toString()
	{
		return bookName +" "+authorName;
	}

}

public class HetroArrayList {


	public static void main(String args[])
	{
		ArrayList alist = new ArrayList();


		Employee e1 = new Employee(100,"ram");
		Employee e2 = new Employee(101,"kesav");
		alist.add(e1);
		alist.add(e2);

		Book b1 = new Book("Java","gosling");
		Book b2 = new Book("J2EE","kathysieera");

		alist.add(b1);
		alist.add(b2);

		System.out.println(alist);

	}

}
