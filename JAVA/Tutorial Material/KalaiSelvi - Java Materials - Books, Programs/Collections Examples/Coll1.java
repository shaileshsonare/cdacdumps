import java.util.*;
class Student {
	  String studentName;
	  Student() { }
	  Student(String studentName) {
	    this.studentName = studentName;
	  }

	  public String toString() {
	    return this.studentName;
	  }
	  
	  public void sampleMap(){
		    TreeMap tm = new TreeMap();
		    tm.put("a","Hello");
		    tm.put("b","Java");
		    tm.put("c","World");
		    Iterator it = tm.keySet().iterator();
		    while(it.hasNext()){
		      System.out.print("sample Method content  :"+it.next());
		    }
		}
	}

public class Coll1 {
	
	
  public static void main(String[] args) {
    Set h = new HashSet();
    h.add("One");
    h.add("Two");
    h.add("Three");
    h.add("Four");
    h.add("One");
    h.add("Four");
    System.out.println("Size:" + h.size());
    List l = new ArrayList();

    l.add("One");
    l.add("Two");
    l.add("Three");

    h.retainAll(l);

    System.out.println("Size:" + l.size() + h.size());
    
    
    Vector col = new Vector();
    col.add(new Integer(1));
    col.add(new Integer("2"));
    col.add(new Float(3.2d));
    col.add(col.elementAt(1));
    col.setElementAt(col.elementAt(2),0);
    System.out.println(col);
    
    
   /* TreeSet students = new TreeSet();
    students.add(new Student("Raju"));
    students.add(new Student("Krishna"));
    students.add(new Student("Vijay"));
    System.out.println(students);*/
    
    String numbers[] = { "One", "Two", "Three", "Four", "Five" };    
    System.out.println(Arrays.asList(numbers));
    
    Double scores[] = {new Double(59.4), new Double(74.3), new Double(84.9), new Double(93.4), new Double(67.8), 
    		new Double(54.9), new Double(66.0), new Double(83.2), new Double(95.3), new Double(79.7) };
   // List<Double> l;
    Arrays.asList(scores);

    Collections.sort(l);
    for(int i=l.size()-1; i>l.size()-4; i--)
    	System.out.print(l.get(i) + " ");  
    
    Student obj=new Student();
    obj.sampleMap();

  }
  
  }
