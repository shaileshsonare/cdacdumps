import java.util.*;
public class ListTest {

   // Statics
   public static void main( String [] args ) {
	      System.out.println( "List Test" );

      // Create a collection
      ArrayList list = new ArrayList();

      // Adding
      String [] toys = { "Shoe", "Ball", "Frisbee" };
      list.addAll( Arrays.toList( toys ) );

      // Sizing
      System.out.println( "List created" + 
        ", size=" + list.size() + 
        ", isEmpty=" + list.isEmpty() );

      // Iteration using indexes.
      System.out.println( "List iteration (unsorted):" );
      for ( int i = 0; i < list.size(); i++ ) 
         System.out.println( "   " + list.get( i ) );

      // Reverse Iteration using ListIterator
      System.out.println( "List iteration (reverse):" );
      ListIterator iterator = list.listIterator( list.size() );
      while ( iterator.hasPrevious() ) 
         System.out.println( "   " + iterator.previous() );

      // Removing
      list.remove( 0 );
      list.clear();
   }
}

