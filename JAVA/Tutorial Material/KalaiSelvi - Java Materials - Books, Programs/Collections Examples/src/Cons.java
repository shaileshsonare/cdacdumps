
public class Cons {
	int i;
	static int s;
	static 
	{
		System.out.println("s="+s);
	}
	Cons(int i)
	{  this(); //default curr obj cons
		this.i=i;
		System.out.println("a ="+this.i);
		this.callMe();
	}
	
	Cons()
	
	{   //this(12); //first stmt only
		i=100;
		System.out.println("i ="+i);
	}
	
	public void callMe()
	{
		System.out.println("CallMe");
	}
  public static void main(String a[])
  {   int l=10;
      System.out.println(l);
	  Cons cobj=new Cons(12);
	  cobj.i=200;
	  System.out.println(cobj.i);
	  System.out.println(s);
  }
	
	
}
