import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HashMapDemo {
  public static void main(String args[]) {

  
	  HashMap hm = new HashMap();

	    hm.put("A", new Double(3434.34));
	    hm.put("B", new Double(123.22));
	    hm.put("C", new Double(1378.00));
	    hm.put("D", new Double(99.22));
	    hm.put("E", new Double(-19.08));


	    // Traversing through HashMap
	    Set<Map.Entry> set = hm.entrySet();

	    for (Map.Entry me : set) {
	      System.out.print(me.getKey() + ": ");
	      System.out.println(me.getValue());
	    }
	  
    System.out.println();

    double balance = Double.parseDouble((hm.get("B").toString()));
    hm.put("B", balance + 1000);

    System.out.println("B's new balance: " + hm.get("B"));
  }
}
