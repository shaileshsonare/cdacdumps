
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MapDemo {

	
	public static void main(String[] args) {
		
		Map<String,Double> hm  =  new  HashMap<String,Double>( );

		   //  Put elements to the map
		   hm.put("John Baron",new Double(3434.34));
		   hm.put("Smith Klein",new Double(123.22));
		   hm.put("James Baker", new Double(1378.00));
		   hm.put("Todd Martin", new Double(99.22));
		   hm.put("Ralph Smith", new Double(19.08));
		   hm.put(null, new Double(20.8));
		   hm.put(null, new Double(21.8));
		   System.out.println("Test");
	
		   //Get a set of the entries
		   Set set =  hm.keySet( );

		  //Get an iterator
		  Iterator<String>  it  =set.iterator( );

		  // Display elements
		   while(it.hasNext ( ) )  {
		   String key  =  it.next ( );
		   System.out.print(key  +  ":  ");
		  System.out.println(hm.get(key));
		   }
	}

}
