import java.util.*;

class TreeMapDemo {
  public static void main(String args[]) {

    TreeMap tm = new TreeMap();


    tm.put("Dhanush", new Double(1434.34));
    tm.put("Selvan", new Double(2234.22));
    tm.put("Balu", new Double(3378.00));
    tm.put("Valluvar", new Double(4299.22));

    Set set = tm.entrySet();

    Iterator i = set.iterator();

    while(i.hasNext()) {
      Map.Entry me = (Map.Entry)i.next();
      System.out.print(me.getKey() + ": ");
      System.out.println(me.getValue());
    }
    System.out.println();


    double amount  =  ((Double)tm.get("Selvan")).doubleValue();
    tm.put("Selvan", new Double(amount + 150));
    System.out.println("Selvan''s new amount: " +
      tm.get("Selvan"));
  }
}

