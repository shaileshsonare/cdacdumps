package com.hcl.bank.login;

import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

public class EmployeeLogin {
	
	public void login()
	{
		Locale.setDefault(Locale.ENGLISH);
		ResourceBundle bundle = ResourceBundle.getBundle("app",Locale.getDefault());
		String uid = bundle.getString("userid");
		String pwd = bundle.getString("password");
		System.out.println("Login Here...........");
		Scanner input = new Scanner(System.in);
		System.out.println("\n Userid   : ");
		String uuid = input.next();
		System.out.println("\n Password : ");
		String ppwd = input.next();
		
		Locale locale = bundle.getLocale();
		
		System.out.println("Resource Bundle Locale : "+locale.getLanguage());
		
		Enumeration elements = bundle.getKeys();
		
		while (elements.hasMoreElements())
		{
			System.out.println(bundle.getObject(elements.nextElement().toString()));
			
		}	
		
		if ((uid.equals(uuid)) && (pwd.equals(ppwd)))
		{
			System.out.println("Welcome "+uid);
		}
		else
			System.out.println("Incorrect userid / password");
			
	}
	
   public static void main(String[] args) {
	
	   new EmployeeLogin().login();
	   
   }
		
}
