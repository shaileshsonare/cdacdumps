package com.hcl.bank.login;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for com.hcl.bank.login");
		//$JUnit-BEGIN$
		suite.addTestSuite(OperationsTest.class);
		suite.addTestSuite(LoginTest.class);
		//$JUnit-END$
		return suite;
	}

}
