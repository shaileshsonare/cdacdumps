import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;

final class Person
{
	Person()
	{
		
	}
}


public class ClassDemo {

	public static void main(String[] args) {
			
		try {
			Class myclass = Class.forName("java.util.Locale");
			System.out.println("Class Name :"+myclass.getName());
			System.out.println("Package : "+myclass.getPackage());
			System.out.println("Modifiers : "+myclass.getModifiers());
			Constructor cons[] = myclass.getConstructors();
			
			for (Constructor c : cons)
			{
				System.out.println("\nConstructors : "+c);
				System.out.println("\nModifiers : "+c.getModifiers());
				System.out.println("\nName : "+c.getName());
				System.out.println("\nVarargs : "+c.isVarArgs());
			}			
			
			 Method methods[] = myclass.getMethods();
			
			 for (Method m : methods)
				{
					System.out.println("\nMethods : "+m);
					System.out.println("\nModifiers : "+m.getModifiers());
					System.out.println("\nName : "+m.getName());
					System.out.println("\nVarargs : "+m.isVarArgs());
				}			
				
				Field fields[] = myclass.getFields();
				
				 for (Field f : fields)
					{
						System.out.println("\nFields : "+f);
						System.out.println("\nModifiers : "+f.getModifiers());
						System.out.println("\nName : "+f.getName());
						System.out.println("Type : "+f.getType());
					}			
					
					 					
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}

}
