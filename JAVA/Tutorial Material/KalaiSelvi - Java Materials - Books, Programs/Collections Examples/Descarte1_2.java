import java.util.*;

public class Descarte {

	//baraja es una matriz
	private Carta baraja[][];	
	
	private static final int OROS = 0;
	private static final int COPAS = 1;
	private static final int ESPADAS = 2;
	private static final int BASTOS = 3;
	
	private	Carta cartaJugador;
	private	Carta cartaMaquina;
	
	public Carta cartajuegojuga() {
		return cartaJugador;
	}
	
	public Carta getCartaJugador() {
		return cartaJugador;
	}

	public void setCartaJugador(Carta cartaJugador) {
		this.cartaJugador = cartaJugador;
	}

	public Carta getCartaMaquina() {
		return cartaMaquina;
	}

	public void setCartaMaquina(Carta cartaMaquina) {
		this.cartaMaquina = cartaMaquina;
	}

	public Descarte(){	
		this.cartaMaquina = new Carta();
		this.cartaJugador = new Carta();
		this.baraja = new Carta[12][4];
	}
	
	//creamos la baraja cons sus propiedades
	// Numero= ser� el n�mero d la escalera de la carta
	//Palo= ser� del 1 al 4 representando los 4 palos de la baraja
	//Usada= ser�s una propidead booleana que permitir� esconder o no las cartas de manera que las usadas no lo podr�n volver a ser
	
	private void LlenarBaraja(){
		for (int x = 1; x < baraja.length; x++) {
			for (int y = 0; y < baraja[x].length; y++) {
				baraja[x][y] = new Carta();
				baraja[x][y].setNumero(y+1);
				baraja[x][y].setPalo(x+1);
				baraja[x][y].setUsada(false);
			}			
		}
	}
	/* Para poder mostrar la baraja con sus palos se quita pq no es mu bonito de ver es "sucio"
	private void MostrarBaraja() {		
		for (int x = 0; x < baraja.length; x++) {
			for (int y = 0; y < baraja[x].length; y++) {
				System.out.print(" ");
				System.out.print(baraja[x][y].getPalo() + ","+ baraja[x][y].getNumero());
			}
			System.out.println();
		}
	}
	*/
	
	//Primer intento de escojer un numero al azar "FALLIDO"
	/*private void EscogerNumeroAzar(){
		//for (int x = 0; x < baraja.length; x++) {
			//for (int y = 0; y < baraja[x].length; y++) {
				double numeroAzar = Math.random();
				double numeroMakinaAzar = baraja[0][0];
				System.out.println(numeroMakinaAzar);
			//}
		
		//}
		
		//int numeroMaquina = Math.random();
	}
	*/
	private Carta dameCarta(Carta carta){
		Carta myCartaAzar = new Carta();
		Random myRandom = new Random();

		int numero = myRandom.nextInt(baraja.length );
		int palo = myRandom.nextInt(baraja[numero].length);
			
		carta.setNumero(numero);
		carta.setPalo(palo);
		if (baraja[numero][palo].getUsada()){
			dameCarta(carta);
		}else{
			baraja[numero][palo].setUsada(true);
		}		
		return myCartaAzar;
			
	}
	

	
	//se crea un objeto carta con sus propiedades
	private class Carta{
		private int numero;
		private int palo;
		private boolean usada;
		
		public void setNumero(int valor){
			this.numero=valor;
		}
		public void setPalo(int tipoPalo){
			this.palo=tipoPalo;
		}
		public void setUsada(boolean uso){
			this.usada=uso;
		}

		public int getNumero(){
			return this.numero;
		}
		public int getPalo(){
			return this.palo;
		}
		public boolean getUsada(){
			return this.usada;
		}
	}
	
	public static void main(String args[]){
		boolean salir = false;
				
		Descarte juego = new Descarte();
		juego.LlenarBaraja();
		//juego.MostrarBaraja();
		
		//declara variable entrada, q cogera el nombre del jugador
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("HoLa BiEnVeNiDoS a @@--DeScArTe--@@ Un JuEgO ApAsIoNaNtE");

		//pregunta al jugador su nombre
		System.out.println("Wenas, como te llamas amigo?");
		//espera a q se lo digas
		String nombre = entrada.nextLine();
		//Caracteristicas para cada tipo de jugador
		if(nombre.equalsIgnoreCase("jordi")) {
			System.out.println("Ese Ripoll!!!!!\nEncara que t'agradin les gordes, calves i amb cal�otets i estiguis amb sobrepes, et deixare jugar");
		}
		
		if(nombre.equalsIgnoreCase("emili")) {
			System.out.println("Vinga Emiliano que em deus una Birra!!!!!!");
		}
		
		if(nombre.equalsIgnoreCase("dani")) {
			System.out.println("Ese Campillo!!!!!\n��campillo mariquita fullerooo!!! ��te voy a fundir!! y que sepas que Mari que es muy guapa va a recibir antes el sofa que tu  je je");
		}
		if(nombre.equalsIgnoreCase("xavi")) {
			System.out.println("Anda el de las entradas...\n��gay!!  ��viu a tarragona!! ��treballa a barcelona!! ��es de oliana!! ��pero li agrada mes peramola!!");
		}
		
		if(nombre.equalsIgnoreCase("faustino")) {
			System.out.println("Mira el que se va a beber una birra de gratis con Ferran");
		}
		if(nombre.equalsIgnoreCase("ferran")) {
			System.out.println("L'increible programador!!!!!");
		}
		//cogemos el nombre y lo a�adimos a un saludo
		System.out.println("\nMuy bien "+nombre+", Vamos all�!");


		
		
		//preguntamos si quiere carta
		System.out.println("\nQuieres una carta? s/n");
		
		String contest = entrada.nextLine();
		
		
		if (contest.equalsIgnoreCase("s")){
			System.out.println("Muy bien, vamos all�...\n");
			salir=false; }
		else {
			System.out.println("Hasta otra, Rajao :)");
			salir=true; 
		}
		
		// si se pide carta empieza el bucle para la comprovaci�n de cartas				
		while(!salir){
			juego.dameCarta(juego.getCartaJugador());
			juego.dameCarta(juego.getCartaMaquina());
			
			
			// Se inician los palos al azar para el Jugador
			String palosJugador = "";
			if(juego.cartaJugador.getPalo() == 0)
			{palosJugador = " OROS  ";
			}
			else if(juego.cartaJugador.getPalo() == 1)
			{palosJugador = "BASTOS ";
							}
			else if(juego.cartaJugador.getPalo() == 2)
			{palosJugador = "ESPADAS";
				
			}
			else if(juego.cartaJugador.getPalo() == 3)
			{palosJugador = " COPAS ";
			}
			
//			 Se inician los palos al azar para el Maquina
			String palosMaquina = "";
			if(juego.cartaMaquina.getPalo() == 0)
			{palosMaquina = " OROS  ";
			}
			else if(juego.cartaMaquina.getPalo() == 1)
			{palosMaquina = "BASTOS ";
				
			}
			else if(juego.cartaMaquina.getPalo() == 2)
			{palosMaquina = "ESPADAS";
				
			}
			else if(juego.cartaMaquina.getPalo() == 3)
			{palosMaquina = " COPAS ";				
			}
			
			System.out.println("********************");
			System.out.println("** CaRtA JuGaDoR  **");
			System.out.println("**     --"+juego.getCartaJugador().getNumero()+"--      **");
			System.out.println("**      de        **");
			System.out.println("**     "+palosJugador+"    **");
			System.out.println("********************");
			System.out.println("");
			System.out.println("********************");
			System.out.println("** CaRtA MaQuInA  **");
			System.out.println("**     --"+juego.getCartaMaquina().getNumero()+"--      **");
			System.out.println("**      de        **");
			System.out.println("**     "+palosMaquina+"    **");
			System.out.println("********************\n");

			if (juego.getCartaJugador().getNumero() >= juego.getCartaMaquina().getNumero()){
				

				System.out.println("GANA JUGADOR!!!!!!!!!!!!!!!!\n");
				
				System.out.println("Quieres seguir?, "+nombre+"? s/n");

				String respuesta = entrada.nextLine();

				if (respuesta.equalsIgnoreCase("s")){
					System.out.println("Vale vamos otra carta!!!\n");
					salir=false;
				}else {
					System.out.println("Hasta otra, Rajao :)");
					salir=true; 
				}

			}else{
				
				System.out.println("GANA LA MAQUINA!!!\n");			
				System.out.println("OOOOOH!!! \nHasta otra, perdedor :)");
				salir=true;
				
			}
		
		}
	}
	
}
