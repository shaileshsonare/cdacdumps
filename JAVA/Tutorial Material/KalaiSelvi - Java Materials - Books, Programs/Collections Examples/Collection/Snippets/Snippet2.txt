	private static String getFormattedDate() {
		java.text.SimpleDateFormat formatter = 
			new java.text.SimpleDateFormat("MMM dd, yyyy hh:mm:ss");
		java.util.Date now = new java.util.Date();
		return formatter.format(now);
	}
