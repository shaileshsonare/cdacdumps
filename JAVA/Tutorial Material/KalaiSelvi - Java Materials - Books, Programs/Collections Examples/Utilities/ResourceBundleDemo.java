import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;


public class ResourceBundleDemo {

	public String uid, pwd;
	
	public void getResource1()
	{
		try
		{
		ResourceBundle res = ResourceBundle.getBundle("App",Locale.ENGLISH);
		if (res!=null)
		{
		uid = res.getString("UserId");
		pwd = res.getString("Password");
		}
		else
			System.out.println("Property file not found");
		}catch(MissingResourceException mre)
		{
			System.out.println("missing");
		}
	}
	
	public void login(String Uid,String Pwd)
	{
		if (uid.equals(Uid) && (pwd.equals(Pwd)))
		{
			System.out.println("Welcome "+uid);
		}
		else
			System.out.println("Incorrect Uid/ Pwd");
	}
	
	
	
	public static void main(String[] args) {
		ResourceBundleDemo obj = new ResourceBundleDemo();
		obj.getResource1();
		obj.login("Admin", "Admin1");
	}

}
