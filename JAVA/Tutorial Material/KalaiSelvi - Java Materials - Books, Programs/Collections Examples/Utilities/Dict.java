import java.util.*;
import java.io.*;

/** 
 * Dictionary class for the keypad entry problem. 
 * SS & RWT, October 2003.
 */ 
public class Dict {

    /** Mapping from letters to digits. */
    private static final int[] digit = {
                    2, 2, 2,   3, 3, 3, 
         4, 4, 4,   5, 5, 5,   6, 6, 6,
      7, 7, 7, 7,   8, 8, 8,   9, 9, 9, 9
    };
    
    private static final String stars = "******************************";
    
    private StringBuffer buffer = new StringBuffer();

    /** Mapping from digit sequences to sets of words. */
    private Map map;
    
    /** Creates a new dictionary. */
    public Dict() {
        this.map = new HashMap();
    }
    
    /** Adds a word to the dictionary. */
    public void addWord(String word) {  
        // Find the digit sequence corresponding to the word. 
        buffer.delete(0, buffer.length());        // buffer = "";
        for (int i = 0; i < word.length(); i++) {
            int d = digit[word.charAt(i) - 'a'];    
            buffer.append(d);                     // buffer += String.valueOf(d);
        }
        String digits = buffer.toString();
        // Add the words to the set of words for that digit sequence.
        List words = (List) map.get(digits);
        if (words == null) {
            words = new LinkedList();
            map.put(digits, words);
        }
        words.add(word);
    }
    
    /** Returns the words associated with a digit sequence. */
    public List getWords(String digits) {
        List words = (List) map.get(digits);
        if (words == null) {
            words = new LinkedList();
            String word = stars.substring(0, digits.length());
            words.add(word);
        }
        return words;
    }
    
}

