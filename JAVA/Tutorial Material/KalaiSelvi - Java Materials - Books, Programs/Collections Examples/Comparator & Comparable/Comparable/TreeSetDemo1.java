import java.util.*;

class  order implements Comparable{

int orderno;
String customername;
float ordvalue;


order(int orderno,String customername, float ordvalue)
{
 this.orderno=orderno;
 this.customername=customername;
this.ordvalue=ordvalue;
}

public String toString()
{
	return "" +orderno +":" +customername +" : " + ordvalue;
}

public int compareTo(Object o)
{
	 order ord=null;
	   int ono=0;
	   if(o instanceof order ){
	     ord=(order)o;
	   }
	   if(ord.orderno<this.orderno)
	     ono=1;
	   else if(this.orderno<ord.orderno)
	     ono=-1;
	   return ono;

	}

}

public class TreeSetDemo1 {


  public static void main(String[] argv) {


   TreeSet tm = new TreeSet();
order o1=new order(1001,"Gosling",4500.0F);
order o2=new order(1002,"da Vinci",5600.0F);
order o3=new order(999,"peter naugthon",7600.0F);
order o4=new order(122,"Ritchie",89600.0F);
    tm.add(o1);
    tm.add(o3);
    tm.add(o4);
    tm.add(o2);

    System.out.println("The Tree Set"+tm);


  }
}