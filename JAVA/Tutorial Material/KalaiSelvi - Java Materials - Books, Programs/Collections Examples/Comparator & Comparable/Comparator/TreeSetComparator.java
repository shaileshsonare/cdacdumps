/********************************************
*	TreeSet with Comparator demo program
*
*
*
***********************************************


import java.util.Comparator;
import java.util.TreeSet;

class MyComparator implements Comparator<String> {
  public int compare(String a, String b) {
    String aStr, bStr;

    aStr = a;
    bStr = b;

    return bStr.compareTo(aStr);
  }
  // No need to override equals.
}

public class TreeSetComparetor {
  public static void main(String args[]) {
    TreeSet<String> ts = new TreeSet<String>(new MyComparator());//new MyComparator()

    ts.add("C");
    ts.add("A");
    ts.add("B");
    ts.add("E");
    ts.add("F");
    ts.add("D");
    MyComparator m=new MyComparator();
    
    // the compare method will compare two string and give the number of characters that are not matching
    System.out.println("the comparision of a and b="+m.compare("dan","daniel")); 
    
    
    for (String element : ts)
      System.out.print(element + " ");

    System.out.println();
  }
}

