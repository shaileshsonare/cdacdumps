import java.util.ArrayList;
import java.util.*;

class AgeComparator implements Comparator {
  public int compare(Object person, Object anotherPerson) {
    int age1 = ((Person) person).getAge();
    int age2 = ((Person) anotherPerson).getAge();
return( age1-age2);

  }
}

class Person  {
  private String firstName;
  private String lastName;
  private int age;

public Person(String firstName,String lastName,int age)
  {
	    this.firstName = firstName;
	  this.lastName = lastName;
    this.age = age;
}
  public String getFirstName() {
    return firstName;
  }


  public String getLastName() {
    return lastName;
  }

    public int getAge() {
    return age;
  }

  public String toString()
  {
	  return firstName + " " + lastName + " "+ age;
}
}

public class Tmapcomprator {

	public static void main(String args[])
	{

		TreeMap tm = new TreeMap(new AgeComparator());
tm.put("ram",new Person("anand","kumar",45));
tm.put("ganesh",new Person("krishna","kumar",35));
tm.put("suresh",new Person("muthu","kumar",25));
tm.put("ganesh",new Person("giri","nath",47));

System.out.println(tm);

		}
	}