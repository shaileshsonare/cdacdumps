
package project1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;


public class BankAccount {
	 public static void main(String[] args) {
	 	
	 	int choice=0;
		ArrayList aList = new ArrayList();
		CreateTextFile createTextFile = new CreateTextFile();
		ReadFromFile readFromFile = new ReadFromFile();
		
		try {
			createTextFile.fileCreation();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			aList = readFromFile.readFromFile();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		while(choice!=6){
			
			System.out.println(" 1. Sorting on Account Number:");
			System.out.println(" 2. Sorting on Account Holder Name: ");
			System.out.println(" 3. Sorting on Account Balance:");
			System.out.println(" 4. Display all the records :");
			System.out.println(" 5. Update in ArayList :");
			System.out.println(" 6. Exit");
			System.out.println("Enter Ur choice: ");
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			try {
				choice = Integer.parseInt(in.readLine());
			} catch (NumberFormatException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			switch(choice){
			
			case 1:
				// Sorting based on Account Number
				java.util.Collections.sort(aList,new AccountNumberComparator());
			break;
			
			case 2:
				// Sorting based on Account Holder name
				java.util.Collections.sort(aList,new AccountHolderNameComparator());
		    break;
			
			case 3:
				// Sorting based on Account Balance
				java.util.Collections.sort(aList,new AccountBalanceComparator());
			break;
			
			case 4:
				// Displaying the arraylist using iterator
				BankAccountDetails bankAccountDetails = new BankAccountDetails();
				Iterator itr =  aList.iterator();
				while(itr.hasNext()){
					bankAccountDetails = (BankAccountDetails)itr.next();
					System.out.println("Account Number is "+bankAccountDetails.getAccNumber());
					System.out.println("Account HolderName is "+bankAccountDetails.getAccHolderName());
					System.out.println("Account balance is "+bankAccountDetails.getBalance());
					System.out.println("********details of a bank record ends here*******");
					
				}
			break;
			
			case 5:
				BankAccountDetails createBankAccount1 = new BankAccountDetails();
				createBankAccount1.setAccHolderName("Srivatsan");
				createBankAccount1.setAccNumber(160);
				createBankAccount1.setBalance(30000);
				aList.set(1,createBankAccount1);
			break;		
					
			
			case 6:
				System.out.println("Program is Terminted Successfully.....");
				System.exit(0);
			break;	
			
			default:
				System.out.println("Wrong choice.... Please Try again.....");
						
			}
		}
	 	
	 }
}
