package project1;

import java.io.*;
import java.util.*;
public class ReadFromFile {

	/**
	 * @param args
	 * @throws IOException 
	 */
	
	public static ArrayList readFromFile() throws IOException
	{
			
		// TODO Auto-generated method stub
		ArrayList bankArrayList=new ArrayList();
		BankAccountDetails bankObj=null;
		  File data = new File("C:\\Account.txt");
	      if (data.exists())
	      {
	         BufferedReader in = new BufferedReader( new FileReader(data));
	          String line = null;
	         // reading data from the file line by line
	         while((line = in.readLine())!=null)
	         {
	        	 String []str=null;
	        	 // splitting a line from the file based on :
	        	 str=line.split(":");
	        	 bankObj = new BankAccountDetails();
	        	 bankObj.setAccNumber(Integer.parseInt(str[0]));
	        	 bankObj.setAccHolderName(str[1]);
	        	 bankObj.setBalance(Float.parseFloat(str[2]));
	        	 bankArrayList.add(bankObj);
	         }
	         
	         in.close();
	      }
	      else
	         System.out.println("File not found - example.txt");
	      return bankArrayList;
	   }
	
	public static void main(String args[]) throws IOException{
		ArrayList arr = new ArrayList();
		ReadFromFile readFromFile = new ReadFromFile();
		arr = ReadFromFile.readFromFile();
		System.out.println(arr);
		
	}
	

}	




