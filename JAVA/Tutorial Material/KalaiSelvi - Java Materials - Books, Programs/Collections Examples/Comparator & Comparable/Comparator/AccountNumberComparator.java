
package project1;

import java.util.Comparator;



public class AccountNumberComparator implements Comparator {
	public int compare(Object bankAccount1, Object bankAccount2) {
	    int accNumber1 = ((BankAccountDetails) bankAccount1).getAccNumber();
	    int accNumber2 = ((BankAccountDetails) bankAccount2).getAccNumber();
	return( accNumber1-accNumber2);
}
}
