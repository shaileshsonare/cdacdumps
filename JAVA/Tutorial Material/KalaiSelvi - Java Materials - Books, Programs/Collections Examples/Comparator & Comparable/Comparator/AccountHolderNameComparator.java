
package project1;

import java.util.Comparator;


public class AccountHolderNameComparator implements Comparator {
	public int compare(Object bankAccount1, Object bankAccount2) {
		String accHolderName1 = ((BankAccountDetails) bankAccount1).getAccHolderName().toUpperCase();
	    String accHolderName2 = ((BankAccountDetails) bankAccount2).getAccHolderName().toUpperCase();
	    return accHolderName1.compareTo(accHolderName2);
	}
}
