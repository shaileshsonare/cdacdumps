package IOPackage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileIO1 {
	
	File f1=new File("abc1.txt");
	//int a[]={1,6,1,654,4,4,5};
	
	public void ReadData()
	{
		try {
			BufferedInputStream bis=new BufferedInputStream(new FileInputStream(f1));
			
			//long len=f1.length();
			//int l=0;
			int i;
			while((i=bis.read())!=-1)
			{
				System.out.println(i);
				
			}
			 
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void WriteData()
	{
		try {
			BufferedOutputStream bos=new BufferedOutputStream(new FileOutputStream(f1));
			
			bos.write(12);
			bos.write(546);
			bos.write(887);
			bos.write(45);
			bos.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		FileIO1 obj=new FileIO1();
		
		obj.WriteData();
		obj.ReadData();
		
	}
}
