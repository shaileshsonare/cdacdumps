package IOPackage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileIOClass {
	
	File f=new File("Test.txt");
	
	public void ReadData()
	{
		try
		{
			FileInputStream fis=new FileInputStream(f);
			
			long len=f.length();
			int l=0;
			while(l<len)
			{
				System.out.println(fis.read());
				l++;
			}
			
		}catch(FileNotFoundException fnf)
		{
			System.out.println(fnf.getMessage());
		}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
	}
	
	public void WriteData()
	{
		try
		{
			FileOutputStream fos=new FileOutputStream(f);
			fos.write(22);
			fos.write(20);
			fos.write(50);
			
		}catch(FileNotFoundException fnf)
		{
			System.out.println(fnf.getMessage());
		}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
		
	}
	
	public static void main(String[] args) {
	
		FileIOClass obj=new FileIOClass();
		
		obj.WriteData();
		obj.ReadData();
	}
}
