package com.cdac.training.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBConnection {

	Connection conn = null;
	PreparedStatement pstmt;
	boolean flag = false;
	
	public boolean dbConnect(String uid, String pwd)
	{
		try {
			//registering and loading the default driver class
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			
			//establishing connection to the db
			conn = DriverManager.getConnection("jdbc:odbc:cdacDSN","admin","admin");
			
			pstmt = conn.prepareStatement("select * from login where userid =? and password=?");
			
			pstmt.setString(1,uid);
			pstmt.setString(2,pwd);
			ResultSet rs = pstmt.executeQuery();						
			if (rs.next())
				flag = true;
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(SQLException sqle)
		{
			sqle.printStackTrace();
		}
		
		finally
		{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return flag;
	}
}
