package com.cdac.training.trainee.pojo;

import com.cdac.training.trainee.bslogic.TraineeLoginLogic;

public class TraineeBean {
	
	//properties
	
	private String userid;
	private String password;
	
	//mutators - setter methods
	
	public void setUserid(String userid)
	{
		this.userid = userid;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}
	
	// Accessors - getter methods
	
	public String getUserid()
	{
		return this.userid;
	}
	
	public String getPassword()
	{
		return password;
	}
	
	public boolean validate(String uid,String pwd)
	{
		
		TraineeLoginLogic logic = new TraineeLoginLogic();
		return logic.loginValidate(uid,pwd);
		
	}
	
}
