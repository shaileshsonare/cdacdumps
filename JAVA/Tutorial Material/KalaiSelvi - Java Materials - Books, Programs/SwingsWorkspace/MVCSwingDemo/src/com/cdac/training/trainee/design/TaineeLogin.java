package com.cdac.training.trainee.design;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.cdac.training.trainee.pojo.TraineeBean;

public class TaineeLogin extends JApplet implements ActionListener{
	
	JLabel lblUid, lblPwd;
	JTextField txtUid;
	JPasswordField txtPwd;
	JButton btnLogin;
	String message = "Login";
	TraineeBean traineeBean;
	
	public void init()
	{
		lblUid = new JLabel("Userid");
		lblPwd = new JLabel("Password");
		txtUid = new JTextField(10);
		txtPwd = new JPasswordField(10);
		btnLogin = new JButton("Login");
		
	}
	
	
	public void start()
	{
		Container c = getContentPane();
		c.setLayout(new FlowLayout());
		c.add(lblUid);
		c.add(txtUid);
		c.add(lblPwd);
		c.add(txtPwd);
		btnLogin.addActionListener(this);
		c.add(btnLogin);
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().equals(btnLogin))
		{
			String uid = txtUid.getText();
			String pwd = txtPwd.getText();
			System.out.println(uid);
			System.out.println(pwd);
			// move the data to the bean
			traineeBean = new TraineeBean();
			traineeBean.setUserid(uid);
			traineeBean.setPassword(pwd);
			boolean flag = traineeBean.validate(uid,pwd);
			if (flag)
				message = "Welcome "+uid;
			else
				message = "Invalid Userid / Password";
			repaint();
		}		
	}
	
	public void paint(Graphics g)
	{
		g.drawString(message, 100, 100);
	}

}
