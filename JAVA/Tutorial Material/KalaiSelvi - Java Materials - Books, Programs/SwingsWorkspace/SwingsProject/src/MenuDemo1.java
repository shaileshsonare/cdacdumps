
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class MenuDemo1 implements ActionListener, MenuListener {
	JFrame f;

	JMenuBar mb;

	JMenu m1, m2, m3;

	JMenuItem m11, m12, m21, m22;

	Container cp;

	public MenuDemo1() {
		f = new JFrame();
		mb = new JMenuBar();
		m1 = new JMenu("color");
		m2 = new JMenu("edit");
		m3 = new JMenu("exit");
		m11 = new JMenuItem("blue");
		m12 = new JMenuItem("red");
		m21 = new JMenuItem("save");
		m22 = new JMenuItem("copy");
		f.setJMenuBar(mb);
		mb.add(m1);
		mb.add(m2);
		mb.add(m3);
		m1.add(m11);
		m1.add(m12);
		m2.add(m21);
		m2.add(m22);
		m1.addMenuListener(this);
		m2.addMenuListener(this);
		m3.addMenuListener(this);
		m11.addActionListener(this);
		m12.addActionListener(this);
		cp = f.getContentPane();
		f.setSize(400, 400);
		f.setVisible(true);
	}

	public void menuSelected(MenuEvent e) {
		if (e.getSource() == "Exit") {
			System.exit(0);
		}
	}

	public void menuDeselected(MenuEvent e) {
	}

	public void menuCanceled(MenuEvent e) {
	}

	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		if (str.equals("blue")) {
			cp.setBackground(Color.blue);
		} else {
			cp.setBackground(Color.red);
		}
	}

	public static void main(String s[]) {
		new MenuDemo1();
	}
}