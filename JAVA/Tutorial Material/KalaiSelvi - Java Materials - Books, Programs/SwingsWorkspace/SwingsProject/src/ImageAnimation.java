import java.awt.*;
import java.awt.event.*;
import javax.swing.JApplet;
public class ImageAnimation extends JApplet 
{
  private int startX, startY, lastX, lastY;

  public void init() {
    addMouseListener(new RectRecorder());
    addMouseMotionListener(new RectDrawer());
    setBackground(Color.white);
  }

  

 void drawRectangle(Graphics g, int startX, int startY,
                             int stopX, int stopY ) {
    int x, y, w, h;
    x = Math.min(startX, stopX);
    y = Math.min(startY, stopY);
    w = Math.abs(startX - stopX);
    h = Math.abs(startY - stopY);
    g.drawRect(x, y, w, h);
  }

  private class RectRecorder extends MouseAdapter {

   

public void mousePressed(MouseEvent event) {
      startX = event.getX();
      startY = event.getY();
      lastX = startX;
      lastY = startY;
    }

    
    public void mouseReleased(MouseEvent event) {
      Graphics g = getGraphics();
      g.setXORMode(Color.lightGray);
      drawRectangle(g, startX, startY, lastX, lastY);
    }
  } 

  private class RectDrawer extends MouseMotionAdapter {

    public void mouseDragged(MouseEvent event) {
      int x = event.getX();
      int y = event.getY();

      Graphics g = getGraphics();
      g.setXORMode(Color.lightGray);
      drawRectangle(g, startX, startY, lastX, lastY);
      drawRectangle(g, startX, startY, x, y);

      lastX = x;
      lastY = y;
    }
  } 
}
