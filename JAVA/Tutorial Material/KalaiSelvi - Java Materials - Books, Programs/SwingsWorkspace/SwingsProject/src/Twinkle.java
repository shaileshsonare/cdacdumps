import java.awt.*;
import java.applet.Applet;
/*
<applet code="Twinkle" height=400 width=400>
</applet>
*/
public class Twinkle extends Applet implements Runnable
{
Thread t=null;
boolean stopflag;
int a=1;
String st="TWINKLE";
Color c1;
//Font F;
Font F=new Font("monotype corsiva",Font.BOLD,10);

public void init()
{
}

public void start()
{
t=new Thread(this);
stopflag=false;
t.start();
}


public void run()
{
for(;;)
{
 try
  {
  repaint();
  Thread.sleep(1000);
   a=a*-1;
   if(stopflag)
    break;
  }
  catch(InterruptedException e)
  { }
 }
}

public void stop()
{
stopflag=false;
t=null;
}

public void paint(Graphics g)
{
setBackground(Color.white);
if(a==1)
g.setColor(Color.pink);
else
g.setColor(Color.white);
int x[]={100,300,150,200,300};
int y[]={100,50,300,25,298};
g.drawPolygon(x,y,5);
g.setFont(F);
g.drawString(st,190,130);
}
}
