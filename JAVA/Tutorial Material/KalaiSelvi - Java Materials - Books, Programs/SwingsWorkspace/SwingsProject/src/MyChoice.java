import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/* <applet code = "MyChoice" height=400 width=400>
   </applet> */

public class MyChoice extends JApplet //implements ItemListener
{
  JLabel country;
  JComboBox nation;
  JTextField message;
  Container con;
  JPanel panel;

  public void init()
  {
     country = new JLabel("My Country");
     nation = new JComboBox();
     nation.setToolTipText("Select Anyone Country");
     message= new JTextField(20);
     con = getContentPane();
     panel = new JPanel();
     nation.addItem("India");
     nation.addItem("Pakistan");
     nation.addItem("Bangaladesh");
     nation.addItem("Srilanka");
     panel.add(country);
     panel.add(nation);
     con.add(panel,BorderLayout.NORTH);
     con.add(message,BorderLayout.CENTER);
     nation.addItemListener(new MyListener());
  }

  class MyListener implements ItemListener
  {
     public void itemStateChanged(ItemEvent e) 
     {
       message.setText("             Your country is  "+nation.getSelectedItem());
      }
  }
}

