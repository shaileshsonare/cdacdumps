import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

class Resume extends JFrame 
{  String str,str1;
    
   ImageIcon im;
   JTextField regno,name,age;
   JButton submit,exit,reset;
   JComboBox day,mon,year;
   JRadioButton f,m;
   ButtonGroup sex;
   JCheckBox read,swim,music;
   JTextArea address,result,res;
   JLabel logo,head,reg,nam,dob,lday,lmon,lyear,ag,addr,sx,hob;
   JPanel p,p1,p2,p3,p4,p5,p6,p7,p8,p9,pa,pb;
   Container con;
   Resume(String title)
   {
      super(title);
      con=getContentPane();
      im=new ImageIcon("logo.gif");
      con.setLayout(new FlowLayout());
      logo=new JLabel("",im,JLabel.RIGHT);
      head=new JLabel("RESUME");
      p=new JPanel();
      p.setLayout(new GridLayout(1,2,8,68));
      p.add(head);
      p.add(logo);
      //p.add(im);
      con.add(p);
     

      reg=new JLabel("Reg. No.");
      regno=new JTextField(12);
      nam=new JLabel("Name"); 
      name=new JTextField(12);
      ag=new JLabel("Age");
      age=new JTextField(12);
      exit=new JButton("Exit");
      reset=new JButton("Reset");
      submit=new JButton("Submit");
      p1=new JPanel();
      p1.setLayout(new GridLayout(3,2,1,1));
      p1.add(reg); 
      p1.add(regno); 
      p1.add(nam); 
      p1.add(name); 
      p1.add(ag); 
      p1.add(age); 
      con.add(p1);
      dob=new JLabel("             DOB");
//      lday=new JLabel("DD   ");
 //    lmon=new JLabel("MM   ");
  //    lyear=new JLabel("YYYY");
      day=new JComboBox();
      mon=new JComboBox();
      year=new JComboBox();
      p3=new JPanel();
      p3.setLayout(new GridLayout(1,2,1,1));
      
      for(int i=1;i<=31;i++)
      {
          day.addItem(i+"");
       }
      for(int i=1;i<=12;i++)
      {
          mon.addItem(i+"");
       }
      for(int i=0;i<=22;i++)
      {
          year.addItem((1980+i)+"");
       }
  
      p9=new JPanel();
      p9.setLayout(new GridLayout(1,3,0,0));
       
      p3.add(dob);
   //   p9.add(lday);
      p9.add(day);
    //  p9.add(lmon);
      p9.add(mon);
      //p9.add(lyear);
      p9.add(year);
      p3.add(p9);
      con.add(p3);

      addr=new JLabel("Address");
      address=new JTextArea(5,12);
      p4=new JPanel();
      p4.setLayout(new GridLayout(1,2,1,1));
      p4.add(addr);
      p4.add(address);
      con.add(p4);

      sx=new JLabel("Sex");
      m=new JRadioButton("Male",true);
      f=new JRadioButton("Female");
      sex=new ButtonGroup();
      sex.add(m);
      sex.add(f);
      pa=new JPanel();
      pa.setLayout(new GridLayout(1,2,1,1));
       pa.add(m);
       pa.add(f);
      p5=new JPanel();
      p5.setLayout(new GridLayout(1,2,1,1));
       p5.add(sx);
       p5.add(pa);
       con.add(p5);
      

      p6=new JPanel();
      p6.setLayout(new GridLayout(1,2,1,1));
      hob=new JLabel("Hobbies                             ");
      p6.add(hob);
       p7=new JPanel();
       p7.setLayout(new GridLayout(3,3,1,1));
       read=new JCheckBox("Reading");
       swim=new JCheckBox("Swiming");
       music=new JCheckBox("Music");
       p7.add(read);
       p7.add(swim);
       p7.add(music);
      p6.add(p7);
     con.add(p6);
       

      p2=new JPanel();
      p2.setLayout(new GridLayout(1,3,1,1));
      p2.add(submit);
      submit.addActionListener(new ActionListener()
    {
     public void actionPerformed(ActionEvent e)
     {
        str=regno.getText().trim()+":"+name.getText().trim()+":"+age.getText().trim()+":"+day.getSelectedItem()+"/"+mon.getSelectedItem()+"/"+year.getSelectedItem()+":"+address.getText()+":";
  if(m.isSelected()==true)
     str+="Male"+":";
   else
     str+="Female"+":";
   if(read.isSelected()==true)
       str+="Reading";
   if(swim.isSelected()==true)
       str+="Swimming";
   if(music.isSelected()==true)
       str+="Music";
    result.setText(str);
   StringTokenizer st=new StringTokenizer(str,":");
   while(st.hasMoreTokens())
     res.append(st.nextToken()+"\n");

  
      }
     });
      p2.add(reset);
      reset.addActionListener(new ActionListener()
   {
     public void actionPerformed(ActionEvent e)
    {
     regno.setText("");
     name.setText("");
     age.setText("");
     address.setText("");
     result.setText("");
     res.setText("");
     day.setSelectedIndex(0);
     mon.setSelectedIndex(0);
     year.setSelectedIndex(0);
    /* m.setState(true);
     read.setState(false);
     swim.setState(false);
     music.setState(false);
   */
    }
   });
      
      p2.add(exit);
      exit.addActionListener(new ActionListener()
   {
     public void actionPerformed(ActionEvent e)
     {
        System.exit(0);
     }
   });
      con.add(p2);


      p8=new JPanel();
      p8.setLayout(new GridLayout(1,2,1,1));
      result=new JTextArea(10,15);
      res=new JTextArea(10,15);
      p8.add(result);
      p8.add(res);
     con.add(p8);
 
  }
public static void main (String args[])
{
   Resume r=new Resume("RESUME");
   r.setBounds(100,100,370,570);
   r.setVisible(true);
}
}
