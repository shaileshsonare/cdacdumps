package layout;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Cal extends JFrame
{
JTextField result;
JButton n;
JButton pl;
JButton mi;
JButton dot;
JButton mu;
JButton di;
JButton eq;
JButton cl;
JButton ex;
JPanel panel;
String s1;
String s2;
String s3;
int n1=0,n2=0;
double f1=0.0,f2=0.0;
boolean flag = false;
boolean flag1=false;
boolean flag2=false;
int cn=0;

Cal(String title)
{
 super(title);
 result=new JTextField(30);
 panel=new JPanel();
 pl=new JButton("+");
 mi=new JButton("-");
 mu=new JButton("*");
 di=new JButton("/");
 eq=new JButton("=");
 cl=new JButton("C");
 dot=new JButton(".");
 ex=new JButton("Exit");
 panel.setLayout(new GridLayout(5,4));
  for(int i=1;i<=3;i++)
   {
    n=new JButton(i+" ");
    panel.add(n);
    n.addActionListener(new MyInnerListener());
   }
    panel.add(pl);
    pl.addActionListener(new MyInnerListener());
  for(int i=4;i<=6;i++)
   {
    n=new JButton(i+" ");
    panel.add(n);
    n.addActionListener(new MyInnerListener());
   }
    panel.add(mi);
    mi.addActionListener(new MyInnerListener());
  for(int i=7;i<=9;i++)
   {
    n=new JButton(i+" ");
    panel.add(n);
    n.addActionListener(new MyInnerListener());
   }
    panel.add(mu);
    mu.addActionListener(new MyInnerListener());
    n=new JButton("0");
    panel.add(n);
    n.addActionListener(new MyInnerListener());
    panel.add(eq);
    eq.addActionListener(new MyInnerListener());
    panel.add(cl);
    cl.addActionListener(new MyInnerListener());
    panel.add(di);
    di.addActionListener(new MyInnerListener());
    panel.add(dot);
    dot.addActionListener(new MyInnerListener());
    ex.addActionListener(new MyInnerListener());
   Container c=getContentPane();
   c.add(result,BorderLayout.NORTH);
   c.add(panel,BorderLayout.CENTER);
   c.add(ex,BorderLayout.SOUTH);
 }
 public static void main(String args[])
 {
 Cal c1 = new Cal("Calculator");
 c1.setBounds(10,10,300,300);
 c1.setVisible(true);
 }

class MyInnerListener implements ActionListener
{
public void actionPerformed(ActionEvent e)
 {
 System.out.println( e.getActionCommand() );

 if(e.getActionCommand().equals("1 ")|| e.getActionCommand().equals("2 ")|| e.getActionCommand().equals("3 ")|| e.getActionCommand().equals("5 ")|| e.getActionCommand().equals("4 ")|| e.getActionCommand().equals("6 ")|| e.getActionCommand().equals("7 ")|| e.getActionCommand().equals("8 ")|| e.getActionCommand().equals("9 ")|| e.getActionCommand().equals("0")) 
    { 
    s3=result.getText()+e.getActionCommand().trim();
    result.setText(s3);   
    }
 else if(e.getActionCommand().equals("Exit"))
     System.exit(0);
  
 else if(e.getActionCommand().equals("."))
    {
    flag=true;
    cn+=1;
    s3=result.getText()+e.getActionCommand().trim();
    result.setText(s3);   
    }   

 else if(e.getActionCommand().equals("+")||e.getActionCommand().equals("-")||e.getActionCommand().equals("*")||e.getActionCommand().equals("/"))
    {
    s1=s3;
    if(flag==true)
     {
      flag1=true;
      f1=Double.parseDouble(s1.trim());
     }
    else
      n1=Integer.parseInt(s1.trim());
    flag=false;  
    result.setText(" ");
    s2=e.getActionCommand();
    }

  else if(e.getActionCommand().equals("="))
   {
    if(flag==true)
     {
       f2=Double.parseDouble(s3.trim());
       flag2=true;
     }
    else
       n2=Integer.parseInt(s3.trim());
    int a=0;
    double s=0.0;
    if(s2.trim().equals("+"))
    {
     if(flag1&&flag2)
      s=f1+f2;
     else if(flag1&& !flag2)
      s=f1+n2;
     else if(!flag1&& flag2)
      s=n1+f2;
     else
      a=n1+n2;
    }
    if(s2.trim().equals("-"))
    {
     if(flag1&&flag2)
      s=f1-f2;
     else if(flag1&& !flag2)
      s=f1-n2;
     else if(!flag1&& flag2)
      s=n1-f2;
     else
      a=n1-n2;
    }
    if(s2.trim().equals("*"))
    {
     if(flag1&&flag2)
      s=f1*f2;
     else if(flag1&& !flag2)
      s=f1*n2;
     else if(!flag1&& flag2)
      s=n1*f2;
     else
      a=n1*n2;
    }
    if(s2.trim().equals("/"))
    {
     if(flag1&&flag2)
      s=f1/f2;
     else if(flag1&& !flag2)
      s=f1/n2;
     else if(!flag1&& flag2)
      s=n1/f2;
     else
      a=n1/n2;
    }
    if(flag1|| flag2)
     result.setText(s+" ");
    else 
     result.setText(a+" ");
   } 
    else if(e.getActionCommand().equals("C"))
    {
    result.setText(" ");
    flag=false;
    flag1=false;
    flag2=false;
    }
  }
 }
}
