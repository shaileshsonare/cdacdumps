
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Popup extends JApplet implements MouseListener {
	JPopupMenu jp;

	public void init() {
		Container cp = this.getContentPane();
		jp = new JPopupMenu();
		jp.add("File");
		jp.add("Open");
		JMenuItem m1 = new JMenuItem("Exit");
		jp.add(m1);
		cp.add(jp);
		cp.setLayout(new FlowLayout());
		cp.addMouseListener(this);
	}

	public void mouseReleased(MouseEvent e) {
		if (e.isPopupTrigger()) {
			jp.show(e.getComponent(), e.getX(), e.getY());
		}
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}
}
/*
 * <applet code=popup.class height=500 width=500> </applet>
 */
