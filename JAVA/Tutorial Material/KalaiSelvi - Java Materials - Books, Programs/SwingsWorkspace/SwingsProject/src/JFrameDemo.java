
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class JFrameDemo extends WindowAdapter implements ActionListener {
	JFrame f;

	JButton b1, b2, b3;

	Container cp;

	JPanel jp;

	public JFrameDemo() {
		f = new JFrame();
		//setBackground(c);
		b1 = new JButton("Red");
		b2 = new JButton("Gray");
		b3 = new JButton("Orange");
		cp = f.getContentPane();
		jp = new JPanel();
		jp.add(b1);
		jp.add(b2);
		jp.add(b3);
		cp.add(jp, "South");
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		f.addWindowListener(this);
		f.setSize(400, 400);
		f.setLocation(50, 50);
		f.setVisible(true);
	}

	public void windowClosing(WindowEvent we) {
		System.exit(0);
	}

	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == b1) {
			cp.setBackground(Color.red);
		} else if (ae.getSource() == b2) {
			cp.setBackground(Color.gray);
		} else if (ae.getSource() == b3) {
			cp.setBackground(Color.orange);
		}
	}

	public static void main(String s[]) {
		new JFrameDemo();
	}
}