
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class JTableDemo {
	JFrame jf;

	String data[][] = { { "Gokul", "53616" }, { "Veera", "52876" },
			{ "Dina", "56609" } };

	String name[] = { "Name", "Phone no" };

	JTable jt;

	Container cp;

	public JTableDemo() {
		jf = new JFrame();
		jt = new JTable(data, name);
		cp = jf.getContentPane();
		cp.setBackground(Color.blue);
		JScrollPane js = new JScrollPane(jt);
		cp.add(js, "Center");
		jf.setSize(400, 400);
		jf.setVisible(true);
	}

	public static void main(String arg[]) {
		new JTableDemo();
	}
}