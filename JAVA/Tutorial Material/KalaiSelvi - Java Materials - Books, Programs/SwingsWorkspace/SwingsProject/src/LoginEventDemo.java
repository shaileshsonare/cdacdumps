import java.awt.*;
import java.applet.*;
import java.awt.event.*;
import javax.swing.*;


public class LoginEventDemo extends Applet implements ActionListener
{
	JLabel l1,l2,l3;
	JTextField t1,t2,t3;
	JButton b1,b2;

	GridBagLayout gbl=new GridBagLayout();
	GridBagConstraints gbc=new GridBagConstraints();

	public void init()
	{
		l1=new JLabel("User Name:");
		l2=new JLabel("Pasword:");
		l3=new JLabel("Result:");

		t1=new JTextField(30);
		t2=new JTextField(30);
		t3=new JTextField(30);
		t3.setEditable(false);

		b1=new JButton("Submit");
		b2=new JButton("Clear");

		//gbc.fill=GridBagConstraints.BOTH;
		gbc.gridx=10;
		gbc.gridy=10;

		gbl.setConstraints(l1,gbc);
		
		gbc.gridx=30;
		gbc.gridy=10;
	
		gbl.setConstraints(t1,gbc);


		gbc.gridx=10;
		gbc.gridy=20;
		gbl.setConstraints(l2,gbc);
		
		gbc.gridx=30;
		gbc.gridy=20;
		gbl.setConstraints(t2,gbc);
		
		gbc.gridx=10;
		gbc.gridy=30;
		gbl.setConstraints(b1,gbc);
		
		gbc.gridx=30;
		gbc.gridy=30;
		gbl.setConstraints(b2,gbc);
				

		gbc.gridx=30;
		gbc.gridy=40;
		gbl.setConstraints(t3,gbc);


		setLayout(gbl);
		add(l1);
		add(l2);
		add(t1);
		add(t2);
		add(b1);
		add(b2);
		add(t3);
		b1.addActionListener(this);
		b2.addActionListener(this);

	}

	public void actionPerformed(ActionEvent a)
	{
		if(a.getSource()==b1)
		{
		
		String s1=t1.getText();
		s1=s1+t2.getText();
		t3.setText(s1);		
		}
		else if(a.getSource()== b2)
		{
			t1.setText("");
			t2.setText("");
			t3.setText("");
		}

	}

}

/*
<applet code=LoginEventDemo.class height=400 width=600>
</applet>
*/
