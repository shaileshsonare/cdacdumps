
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class Jlist implements ListSelectionListener {
	JFrame f;

	JList l;

	JScrollPane jsp;

	JTextField t1;

	Container cp;

	public Jlist() {
		f = new JFrame();
		String str[] = { "Blue", "red", "Green", "Yellow" };
		l = new JList(str);
		t1 = new JTextField(20);
		jsp = new JScrollPane(l);
		l.addListSelectionListener(this);
		cp = f.getContentPane();
		cp.setLayout(new FlowLayout());
		cp.add(jsp);
		cp.add(t1);
		f.setSize(400, 400);
		f.setVisible(true);
	}

	public void valueChanged(ListSelectionEvent e) {
		String s = l.getSelectedValue().toString();
		if (s.equals("red")) {
			cp.setBackground(Color.red);
			t1.setText(s);
		}

	}

	public static void main(String s[]) {
		new Jlist();
	}
}

