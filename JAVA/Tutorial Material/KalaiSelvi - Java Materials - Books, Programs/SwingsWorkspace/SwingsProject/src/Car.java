import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class  Car1 extends JFrame
{
  JCheckBox c1,c2,c3,c4;
  JTextField message;
  JPanel panel;
  Container con;
  Car1(String title)
  {
     super(title);
     c1 = new JCheckBox("Tata Safari");
     c2 = new JCheckBox("Tata Sierra");
     c3 = new JCheckBox("Tata Sumo");
     c4 = new JCheckBox("Tata Indica");
     message = new JTextField(30);
     con = getContentPane();
     panel = new JPanel();
     panel.setBackground(Color.blue);
     panel.add(c1);
     panel.add(c2);
     panel.add(c3);
     panel.add(c4);
     con.add(panel,BorderLayout.CENTER);
     con.add(message,BorderLayout.SOUTH);
     c1.addItemListener(new InnerListener());
     c2.addItemListener(new InnerListener());
     c3.addItemListener(new InnerListener());
     c4.addItemListener(new InnerListener());
  }

class InnerListener implements ItemListener
{
     public void itemStateChanged(ItemEvent e)
     {
        if (e.getItemSelectable()==c1)
	  message.setText("My Favourite Car is " + c1.getText());
	else if (e.getItemSelectable()==c2)
	  message.setText("My Favourite Car is " + c2.getText());
	else if (e.getItemSelectable()==c3)
	  message.setText("My Favourite Car is " + c3.getText());
	else if (e.getItemSelectable()==c4)
	  message.setText("My Favourite Car is " + c4.getText());
/*	if (c1.getState()==true)
	  message.setText("My Favourite Car is " + c1.getLabel());
	else if (c2.getState()==true)
	  message.setText("My Favourite Car is " + c2.getLabel());
	else if (c3.getState()==true)
	  message.setText("My Favourite Car is " + c3.getLabel());
	else if (c4.getState()==true)
	  message.setText("My Favourite Car is " + c4.getLabel());*/
     }
}
}
class Driver1
{
 public static void main(String args[])
 {
    Car1 c = new Car1("MY FAVOURITE CAR");
    c.setBounds(10,10,500,500);
    c.setVisible(true);
  }
}


