import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Jprogress extends Thread implements ActionListener {
	Thread thr;

	ProgressMonitor pm;

	JFrame jf;

	boolean stop = false;

	boolean start = false;

	public Jprogress() {
		JPanel p1 = new JPanel();
		JButton b1 = new JButton("START");
		JButton b2 = new JButton("STOP");
		b1.addActionListener(this);
		b2.addActionListener(this);
		p1.add(b1);
		p1.add(b2);
		jf = new JFrame();
		jf.setSize(400, 400);
		Container cp = jf.getContentPane();
		cp.add(p1, "South");
		pm = new ProgressMonitor(jf, "PROGRESS SCHEDULE", "NOT STARTED", 0, 100);
		jf.setVisible(true);
	}

	public void startrunning() {
		thr = new Thread(this);
		thr.start();
	}

	public void stoprunning() {
		stop = true;
		thr.stop();
		thr = null;
	}

	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		if (str.equals("START")) {
			stop = false;
			startrunning();
		} else {
			stoprunning();
		}
	}

	public void run() {
		int min = 0, max = 100;
		pm.setNote("STARTED");
		for (int i = 1; i <= max; i++) {
			if (stop) {
				pm.close();
			} else {
				pm.setProgress(i);
				pm.setNote(i + "% Completed");
				try {
					Thread.sleep(250);
				} catch (Exception ee) {
				}
			}
		}
	}

	public static void main(String arg[]) {
		new Jprogress();
	}
}