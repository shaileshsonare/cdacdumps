import javax.swing.*;
import java.awt.*;

public class flow_layout extends JApplet
{
	JButton b1,b2,b3;
	FlowLayout flow;	
	public void init()
	{
		JPanel p=new JPanel();
		
		flow=new FlowLayout();
		p.setLayout(flow);

		b1=new JButton("Button 1");
		b2=new JButton("Button 2");
		b3=new JButton("Button 3");
	
		p.add("Button 1",b1);
		p.add("Button 2",b2);
		p.add("Button string 3",b3);
		getContentPane().add(p);

	}
}

/* <applet code="flow_layout" height=200 width=400>
</applet>
*/