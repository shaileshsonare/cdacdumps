// <applet code=TabToolTip1.class width=400 height=400></applet>

import java.awt.*;
import java.applet.*;
import java.awt.event.*;
import javax.swing.*;

public class TabToolTip1 extends JApplet implements ActionListener {
	JLabel l;

	JTabbedPane tab;

	public void init() {
		tab = new JTabbedPane(SwingConstants.TOP);
		JButton b1 = new JButton("Click Here");
		JButton b2 = new JButton("click me");
		b1.setMnemonic('p');

		b1.setToolTipText("press  the button for changing text");
		l = new JLabel("how are you");
		tab.addTab("how are you", l);
		tab.addTab("Click", b1);
		JPanel p1 = new JPanel();
		p1.setBackground(Color.green);
		p1.add(b2);
		p1.add(new JTextArea(20, 30));
		tab.add("Hello", p1);
		Color c = Color.red;
		tab.setBackgroundAt(1, c);
		tab.setBackgroundAt(0, Color.blue);
		tab.setBackgroundAt(2, Color.green);

		getContentPane().add("Center", tab);
		/*
		 * Container pane=getContentPane(); pane.add("Center",tab);
		 */
		b1.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		JPanel pp = new JPanel();
		pp.setBackground(Color.yellow);
		tab.insertTab("soft",null,"s" pp);
		tab.removeTabAt(0);
	}

}

