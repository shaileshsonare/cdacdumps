import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.DefaultHighlighter;
import sun.applet.AppletAudioClip;
public class First extends JApplet implements ActionListener,Runnable
{
	Thread datetimethread;
	Date date;
	GregorianCalendar calendar;
	String strdate, strtime, strstatus;
	AppletAudioClip clip;
    String soundURL;
    boolean loop;
    int b,e;
	String TITLE = "THIS IS MY FIRST WINDOW";
	Toolkit tk;
	Dimension d;
	JPanel p=new JPanel();
	Component	parentComponent;
	JMenuItem	ShowAction	= null;
	JRadioButtonMenuItem lineItem, rectangleItem, circleItem, // Types
	curveItem, textItem;
	JCheckBoxMenuItem redItem, yellowItem, // Colors
	greenItem, blueItem ;
	JTextArea leftArea ;
	JFileChooser c = new JFileChooser();
	int mouseX, mouseY;
	boolean mouseclicked = false;
	JRadioButtonMenuItem cursorItem;
	JButton b1;
	int startX, startY, lastX, lastY;
	int count=0;
	Thread t=new Thread();
	public void init() 
	{
		new login(null);
        
		
		JTextArea leftArea = new JTextArea();
	    JScrollPane leftScrollPane = new JScrollPane(leftArea);
	    leftScrollPane.setPreferredSize(d);
	    
	    loop=true;
        soundURL = getParameter("file");
        clip = (AppletAudioClip) getAudioClip(getCodeBase(), soundURL);
        if(clip != null)
        {
        	
        	Container ct=getContentPane();
            JLayeredPane rightArea = new JLayeredPane();
            JScrollPane rightScrollPane = new JScrollPane(rightArea);
    	    rightScrollPane.setPreferredSize(d);
    	    
    	 
    	    JPanel p;
    	    p=new JPanel();
    	    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
    	        p,leftScrollPane);
    	    ct.add(splitPane);
    	    
    	    
    	        	   // getContentPane().add(split);
    	   
    	    JMenuBar menuBar = new JMenuBar();
    	    setJMenuBar(menuBar);
    	    JMenu file=new JMenu("File");
    		file.setMnemonic('F');
    		
    	    JMenuItem item1,item2,item3,item4;
    		
    		file.add(item2=new JMenuItem("New"));
    		
    		
    		
    			
    		file.add(item2=new JMenuItem("Open"));
    		file.add(item3=new JMenuItem("Save"));
    		file.addSeparator();
    		file.add(item4=new JMenuItem("Close"));
    		menuBar.add(file);
    		JMenu edit = new JMenu("Edit");
    		edit.setMnemonic('E');
    		JMenuItem item6, item7, item8, item9, item10;
    		edit.add(item6 = new JMenuItem("Cut"));
    		edit.add(item7 = new JMenuItem("Copy"));
    		edit.add(item8 = new JMenuItem("Pase"));
    		edit.addSeparator();
    		edit.add(item10 = new JMenuItem("Select All"));
    		menuBar.add(edit);
    		JMenu menu = new JMenu("View");
    	    menuBar.add(menu);
    	    JMenuItem menuItem;

    	    Action readAction = leftArea.getActionMap().get(
    	        DefaultEditorKit.readOnlyAction);
    	    menuItem = menu.add(readAction);
    	    menuItem.setText("Readonly");
    	    
    	    Action writeAction = leftArea.getActionMap().get(
    	        DefaultEditorKit.writableAction);
    	    menuItem = menu.add(writeAction);
    	    menuItem.setText("Writable");
    	    
    	    Action colorAction = leftArea.getActionMap().get(
    	    				DefaultHighlighter.DefaultPainter);
    	    		    menuItem = menu.add(readAction);
    	    		    menuItem.setText("Paint");

    	    menu.addSeparator();

    	    Action cutAction = leftArea.getActionMap().get(
    	        DefaultEditorKit.cutAction);
    	    menuItem = menu.add(cutAction);
    	    menuItem.setText("Cut");
    	    
    	    Action copyAction = leftArea.getActionMap().get(
    	        DefaultEditorKit.copyAction);
    	    menuItem = menu.add(copyAction);
    	    menuItem.setText("Copy");
    	    
    	    Action pasteAction = leftArea.getActionMap().get(
    	        DefaultEditorKit.pasteAction);
    	    menuItem = menu.add(pasteAction);
    	    menuItem.setText("Paste");
    	    
    	 
    	    JMenu elementMenu = new JMenu("Elements");
    	  
    		
    	    elementMenu.add(lineItem = new JRadioButtonMenuItem("Line", false));
    	    elementMenu.add(rectangleItem = new JRadioButtonMenuItem("Rectangle", false));
    	    elementMenu.add(circleItem = new JRadioButtonMenuItem("Circle", false));
    	    elementMenu.add(curveItem = new JRadioButtonMenuItem("Curve", false));
    	    ButtonGroup types = new ButtonGroup();
    	   
    	    types.add(lineItem);
    	    types.add(rectangleItem);
    	    types.add(circleItem);
    	    types.add(curveItem);
    	    elementMenu.addSeparator();
    	    elementMenu.add(redItem = new JCheckBoxMenuItem("Red", false));
    	    elementMenu.add(yellowItem = new JCheckBoxMenuItem("Yellow", false));
    	    elementMenu.add(greenItem = new JCheckBoxMenuItem("Green", false));
    	    elementMenu.add(blueItem = new JCheckBoxMenuItem("Blue", false));
    	    menuBar.add(elementMenu);
    	    JButton b=new JButton("Click");
    	    p.add(b);
    	    JButton b1=new JButton("Do u like to Add Frame");
    	    p.add(b1);
    	    p.addMouseListener(new RectRecorder());
    	    p.addMouseMotionListener(new RectDrawer());
    	    p.setBackground(Color.DARK_GRAY);

    	    splitPane.setDividerLocation(.5);
    	    
    	    getContentPane().add(splitPane, BorderLayout.CENTER);
    	    datetimethread=new Thread(this);
    	    datetimethread.start();
    	    b.addActionListener(this);
    	    b1.addActionListener(this);
            clip.play();
        }
        	 	 		
	}
	public void stop()
    {
        if(clip != null)
            clip.stop();
    }
	public boolean mouseDown(Event e, int x, int y ) 
	{
	      mouseX=x; mouseY=y;
	      mouseclicked = true;
	      repaint();
	      return true;
	}
	   
	 public void actionPerformed(ActionEvent ae)
	 {
	 	//JButton source=(JButton)ae.getSource();
	 	Graphics g=getGraphics();
	 	String str=ae.getActionCommand();
	 	int x=0;
 		int y=0;
	 	if(str.equals("Click"))
	 	{
	 			count+=1;
	 		
	 		//if(count==1)
	 			g.drawOval(100+40,100+40,120,150);
	 		//g.setColor(Color.DARK_GRAY);
	 		/*g.setColor(Color.BLACK);
	 		g.drawOval(60,60,200,200);
	 		g.fillOval(90,120,50,20);
	 		g.fillOval(190,120,50,20);
	 		g.drawLine(165,125,165,175);
	 		g.drawArc(110,130,95, 95,0,-180);*/
	 		if(count==2)
	 			g.drawOval(77+80,95+80,30,20);
	 		if(count==3)
	 			g.drawOval(130+80,95+80,30,20);
	 		if(count==4)
	 			g.fillOval(88+80,101+80,10,10);
	 		if(count==5)
	 			g.fillOval(141+80,101+80,10,10);
	 		if(count==6)
	 			g.drawOval(105+80,120+80,30,30);
	 		if(count==7)
	 			g.fillArc(80+80,145+80,80,40,180,180);
	 		if(count==8)
	 			g.drawOval(45+80,112+80,15,30);
	 		if(count==9)
	 			g.drawOval(180+80,112+80,15,30);
	 		if(count==10)
	 			g.drawLine(200,290,200,400);
	 		for(int k=250;k<=400;k++)
	 		{
	 			if(count==11)
	 				g.drawLine(150,400,200,320);
	 			if(count==12)
	 				g.drawLine(200+x,320+y,250,400);
	 			if(count==13)
	 				{
	 					g.drawLine(150,520,200,400);
	 					g.drawLine(150,520,135,520);
	 				}
	 				if(count==14)
	 				{
	 					g.drawLine(200,400,250,520);
	 					g.drawLine(250,520,265,520);
	 				}
	 				
	 			if(x<400)
	 			{
	 				x=x+10;
	 				y=y-10;
	 			}
	 			try
				{
	 				t.sleep(1000);
				}
	 			catch(InterruptedException e)
				{
 					System.out.println("Exception Occured");
				}
	 		}
	 		
	 	 }
	 	if(str.equals("Do u like to Add Frame"))
	 	{
	 		//Graphics g=getGraphics();
	 		for(int i=0;i<=4;i++)
	 		{
	 			
	 			if((i%2)==0)
	 			{
	 				g.drawOval(60,i*60+30,50,50);
	 				try
					{
	 					t.sleep(1000);
					}
	 				catch(InterruptedException e)
					{
	 					System.out.println("Exception Occured");
					}
	 			}
	 			else
	 			{
	 				g.fillOval(60,i*60+30,50,50);
	 				try
					{
	 					t.sleep(1000);
					}
	 				catch(InterruptedException e)
					{
	 					System.out.println("Exception Occured");
					}
	 			}
	 		}
	 	}
	 }
	 void drawRectangle(Graphics g, int startX, int startY,
	 				int stopX, int stopY ) 
	 {
	 	int x, y, w, h;
	 	x = Math.min(startX, stopX);
	 	y = Math.min(startY, stopY);
	 	w = Math.abs(startX - stopX);
	 	h = Math.abs(startY - stopY);
	 	g.drawRect(x, y, w, h);
	 }

	 private class RectRecorder extends MouseAdapter 
	 {
	 	public void mousePressed(MouseEvent event)
	 	{
	 		startX = event.getX();
	 		startY = event.getY();
	 		lastX = startX;
	 		lastY = startY;
	 	}


	 	public void mouseReleased(MouseEvent event) 	
	 	{
	 		Graphics g = getGraphics();
	 		g.setXORMode(Color.lightGray);
	 		drawRectangle(g, startX, startY, lastX, lastY);
	 	}
	 } 

	 private class RectDrawer extends MouseMotionAdapter 	
	 {

	 	public void mouseDragged(MouseEvent event) {
	 		int x = event.getX();
	 		int y = event.getY();

	 		Graphics g = getGraphics();
	 		g.setXORMode(Color.lightGray);
	 		drawRectangle(g, startX, startY, lastX, lastY);
	 		drawRectangle(g, startX, startY, x, y);

	 		lastX = x;
	 		lastY = y;
	 	}
	 }
	 public void run()
	 {
	 	while(datetimethread!=null)
	 	{
	 		disp();
	 	try
	 	{
	 		datetimethread.sleep(500);
	    }
	 	catch(InterruptedException e)
	 	{
	 		getAppletContext().showStatus("Thread interrupted");
	 	}
	 }
 }
	 public void disp()
	 {
	 	date=new Date();
	 	calendar=new GregorianCalendar();
	 	calendar.setTime(date);

	 	strtime=calendar.get(Calendar.HOUR)+":"+calendar.get(Calendar.MINUTE)+":"+calendar.get(Calendar.SECOND);
	 	strdate=calendar.get(Calendar.DATE)+"/"+calendar.get(Calendar.MONTH)+"/"+calendar.get(Calendar.YEAR);
	 	strstatus=strtime+"   "+strdate;
	 	getAppletContext().showStatus(strstatus);
	 }
	 public void runnable()
	 {
	 	
	 }
}