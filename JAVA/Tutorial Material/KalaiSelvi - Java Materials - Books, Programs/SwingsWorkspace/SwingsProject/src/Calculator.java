import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Calculator extends JFrame
{
   String str;
   String str1;
   String str2;
   String sym;
   JTextField result;
   JButton plusbutton,minusbutton,multiplybutton,dividebutton,equalbutton,clearbutton,number;
   JPanel panel;

   Calculator(String title)
   {
      super(title);
      result = new JTextField(30);
      panel = new JPanel();
      plusbutton = new JButton("+");
      minusbutton = new JButton("-");
      multiplybutton = new JButton("*");
      dividebutton = new JButton("/");
      equalbutton = new JButton("=");
      clearbutton = new JButton("C");
      panel.setLayout(new GridLayout(4,4,3,3));
      for(int i=0; i<=9; i++)
      {
	 number = new JButton(i+""); 
	 panel.add(number);
         number.addActionListener(new MyInnerListener());
      }
      panel.add(plusbutton);
      panel.add(minusbutton);
      panel.add(multiplybutton);
      panel.add(dividebutton);
      panel.add(equalbutton);
      panel.add(clearbutton);
      plusbutton.addActionListener(new MyInnerListener());
      minusbutton.addActionListener(new MyInnerListener());
      multiplybutton.addActionListener(new MyInnerListener());
      dividebutton.addActionListener(new MyInnerListener());
      equalbutton.addActionListener(new MyInnerListener());
      clearbutton.addActionListener(new MyInnerListener());
      Container c = getContentPane();
      c.add(result,BorderLayout.NORTH);
      c.add(panel,BorderLayout.CENTER);
   }

   public static void main(String args[])
   {
      Calculator c1 = new Calculator("CALCULATOR");
      c1.setBounds(5,10,300,300);
      c1.setVisible(true);
   }

   class MyInnerListener implements ActionListener
   {
      public void actionPerformed(ActionEvent e)
       {
	 if( e.getActionCommand().charAt(0) >= '0' &&
	     e.getActionCommand().charAt(0) <= '9' )
	  /*  if ((e.getActionCommand().equals("1"))||
	      (e.getActionCommand().equals("2"))||
	      (e.getActionCommand().equals("3"))||
	      (e.getActionCommand().equals("4"))||
	      (e.getActionCommand().equals("5"))||
	      (e.getActionCommand().equals("6"))||
	      (e.getActionCommand().equals("7"))||
	      (e.getActionCommand().equals("8"))||
	      (e.getActionCommand().equals("9"))||
	      (e.getActionCommand().equals("0"))) */
	       {
                  str = result.getText()+e.getActionCommand();
		  result.setText(str);
                }
	if((e.getSource()==plusbutton)||
	    (e.getSource()==minusbutton)||
	    (e.getSource()==multiplybutton)||
	    (e.getSource()==dividebutton))
	    {
		sym=e.getActionCommand(); 
		str1=result.getText();
	        result.setText("");
            }

     if(e.getSource() == equalbutton)
	    {
	    char ch = sym.charAt(0);
	    int a = Integer.parseInt(str1);
	    int b = Integer.parseInt(str);
	    switch(ch)
	    {
	      case '+': result.setText((a+b)+""); break;
	      case '-': result.setText((a-b)+""); break;
	      case '*': result.setText((a*b)+""); break;
	      case '/': result.setText((a/b)+""); break;
	      default : result.setText("Invalid");	    
            }
	    }
     if(e.getSource() == clearbutton)
        {
        	result.setText("");
	}
     }
   }
}