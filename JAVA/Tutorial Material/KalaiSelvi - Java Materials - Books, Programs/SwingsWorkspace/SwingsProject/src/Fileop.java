import java.io.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
class Fileop extends JFrame
{
  JLabel src,des,status;
   int i;
  JTextField src1,des1;
  JButton exit,copy,append,show,create,clear;
  JTextArea res;
  JPanel p1,p2,p3,p4,p5,p6;
  JSplitPane split;
  JScrollPane scroll;
  Container con;

  RandomAccessFile f;
  File file,file2;
  FileInputStream fin;
  FileOutputStream fout;
 Fileop(String t)
 {
   super(t);
   src=new JLabel("Source File"); 
   des=new JLabel("Destination File"); 
   src1=new JTextField(15);
   des1=new JTextField(15);
   p1=new JPanel();
   p1.setLayout(new GridLayout(2,2,1,1));
   p1.add(src);
   p1.add(src1);
   p1.add(des);
   p1.add(des1);
   exit=new JButton("Exit");
   copy=new JButton("Copy");
   append=new JButton("Append");
   show=new JButton("Show");
   create=new JButton("Create");
   clear=new JButton("Clear");
   p2=new JPanel();
   p2.setLayout(new GridLayout(1,3,1,1));
   p2.add(create);
   p2.add(copy);
   p2.add(append);
   p3=new JPanel();
   p3.setLayout(new GridLayout(1,3,1,1));
   p3.add(show);
   p3.add(exit);
   p3.add(clear);
   status=new JLabel(""); 
   p5=new JPanel();
   p5.add(status);
    
   p5.setLayout(new GridLayout(1,1,1,1));
   p4=new JPanel();
   p4.setLayout(new BoxLayout(p4,BoxLayout.Y_AXIS));  
   p4.add(p1);
   p4.add(p2);
   p4.add(p3);
   p4.add(p5);
   res=new JTextArea(20,30);
   scroll=new JScrollPane(res,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
  
   split=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,p4,scroll);
   con=getContentPane();
   con.setLayout(new FlowLayout());
   con.add(split);
   append.setEnabled(false);
   copy.setEnabled(false);
   show.setEnabled(false);
   create.setEnabled(false);
   exit.setEnabled(true);
   status.setText("Enter File Name......");
  src1.addFocusListener(new FocusListener()
  {
     public void focusGained(FocusEvent e)
     {
       if((src1.getText().trim()).length()>0)
        {
          show.setEnabled(true);
          create.setEnabled(true);
         }
      }

     public void focusLost(FocusEvent e)
     {
       if((src1.getText().trim()).length()>0)
        {
          show.setEnabled(true);
          create.setEnabled(true);
         }
      }
  });
  des1.addFocusListener(new FocusListener()
  {
     public void focusGained(FocusEvent e)
  {
       if( (src1.getText().trim()).length()>0 && (des1.getText().trim()).length()>0)
        {
   append.setEnabled(true);
   copy.setEnabled(true);
          show.setEnabled(false);
          create.setEnabled(false);
         }
     }     
     public void focusLost(FocusEvent e)
  {
       if( (src1.getText().trim()).length()>0 && (des1.getText().trim()).length()>0)
        {
   append.setEnabled(true);
   copy.setEnabled(true);
          show.setEnabled(false);
          create.setEnabled(false);
         }
     }     
});
 
    clear.addActionListener(new ActionListener()
   {
    public void actionPerformed(ActionEvent e)
     {
       src1.setText("");
       des1.setText("");
       res.setText("");
   append.setEnabled(false);
   copy.setEnabled(false);
          show.setEnabled(false);
          create.setEnabled(false);
      }
     });
  exit.addActionListener(new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
         System.exit(0);
    }
});

  copy.addActionListener(new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
   {
    try{
     res.setText("");
     file=new File(src1.getText().trim());
     file2=new File(des1.getText().trim());
     if(file.exists()==true && file.isFile()==true
 && file2.exists()==false)
      {
        int i;
        status.setText("File Copying ....");
        fin=new FileInputStream(file);
        fout=new FileOutputStream(file2);
        do
         {
          i=fin.read();
          if(i!=-1)
            fout.write((char)i);
         }while(i!=-1);
         fin.close();
         fout.close();
        status.setText("File Copyed ....");
        }
      else
         status.setText("Error in File Copying.....");
   }catch(Exception e2)  { }
    }
  });


append.addActionListener(new ActionListener()
{
   public void actionPerformed(ActionEvent e)
   {
     try
     {
     res.setText("");
     file=new File(src1.getText().trim());
     file2=new File(des1.getText().trim());
     if(  file.isFile()==true && file2.isFile()==true)
      {
        int i;
        status.setText("File Appending ....");
        fin=new FileInputStream(file);
         f=new RandomAccessFile(file2,"rw");
        long len=f.length();
         f.seek(len);
        do
         {
          i=fin.read();
          if(i!=-1)
            f.write((char)i);
         }while(i!=-1);
        status.setText("File Appended ....");
         fin.close();
         fout.close();
        }
      else
        status.setText("Error in File Opening ....");
      
       }
        catch(FileNotFoundException e6)
         {
        status.setText("Error in File Not Found for Opening ....");}
        catch(IOException e8)
         {
       status.setText("Error during IO Operation ....");}
    
     }
});
         


  create.addActionListener(new ActionListener()
   {
     public void actionPerformed(ActionEvent e)
     {
    try
    {
      if((src1.getText().trim().length())>0)
       { 
         String sn=new String(src1.getText().trim());
         String sn1=new String(res.getText().trim());
            ByteArrayInputStream br;
 br=new ByteArrayInputStream(sn1.getBytes());
         file=new File(sn);
          if(!(file.exists()))
         {
          RandomAccessFile f=new RandomAccessFile(sn,"rw");
            
                 System.out.print(file.isFile()+" "+file.exists());
               while((i=br.read())!=-1)
               {
                 f.write((char)i);
                 System.out.print((char)i);
                }
                br.close();
               status.setText("File is created......");
            
               f.close(); 
              }
           else
          {   status.setText("Enter New File Name......");
                 System.out.print(file.isFile()+" "+file.exists());
                 System.out.print("Hi3");}
                

        }
       else
        {
         status.setText("Enter New File Name......");
         }
      }catch(Exception e2)
       {}
      }
     
    });



show.addActionListener(new ActionListener()
 {  
    public void actionPerformed(ActionEvent e)
    {
    try
     {
   String stt="";
     res.setText("");
      status.setText("File Opening....."); 
      file=new File(src1.getText().trim());
      if(file.isFile()&& file.exists())
      {
          fin=new FileInputStream(file);
         do
        {
          i=fin.read();
           if(i!=-1)
             res.append((char)i+"");
            // stt=stt.trim()+(char)i;
        }while(i!=-1);
        /*ByteArrayOutputStream b1=new ByteArrayOutputStream();
   
         do
        {
          i=b1.read();
           if(i!=-1)
             res.append((char)i+"");
        }while(i!=-1);
         */
        
      status.setText("File Opened....."); 
      }
      fin.close();
      }catch(Exception e1)
      { }

     
}
});

  }
public static void main(String args[])
{
   Fileop f1=new Fileop("File Operations");
   f1.setBounds(100,100,750,550);
   f1.setVisible(true);
   }
}
   
   
  

