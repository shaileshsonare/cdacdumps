//package fileinput;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

class ReadFile extends JFrame //implements ActionListener
{
   JLabel filename;
   JTextField name;
   JButton showbutton,exitbutton;
   JTextArea contents;
   JSplitPane split;
   JScrollPane scroll;
   JPanel panel1,panel2,panel3;
   Container con;
   File file;
   FileInputStream fin;
   int i,v,h;
   char c;

   public ReadFile(String title)
   {
      super(title);
      filename = new JLabel("Filename");
      name = new JTextField(20);
      panel1 = new JPanel();
      panel2 = new JPanel();
      panel3 = new JPanel();
      showbutton = new JButton("Show");
      exitbutton = new JButton("Exit");
      contents = new JTextArea(50,60);
      v = ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
      h = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS;
      scroll = new JScrollPane(contents,v,h);
      panel1.add(filename);
      panel1.add(name);
      panel2.add(showbutton);
      panel2.add(exitbutton);
      panel3.setLayout(new BoxLayout(panel3,BoxLayout.Y_AXIS));
      panel3.add(panel1);
      panel3.add(panel2);
      split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,panel3,scroll);
      con = getContentPane();
      con.setLayout(new FlowLayout());
      con.add(split);

      showbutton.addActionListener(new ActionListener()
      {
	public void actionPerformed(ActionEvent e)
	 {
	   try
	      {
		file = new File(name.getText().trim());
		if (file.exists() && file.isFile())
		  {
		    fin = new FileInputStream(file);
		    do 
		      {
			i = fin.read();
			if (i!=-1)
			   contents.append((char)i+"");
                      }while(i!=-1);
                    fin.close();
                  } 
              }
           catch(FileNotFoundException f)
	      {
		System.out.println("FNF Exception");
	      }
           catch(IOException i)
	      {
		System.out.println("IOException");
	      }
	      }
       });

     exitbutton.addActionListener(new ActionListener()
       {
	 public void actionPerformed(ActionEvent e)
	   {
	      System.exit(0);
           }
       });
}
}

class FDriver
{
   public static void main(String args[])
    {
       ReadFile r = new ReadFile("");
       r.setBounds(30,30,50,50);
       r.setVisible(true);
    }
}

			   

      
