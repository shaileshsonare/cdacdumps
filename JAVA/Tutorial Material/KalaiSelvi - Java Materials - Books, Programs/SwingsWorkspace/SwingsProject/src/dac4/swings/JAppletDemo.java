package dac4.swings;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;

public class JAppletDemo extends JApplet implements ActionListener{
 	
	
	
	JButton btn;
	Icon icon = new ImageIcon("copy.gif");
	public void init()
	{
	
		btn = new JButton(icon);
		btn.setSize(30, 20);
	}
	
	
	public void start()
	{
		Container c = getContentPane();
		c.setSize(800, 800);
		c.setLayout(new FlowLayout());
		btn.addActionListener(this);
		c.add(btn);
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		
	}
}
