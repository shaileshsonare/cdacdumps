interface Calculator1
{
   double pi = 3.14;
   double sum(double num1,double num2);
   double subtract(double num1,double num2);
}

interface Calculator2
{
   double power(double num1,double num2);
   double mul(double num1,double num2);
   double div(double num1,double num2);
   double mysqrt(double number);
}

class Mathprocessor implements Calculator1,Calculator2
{
   public double sum(double n1,double n2)
     {
	return (n1 + n2);
     }

   public double subtract(double n1,double n2)
     {
	return (n1 - n2);
     }
   public double mul(double n1,double n2)
     {
	return (n1 * n2);
     }
   public double div(double n1,double n2)
     {
	return (n1 / n2);
     }


   public double power(double n1,double n2)
     {
       double n3 = 1;
       for (int i=1; i<=n2; i++)
	   n3 *= n1;
	   return n3;
     }
    
   public double mysqrt(double n)
     {
	return Math.sqrt(n);
     }
}


class Arith
{
   public static void main(String args[])
    {
       Mathprocessor m=new Mathprocessor();
       m.sum(50,30);
       m.subtract(100,25);
       m.power(2,4);
       m.mul(5,4);
       m.div(6,2);
       m.mysqrt(25);
       System.out.println(Calculator1.pi);
       System.out.println(m.pi);
     }
}


