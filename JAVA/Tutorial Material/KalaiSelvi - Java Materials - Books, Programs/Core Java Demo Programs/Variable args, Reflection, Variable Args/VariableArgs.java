
public class VariableArgs {

	public void display1(int...var1){
		 System.out.println("length integer="+var1.length);	
		}
	public void display(float...var2){
		 System.out.println("length="+var2.length);	
		}
	public void display(){
		 System.out.println("ok");	
		}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		VariableArgs var=new VariableArgs();
		 var.display(1.2f,4.1f);
		var.display1(1,2,3,4);
	var.display();
	
	//	var.display();
	
		
	}

}

Enhancements in JDK 5
http://java.sun.com/j2se/1.5.0/docs/guide/language/index.html
J2SE 5.0 in a Nutshell 
http://java.sun.com/developer/technicalArticles/releases/j2se15/



public class TestSinus {
public static void main(String[] args) {
double x = Double.parseDouble(args[0]) * Math.PI / 180;
double y = Math.sin(x);
System.out.println("sin(" + x + ") = " + y);
}
}

import static java.lang.Double.parseDouble;
import static java.lang.Math.*;
import static java.lang.System.*;
public class TestSinus {
public static void main(String[] args) {
double x = parseDouble(args[0]) * PI / 180;
double y = sin(x);
out.println("sin(" + x + ") = " + y);
}
}