class ExeDemo
{
public static void main(String args[])
 {
  try 
   {
   int i= Integer.parseInt(args[0]);
   int j= Integer.parseInt(args[1]);
   int k = i/j;
   System.out.println("K = "+k);
   }

  catch(ArithmeticException a)
   {
   System.out.println("Exception Caught : " + a);
   }
  
  catch(ArrayIndexOutOfBoundsException a)
   {
   System.out.println("Exception Caught : " + a);
   }
  
  catch(NumberFormatException a)
   {
   System.out.println("Exception Caught : " + a);
   }
 System.out.println("Exiting main........");
  }
}
