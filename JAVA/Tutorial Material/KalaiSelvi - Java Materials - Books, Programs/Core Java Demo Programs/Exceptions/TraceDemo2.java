class TraceDemo2
{
public static void main(String args[])
 {
  try 
   {
   int i= Integer.parseInt(args[1]);
   int j= Integer.parseInt(args[0]);
   int k = i/j;
   System.out.println("K = "+k);
   }

  catch(Exception a)/*catch(ArrayIndexOutOfBoundsException a)
   {
   System.out.println("Exception Caught : " + a);
   StackTraceElement s[]; 
   //Throwable t= new Throwable();
   //t.printStackTrace();
   s=a.getStackTrace();*/
   {
   StackTraceElement s[]; 
   s=a.getStackTrace();
   for(int i=0;i<s.length;i++)
    {
     System.out.println("File Name : "+s[i].getFileName()+"i = "+i);
     System.out.println("Class Name : "+s[i].getClassName());
     System.out.println("Method Name : "+s[i].getMethodName());
     System.out.println("Line No. : "+s[i].getLineNumber());
    }
   }
 System.out.println("Exiting main........");
  }
}

