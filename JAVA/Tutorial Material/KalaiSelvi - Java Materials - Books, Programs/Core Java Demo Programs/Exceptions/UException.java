import exceptionpack.IndiaException;
class UException
{
 public static void main(String args[]) //throws IndiaException
 {
 try
 {
  TestException t = new TestException();
  t.check(args[0]);
  }
  catch(IndiaException a)
  { System.out.println(a);
   StackTraceElement s[]=a.getStackTrace();
   for(int i=0;i<s.length;i++)
   {
     System.out.println("File Name : "+s[i].getFileName());
     System.out.println("Class Name : "+s[i].getClassName());
     System.out.println("Method Name : "+s[i].getMethodName());
     System.out.println("Line No. : "+s[i].getLineNumber());
   }
  }
 }
}

