import exceptionpack.MyException;

class UseException
{
 public static void main(String args[])
 {
 int j;
 try
  {
   j=Integer.parseInt(args[0]);
    if(j<0)
     throw new MyException(j);
    else
   System.out.println("AGE =  "+j);
  }

  catch(MyException m)
  {
  StackTraceElement s[];
  s=m.getStackTrace();
  for(int i=0;i<s.length;i++)
  {
  System.out.println("File Name : "+s[i].getFileName());
  System.out.println("Class Name : "+s[i].getClassName());
  System.out.println("Method Name : "+s[i].getMethodName());
  System.out.println("Line No. : "+s[i].getLineNumber());
  }
  System.out.println(m);
  }
}
}

