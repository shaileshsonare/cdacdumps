package com.vam.inner;

public class InnerclassDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*MyOuter mo = new MyOuter();
		MyOuter.MyInner inner = mo.new MyInner();
		inner.seeOuter();*/
		
		

	}

}

class MyOuter {
	private int x = 7;

	public void makeInner() {
		MyInner in = new MyInner();
		in.seeOuter();
	}

	class MyInner {
		public void seeOuter() {
			System.out.println("Outer x is " + x);
		}
	}
}

class MyOuter2 {
	private String x = "Outer2";

	void doStuff() {
		class MyInner {
			public void seeOuter() {
				System.out.println("Outer x is " + x);
			}

			MyInner mi = new MyInner();
		}
	}
}

class MyOuter3 {
	private String x = "Outer2";

	void doStuff() {
		final String  z = "local variable";
		class MyInner {
			public void seeOuter() {
				System.out.println("Outer x is " + x);
				System.out.println("Local variable z is " + z); // Won't
				// Compile!
			}
		}
	}
}
	class Popcorn {
		public void pop() {
			System.out.println("popcorn");
		}
	}

	class Food {
		Popcorn p = new Popcorn() {
			public void pop() {
				System.out.println("anonymous popcorn");
			}
		};
		public void popIt() {
			p.pop(); 
			p.sizzle(); 
			}
		
	}
