public class InnerClass
{  
    static class s
    {   s()
    {System.out.println("Inner Class");}
    }
    
    public static void main(String[] args)
    {  final int a1=10;  // member variables access modifier can only be final
        s objk=new s();
         class a{  //methods access modifier cannot be static because it can be accesses only in static /  toplevel type
             //If the class is static the foll is the error
             //java.lang.Error: Unresolved compilation problem: 
         	//Illegal modifier for the local class a; only one of abstract or final is permitted

        	//at InnerClass.main(InnerClass.java:21)
        //Exception in thread "main" 
           a(){System.out.println("I am inner class constructor");}
            public void method1()
            {System.out.println("I am inner class public method");
            System.out.println("Value of a = "+a1);}
            final void method2()
            {System.out.println("I am inner class static method");}
            private void method3()
           {System.out.println("I am inner class private method");}
            }
        a obj=new a();
        obj.method1();
        obj.method2();
       obj.method3();
        
    }
}