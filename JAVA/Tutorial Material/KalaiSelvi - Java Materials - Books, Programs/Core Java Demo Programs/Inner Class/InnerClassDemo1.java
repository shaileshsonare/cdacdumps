class AAA
{
	abstract class BBB
	{
		abstract void bbb1();
		public void bbb()
		{
			System.out.println("bbb");
		}
		
	}
	
	final class FinalClass
	{
		
	}
	void bbb1()
	{
		System.out.println("aaa");
	}
	
	public void access()
	{
		AAA a =  new AAA();
		a.bbb1();
				
		
	}
}
public class InnerClassDemo {
      class One
      {
    	  public void innermethod()
    	  {
    		  System.out.println("inner class method");
    	  }
    	  
    	  private void privatemethod()
          {
        	  System.out.println("private method");
          }
          
         	
      }
      public void outermethod()
      {
    	  System.out.println("outer method calling inner class method");
      One one = new One();
      one.innermethod();
      one.privatemethod();
     
      }
      
     
	public static void main(String[] args) {
		
		new InnerClassDemo().outermethod();
		
		One obj=new InnerClassDemo().new One();
		obj.innermethod();
		obj.privatemethod();
		
		
		

	}

}
