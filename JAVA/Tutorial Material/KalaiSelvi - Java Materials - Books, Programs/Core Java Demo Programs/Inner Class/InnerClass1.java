public class InnerClass1
{
      
   // member variables access modifier can only be final
    public void outermethod(){ final int a1=10;  
                class a{ 
                    int aa=90;
                    //methods access modifier cannot be static because it can be accesses only in static /  toplevel type
           a(){System.out.println("I am inner class constructor");}
            public void method1()
            {System.out.println("I am inner class public method");
            System.out.println("Value of a = "+a1);}
            final void method2()
            {System.out.println("I am inner class static method");}
            private void method3()
           {System.out.println("I am inner class private method");}
           
        }
                a obj=new a();
               obj.method1();
               obj.method2();
               obj.method3();}
        public static void main(String[] args)
        {
       InnerClass1 obj=new InnerClass1();
       obj.outermethod();
        
    }
        }