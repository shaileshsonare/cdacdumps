public class PrivateInnerClass {
       private class PrivateClass
       {
    	   public void add()
    	   {
    		   System.out.println("add");
    	   }
    	   
    	   private void sub()
    	   {
    		   System.out.println("sub");
    	   }
    	
       }
	public static void main(String[] args) {
		PrivateInnerClass.PrivateClass obj = new PrivateInnerClass().new PrivateClass();
		obj.add();
		obj.sub();

	}

}
