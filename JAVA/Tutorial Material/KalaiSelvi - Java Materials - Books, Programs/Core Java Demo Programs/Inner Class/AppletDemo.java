import java.awt.TextField;
import java.awt.Button;
import java.applet.Applet;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class AppletDemo extends Applet{
	
	TextField txtValue1, txtValue2, txtResult;
	
	public void init()
	{
		System.out.println("Initialized");
	}
	
	public void start()
	{
		txtValue1 = new TextField(15);
		txtValue2 = new TextField(15);
		txtResult = new TextField(15);
		add(txtValue1);
		add(txtValue2);
		add(txtResult);
		txtValue2.addFocusListener(new FocusAdapter()
		{
			public void focusLost(FocusEvent fe) {
			      if (fe.getSource().equals(txtValue2))
			      {
			    	  System.out.println("dsfd");
			    	  int v1 = Integer.parseInt(txtValue1.getText().toString());
			    	  int v2 = Integer.parseInt(txtValue2.getText().toString());
			    	  Integer res = v1+v2;
			    	  txtResult.setText(res.toString());  
			      }
			}
			
		});
		
	}
	

}
