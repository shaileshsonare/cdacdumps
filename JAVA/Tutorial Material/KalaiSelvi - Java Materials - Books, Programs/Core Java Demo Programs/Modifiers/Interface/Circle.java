interface shape
{
		void draw();
		void erase();
}
class  Circle implements Shape
{
	public void draw()
	{
			System.out.println(" Draw a circle");
	}
	public void erase()
	{
			System.out.println(" erase a circle");
	}
	public void color()
	{
			System.out.println(" Fill color to circle");
	}

	public static void main(String[] args) 
	{
		/*Shape sh=new Shape();
				sh.draw();
				sh.erase();
				sh.color();
			*/
			Circle cl=new Circle();
					cl.draw();
					cl.erase();
					cl.color();
			/* Shape sh1=new Circle();
				sh1.draw();
				sh1.erase();
				sh1.color();
	*/					
	}
}
