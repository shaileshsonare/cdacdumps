/*

* ThisDemo.java

* The code demonstrates the usage of this keyword

*/

public class ThisDemo{
    private int data;
    public ThisDemo(){
        System.out.println("Default Constructor");
    }
    public ThisDemo(int data){
        this();//invoking default constructor
        System.out.println("Overloaded Constructor"); 
        this.data=data;//to avoid name conflict,instance variable referred with this keyword
        System.out.println("The value assigned "+data); 
    }
    public static void main(String args[]){
        ThisDemo object=new ThisDemo(100);
    }
} 