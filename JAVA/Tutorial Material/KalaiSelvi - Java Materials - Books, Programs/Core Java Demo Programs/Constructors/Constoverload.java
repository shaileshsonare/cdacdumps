package com.vam.cons;

public class Constoverload {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hyundai hyn = new Hyundai("green");
	}
}

class Car {
	Car() {

	}

	Car(String color) {
		System.out.println("car constructor2");
	}
}

class Hyundai extends Car {
	Hyundai(String color) {
		super("blue");
		System.out.println("hyundai constructor");
	}
}

class Bike{
	String bikeName;
	double bikePrice;
	String bikeColor;
	
	void driveBike(){
		System.out.println("drive bike");
	}
	
	void checkFuel(){
		System.out.println("check fuel");
	}
	
}