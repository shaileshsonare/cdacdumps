import java.util.Scanner;
public class Example_6_6 {
    public static void main(String args[]) {
        String sentence = "Banks, Rob, 34, Ottawa, 12.67";
        Scanner s = new Scanner(sentence);
        s.useDelimiter(", ");
        System.out.println(s.next());
        System.out.println(s.next());
        System.out.println(s.nextInt());
        System.out.println(s.next());
        System.out.println(s.nextFloat());
        s.close();
    }
}
