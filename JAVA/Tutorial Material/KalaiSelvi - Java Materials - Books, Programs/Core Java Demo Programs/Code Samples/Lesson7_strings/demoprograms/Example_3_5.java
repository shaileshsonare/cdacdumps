public class Example_3_5 {
    public static void main(String[] args) {

        String copyFromMe = "Copy this string until you encounter the letter 'g'.";
        StringBuffer copyToMe = new StringBuffer();

        int i = 0;
      char c;
        do {
         c = copyFromMe.charAt(i);

            copyToMe.append(c);
            c = copyFromMe.charAt(++i);
        } while (c != 'g');
        System.out.println(copyToMe);
    }
}
