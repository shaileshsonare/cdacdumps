// Interface
interface Account 
{
	public boolean isSavings();
	public boolean isCurrent();
}

//Class to find Account Type
class findAccount
{
	static String accountType(Account act)
	{
		if((act.isSavings()==true)&&(act.isCurrent()==false))
			return "This is a Savings Account";
		else if((act.isSavings()==false)&&(act.isCurrent()==true))
			return "This is a Current Account";
		else
			return null;
	}
	
}

// Class Implementing Interface Account
public class EgInterface implements Account
{

	int actNo;				//Account Number
	boolean sav = false;	//Account Type Savings
	boolean cur = false;	//Account Type Current
	float bal;				//Account Balance
	
	//Constructor
	EgInterface(int actNo,boolean sav,boolean cur,float bal)
	{
		this.actNo=actNo;
		this.sav=sav;
		this.cur=cur;
		this.bal=bal;
	}
	
	// Returns True if Account is Savings
	public boolean isSavings()
	{
		return sav;
	}
	
//	 Returns True if Account is Current
	public boolean isCurrent()
	{
		return cur;
	}
	
	// Display account Details
	public void show()
	{
		
		System.out.println(" ");
		System.out.println("Account Details");
		System.out.println(" ");
		System.out.println("---------------------------------------");
		System.out.println("Account Number : " + this.actNo);
		//System.out.println(" " );
		System.out.println("Account Type : " + findAccount.accountType(this));
		//System.out.println(" " );
		System.out.println("Account Balance : " + this.bal);
		System.out.println("---------------------------------------");
	}
	public static void main(String[] args) 
	{
		// Creating An Account Current Type  
		EgInterface Eg1=new EgInterface(123,false,true,5000);
		Eg1.show();
		// Creating An Account Savings Type  
		EgInterface Eg2=new EgInterface(112,true,false,13650);
		Eg2.show();
	}
}

