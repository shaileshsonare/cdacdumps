import java.awt.*;
import java.awt.event.*;

public class Outer extends java.awt.Frame
{
    public Outer()
    {
        java.awt.Button button = new java.awt.Button("Click on me");
        button.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                System.out.println("You clicked");
            }
        });
    }
}
