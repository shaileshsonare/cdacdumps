
class Outer {
  int outer_x = 100;

  void test() {
    Inner inner = new Inner();
    System.out.println("inside outer calling inner display");
    inner.display();
  }

  // this is an innner class
  static class Inner {
    void display() {
      System.out.println("display: inside static class");
    }
  }
}

class Example_6 {
  public static void main(String args[]) {
    Outer outer = new Outer();
    outer.test();
    Outer.Inner n =new Outer.Inner();
    n.display();
  }
}

