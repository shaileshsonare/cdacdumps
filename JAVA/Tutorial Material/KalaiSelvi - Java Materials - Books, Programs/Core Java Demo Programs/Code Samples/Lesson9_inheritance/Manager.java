public class Manager extends employee {

	public float bonus;

	public Manager(int empId,String empname,float salary)
	{
		super(empId,empname,salary);
		}

	public float getManagerSalary()
	{
		float basesalary=super.getSalary();
		return salary+bonus;
	}

	public void setBonus(float b)
{
	bonus=b;
}

public static void main(String args[])
{
	Manager man1=new Manager(101,"Sri",12000.0f);
	man1.setBonus(5000.0f);

	System.out.println("New salary"+man1.getManagerSalary());
}
}
