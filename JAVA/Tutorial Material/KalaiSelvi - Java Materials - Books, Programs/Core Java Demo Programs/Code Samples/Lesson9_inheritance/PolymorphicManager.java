public class PolymorphicManager extends employee {

	public float bonus;

	public PolymorphicManager(int empId,String empname,float salary)
	{
		super(empId,empname,salary);
		bonus=0;
	}

	public float getSalary()
	{
		float basesalary=super.getSalary();
		return basesalary+bonus;
	}

	public void setBonus(float b)
{
	bonus=b;
}

public static void main(String args[])
{
	Manager man1=new Manager(101,"Sri",12000.0f);
	man1.setBonus(5000.0f);

	employee[] staff=new employee[4];
	staff[0]=man1;

	staff[1]=new employee(102,"ram",3400.00f);
	staff[2]=new employee(103,"raj",4500.0f);
//Manager man2=staff[2];  //error

	System.out.println("New salary"+staff[0].getSalary());
    staff[0].incSalary(.50f);
	System.out.println("Inc salary"+staff[0].getSalary());
	System.out.println("New salary"+staff[1].getSalary());
	//staff[1].setBonus(4000.0f);  //Error
}
}
