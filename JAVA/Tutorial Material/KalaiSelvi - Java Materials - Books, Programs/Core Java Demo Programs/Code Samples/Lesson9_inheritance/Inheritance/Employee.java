package JavaProject.Inheritance;
/**
 * @author Venugopal
 */

public class Employee extends Person{

	int employeeNumber;
	float salary;
	int employeeGrade;
	float Bonus;
	/**
	 * @return Returns the bonus.
	 */
	public float getBonus() {
		return Bonus;
	}
	/**
	 * @param bonus The bonus to set.
	 */
	public void setBonus(float bonus) {
		Bonus = bonus;
	}
	/**
	 * @return Returns the salary.
	 */
	public float getSalary() {
		return salary;
	}
	/**
	 * @param salary The salary to set.
	 */
	public void setSalary(float salary) {
		this.salary = salary;
	}
	/**
	 * @return Returns the employeeNumber.
	 */
	public int getEmployeeNumber() {
		return employeeNumber;
	}
	/**
	 * @param employeeNumber The employeeNumber to set.
	 */
	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	/**
	 * Default constructor
	 */
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return Returns the employeeGrade.
	 */
	public int getEmployeeGrade() {
		return employeeGrade;
	}
	/**
	 * @param employeeGrade The employeeGrade to set.
	 */
	public void setEmployeeGrade(int employeeGrade) {
		this.employeeGrade = employeeGrade;
	}
	
	
	
	
}
