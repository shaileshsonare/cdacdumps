/**
 * 
 */
package JavaProject.Inheritance;

/**
 * @author blrlc70
 *
 */
import java.io.*;
import java.util.ArrayList;
public class EmpDetails {
	
	
	public static void main(String args[]) throws IOException{
		int noOfEmp=0;
		int choice=0;
		String empname=null;
		String empAddress=null;
		int empno=0;
		int empGrade=0;
		float empSal=0;
		float bonus=0;
		Employee emp = null;
		int removeEmpo = 0;
		int mField=0;
		ArrayList EmpDet = new ArrayList();
		boolean flag=false;
		System.out.println("Enter Number of Employees:  ");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		try {
			noOfEmp = Integer.parseInt(in.readLine());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		while(choice!=5){
			flag=false;
				System.out.println("1. Enter new Employee: ");
				System.out.println("2. View Details of Employee: ");
				System.out.println("3. Calculate Bonus: ");
				System.out.println("6. Modify an Employee: ");
				System.out.println("7. Delete an Employee: ");
				System.out.println("8. View all Employee");
				System.out.println("5. Exit");
				
				System.out.println("Enter an option: ");
				try {
					choice = Integer.parseInt(in.readLine());
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				switch(choice){
					case 1:
						if(EmpDet.size()==noOfEmp){
							System.out.println("U can Enter only "+noOfEmp+" Employees");
						}
						else{
								emp = new Employee();
								System.out.println("Enter the name of the employee: ");
								try {
									empname = in.readLine();
								} catch (IOException e) {
								// TODO Auto-generated catch block
									e.printStackTrace();
								}
								emp.setName(empname);
								System.out.println("Enter the ID of the employee: ");
								try {
									empno = Integer.parseInt(in.readLine());
								} catch (IOException e) {
								// TODO Auto-generated catch block
									e.printStackTrace();
								}
								emp.setEmployeeNumber(empno);
								System.out.println("Enter the Address of the employee: ");
								try {
									empAddress = in.readLine();
								} catch (IOException e) {
								// TODO Auto-generated catch block
									e.printStackTrace();
								}
								emp.setAddress(empAddress);
								System.out.println("Enter the Grade of the employee: ");
								try {
									empGrade = Integer.parseInt(in.readLine());
									if(empGrade>3){
										System.out.println("Grade should be less than 3....Default grade 2 is set ");
										emp.setEmployeeGrade(2);
									}
									else{
										emp.setEmployeeGrade(empGrade);
									}
								} catch (IOException e) {
								// TODO Auto-generated catch block
									e.printStackTrace();
								}
								System.out.println("Enter Salary of the employee: ");
								try {
									empSal = Float.parseFloat(in.readLine());
								} catch (IOException e) {
								// TODO Auto-generated catch block
									e.printStackTrace();
								}
							
								emp.setSalary(empSal);
								emp.setBonus(bonus);
							
								EmpDet.add(emp);
						}		
					break;
							
					case 2:
								if(EmpDet.size()==0){
									System.out.println("There are no Employes to view");
								}
							else{
										System.out.println("Enter the employee ID to view: ");
										try {
											empno = Integer.parseInt(in.readLine());
										} catch (NumberFormatException e) {
										// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										for(int i=0;i<EmpDet.size();i++){
											emp = (Employee)EmpDet.get(i);
											System.out.println("The Employee number: "+empno);
											System.out.println("The Employee number====: "+emp.getEmployeeNumber());
											if(empno == emp.getEmployeeNumber()){
												flag=true;
												System.out.println(empno);
												break;
												
											}		
										}
										System.out.println("The Details of the Employee is:    ");
										if(flag==true){
											System.out.println("Name * * * * * * * * * * : "+emp.getName());
											System.out.println("Address * * * * * * * * : "+emp.getAddress());
											System.out.println("Salary * * * * * * * * * * *: "+emp.getSalary());
											System.out.println("Bonus * * * * * * * * * * * :  "+emp.getBonus());
										}
										else{
											System.out.println("There is no employee with employee number "+empno);
										}
								
								}	
					break;
							
					case 3: 	
									if(EmpDet.size()==0){
										System.out.println("There are no Employes to Calculate Bonus");
									}
									else{
											for(int i=0;i<EmpDet.size();i++){
												emp = (Employee)EmpDet.get(i);
												if(emp.getEmployeeGrade()== 1){
													bonus = emp.getSalary()+3000;
												}
												else if(emp.getEmployeeGrade()==2){
													bonus = emp.getSalary()+2000;
												}
												else if(emp.getEmployeeGrade()==3){
													bonus = emp.getSalary()+1000;
												}
												emp.setBonus(bonus);
												
											}
											System.out.println("Bonus is Successfully Calculated.......");
									}
					break;
								
					case 7:
								if(EmpDet.size()==0){
									System.out.println("There are no Employees to delete");
								}
								else{
									System.out.println("Enter the employee ID to Delete: ");
									try {
										empno = Integer.parseInt(in.readLine());
									} catch (NumberFormatException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									for(int i=0;i<EmpDet.size();i++){
										emp = (Employee)EmpDet.get(i);
										if(empno == emp.getEmployeeNumber()){
											flag=true;
											removeEmpo = i;
											
										}
									
									}	
									if(flag == true)	{
										EmpDet.remove(removeEmpo);
										System.out.println("Employee with employee number "+empno+" is deleted");
									}
									else{
										System.out.println("There is no employee with employee number "+empno);
									}
								}     
					break;
									
					case 6:
							if(EmpDet.size()==0){
								System.out.println("There are no Employes to modify");
							}
							else{
									System.out.println("Enter the employee ID to modify detail: ");
									try {
										empno = Integer.parseInt(in.readLine());
									} catch (NumberFormatException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									}
									for(int i=0;i<EmpDet.size();i++){
										emp = (Employee)EmpDet.get(i);
							
										if(empno == emp.getEmployeeNumber()){
											flag=true;
											System.out.println(empno);
											break;
								}
							}	
								if(flag == true){
									System.out.println("1: Modify Name:");
									System.out.println("2: Modify Salary: ");
									System.out.println("3: Modify Address: ");
									System.out.println("Which field do u want to modify: ");
									try {
										mField = Integer.parseInt(in.readLine());
									} catch (NumberFormatException e) {
									// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
									// TODO Auto-generated catch block
										e.printStackTrace();
									}
									switch(mField){
								
									case 1: 
												System.out.println("Enter new Name: ");
												empname = in.readLine();
												emp.setName(empname);
									break;
									case 2:
											System.out.println("Enter new Salary: ");
											empSal = Float.parseFloat(in.readLine());
											emp.setSalary(empSal);
											emp.setBonus(0);
									break;
								case 3:
											System.out.println("Enter new Address: ");
											empAddress = in.readLine();
											emp.setAddress(empAddress);
								break;
								
								default:
									System.out.println("Wrong Choice Pls Try Again....");
						       }
							}
							else{
									System.out.println("There is no employee with employee number "+empno);
							}
						}		
				break;
								
										
				case 5:
								System.out.println("Program is Termintated");
								System.exit(0);
				break;
				
									
					default: 
								System.out.println("Wrong Choice Try Again.....");
				}		
					
	
		}
	}	
	}
