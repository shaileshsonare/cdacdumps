package JavaProject.Inheritance;

public class Customer {

	/**
	 * @author enuGopal
	 */
	
	String[] itemsPurchased;
	String[] purchaseHistory;
	/**
	 * Default Constructor
	 */
	public Customer() {
		
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return Returns the itemsPurchased.
	 */
	public String[] getItemsPurchased() {
		return itemsPurchased;
	}
	/**
	 * @param itemsPurchased The itemsPurchased to set.
	 */
	public void setItemsPurchased(String[] itemsPurchased) {
		this.itemsPurchased = itemsPurchased;
	}
	/**
	 * @return Returns the purchaseHistory.
	 */
	public String[] getPurchaseHistory() {
		return purchaseHistory;
	}
	/**
	 * @param purchaseHistory The purchaseHistory to set.
	 */
	public void setPurchaseHistory(String[] purchaseHistory) {
		this.purchaseHistory = purchaseHistory;
	}
	
	
	

}
