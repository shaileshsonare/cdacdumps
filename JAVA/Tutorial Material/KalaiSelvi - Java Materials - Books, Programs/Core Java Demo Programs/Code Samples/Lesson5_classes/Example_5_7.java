 class Book {

	private int bookno;
	private String bookname;
	private String auhtorname;

	public String getAuthorname() {
		return auhtorname;
	}
	public void setAuthorname(String auhtorname) {
		this.auhtorname = auhtorname;
	}
	public String getBookname() {
		return bookname;
	}
	public void setBookname(String bookname) {
		this.bookname = bookname;
	}
	public int getBookno() {
		return bookno;
	}
	public void setBookno(int bookno) {
		this.bookno = bookno;
	}


}

class Example_5_7 {
  public static void main(String args[]) {
    Book book1 = new Book();

    book1.setBookno(10);
    book1.setAuthorname("gosling");
    book1.setBookname("java");

     System.out.println("Book Number :" + book1.getBookno());

    System.out.println("Book Name : " +book1.getBookname());
    System.out.println("Book Name : " +book1.getAuthorname());
  }
}

