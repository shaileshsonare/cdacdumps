// Invoking one overloaded constructor from another
public class Flower{
  int petalCount = 0;
  String s = new String("null");

  Flower(int petals) {
    petalCount = petals;
    System.out.println("Constructor w/ int arg only, petalCount= "
      + petalCount);
  }
  Flower(String s, int petals) {
    this(petals);

    this.s = s; // Another use of "this"
    System.out.println("String & int args"+s);
  }
  Flower() {
    this("hi", 47);
    System.out.println("default constructor (no args)");
  }
  void print() {

    System.out.println(
      "petalCount = " + petalCount + " s = "+ s);
  }
  public static void main(String[] args) {
    Flower x = new Flower();
    Flower y=  new Flower("hello",67);
    x.print();
    y.print();
  }
}

