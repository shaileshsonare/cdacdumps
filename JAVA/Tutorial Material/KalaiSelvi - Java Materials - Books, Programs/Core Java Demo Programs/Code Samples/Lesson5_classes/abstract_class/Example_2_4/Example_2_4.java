
class Cat extends Mammal {
private int age;
public static final int MAX_LIFE = 25;
public Cat(String iName, int iAge) {
super(iName);
setAge(iAge);
}
public boolean setAge(int nAge) {
if (nAge >= 0 && nAge <= MAX_LIFE) {
age = nAge;
return true;
}
else
return false;
}
public int getAge() {
return age;
}
public String toString() {
return getName() + " (cat), " + age;
}
}

class Mouse extends Mammal {
private int age;
public static final int MAX_LIFE = 5;
public Mouse(String iName, int iAge) {
super(iName);
setAge(iAge);
}
public boolean setAge(int nAge) {
if (nAge >= 0 && nAge <= MAX_LIFE) {
age = nAge;
return true;
}
else
return false;
}
public int getAge() {
return age;
}
public String toString() {
return this.getName() + " (mouse), " + age;
}
}

public class Example_2_4 {
public static void main(String[] args) {

// Declare an array of superclass object references.

Mammal[] pets = new Mammal[5];

// Load the array with objects that descend from the superclass.

pets[0] = new Cat("Fluffy", 10);
pets[1] = new Mouse("Speedy", 1);
pets[2] = new Mouse("Mickey", 2);
pets[3] = new Cat("Tiger", 3);
pets[4] = new Cat("Garfield", 15);

// Use polymorphism to display the contents of the array.


for (int i = 0; i < pets.length; i++) {
System.out.println(" " + pets[i].toString());
}
}
}

