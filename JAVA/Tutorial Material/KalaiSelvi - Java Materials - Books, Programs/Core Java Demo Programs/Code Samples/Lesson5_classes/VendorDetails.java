class VendorDetails {

	public String vendorname;
	public String vendorcode;
	public double contactnumber;
	public String itemsupplied;


	public VendorDetails()
	{
		this("abc","123");

		}

	public VendorDetails(String vname,String vcode)
	{
		    this(vname,234444,"statonery");
		   this.vendorname=vname;
		   this.vendorcode=vcode;

	   }

	   public VendorDetails(String vname,double cno, String isup)
	   {
		      this.vendorname=vname;
		      this.contactnumber=cno;
		      this.itemsupplied=isup;

		  }

        public void print()
        {
			 System.out.println("Vendor Name :" +vendorname);
			 System.out.println("Vendor Code :" +vendorcode);
			 System.out.println("Item Supplied :"+itemsupplied);
			 System.out.println("Contact Number :"+contactnumber);
	  }

}


