// same package non sub class
package com.training;

public class test2
{

public static void main(String args[])
{
	Example_Access1 ex1 = new Example_Access1();

	   ex1. prtdisplay(); //can access
     ex1.pvtdisplay();  // no access to private members
    ex1.pubdisplay(); //can access
      ex1.defltdisplay(); //can access
  }

}
