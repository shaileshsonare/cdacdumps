// diff package non sub class - can only access public
package com.java.training;

import com.training.*;


public class test4
{

public static void main(String args[])
{
	Example_Access1 ex1 = new Example_Access1();

	   //ex1. prtdisplay(); // no access
       // ex1.pvtdisplay(); // no access
    ex1.pubdisplay();
      //ex1.defltdisplay(); // no access
  }

}
