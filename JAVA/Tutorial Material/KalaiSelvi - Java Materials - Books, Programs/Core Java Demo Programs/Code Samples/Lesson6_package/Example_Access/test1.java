// same package sub class- no access to private members
package com.training;

public class test1 extends Example_Access1
{

public static void main(String args[])
{
	Example_Access1 ex1 = new Example_Access1();

	   ex1. prtdisplay();  //can access
        // ex1.pvtdisplay(); // cannot access private members
    ex1.pubdisplay(); //can access
      ex1.defltdisplay();  //can access
  }

}
