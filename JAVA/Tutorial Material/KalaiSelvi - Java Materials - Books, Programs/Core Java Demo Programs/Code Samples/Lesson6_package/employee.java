package com.training;
public class employee{
	int empId;
	String empName;
	float salary;

	public employee(int empid, String empname,float salary)
	{
		this.empId=empid;
		this.empName=empname;
		this.salary=salary;
	}

	public String getEmpName()
	{
		return empName;
	}

	public float getSalary()
	{ return salary;
}

public void incSalary(float percent)
{
	 float raiseSalary= salary * percent;
	 salary+=raiseSalary;
 }
}




