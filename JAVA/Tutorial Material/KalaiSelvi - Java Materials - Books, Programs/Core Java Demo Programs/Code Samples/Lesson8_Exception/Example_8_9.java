class MyExeception extends Exception {

	private int detail;

	MyExeception(int a) {

		detail=a;
	}

	public String toString() {

		return "My Exception["+detail+"]";
	}

}

class Example_8_9 {

	static void compute(int a) throws MyExeception{

		System.out.println("Called compute("+a+")");
		if (a>10)
		throw new MyExeception(a);

		System.out.println("Normal Exit");
	}

	public static void main(String args[]) {
		try {
			compute(1);
			compute(20);
		} catch(MyExeception e) {

			System.out.println("caught" +e);
 e.printStackTrace();
 }

	}

}

