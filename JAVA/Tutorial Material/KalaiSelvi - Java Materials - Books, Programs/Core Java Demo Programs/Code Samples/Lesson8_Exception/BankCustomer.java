
/* Simple example to explain System Defined Exceptions */

import java.io.*;

public class BankCustomer
{
	int custId;
	String custpass;
	int balance;
	static int i=0;
	
	public void setcustId(int custId)
	{
		this.custId=custId;
	}	
	public void setcustpass(String custpass)
	{
		this.custpass=custpass;
	}	
	public void setcustbalance(int balance)
	{
		this.balance=balance;
	}
	
	public static void main(String args[]) throws IOException
	{
		BankCustomer cust[] = new BankCustomer[5];
		for(int i=0;i<5;i++)
		{
			cust[i] = new BankCustomer();
		}
		
		BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
		boolean flag=true;
		int choice;
		int i=0;
		do{
			try{
			System.out.println("Choose ur option: 1.Create a new customer");
			System.out.println("                  2.Check the balance");
			System.out.println("                  3.Exit");
			choice=Integer.parseInt(in.readLine());
			}
			catch(NumberFormatException ne)
			{
				ne.printStackTrace();
				continue;
			}
	
			switch(choice)
			{
				case 1:		
					try						//try block for IO EXception.
					{						
						try					// try block for ArrayIndexOutOfBoundsException.
						{
							try				// try block for NumberFormatException.
							{
								System.out.println("Enter the Id of Customer");
								int id=Integer.parseInt(in.readLine());
								cust[i].setcustId(id);
							}
							catch(NumberFormatException ne)
							{
								System.out.println("invalid format");
								ne.printStackTrace();
								continue;
							}
						}
						catch(ArrayIndexOutOfBoundsException ae)
						{
							System.out.println("the array out of bounds");
							ae.printStackTrace();
							break;
						}
						try
						{							
							System.out.println("Enter the Password of Customer");
							String pass=in.readLine();
							cust[i].setcustpass(pass);
						}
						catch(ArrayIndexOutOfBoundsException ae)
						{
							System.out.println("the array out of bounds");
							ae.printStackTrace();
							break;
						}
						try
						{
							try
							{
								System.out.println("Enter the Balance of Customer");
								int bal=Integer.parseInt(in.readLine());
								cust[i].setcustbalance(bal);
							}
							catch(NumberFormatException ne)
							{
								System.out.println("invalid format");
								ne.printStackTrace();
								continue;
							}
						}
						catch(ArrayIndexOutOfBoundsException ae)
						{
							System.out.println("the array out of bounds");
							ae.printStackTrace();
							break;
						}
					}
					catch(IOException e)
					{
						System.out.println(" io exception");
						e.printStackTrace();
					}
					break;
		
				case 2:
					try
					{	
						int id = 0;
						try
						{
						System.out.println(" Enter Your ID");
						id=Integer.parseInt(in.readLine());
						}
						catch(NumberFormatException ne)
						{
							System.out.println("invalid format");
							ne.printStackTrace();
							continue;
						}

						try
						{
							for(int j=0;j<5;j++)
							{
								
									if(id==cust[j].custId)
									{
										System.out.println("Enter Your Password");
										String pass=in.readLine();
										if(pass.equals(cust[j].custpass))
										{
											System.out.println("Your Balance is : " +cust[j].balance);
											break;
										}
										else
										{
											System.out.println("Invalid User Id or password.");
											break;
										}
									}
																
							}
						}
						catch(ArrayIndexOutOfBoundsException ae)
						{
							System.out.println("the array out of bounds");
							ae.printStackTrace();
							break;
						}
					break;
					}
					catch(IOException e)
					{
						System.out.println("io exception");
						e.printStackTrace();
					}
				case 3:
					System.exit(0);
					break;
				default:
					flag=false;
					System.out.println("Invalid choice");
			}	
			i++;
		}while(flag);
		
	}
}