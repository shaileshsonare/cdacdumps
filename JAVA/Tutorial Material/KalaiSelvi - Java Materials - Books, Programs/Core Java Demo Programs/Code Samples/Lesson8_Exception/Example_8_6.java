class Example_8_6 {
	static void procA(){
		try {

			System.out.println("inside procA");
			throw new RuntimeException("d");
		} finally {
			System.out.println("procA finally");
		}
	}

static void procB(){

		try {

			System.out.println("inside procB");

		} finally {
			System.out.println("procB finally");
		}
	}

	static void procC(){

			try {

				System.out.println("inside procC");

			} finally {
				System.out.println("procC finally");
			}
	}


	public static void main(String args[]) {

		try {

			   procA();
		   } catch (Exception e) {

			  // System.out.println("Exception caught");
			  e.printStackTrace();
		   }

		   procB();
		   procC();

	   }


  }