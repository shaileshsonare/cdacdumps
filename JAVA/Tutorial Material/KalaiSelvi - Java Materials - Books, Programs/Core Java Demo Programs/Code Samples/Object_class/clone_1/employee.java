class employee implements Cloneable {
	private int empno;
	private String empname;

	public void setData(int empno,String empname)
	{
		this.empno=empno;
		this.empname=empname;
	}

	public String toString()
	{
		return empno+" :" +empname;
	}

	public Object clone()
	{
		try {
		return super.clone( );
		} catch (CloneNotSupportedException e ) {
		throw new Error("This should never happen!");
	}
}
}

