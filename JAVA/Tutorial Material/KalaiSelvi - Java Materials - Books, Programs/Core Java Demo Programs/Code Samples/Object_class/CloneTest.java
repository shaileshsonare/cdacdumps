import java.util.*;

class Manager implements Cloneable{
	private String name;
	private double salary;
	private GregorianCalendar  hireDay;

	public Manager(String n,double s)
	{ name = n;
	  salary = s;
  }

  public Object clone() throws CloneNotSupportedException
  {
	  Manager cloned =(Manager)super.clone();

	  cloned.hireDay = (GregorianCalendar)hireDay.clone();

	  return cloned;
  }

  public void setHireDay(int year, int month,int day)
  {
	  hireDay = new GregorianCalendar(year,month-1,day);
  }
  public String toString()
  {
	  Calendar calendar = hireDay;
	  return name +" "+salary + " "+calendar.get(Calendar.DAY_OF_MONTH);
  }
}
public class CloneTest {

	public static void main(String args[])
	{
		try{
			Manager original = new Manager("ram",50000);
			original.setHireDay(2006,8,12);
			Manager copy = (Manager)original.clone();
			System.out.println(original);
			System.out.println(copy);
			} catch(CloneNotSupportedException e) {e.printStackTrace();}
	}

}