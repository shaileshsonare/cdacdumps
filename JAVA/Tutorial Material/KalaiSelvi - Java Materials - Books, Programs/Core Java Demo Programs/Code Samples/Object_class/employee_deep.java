class employee_deep implements Cloneable {
	public int empno;
	public String empname;

	public void setData(int empno,String empname)
	{
		this.empno=empno;
		this.empname=empname;
	}

	public String toString()
	{
		return empno+" :" +empname;
	}

	public Object clone() throws CloneNotSupportedException
	{
employee_deep copy = (employee_deep)super.clone( );
return copy;
}
}
