import java.io.DataInputStream;
import java.io.IOException;

class Employee
{
     int empCode;
     String name;
     public Employee(int ecode, String name)
     {    this.empCode = ecode;
          this.name = name;
              	 
     }
}

class Department
{   int deptCode;
    String name;
    Department(int dcode, String dname,Employee emp)
    {   this.deptCode = dcode;
        this.name=dname;
    	System.out.println( "Employee id of "+emp.name+ " is "+emp.empCode);
    	System.out.println( emp.name+ " works for "+ this.deptCode);
    }
    
	
}

public class CallByReference{
	public static void main(String[] args) {
       int code;
       String name;
       System.out.println("Enter the Employee ");
       	try{
    		System.out.println("Employee Code: ");
    	    DataInputStream d = new DataInputStream(System.in);    
            code=Integer.parseInt(d.readLine());
            System.out.print("Employee Name: ");
            name=d.readLine();
            Employee empobj=new Employee(code,name);
            Department deptobj=new Department(12,"Finance",empobj);
            
           }catch(IOException ioe)
		  {System.out.println("Error " + ioe);}
       }
	}
