.  What will be the output of the program? 

class MyThread extends Thread 
{
    MyThread() 
    {
        System.out.print(" MyThread");
    }
    public void run() 
    {
        System.out.print(" bar");
    }
    public void run(String s) 
    {
        System.out.println(" baz");
    }
}
public class TestThreads 
{
    public static void main (String [] args) 
    {
        Thread t = new MyThread() 
        {
            public void run() 
            {
                System.out.println(" foo");
            }
        };
        t.start();
    }
}


 
A. foo B. MyThread foo 
C. MyThread bar D. foo bar 
 
Answer & ExplanationAnswer: Option B

Explanation:


Option B is correct because in the first line of main we're constructing an instance of an anonymous inner class extending from MyThread. So the MyThread constructor runs and prints "MyThread". The next statement in main invokes start() on the new thread instance, which causes the overridden run() method (the run() method defined in the anonymous inner class) to be invoked, which prints "foo"


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
2.  What will be the output of the program? 

class MyThread extends Thread 
{
    public static void main(String [] args) 
    {
        MyThread t = new MyThread();
        t.start();
        System.out.print("one. ");
        t.start();
        System.out.print("two. ");
    }
    public void run() 
    {
        System.out.print("Thread ");
    }
}


 
A. Compilation fails 
B. An exception occurs at runtime. 
C. It prints "Thread one. Thread two." 
D. The output cannot be determined. 
 
Answer & ExplanationAnswer: Option B

Explanation:


When the start() method is attempted a second time on a single Thread object, the method will throw an IllegalThreadStateException (you will not need to know this exception name for the exam). Even if the thread has finished running, it is still illegal to call start() again.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
3.  What will be the output of the program? 

class MyThread extends Thread 
{ 
    MyThread() {} 
    MyThread(Runnable r) {super(r); } 
    public void run() 
    { 
        System.out.print("Inside Thread ");
    } 
} 
class MyRunnable implements Runnable 
{ 
    public void run() 
    { 
        System.out.print(" Inside Runnable"); 
    } 
} 
class Test 
{  
    public static void main(String[] args) 
    { 
        new MyThread().start(); 
        new MyThread(new MyRunnable()).start(); 
    } 
}


 
A. Prints "Inside Thread Inside Thread" 
B. Prints "Inside Thread Inside Runnable" 
C. Does not compile 
D. Throws exception at runtime 
 
Answer & ExplanationAnswer: Option A

Explanation:


If a Runnable object is passed to the Thread constructor, then the run method of the Thread class will invoke the run method of the Runnable object.

In this case, however, the run method in the Thread class is overridden by the run method in MyThread class. Therefore the run() method in MyRunnable is never invoked.

Both times, the run() method in MyThread is invoked instead. 


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
4.  What will be the output of the program? 

class s1 implements Runnable 
{ 
    int x = 0, y = 0; 
    int addX() {x++; return x;} 
    int addY() {y++; return y;} 
    public void run() { 
    for(int i = 0; i < 10; i++) 
        System.out.println(addX() + " " + addY()); 
} 
    public static void main(String args[]) 
    { 
        s1 run1 = new s1(); 
        s1 run2 = new s1(); 
        Thread t1 = new Thread(run1); 
        Thread t2 = new Thread(run2); 
        t1.start(); 
        t2.start(); 
    } 
}


 
A. Compile time Error: There is no start() method 
B. Will print in this order: 1 1 2 2 3 3 4 4 5 5... 
C. Will print but not exactly in an order (e.g: 1 1 2 2 1 1 3 3...) 
D. Will print in this order: 1 2 3 4 5 6... 1 2 3 4 5 6... 
 
Answer & ExplanationAnswer: Option C

Explanation:


Both threads are operating on different sets of instance variables. If you modify the code of the run() method to print the thread name it will help to clarify the output:

public void run() 
{ 
    for(int i = 0; i < 10; i++) 
        System.out.println( Thread.currentThread().getName() + ": " + addX() + " " + addY()); 
}


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
5.  What will be the output of the program? 

public class Q126 implements Runnable 
{ 
    private int x; 
    private int y; 

    public static void main(String [] args) 
    { 
        Q126 that = new Q126(); 
        (new Thread(that)).start( ); /* Line 8 */
        (new Thread(that)).start( ); /* Line 9 */
    } 
    public synchronized void run( ) /* Line 11 */
    { 
        for (;;) /* Line 13 */
        { 
            x++; 
            y++; 
            System.out.println("x = " + x + "y = " + y); 
        } 
    } 
}


 
A. An error at line 11 causes compilation to fail 
B. Errors at lines 8 and 9 cause compilation to fail. 
C. The program prints pairs of values  
 
