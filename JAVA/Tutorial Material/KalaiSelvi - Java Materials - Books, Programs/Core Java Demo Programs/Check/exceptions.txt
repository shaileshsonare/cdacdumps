 What will be the output of the program? 

public class Foo 
{  
    public static void main(String[] args) 
    {
        try 
        { 
            return; 
        } 
        finally 
        {
            System.out.println( "Finally" ); 
        } 
    } 
}


 
A. Finally 
B. Compilation fails. 
C. The code runs with no output. 
D. An exception is thrown at runtime. 
 
Answer & ExplanationAnswer: Option A

Explanation:

If you put a finally block after a try and its associated catch blocks, then once execution enters the try block, the code in that finally block will definitely be executed except in the following circumstances: 

An exception arising in the finally block itself. 
The death of the thread. 
The use of System.exit() 
Turning off the power to the CPU. 
I suppose the last three could be classified as VM shutdown.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
2.  What will be the output of the program? 

try 
{ 
    int x = 0; 
    int y = 5 / x; 
} 
catch (Exception e) 
{
    System.out.println("Exception"); 
} 
catch (ArithmeticException ae) 
{
    System.out.println(" Arithmetic Exception"); 
} 
System.out.println("finished");


 
A. finished B. Exception 
C. Compilation fails. D. Arithmetic Exception 
 
Answer & ExplanationAnswer: Option C

Explanation:


Compilation fails because ArithmeticException has already been caught. ArithmeticException is a subclass of java.lang.Exception, by time the ArithmeticException has been specified it has already been caught by the Exception class.

If ArithmeticException appears before Exception, then the file will compile. When catching exceptions the more specific exceptions must be listed before the more general (the subclasses must be caught before the superclasses).


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
3.  What will be the output of the program? 

public class X 
{  
    public static void main(String [] args) 
    {
        try 
        {
            badMethod();  
            System.out.print("A"); 
        }  
        catch (Exception ex) 
        {
            System.out.print("B");  
        } 
        finally 
        {
            System.out.print("C"); 
        } 
        System.out.print("D"); 
    }  
    public static void badMethod() 
    {
        throw new Error(); /* Line 22 */
    } 
}


 
A. ABCD 
B. Compilation fails. 
C. C is printed before exiting with an error message. 
D. BC is printed before exiting with an error message. 
 
Answer & ExplanationAnswer: Option C

Explanation:


Error is thrown but not recognised line(22) because the only catch attempts to catch an Exception and Exception is not a superclass of Error. Therefore only the code in the finally statement can be run before exiting with a runtime error (Exception in thread "main" java.lang.Error). 


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
4.  What will be the output of the program? 

public class X 
{  
    public static void main(String [] args) 
    {
        try 
        {
            badMethod();  
            System.out.print("A");  
        } 
        catch (RuntimeException ex) /* Line 10 */
        { 
            System.out.print("B"); 
        } 
        catch (Exception ex1) 
        { 
            System.out.print("C"); 
        } 
        finally 
        {
            System.out.print("D"); 
        } 
        System.out.print("E"); 
    } 
    public static void badMethod() 
    { 
        throw new RuntimeException(); 
    } 
}


 
A. BD B. BCD 
C. BDE D. BCDE 
 
Answer & ExplanationAnswer: Option C

Explanation:


A Run time exception is thrown and caught in the catch statement on line 10. All the code after the finally statement is run because the exception has been caught.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
5.  What will be the output of the program? 

public class RTExcept 
{
    public static void throwit () 
    {
        System.out.print("throwit ");
        throw new RuntimeException();
    }
    public static void main(String [] args) 
    {
        try 
        {
            System.out.print("hello ");
            throwit();
        }
        catch (Exception re ) 
        {
            System.out.print("caught ");
        }
        finally 
        {
            System.out.print("finally ");
        }
        System.out.println("after ");
    }
}


 
A. hello throwit caught 
B. Compilation fails 
C. hello throwit RuntimeException caught after 
D. hello throwit caught finally after 
 
Answer & ExplanationAnswer: Option D

Explanation:


The main() method properly catches and handles the RuntimeException in the catch block, finally runs (as it always does), and then the code returns to normal.

A, B and C are incorrect based on the program logic described above. Remember that properly handled exceptions do not cause the program to stop executing.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  
6.  What will be the output of the program? 

public class Test 
{  
    public static void aMethod() throws Exception 
    {
        try /* Line 5 */
        {
            throw new Exception(); /* Line 7 */
        } 
        finally /* Line 9 */
        {
            System.out.println("finally"); /* Line 11 */
        } 
    } 
    public static void main(String args[]) 
    {
        try 
        {
            aMethod();  
        } 
        catch (Exception e) /* Line 20 */
        {
            System.out.println("exception"); 
        } 
        System.out.println("finished"); /* Line 24 */
    } 
}


 
A. finally 
B. exception finished 
C. finally exception finished 
D. Compilation fails 
 
Answer & ExplanationAnswer: Option C

Explanation:


This is what happens: 


(1) The execution of the try block (line 5) completes abruptly because of the throw statement (line 7).

(2) The exception cannot be assigned to the parameter of any catch clause of the try statement therefore the finally block is executed (line 9) and "finally" is output (line 11).

(3) The finally block completes normally, and then the try statement completes abruptly because of the throw statement (line 7).

(4) The exception is propagated up the call stack and is caught by the catch in the main method (line 20). This prints "exception".

(5) Lastly program execution continues, because the exception has been caught, and "finished" is output (line 24).


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
7.  What will be the output of the program? 

public class X 
{ 
    public static void main(String [] args) 
    {
        try 
        {
            badMethod();  
            System.out.print("A"); 
        }  
        catch (Exception ex) 
        {
            System.out.print("B"); 
        }  
        finally 
        {
            System.out.print("C"); 
        }  
        System.out.print("D"); 
    }  
    public static void badMethod() {} 
} 


 
A. AC B. BC 
C. ACD D. ABCD 
 
Answer & ExplanationAnswer: Option C

Explanation:


There is no exception thrown, so all the code with the exception of the catch statement block is run.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
8.  What will be the output of the program? 

public class X 
{  
    public static void main(String [] args) 
    {
        try 
        {
            badMethod(); /* Line 7 */
            System.out.print("A"); 
        } 
        catch (Exception ex) /* Line 10 */
        {
            System.out.print("B"); /* Line 12 */
        } 
        finally /* Line 14 */
        {
            System.out.print("C"); /* Line 16 */
        }  
        System.out.print("D"); /* Line 18 */
    } 
    public static void badMethod() 
    {
        throw new RuntimeException(); 
    } 
}


 
A. AB B. BC 
C. ABC D. BCD 
 
Answer & ExplanationAnswer: Option D

Explanation:


(1) A RuntimeException is thrown, this is a subclass of exception.

(2) The exception causes the try to complete abruptly (line 7) therefore line 8 is never executed.

(3) The exception is caught (line 10) and "B" is output (line 12)

(4) The finally block (line 14) is always executed and "C" is output (line 16).

(5) The exception was caught, so the program continues with line 18 and outputs "D".


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
9.  What will be the output of the program? 

public class MyProgram 
{
    public static void main(String args[])
    {
        try 
        {
            System.out.print("Hello world ");
        }
        finally 
        {
            System.out.println("Finally executing ");
        }
    }
}


 
A. Nothing. The program will not compile because no exceptions are specified. 
B. Nothing. The program will not compile because no catch clauses are specified. 
C. Hello world. 
D. Hello world Finally executing 
 
Answer & ExplanationAnswer: Option D

Explanation:


Finally clauses are always executed. The program will first execute the try block, printing Hello world, and will then execute the finally block, printing Finally executing.

Option A, B, and C are incorrect based on the program logic described above. Remember that either a catch or a finally statement must follow a try. Since the finally is present, the catch is not required.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
10.  What will be the output of the program? 

class Exc0 extends Exception { } 
class Exc1 extends Exc0 { } /* Line 2 */
public class Test 
{  
    public static void main(String args[]) 
    { 
        try 
        {  
            throw new Exc1(); /* Line 9 */
        } 
        catch (Exc0 e0) /* Line 11 */
        {
            System.out.println("Ex0 caught"); 
        } 
        catch (Exception e) 
        {
            System.out.println("exception caught");  
        } 
    } 
}


 
A. Ex0 caught 
B. exception caught 
C. Compilation fails because of an error at line 2. 
D. Compilation fails because of an error at line 9. 
 
Answer & ExplanationAnswer: Option A

Explanation:


An exception Exc1 is thrown and is caught by the catch statement on line 11. The code is executed in this block. There is no finally block of code to execute.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  



1.  
import java.io.*;
public class MyProgram 
{
    public static void main(String args[])
    {
        FileOutputStream out = null;
        try 
        {
            out = new FileOutputStream("test.txt");
            out.write(122);
        }
        catch(IOException io) 
        {
            System.out.println("IO Error.");
        }
        finally 
        {
            out.close();
        }
    }
}

and given that all methods of class FileOutputStream, including close(), throw an IOException, which of these is true?
 
A. This program will compile successfully. 
B. This program fails to compile due to an error at line 4. 
C. This program fails to compile due to an error at line 6. 
D. This program fails to compile due to an error at line 13. 
 
Answer & ExplanationAnswer: Option D

Explanation:


Any method (in this case, the main() method) that throws a checked exception (in this case, out.close() ) must be called within a try clause, or the method must declare that it throws the exception. Either main() must declare that it throws an exception, or the call to out.close() in the finally block must fall inside a (in this case nested) try-catch block.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
2.  
public class MyProgram 
{
    public static void throwit() 
    {
        throw new RuntimeException();
    }
    public static void main(String args[])
    {
        try 
        {
            System.out.println("Hello world ");
            throwit();
            System.out.println("Done with try block ");
        }
        finally 
        {
            System.out.println("Finally executing ");
        }
    }
}

which answer most closely indicates the behavior of the program?
 
A. The program will not compile. 
B. The program will print Hello world, then will print that a RuntimeException has occurred, then will print Done with try block, and then will print Finally executing. 
C. The program will print Hello world, then will print that a RuntimeException has occurred, and then will print Finally executing. 
D. The program will print Hello world, then will print Finally executing, then will print that a RuntimeException has occurred. 
 
Answer & ExplanationAnswer: Option D

Explanation:


Once the program throws a RuntimeException (in the throwit() method) that is not caught, the finally block will be executed and the program will be terminated. If a method does not handle an exception, the finally block is executed before the exception is propagated.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
3.  
public class ExceptionTest 
{ 
    class TestException extends Exception {} 
    public void runTest() throws TestException {} 
    public void test() /* Point X */ 
    { 
        runTest(); 
    } 
}

At Point X on line 5, which code is necessary to make the code compile?
 
A. No code is necessary. 
B. throws Exception 
C. catch ( Exception e ) 
D. throws RuntimeException 
 
Answer & ExplanationAnswer: Option B

Explanation:


Option B is correct. This works because it DOES throw an exception if an error occurs.

Option A is wrong. If you compile the code as given the compiler will complain:

"unreported exception must be caught or declared to be thrown" The class extends Exception so we are forced to test for exceptions.

Option C is wrong. The catch statement belongs in a method body not a method specification.

Option D is wrong. TestException is a subclass of Exception therefore the test method, in this example, must throw TestException or some other class further up the Exception tree. Throwing RuntimeException is just not on as this belongs in the java.lang.RuntimeException branch (it is not a superclass of TestException). The compiler complains with the same error as in A above.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
4.  
System.out.print("Start ");
try 
{
    System.out.print("Hello world");
    throw new FileNotFoundException();
}
System.out.print(" Catch Here "); /* Line 7 */
catch(EOFException e) 
{
    System.out.print("End of file exception");
}
catch(FileNotFoundException e) 
{
    System.out.print("File not found");
}

and given that EOFException and FileNotFoundException are both subclasses of IOException, and further assuming this block of code is placed into a class, which statement is most true concerning this code?
 
A. The code will not compile. 
B. Code output: Start Hello world File Not Found. 
C. Code output: Start Hello world End of file exception. 
D. Code output: Start Hello world Catch Here File not found. 
 
Answer & ExplanationAnswer: Option A

Explanation:


Line 7 will cause a compiler error. The only legal statements after try blocks are either catch or finally statements.

Option B, C, and D are incorrect based on the program logic described above. If line 7 was removed, the code would compile and the correct answer would be Option B.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
5.  Which statement is true?
 
A. catch(X x) can catch subclasses of X where X is a subclass of Exception. 
B. The Error class is a RuntimeException. 
C. Any statement that can throw an Error must be enclosed in a try block. 
D. Any statement that can throw an Exception must be enclosed in a try block. 
 
Answer & ExplanationAnswer: Option A

Explanation:


Option A is correct. If the class specified in the catch clause does have subclasses, any exception object that subclasses the specified class will be caught as well.

Option B is wrong. The error class is a subclass of Throwable and not Runtime Exception.

Option C is wrong. You do not catch this class of error. 

Option D is wrong. An exception can be thrown to the next method higher up the call stack. 


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------

12Next >
 
6.  Which four can be thrown using the throw statement? 

Error 
Event 
Object 
Throwable 
Exception 
RuntimeException 

 
A. 1, 2, 3 and 4 B. 2, 3, 4 and 5 
C. 1, 4, 5 and 6 D. 2, 4, 5 and 6 
 
Answer & ExplanationAnswer: Option C

Explanation:


The (1), (4), (5) and (6) are the only four that can be thrown. 

An Error is a subclass of Throwable that indicates serious problems that a reasonable application should not try to catch.

The Throwable class is the superclass of all errors and exceptions in the Java language.

The class Exception and its subclasses are a form of Throwable that indicates conditions that a reasonable application might want to catch (checked exceptions)

RuntimeException is the superclass of those exceptions that can be thrown during the normal operation of the Java Virtual Machine.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
7.  Which statement is true?
 
A. A try statement must have at least one corresponding catch block. 
B. Multiple catch statements can catch the same class of exception more than once. 
C. An Error that might be thrown in a method must be declared as thrown by that method, or be handled within that method. 
D. Except in case of VM shutdown, if a try block starts to execute, a corresponding finally block will always start to execute. 
 
Answer & ExplanationAnswer: Option D

Explanation:


A is wrong. A try statement can exist without catch, but it must have a finally statement.

B is wrong. A try statement executes a block. If a value is thrown and the try statement has one or more catch clauses that can catch it, then control will be transferred to the first such catch clause. If that catch block completes normally, then the try statement completes normally.

C is wrong. Exceptions of type Error and RuntimeException do not have to be caught, only checked exceptions (java.lang.Exception) have to be caught. However, speaking of Exceptions, Exceptions do not have to be handled in the same method as the throw statement. They can be passed to another method.

If you put a finally block after a try and its associated catch blocks, then once execution enters the try block, the code in that finally block will definitely be executed except in the following circumstances: 

An exception arising in the finally block itself. 
The death of the thread. 
The use of System.exit() 
Turning off the power to the CPU. 
I suppose the last three could be classified as VM shutdown. 


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  
