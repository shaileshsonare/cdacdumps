class A {
int value;
}
class B extends A {
B(int val) {
value = val;
}
}
class C extends A {
C(int val) {
value = val;
}
}

public class ObjectTesting {
	
	static void check1()
	{
		Integer i = new Integer(0);
		Integer i1 = new Integer(0);
		Float f = new Float(0);
		System.out.println(i==i1);
		System.out.println(i.equals(f));
		//System.out.println(i==f);
		System.out.println(i.equals(i1));

	}
	
	
	public static void main(String[] args) {
	//	check1();
		A b = new B(1000);
		A c = new C(1000);
	//	System.out.println(b==c);
		//System.out.println(b &lt;= c)
		
		//new ObjectTesting().go();
		
	/*	doStuff("abcd"); 
		doStuff("efg"); 
		doStuff("hi"); */

			
		boolean stmt1 = "champ" == "champ";
		boolean stmt2 = new String("champ") == "champ";
		boolean stmt3 = new String("champ") == new String("champ");
		System.out.println(stmt1 && stmt2 || stmt3);
		



	}
	
	int doX(Long x, Long y) { return 1; } 
	//int doX(long... x) { return 2; }
	int doX(Integer x, Integer y) { return 3; } 
	//int doX(Number n, Number m) { return 4; } 
	
	void go() {
		short s = 7; 
		System.out.print(doX(s,s) + " "); 
		System.out.println(doX(7,7)); 
		} 

	
	private static void doStuff(String str) {
		int var = 4; 
		if (var == str.length()) { 
			
		System.out.print(str.charAt(var--) + " "); 
		} 
		else { 
		System.out.print(str.charAt(0) + " ");
		}
		}

	
}
