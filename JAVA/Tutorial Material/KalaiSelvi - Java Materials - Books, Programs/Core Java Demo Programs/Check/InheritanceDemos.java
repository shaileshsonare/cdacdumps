class C1 {
void printS1() {System.out.print("C.printS1 ");}
static void printS2() {System.out.print("C.printS2 ");}
}
class D extends C1 {
void printS1(){System.out.print("D.printS1 ");}
void printS2() {System.out.print("D.printS2 ");}
}

public class InheritanceDemos {
	public static void main (String args[]) {
		C1 c = new D(); c.printS1(); c.printS2();
		}
}
