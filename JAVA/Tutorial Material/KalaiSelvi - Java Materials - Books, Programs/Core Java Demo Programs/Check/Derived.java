class SuperClass 
      {
public int doIt(String str, Integer... data)throws Exception{
String signature = "(String, Integer[])";
System.out.println(str + " " + signature);
return 1;
                                          }
}

public class Derived extends SuperClass{
public int doIt(String str, Integer... data)
{
   String signature = "(String, Integer[])";
   System.out.println("Overridden: " + str + " " +signature);
   return 0;
}
public static void main(String... args)
{
SuperClass sb = new Derived();
//sb.doIt("hello", 3);
}
}


class AA {
 static int add(int i, int j){
 return i+j;
 }
}
 class BB extends AA{
 public static void main(String argv[]){
 short s = 9;
 System.out.println(add(s,6));
 }
}
