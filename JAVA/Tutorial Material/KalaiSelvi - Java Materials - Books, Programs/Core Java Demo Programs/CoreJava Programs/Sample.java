/* This java program contains a class with a 
 * method to print a message "Hello World". 
*/

/**
* This class contains a method to print the message.
* Date: 5-May-2007
* @author KalaiSelvi, Jeevan IT Academy
* @version 1.0
*/


class Sample
{

/**
* Prints the message "Hello World"
* @param String[] - if any arguments from command line
* @return : none
*/

public static void main(String[] args)
{
     System.out.println("Hello World");
}
}