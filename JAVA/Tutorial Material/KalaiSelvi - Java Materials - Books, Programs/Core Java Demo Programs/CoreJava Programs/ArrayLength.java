
class ArrayLength
{

public static void main(String a[])
{
   int[] count = {1,2,3};
   System.out.println("Length of integer array "+count.length);
   
   char vowel[]={'a' , 'e' , 'i' , 'o' , 'u'};
   System.out.println("Length of character array "+vowel.length);
   
   double[][] matrix = new double[3][];
   System.out.println("Length of double dimensional matrix array "+matrix.length);

   ArrayLength arrobj[]=new ArrayLength[3];
   System.out.println("Length of array of objects "+arrobj.length); 	

}
}