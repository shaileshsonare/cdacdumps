
public class ForEachLoop 
{       
	public static void main(String[] args) {
		int values[]={11,12,13,14,15,16,17,18,19,20};
		int sum=0;
		for (int i:values)
		{
			System.out.println("Value is "+i);
			sum+=i;
		}
			System.out.println("Sum is "+sum);
	}
}
