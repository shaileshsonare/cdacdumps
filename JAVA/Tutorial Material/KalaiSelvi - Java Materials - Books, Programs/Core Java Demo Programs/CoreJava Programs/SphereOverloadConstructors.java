class Sphere {
    private double x;
    private double y;
    private double z;
    private double radius;

    public Sphere(){
        radius = 1;
    }

    public Sphere(double aradius){
        radius = aradius;
    }

    public Sphere(double ax, double ay, double az, double aradius){
        x = ax;
        y = ay;
        z = az;
        radius = aradius;
    }
    public void getValues(){
        System.out.println("x ="+x);
        System.out.println("y ="+y);
        System.out.println("z ="+z);
        System.out.println("radius ="+radius); 
    }
} 

public class SphereOverloadConstructors{
    public static void main(String args[]){

	Sphere s1 = new Sphere();
	s1.getValues();
 
 	Sphere s2 = new Sphere(2);
        s2.getValues(); 
    
        Sphere s3 = new Sphere(100, -40, 56.5, 8);
	s3.getValues();
   }
}