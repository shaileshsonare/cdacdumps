public class DoWhileDemo {
    public static void main(String[] args) {

        int value = 9;
	int sum=0;
	int i=0;

        do {
    	    System.out.println("Inside the Loop"+i);
            sum=sum+i;
	    i++;
        } while (i<=value);
        System.out.println(sum);
    }
}
