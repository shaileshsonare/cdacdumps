
    class A 
    {
     public B b ;
     public A()
     {
     b= new B(this);
      System.out.println("Constructor of A() is invoked"); 
      b.disp();
     }
     public void killB()
     {     
      b.finalize(); 
      b.disp();
     }


    }

    class B 
    {
     public A a;
     public B(A someA)
     {
      a = someA;
    System.out.println("Constructor of B() is invoked");
     }
    void disp()
    {
    System.out.println("B is alive");
    }

    protected void finalize() 
    {
    System.out.println("The finalize method has been invoked");
     a.b = null; // Nullifying the object !
    
    System.out.println("B is killed");
     a.b=this;  //Resurrection
  System.out.println("B is resurrected");
     }  
    }

    public class Resurrection
    {
    public static void main(String args[])
    {
    System.out.println("This is a sample program for resurrection");
    A a=new A();

    
  a.killB();
    System.out.println("Exiting ...............");
    }
    }



