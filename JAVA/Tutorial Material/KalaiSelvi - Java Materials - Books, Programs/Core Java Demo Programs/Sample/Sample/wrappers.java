import java.io.DataInputStream;

/*
 * Created on Mar 26, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author kalaiselvi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class wrappers {

    public static void main(String[] args) {
       int i=100;
       System.out.println("Wrapper Classes");
       System.out.println("I. Converting Primitive data to Object");
       System.out.println("Value of Primitive data type i = "+i);
       try{
       Integer I=new Integer(i);
       System.out.println("Value of Integer Object I = "+I);
       System.out.println("II. Converting Object to Primitive data");
       int ii=I.intValue();
       System.out.println("Value of Primitive data i="+ii);
       System.out.println("III. Converting Numeric String to Primitive data");
       DataInputStream dis=new DataInputStream(System.in); //getting value from user
       int value=Integer.parseInt(dis.readLine());  //conversion using static method
       System.out.println("Value of Primitive data value="+value);
       System.out.println("IV. String Object to Numeric Object");
       Integer objectvalue=Integer.valueOf( (dis.readLine()));  //conversion using static method
       System.out.println("Value of Object objectvalue="+objectvalue);
       System.out.println("V. Numeric Object to String");
       String svalue=objectvalue.toString(); //conversion
       System.out.println("Value of String Object svalue="+svalue);
       //float f=10.90f;
       //Float F=new Float(f);
       //System.out.println("Value of Float Object F = "+F);
       
       
       }catch(Exception e){}
    
    
    }
}
