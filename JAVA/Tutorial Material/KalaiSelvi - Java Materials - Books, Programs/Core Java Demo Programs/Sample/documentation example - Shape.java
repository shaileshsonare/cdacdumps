
/**
 * @author Kalaiselvi
 * @Version 1.0
 * 
 */

public class Shape  {
		
	/**
	 * @param side	side of an area
	 * @return int area of a square
	 * @see displayArea(int len, int wid)
	 * @throws Exception
	 */
	public static int displayArea(int side){
		return 4 * side;
	}
	
	/**
	 * @param len length of an area
	 * @param wid width of an area
	 * @return int area of a rectangle
	 * @see displayArea(int side)
	 * @throws no exception
	 */
	public static int displayArea(int len, int wid){
		return len * wid;
	}
	
	
}
