public class StaticDemo {
	   static int i = 5;

	   static {
	       System.out.println("Static code: i = " + i++);
	   }
	   
           public static void main(String args[]) {
	   System.out.println("main: i = " + i++ );
	   }
	}
