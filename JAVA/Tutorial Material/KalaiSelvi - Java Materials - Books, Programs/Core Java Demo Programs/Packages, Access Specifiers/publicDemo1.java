class publicDemo   
 {
    public int x;	        //Public variable declaration
        
    public publicDemo()    // Public constructor
     {
   
        System.out.println("This is a constructor with public access specifier!");
    }
  
    public void printdata()  //Public Methods
    {
   
        System.out.println("This is a method with public access specifier!");
    }

 }

public class publicDemo1	//Public class
 {
    public static void main(String[] args) 
   {
        publicDemo obj = new publicDemo();
        obj.x = 20;
        System.out.println(obj.x);
        obj.printdata();
    }
 }