package Mypack1;

class Son extends Father1
{
   int age;
   String name;

   Son()
      {
	System.out.println("Son's      Default Constructor");
	age = 22;
	name = "Rohan";
      }

    
    void showSon()
      {
	  System.out.println("Son's Age    : "+age);
	  System.out.println("Son's Name   : "+name);
      }
}
