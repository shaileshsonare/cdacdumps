import mypack.*;
class DerivedAccess extends Access
{

DerivedAccess(int i,int j,int k,int l)
{
super(i,j,k,l);
}


void derivedShow()
{
//System.out.println("private i :"+i); 
System.out.println("private j :"+j); 
System.out.println("protected k :"+k); 
System.out.println("private l :"+l); 
}

}

class Test1
{
public static void main(String args[])
 {
 DerivedAccess d= new DerivedAccess(10,20,30,40);
 d.accessShow();
 d.derivedShow();
// System.out.println(d.i);
 //System.out.println(d.j);
 System.out.println(d.k);
 System.out.println(d.l);
 }
}
