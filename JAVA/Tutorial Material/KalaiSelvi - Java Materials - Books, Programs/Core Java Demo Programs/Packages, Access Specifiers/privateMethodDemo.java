public class privateMethodDemo {

	private int a = 10;	       //Private Variable Declaration
	private void method1()   //Private Method Declaration
	{
		System.out.println("I am a private method");
	}
	public static void main(String[] args) 
	{
	    privateMethodDemo obj = new privateMethodDemo();
	    System.out.println(obj.a);
	    obj.method1();
		

	}

}
