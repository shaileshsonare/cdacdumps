package Apr9;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
class TextFileFilter implements FilenameFilter
{

	@Override
	public boolean accept(File dir, String name) {
		if(name.endsWith("txt"))return true;
		return false;
	}
	
}
class ImageFileFilter implements FilenameFilter
{

	@Override
	public boolean accept(File dir, String name) {
		if(name.endsWith(".gif")||name.endsWith(".bmp")||name.endsWith(".jpg"))return true;
		
		return false;
	}
	
}
public class DemoCopyFolder {


	public static void main(String[] args) {
		prepare();
		File[] textfiles=checkForTextFiles("c://Guindy");
		copyTextFilesToDestination("c://Saidapet",textfiles);
		
		File[] imagefiles=checkForImageFiles("c://Guindy");
		copyBinaryFilesToDestination("c://Saidapet",imagefiles);
		
	}

	private static void copyBinaryFilesToDestination(String directory,File[] imagefiles) {
		for(File imagefile:imagefiles)
		{
			try
			{
				FileInputStream fis=new FileInputStream(imagefile);
				FileOutputStream fos=new FileOutputStream(new File(directory,imagefile.getName()));
				int x=-1;
				while((x=fis.read())!=-1)
				{
					fos.write(x);
				}
				fis.close();
				fos.close();
			}
			catch(FileNotFoundException e){
				e.printStackTrace();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		
	}

	private static File[] checkForImageFiles(String folder) {
		File dir=new File(folder);
		File[] textFiles=dir.listFiles(new ImageFileFilter());
		for(File v:textFiles)
		{
			System.out.println(v);
		}
		
		return textFiles;
	}

	private static void copyTextFilesToDestination(String folder,File[] files) {
		for(File file:files)
		{
			try{
				FileReader fr=new FileReader(file);
				FileWriter fw=new FileWriter(new File(folder,file.getName()));
				int x=-1;
				while((x=fr.read())!=-1)
				{
					fw.write(x);
					
				}
				fr.close();
				fw.close();
			}
			catch(FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	private static File[] checkForTextFiles(String folder) {
		File dir=new File(folder);
		File[] textFiles=dir.listFiles(new TextFileFilter());
		for(File f: textFiles)
		{
			System.out.println(f);
		}
		return textFiles;
	}

	private static void prepare() {
		File file=new File("c:\\Saidapet");
		file.mkdir();
		
		
	}
	

}
