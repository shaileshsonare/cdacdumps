package apr8;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class demo5 {

	
	public static void main(String[] args) {
		try
		{
			FileInputStream fis=new FileInputStream("d:\\new.wma");
			BufferedInputStream bis=new BufferedInputStream(fis);
			FileOutputStream fos=new FileOutputStream("d:\\sun2.txt");
			BufferedOutputStream bos=new BufferedOutputStream(fos);
			
			int x=-1;
			long secs_start=System.currentTimeMillis();
			while((x=bis.read())!=-1)
			{
				bos.write(x);
			}
			bis.close();
			bos.close();
			fos.close();
			fis.close();
			
			long secs_end=System.currentTimeMillis();
			
			System.out.println(secs_end-secs_start);
			
			
			
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

	}

}
