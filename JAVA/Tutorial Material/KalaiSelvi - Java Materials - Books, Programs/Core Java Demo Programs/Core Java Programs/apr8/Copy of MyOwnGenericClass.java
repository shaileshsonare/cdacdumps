package apr8;

import java.util.ArrayList;

public class MyOwnGenericClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Number n1=new Integer(5);
		String s1=new String("sun");
		Pair<Number,String> p1=new Pair<Number,String>(n1,s1);
		System.out.println("first of p1(right after creation) = "+p1.getFirst());
		System.out.println("second of p2(right after creation) ="+p1.getSecond());

		p1.setFirst(new Long(6L));
		p1.setSecond(new String("Rises"));
		System.out.println("first of p1(after setting values)= "+p1.getFirst());
		System.out.println("second of p1(after setting values)= "+p1.getFirst());
		
		Number n2=new Integer(15);
		String s2=new String("again");
		Pair<?,?>p2=new Pair<Number ,String>(n2,s2);
		System.out.println("first of p2 = "+p2.getFirst());
		System.out.println("Second of p2 = "+p2.getSecond());
		
		
		
		Number n3=new Integer(25);
		String s3=new String("and again");
		Pair<? extends String,?>p3=new Pair<String,String>(s3,s3);
		System.out.println("first of p3 = "+p3.getFirst());
		System.out.println("second of p3 = "+p3.getSecond());
		
		Number n4=new Long(300L);
		String s4=new String("James");
		Integer i4=new Integer(7);
		PairExtended<Number,String,Integer>pe4=new PairExtended<Number,String,Integer>(n4,s4,i4);
		System.out.println("first of PairExtended pe4 = "+pe4.getFirst());
		System.out.println("second of PairExtended pe4 = "+pe4.getSecond());
		System.out.println("third of PairExtended pe4 = "+pe4.getThird());
		
		ArrayList<Integer> ar4=new ArrayList<Integer>();
		
		ar4.add(6000);
		ar4.add(7000);
		PairExtended<Number,String,ArrayList<Integer>> pe5=new PairExtended<Number, String, ArrayList<Integer>>(n4,s4,ar4);
		System.out.println("first of PairExtended pe5 with array = "+pe5.getFirst());
		System.out.println("second of PairExtended pe5 with array = "+pe5.getSecond());
		System.out.println("third of PairExtended pe5  with array = "+pe5.getThird());
	}

}
