package apr8;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Camel implements Serializable {
	//private static final long serialVersionUID=1L;
	float capacity;
	double height;
	transient String desert;
	public String toString()
	{
		return "Capacity="+capacity+"\n"+"Desert="+desert+"\n"+"Height="+height;
	}
}