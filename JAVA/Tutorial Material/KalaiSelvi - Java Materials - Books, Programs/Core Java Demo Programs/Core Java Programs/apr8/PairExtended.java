package apr8;

public class PairExtended<F,S,T> extends Pair<F,S> {
	
	T third;

	public PairExtended(F first, S second, T third) {
		super(first, second);
		this.third = third;
	}

	public T getThird() {
		return third;
	}

	public void setThird(T third) {
		this.third = third;
	}
	
	

}
