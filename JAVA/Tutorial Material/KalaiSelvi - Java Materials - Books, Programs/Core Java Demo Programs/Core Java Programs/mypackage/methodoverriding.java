package mypackage;

public class methodoverriding {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Aa aref=new D();
		aref.m1();
		aref.m2();
		aref.m3();
		aref.m4();
		

	}

}
class Aa
{
	void m1(){System.out.println("in m1() of Aa");}
	void m2(){System.out.println("in m2() of Aa");}
	void m3(){System.out.println("in m3() of Aa");}
	void m4(){System.out.println("in m4() of Aa");}
	
}
class Bb extends Aa
{
	void m2(){System.out.println("in m2() of Bb");}
}
class C extends Bb
{
	void m3(){System.out.println("in m3() of C");}
}
class D extends C
{
	void m4(){System.out.println("in m4() of D");}
	
}