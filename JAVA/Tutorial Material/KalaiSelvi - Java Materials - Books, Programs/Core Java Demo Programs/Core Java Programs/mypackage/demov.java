package mypackage;

public class demov {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	m1();
	m1(12);
	m1(12,23);
	m1(12,23,45);
	m1(12,23,45,67);
	m1(12,23,45,67,89);
	m1(12,23,45,67,89,98,78,65,45,32,12,34,56,87,64);

	}
	static void m1(int...v)
	{
		int sum=0;
		for(int b:v)
		{
			sum+=b;
		}
		System.out.println("sum="+sum);
	}

}
