package mypackage;

public class question {
int qno;
String questiondetail;
int correctchoice;
choice [] choices=new choice[4];

public int getQno() {
	return qno;
}
public void setQno(int qno) {
	this.qno = qno;
}
public String getQuestiondetail() {
	return questiondetail;
}
public void setQuestiondetail(String questiondetail) {
	this.questiondetail = questiondetail;
}
public int getCorrectchoice() {
	return correctchoice;
}
public void setCorrectchoice(int correctchoice) {
	this.correctchoice = correctchoice;
}
public question(choice[] choices, int correctchoice, int qno,
		String questiondetail) {
	super();
	this.choices = choices;
	this.correctchoice = correctchoice;
	this.qno = qno;
	this.questiondetail = questiondetail;
}
public choice[] getChoices() {
	return choices;
}
public void setChoices(choice[] choices) {
	this.choices = choices;
}
public void displayname()
{
	System.out.println(qno+")."+questiondetail+".");
	for(int i=0;i<4;i++){
		switch(i)
		{
		case 0:
			System.out.println("a)"+choices[i].getChoice_desc());
			break;
		case 1:
			System.out.println("b)"+choices[i].getChoice_desc());
			break;
		case 2:
			System.out.println("c)"+choices[i].getChoice_desc());
			break;
		case 3:
			System.out.println("d)"+choices[i].getChoice_desc());
			break;
		}
		
	}
}
}