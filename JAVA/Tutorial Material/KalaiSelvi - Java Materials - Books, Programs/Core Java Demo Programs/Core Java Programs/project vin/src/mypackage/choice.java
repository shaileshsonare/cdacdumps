package mypackage;

public class choice {
	int choiceno;
	String choice_desc;
	public choice(int choiceno,String choice_desc ) {
		super();
		this.choice_desc = choice_desc;
		this.choiceno = choiceno;
	}
	public int getChoiceno() {
		return choiceno;
	}
	public void setChoiceno(int choiceno) {
		this.choiceno = choiceno;
	}
	public String getChoice_desc() {
		return choice_desc;
	}
	public void setChoice_desc(String choice_desc) {
		this.choice_desc = choice_desc;
	}
	

}
