package Apr9;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnno {//annotation declaration
	String str();
	int val();

}
class Meta
{
	@MyAnno(str="Annotation Example",val=1234)
	public static void  MyMeth()
	{
		Meta ob=new Meta();
		
		try
		{
			Class c=ob.getClass();
			Method m=c.getMethod("MyMeth");
			
			MyAnno anno=m.getAnnotation(MyAnno.class);
			System.out.println(anno.str()+" "+anno.val());
		}
		catch(NoSuchMethodException exc)
		{
			System.out.println("method not found");
		}
	}
	public static void main(String[] args)
	{
		MyMeth();
	}
	
}
