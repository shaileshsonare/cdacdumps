package Apr9;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class NIOCopier {

	
	public static void main(String[] args) throws IOException{
		FileInputStream inFile=new FileInputStream(args[0]);
		FileOutputStream outFile=new FileOutputStream(args[1]);
		FileChannel inchannel=inFile.getChannel();
		FileChannel outchannel=outFile.getChannel();
		for(ByteBuffer buffer=ByteBuffer.allocate(1024*1024);inchannel.read(buffer)!=-1;buffer.clear())
		{
			buffer.flip();
			while(buffer.hasRemaining())
				outchannel.write(buffer);
			
		}
		inchannel.close();
		outchannel.close();
		
	}
}
