package Apr9;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;


@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnno1 {
	String str();
	int val();

}
@Retention(RetentionPolicy.RUNTIME)
@interface what
{
	String description();
}
@what(description= "An annotation test class")
@MyAnno1(str="Meta2",val=99)
class Meta2
{
	@what(description="An annotation test method")
	@MyAnno1(str="Testing",val=100)
	public static void myMeth()
	{
		Meta2 ob=new Meta2();
		try
		{
			Annotation annos[]=ob.getClass().getAnnotations();
			
			System.out.println("All annotation for Meta2 :");
			for(Annotation a:annos)
				System.out.println(a);
			System.out.println();
			Method m=ob.getClass().getMethod("myMeth");
			annos=m.getAnnotations();
			
			System.out.println("All annotation for myMeth:");
			
			for(Annotation a:annos)
				System.out.println(a);
			
			
		}
		catch(NoSuchMethodException exc)
		{
			System.out.println("Method not found.");
		}
	}
	public static void main(String[] args)
	{
		myMeth();
	}
}
