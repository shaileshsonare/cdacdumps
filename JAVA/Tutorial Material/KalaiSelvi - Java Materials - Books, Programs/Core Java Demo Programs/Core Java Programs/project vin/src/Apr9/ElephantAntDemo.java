package Apr9;

public class ElephantAntDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	Ant[] ants=new Ant[10];
	Elephant[] elephants=new Elephant[20];
	for(int i=0;i<ants.length;i++)
	{
		ants[i]=new Ant("ant "+(i+1));
	}
	for(int i=0;i<elephants.length;i++)
	{
		elephants[i]=new Elephant("elephant "+(i+1));
	}
    for(Ant a:ants)
	{
		Thread t1=new Thread(a);
		t1.start();
		
	}
	for(Elephant e:elephants)
	{
		Thread t1=new Thread(e);
		t1.start();
	}
	}

}
