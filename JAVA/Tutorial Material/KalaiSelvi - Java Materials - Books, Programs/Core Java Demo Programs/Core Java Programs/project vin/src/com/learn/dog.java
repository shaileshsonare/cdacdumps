package com.learn;

public abstract class dog {
	public abstract  void bark();
}
class alsessiondog extends dog
{
	@Override
	public void bark() {
		System.out.println("alsession dog barks");
		
	}

	public void specialmethod() {
		System.out.println("alsession special");
		
	}
}
class germanshepard extends dog
{
	@Override
	public void bark() {
		
		System.out.println("ger shep dog barks");
	}

	public void specialmethod() {
		System.out.println("germanshepard special");
	}
}