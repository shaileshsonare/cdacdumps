package com.learn;

public class student implements Comparable {
String fname;
String lname;
int height;

public int getHeight() {
	return height;
}
public void setHeight(int height) {
	this.height = height;
}
public student(String fname,String lname)
{
	this.fname=fname;
	this.lname=lname;
}
@Override
public String toString()
{
	return "First name:"+fname +"\t last name:"+lname;
	
}
@Override
public int compareTo(Object o) {
	student s=(student)o;
	
	return this.height-s.height;
}
}
