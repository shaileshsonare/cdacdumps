package Apr7;

import java.util.Arrays;
import java.util.List;

public class Arraysdemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int array[]={32,19,-23,-23,21,34,22,56,-13,0,20};
		System.out.print("Original Contents :");
		display(array);
		
		Arrays.sort(array);
		
		System.out.print("Sorted :");
		display(array);
		
		Arrays.fill(array,2,6,-1);
		System.out.print("After fill() :");
		display(array);
		//sort and display
		Arrays.sort(array);
		System.out.print("After sorting again :");
		display(array);
		//binary search for -9
		System.out.print("The value 56 is at location : ");
		int index=Arrays.binarySearch(array, 56);
		System.out.println(index);
		
		//List<Integer> iii=Arrays.asList(1,2,3);
		//List<String> sss=Arrays.asList("","","");
		
		//float[] vv=new float[]{23,4f,12,4f,15,4f};

	}

	private static void display(int[] array) {
		for(int i=0;i<array.length;i++)
		System.out.print(array[i]+" "); 
		System.out.println("");
		
		
	}

}
