package Apr7;

import java.util.Iterator;
import java.util.TreeSet;

public class compdemo {

	public static void main(String[] args) {
		//Create a tree set
		TreeSet ts=new TreeSet(new mycomp());
		//Add elements to the tree
		ts.add("C");
		ts.add("A");
		ts.add("B");
		ts.add("E");
		ts.add("F");
		ts.add("D");
		//Get an iterator
		Iterator i=ts.iterator();
		//Display Elements
		while(i.hasNext())
		{
			Object elements=i.next();
			System.out.println(elements+"");
		}
		System.out.println();
	}

}
