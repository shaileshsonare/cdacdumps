package com.learn;

public interface I1 {

	void m1();
	void m2();
	
}
interface I2
{
	void m3();
	void m4();
}
interface I3 extends I1,I2
{
	void m5();
	
}
class demoII implements I3
{

	@Override
	public void m5() {
		System.out.println("m5");
		
	}

	@Override
	public void m1() {
		System.out.println("m1");
		
	}

	@Override
	public void m2() {
		System.out.println("m2");
		
	}

	@Override
	public void m3() {
		System.out.println("m3");
	}

	@Override
	public void m4() {
		System.out.println("m4");
		
	}
	
}
