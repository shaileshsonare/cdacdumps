package getinputScaner;

import static java.lang.Math.E;
import static java.lang.Math.PI;
import static java.lang.Math.random;

import java.util.Scanner;

public class getinput {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		/**
		 * This is to demonstrate the static import
		 */
		
		double de=E;
		double rand=random();
		System.out.println("the random number is="+rand);
		System.out.println("the E value is="+de);
		double d=PI;
		System.out.println("the static var import="+d);
		
		
		/**
		 * This is to demonstrate the scanner class
		 */

		Scanner sc=new Scanner(System.in);
		System.out.println("enter the int for input:");
		int i=sc.nextInt();
		System.out.println("enter the second int for input:");
		int j=sc.nextInt();
		System.out.println("the input typed is="+(i+j));
		
		
		/**
		 * The printf() method is used to format the output to the console like the 'c' Language 
		 */
		
		double du=0;
		System.out.println("Enter the double as Input");
		System.out.printf("enter the double %6.3e",du=sc.nextDouble());
		System.out.println("\nthe int value is ="+du+"\n");
		
		
		System.out.println("get float as input:");
		float fl=sc.nextFloat();
		System.out.println("the float given is="+fl);
		
		
		
		System.out.printf("the int=%2d the double=%e the float is=%f",i,du,fl);
		
		
		
	}

}
