//invoking overloaded methods using object as parameners

class animal{}

class horse extends animal{}

public class useanimal{

	public void doStuff(animal a){
		System.out.println("in the animalversion");
	}

    void doStuff(horse b){
		System.out.println("in the horse version");
	}		

	public static void main(String [] arg){

		useanimal us=new useanimal();

		animal ani=new animal();
		horse hr=new horse();

//horse object refered to animal
		animal anihr=new horse();

		us.doStuff(ani);
		us.doStuff(hr);

//calls the animal version desides the object at compile time
        us.doStuff(anihr);
		
	}
}
