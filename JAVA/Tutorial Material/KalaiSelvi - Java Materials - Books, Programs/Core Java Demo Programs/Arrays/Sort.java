class Array
{
 void sort(int n[][])
  {
    for(int i=0;i<n.length-1;i++)
     {
      for(int j=i+1;j<n.length;j++)
      {  
      if(n[i]>n[j])
       {
	int t=n[i];
	n[i]=n[j];
	n[j]=t;
       }
      }
     }
  }
 public static void main(String args[])
  {
   Sort s=new Sort(); 
   int a=Integer.parseInt(args[0]);
   int n[]=new int[a];
    for(int i=0;i<n.length;i++)
     n[i]=Integer.parseInt(args[i+1]);
   System.out.println("Unsorted list.......");
    for(int i=0;i<n.length;i++)
      System.out.print(n[i]+" ");
   System.out.println();
   s.sort(n);
   System.out.println("Sorted list.......");
    for(int i=0;i<n.length;i++)
      System.out.print(n[i]+" ");
   System.out.println();
  }
}
