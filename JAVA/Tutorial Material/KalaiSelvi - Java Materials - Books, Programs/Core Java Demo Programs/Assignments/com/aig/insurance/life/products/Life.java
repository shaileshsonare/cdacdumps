package com.aig.insurance.life.products;

public class Life {
	//data members
	 public int pno;
	 int sno;
	 private void getData()
	 {
		 pno=100;
		 sno=10;
	 }
	 protected void dispData()
	 {   getData();
		 System.out.println(pno);
		 System.out.println(sno);
	 }
	 
	 class AccessData
	 {
	   public void access()
	   {
		   Life obj=new Life();
		   System.out.println(obj.pno);
		   System.out.println(obj.sno);
		   obj.dispData();
	   }
	 }

}
