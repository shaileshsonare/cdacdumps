abstract class Shape2
{
	abstract public void calcArea();
	public void check()
	{
		System.out.println("Implemented");
	}
}
interface Shape1
{   public static final int area=0;
	abstract public void draw();
}
class Rect1 extends Shape2 implements Shape1
{
	public void draw()
	{
		System.out.println("Rectangle");
	}
	
	public void calcArea()
	{
		System.out.println("area");
	}
}

public class InterfaceEx {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
     Rect1 obj=new Rect1();
     obj.draw();
	}
}
