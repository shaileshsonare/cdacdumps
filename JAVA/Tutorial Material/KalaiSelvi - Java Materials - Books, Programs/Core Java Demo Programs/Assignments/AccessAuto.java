 class FClass
{
	final public void m1()
	{
		System.out.println("final");
	}
	
	static void add()
	{
		System.out.println("Static Method");
	}
	
}


public class AccessAuto extends Automobile{

	/**
	 * @param args
	 */
	public void move()
	{
		System.out.println("move");
	}
	public static void main(String[] args) {
		Automobile obj=new AccessAuto();
		obj.implementedm1();
		obj.move();
		FClass l=new FClass();
		l.m1();
		FClass.add();
	}
}
