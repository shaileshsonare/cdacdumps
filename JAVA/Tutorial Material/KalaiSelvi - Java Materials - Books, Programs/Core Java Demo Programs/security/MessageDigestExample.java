package security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MessageDigestExample {
    public static void main(String args[])
    {
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            byte[] data1 = {65,66,67,68,69};
            byte[] data2 = {70,71,72,73,74};

            sha.update(data1);
            sha.update(data2);
            byte[] msgDigest = sha.digest();

            // Can also combine the final update with digest like this:
            // byte[] msgDigest = sha.digest(data2);

            System.out.println("--- Message Digest ---");
            for(int i=0; i<msgDigest.length; i++) {
                System.out.print(msgDigest[i] + " ");
            }

            System.out.println("");
        } catch(NoSuchAlgorithmException nsae) {
            System.out.println("Exception: " + nsae);
            nsae.printStackTrace();
        }
    }
}