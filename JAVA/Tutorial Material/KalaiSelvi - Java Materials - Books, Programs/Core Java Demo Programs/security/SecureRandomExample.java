package security;

import java.security.SecureRandom;
import java.security.NoSuchAlgorithmException;

public class SecureRandomExample {
    public static void main(String args[])
    {
        try {
            SecureRandom rng = SecureRandom.getInstance("SHA1PRNG");
            rng.setSeed(711);

            int numberToGenerate = new Integer(args[0]).intValue();
            byte randNumbers[] = new byte[numberToGenerate];

            rng.nextBytes(randNumbers);
            for(int j=0; j<numberToGenerate; j++) {
                System.out.println("The random "+randNumbers[j] + " ");
            }

            System.out.println("");
        } catch(NoSuchAlgorithmException nsae) {
            System.out.println("Exception: " + nsae);
            nsae.printStackTrace();
        }
    }
}