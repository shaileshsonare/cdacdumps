package security;

import java.security.*;
import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.io.*;

public class CipherExample {
    private Cipher m_encrypter;
    private Cipher m_decrypter;

    public void init(SecretKey key)
    {
        // for CBC; must be 8 bytes
        byte[] initVector = new byte[]{0x10, 0x10, 0x01, 0x04, 0x01, 0x01, 0x01, 0x02};

        AlgorithmParameterSpec algParamSpec = new IvParameterSpec(initVector);

        try {
            m_encrypter = Cipher.getInstance("DES/CBC/PKCS5Padding");
            m_decrypter = Cipher.getInstance("DES/CBC/PKCS5Padding");

            m_encrypter.init(Cipher.ENCRYPT_MODE, key, algParamSpec);
            m_decrypter.init(Cipher.DECRYPT_MODE, key, algParamSpec);
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("Exception: " + e);
        } catch (NoSuchPaddingException e) {
            System.out.println("Exception: " + e);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception: " + e);
        } catch (InvalidKeyException e) {
            System.out.println("Exception: " + e);
        }
    }

    public void write(byte[] bytes, OutputStream out)
    {
        try {
            CipherOutputStream cos = new CipherOutputStream(out, m_encrypter);

            cos.write(bytes, 0, bytes.length);

            cos.close();
        } catch(IOException ioe) {
            System.out.println("Exception: " + ioe);
        }
    }

    public void read(byte[] bytes, InputStream in)
    {
        try {
            CipherInputStream cis = new CipherInputStream(in, m_decrypter);
            int pos=0, intValue;

            while( (intValue = cis.read()) != -1) {
                bytes[pos] = (byte)intValue;
                pos++;
            }
        } catch(IOException ioe) {
            System.out.println("Exception: " + ioe);
        }
    }

    public byte[] encrypt(byte[] input)
    {
        try {
            return(m_encrypter.doFinal(input));
        } catch(IllegalBlockSizeException ibse) {
            System.out.println("Exception: " + ibse);
        } catch(BadPaddingException bpe) {
            System.out.println("Exception: " + bpe);
        }

        return(null);
    }

    public byte[] decrypt(byte[] input)
    {
        try {
            return(m_decrypter.doFinal(input));
        } catch(IllegalBlockSizeException ibse) {
            System.out.println("Exception: " + ibse);
        } catch(BadPaddingException bpe) {
            System.out.println("Exception: " + bpe);
        }

        return(null);
    }

    public static void main(String args[])
    {
        try {
            CipherExample ce = new CipherExample();

            SecretKey key = KeyGenerator.getInstance("DES").generateKey();

            ce.init(key);

            System.out.println("Testing encrypt/decrypt of bytes");
            byte[] clearText = new byte[]{65,73,82,68,65,78,67,69};
            byte[] encryptedText = ce.encrypt(clearText);
            byte[] decryptedText = ce.decrypt(encryptedText);

            String clearTextAsString = new String(clearText);
            String encTextAsString = new String(encryptedText);
            String decTextAsString = new String(decryptedText);

            System.out.println("   CLEARTEXT: " + clearTextAsString);
            System.out.println("   ENCRYPTED: " + encTextAsString);
            System.out.println("   DECRYPTED: " + decTextAsString);

            System.out.println("\nTesting encrypting of a file\n");

            //File f=new File("cipherTest.in");
            FileInputStream fis = new FileInputStream("D:/3edge/jdk1.5programs/security/cipherTest.in");
            FileOutputStream fos = new FileOutputStream("D:/3edge/jdk1.5programs/security/cipherTest.out");
            int dataInputSize = fis.available();

            byte[] inputBytes = new byte[dataInputSize];
            fis.read(inputBytes);
            //ce.write(inputBytes, fos);
            fos.flush();
            fis.close();
            fos.close();

            String inputFileAsString = new String(inputBytes);
            System.out.println("INPUT FILE CONTENTS\n" + inputFileAsString + "\n");

            System.out.println("File encrypted and saved to disk\n");

            fis = new FileInputStream("D:/3edge/jdk1.5programs/security/cipherTest.out");

            byte[] decrypted = new byte[dataInputSize];
            ce.read(decrypted, fis);

            fis.close();
            String decryptedAsString = new String(decrypted);

            System.out.println("DECRYPTED FILE:\n" + decryptedAsString + "\n");
        } catch(IOException ioe) {
            System.out.println("Exception: " + ioe);
        } catch(NoSuchAlgorithmException e) {
            System.out.println("Exception: " + e);
        }
    }
}
