class Month1
{
   public static void main(String args[])
    {
       String str = args[0];
       System.out.println("The Entered Character  : "+str);
       System.out.println("The Corresponding Month:");
       switch(str.charAt(0))
	  {
	    case 'J': System.out.println("January");break;
	    case 'F': System.out.println("February");break;
	    case 'M': System.out.println("March");break;
	    case 'A': System.out.println("April");break;
	    case 'Y': System.out.println("May");break;
	    case 'j': System.out.println("June");break;
	    case 'u': System.out.println("July");break;
	    case 'G': System.out.println("August");break;
	    case 'S': System.out.println("September");break;
	    case 'O': System.out.println("October");break;
	    case 'N': System.out.println("November");break;
	    case 'D': System.out.println("December");break;
            default: System.out.println("Invalid Month");break;
         }
}
}
