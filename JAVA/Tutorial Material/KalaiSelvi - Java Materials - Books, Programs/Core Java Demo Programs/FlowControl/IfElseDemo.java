class Grade
{
 public static void main(String args[])
  {
  int m;
  m=Integer.parseInt(args[0]); 
  if(m>=80)
    System.out.println("Outstanding......");
  else if(m>=75)
    System.out.println("Excellent......");
  else if(m>=70)
    System.out.println("Very Good......");
  else if(m>=60)
    System.out.println("Good......");
  else if(m>=50)
    System.out.println("Fair......");
  else
    System.out.println("Need Improvement......");
  }
}
