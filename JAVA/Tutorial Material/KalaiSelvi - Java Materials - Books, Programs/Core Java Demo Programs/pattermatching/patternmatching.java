package pattermatching;

import static java.util.regex.Pattern.matches;

public class patternmatching {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/**
		 * Pattern matching examples
		 */
		 
		
		//here the X schould accure X{n} n times
		boolean match=matches("X{4}", "XXXX"); 
		System.out.println("the pattern matches="+match);//X 5 times so false

		//here X atleast n times
		boolean match1=matches("X{2,}", "XXX");//X should accure atleast 2 times or false value 
		System.out.println("the pattern matches1="+match1);

		// here X atleast 4 times but not more than 6 times
		boolean match2=matches("X{4,6}", "XXXXXX"); 
		System.out.println("the pattern matches2="+match2);

		//will accept ony single character a-z or A-Z and not abcde or LMNOPQ etc
		/*boolean match3=matches("[a-zA-Z]", "E"); 
		System.out.println("the pattern [a-zA-Z]Character matches3="+match3);

		//other than a or b or c character
		boolean match4=matches("[^abc]", "c"); 
		System.out.println("the pattern Character [^abc]which should not match matches4="+match4);

		//the first character shold be 0-9 the second a-z and the third char A-Z
		boolean match5=matches("[0-9][a-z][A-Z]", "3TR"); 
		System.out.println("the pattern Character which should not match matches5="+match5);
*/
	}

}
