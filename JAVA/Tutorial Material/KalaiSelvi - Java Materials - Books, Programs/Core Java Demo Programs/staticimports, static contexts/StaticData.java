class StaticData
{
  static int i;
  int j;

  StaticData(int j)
  {
    i++;
    this.j=j;
  }

  void show()
  {
    System.out.println("Class Variable:"+i);
    System.out.println("Object Variable:"+j);
  }

  public static void main(String args[])
  {
    StaticData s1 = new StaticData(5);
    s1.show();
    StaticData s2 = new StaticData(10);
    s2.show();
    s1.show();
    i=10;
    s1.j=20;
    s2.j=30;
    s1.i=15;
    System.out.println(s2.i);
    System.out.println(s1.j);
  }

}


