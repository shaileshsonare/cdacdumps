package staticimports;


import static java.lang.Math.*;


import java.util.InputMismatchException;
import java.util.Scanner;
import static java.util.regex.Pattern.matches;


public class staticimport {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		
		double de=E;
		double rand=random();
		System.out.println("the random number is="+rand);
		System.out.println("the E value is="+de);
		double d=PI;
		System.out.println("the static var import="+d);
		
		
		/**
		 * Scanner class to read input from keyboard
		 */

		/*try
		{
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the int for input:");
		int i=sc.nextInt();
		System.out.println("enter the second int for input:");
		int j=sc.nextInt();
		System.out.println("the input typed is="+(i+j));
		
		
		double du=0;
		System.out.printf("enter the double %6.3e",du=sc.nextDouble());
		System.out.println("\nthe int value is ="+du+"\n");
		
		
		System.out.println("get float as input");
		float fl=sc.nextFloat();
		System.out.println("the float given is="+fl);
		
		
		
		System.out.printf("the int=%2d the double=%e the float is=%f",i,du,fl);
		
		}
		catch (InputMismatchException e) {
			System.out.println("\n the input is mismatched="+e);
		}
*/
		/**
		 * Pattern matching examples
		 */
		 
		
		//here the X schould acure X{n} n times
		boolean match=matches("X{4}", "XXXX"); 
		System.out.println("the pattern matches="+match);//X 5 times so false

		//here X atleast n times
		boolean match1=matches("X{2,}", "XYX");//X should accure atleast 2 times or false value 
		System.out.println("the pattern matches1="+match1);

		// here X atleast 4 times but not more than 6 times
		boolean match2=matches("X{4,6}", "XXXXXX"); 
		System.out.println("the pattern matches2="+match2);

		//will accept ony single character a-z or A-Z and not abcde or LMNOPQ etc
		boolean match3=matches("[a-zA-Z]", "E"); 
		System.out.println("the pattern [a-zA-Z]Character matches3="+match3);

		//other than a or b or c character
		boolean match4=matches("[^abc]", "c"); 
		System.out.println("the pattern Character [^abc]which should not match matches4="+match4);

		//the first character shold be 0-9 the second a-z and the third char A-Z
		boolean match5=matches("[0-9][a-z][A-Z]", "3TR"); 
		System.out.println("the pattern Character which should not match matches5="+match5);
		
	}

}
