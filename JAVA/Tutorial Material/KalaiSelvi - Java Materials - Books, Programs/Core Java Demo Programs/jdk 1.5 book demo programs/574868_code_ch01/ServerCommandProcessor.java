import java.lang.annotation.*;

@CodeTag(authorName="Dilbert",
         lastModificationDate="Mar 23, 2004")
public class ServerCommandProcessor {
    @CodeTag(authorName="Dilbert",
             lastModificationDate="Mar 24, 2004")
    public void setParams(String serverName)
    {
        // ...
    }

    public void executeCommand(String command, Object... params)
    {
        // ...
    }
}
