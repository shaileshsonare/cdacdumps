import java.util.*;

interface BaseInterface<A> {
    A getInfo();
}

class ParentClass implements BaseInterface<Integer> {
    public Integer getInfo()
    {
        return(null);
    }
}

class ChildClass extends ParentClass implements BaseInterface<String> { }

public class BadParents {
    public static void main(String args[])
    {
        Vector<Integer> v1 = new Vector<Integer>();
        Vector v2;

        v1 = v2;
        v2 = v1;
    }
}