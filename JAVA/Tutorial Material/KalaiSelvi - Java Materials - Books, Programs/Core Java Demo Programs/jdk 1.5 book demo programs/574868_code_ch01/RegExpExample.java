import java.util.*;
import java.util.regex.*;
import java.io.*;

public class RegExpExample {

    public static void main(String args[])
    {
        String fileName = "RETestSource.java";

        String unadornedClassRE = "^\\s*class (\\w+)";
        String doubleIdentifierRE = "\\b(\\w+)\\s+\\1\\b";

        Pattern classPattern = Pattern.compile(unadornedClassRE);
        Pattern doublePattern = Pattern.compile(doubleIdentifierRE);
        Matcher classMatcher, doubleMatcher;

        int lineNumber=0;

        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line;

            while( (line=br.readLine()) != null) {
                lineNumber++;

                classMatcher = classPattern.matcher(line);
                doubleMatcher = doublePattern.matcher(line);

                if(classMatcher.find()) {
                    System.out.println("The class [" +
                                           classMatcher.group(1) +
                                       "] is not public");
                }

                while(doubleMatcher.find()) {
                    System.out.println("The word \"" + doubleMatcher.group(1) +
                                       "\" occurs twice at position " +
                                       doubleMatcher.start() + " on line " +
                                       lineNumber);
                }
            }
        } catch(IOException ioe) {
            System.out.println("IOException: " + ioe);
            ioe.printStackTrace();
        }
    }
}
