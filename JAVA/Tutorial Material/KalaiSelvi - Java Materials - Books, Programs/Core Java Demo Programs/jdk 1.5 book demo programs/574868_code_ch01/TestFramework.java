import java.lang.reflect.*;
import java.lang.annotation.*;
import java.util.*;

public class TestFramework {
    static void executeTests(String className)
    {
        try {
            Object obj = Class.forName(className).newInstance();

            TestParameters tp = obj.getClass().getAnnotation(TestParameters.class);

            if(tp != null) {
                String methodList = tp.testMethods();
                StringTokenizer st = new StringTokenizer(methodList, ",");
                while(st.hasMoreTokens()) {
                    String methodName = st.nextToken();

                    Method m = obj.getClass().getDeclaredMethod(methodName);
                    System.out.println(methodName);
                    System.out.println("----------------");
                    String result = invoke(m, obj);
                    System.out.println("Result: " + result);
                }
            } else {
                System.out.println("No annotation found for " + obj.getClass());
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    static String invoke(Method m, Object o)
    {
        String result = "PASSED";

        try {
            m.invoke(o);
        } catch(Exception ex) {
            result = "FAILED";
        }

        return(result);
    }

    public static void main(String [] args)
    {
        executeTests(args[0]);
    }
}
