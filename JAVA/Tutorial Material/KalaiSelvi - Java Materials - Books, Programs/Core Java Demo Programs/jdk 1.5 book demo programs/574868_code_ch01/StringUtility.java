
public class StringUtility {
@TestParameters(testStage="Unit",
                testMethods="testConcat,testSubstring",
                testOutputType="screen",
                testOutput="")
    public String concat(String s1, String s2)
    {
        return(s1 + s2);
    }

    public String substring(String str, int start, int end)
    {
        return(str.substring(start, end));
    }

    public boolean testConcat()
    {
        String s1 = "test";
        String s2 = " 123";

        return(concat(s1,s2).equals("test 123"));
    }

    public boolean testSubstring()
    {
        String str = "The cat landed on its feet";

        return(substring(str, 4, 3).equals("cat"));
    }
}