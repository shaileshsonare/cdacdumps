public class StringBufferDemo {

	public static void main(String[] args) {
		/***************************************/
		StringBuffer sb = new StringBuffer("abc");
		sb.append("def");
		System.out.println("sb = " + sb);
		
		/***********************************/
		StringBuffer sbr = new StringBuffer("This is a test.");
	    sbr.replace(5, 7, "was");
	    System.out.println("After replace: " + sbr);

	    /**************************************/
	    StringBuffer sb1 = new StringBuffer("abcde");
	    sb1.append("abcdefghij");
	    StringBuffer sb2 = sb1.reverse();
	    System.out.println(sb1);
	    System.out.println(sb2);

		
	}
}
