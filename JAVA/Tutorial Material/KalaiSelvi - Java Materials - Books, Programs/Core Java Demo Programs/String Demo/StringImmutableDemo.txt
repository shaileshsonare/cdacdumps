public class StringImmutableDemo {
	
	public static void main(String[] args) {
		String s="abc"; // new string object "abc" is created
		String s2=s;	// s2 is reffered to s
		s=s.concat("def"); // s value is assigned to new string object "abcdef"
		// there is another string object "def" created, but no reference to it.
		System.out.println("the string is="+s);
	}
}
