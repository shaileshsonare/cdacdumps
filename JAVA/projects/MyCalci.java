import java.applet.Applet;
import java.awt.Button;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MyCalci extends Applet implements ActionListener {

	TextField txtBox;
	Button btn1;
	Button btn2;
	Button btn3;
	Button btn4;
	Button btn5;
	Button btn6;
	Button btn7;
	Button btn8;
	Button btn9;
	Button btn0;
	Button btnplus;
	Button btnminus;
	Button btnmul;
	Button btndiv;
	Button btnequal;
	Button btnclear;
	
	String s1;
	int num1;
	int result;
	int num2;
	String resmsg;
	String oper;
	public void init()
	{
		txtBox=new TextField(12);
		btn1=new Button("1");
		btn2=new Button("2");
		btn3=new Button("3");
		btn4=new Button("4");
		btn5=new Button("5");
		btn6=new Button("6");
		btn7=new Button("7");
		btn8=new Button("8");
		btn9=new Button("9");
		btn0=new Button("0");
		btnplus=new Button("+");
		btnminus=new Button("-");
		btnmul=new Button("*");
		btndiv=new Button("/");
		btnequal=new Button("=");
		btnclear=new Button("C");
		s1="";
		num1=num2=result=0;
		oper="";
	}
	
	public void start()
	{
		add(txtBox);
		btn1.addActionListener(this);
		add(btn1);
		btn2.addActionListener(this);
		add(btn2);
	//	btn2.addActionListener(this);
		add(btn3);
		btn3.addActionListener(this);
		add(btnplus);
		btnplus.addActionListener(this);
		add(btn4);
		btn4.addActionListener(this);
		add(btn5);
		btn5.addActionListener(this);
		add(btn6);
		btn6.addActionListener(this);
		add(btnminus);
		btnminus.addActionListener(this);
		add(btn7);
		btn7.addActionListener(this);
		add(btn8);
		btn8.addActionListener(this);
		add(btn9);
		btn9.addActionListener(this);
		add(btnmul);
		btnmul.addActionListener(this);
		add(btnclear);
		btnclear.addActionListener(this);
		add(btn0);
		btn0.addActionListener(this);
		add(btnequal);
		btnequal.addActionListener(this);
		add(btndiv);
		btndiv.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		
		showStatus(ae.getActionCommand());
		if(ae.getSource().equals(btn1))
		{
			s1=txtBox.getText();
			s1+="1";
			//txtBox.setText(s1);
		}
		
		if(ae.getSource().equals(btn2))
		{
			
			s1=txtBox.getText();
			s1+="2";
			//String s=s1.substring(0,1);
			txtBox.setText(s1);
			
		}
		
		if(ae.getSource().equals(btn3))
		{
			
			s1=txtBox.getText();
			s1+="3";
			txtBox.setText(s1);
			
		}
		
		if(ae.getSource().equals(btn4))
		{
			
			s1=txtBox.getText();
			s1+="4";
			txtBox.setText(s1);
			
		}
		
		if(ae.getSource().equals(btn5))
		{
			
			s1=txtBox.getText();
			s1+="5";
			txtBox.setText(s1);
			
		}
		
		if(ae.getSource().equals(btn6))
		{
			
			s1=txtBox.getText();
			s1+="6";
			txtBox.setText(s1);
			
		}
		
		if(ae.getSource().equals(btn7))
		{
			
			s1=txtBox.getText();
			s1+="7";
			txtBox.setText(s1);
			
		}
		
		if(ae.getSource().equals(btn8))
		{
			
			s1=txtBox.getText();
			s1+="8";
			txtBox.setText(s1);
			
		}
		
		if(ae.getSource().equals(btn9))
		{
			
			s1=txtBox.getText();
			s1+="9";
			txtBox.setText(s1);
			
		}
		
		if(ae.getSource().equals(btn0))
		{
			
			s1=txtBox.getText();
			s1+="0";
			txtBox.setText(s1);
			
		}
		if(ae.getSource().equals(btnplus))
		{
			num1=Integer.parseInt(txtBox.getText());
			s1="";
			oper="+";
			txtBox.setText(s1);
		}	
		
		
		
		if(ae.getSource().equals(btnminus))
		{
			num1=Integer.parseInt(txtBox.getText());
			s1="";
			oper="-";
			txtBox.setText(s1);
			
		}
		
		if(ae.getSource().equals(btnmul))
		{
			num1=Integer.parseInt(txtBox.getText());
			s1="";
			oper="*";
			txtBox.setText(s1);
			
		}
		
		if(ae.getSource().equals(btndiv))
		{
			num1=Integer.parseInt(txtBox.getText());
			s1="";
			oper="/";
			txtBox.setText(s1);
			
		}
		if(ae.getSource().equals(btnclear))
		{
			num1=num2=result=0;
			s1="";
			oper="";
			txtBox.setText(s1);
			
		}
		
		
		if(ae.getSource().equals(btnequal))
		{
			num2=Integer.parseInt(txtBox.getText());
			txtBox.setText("");
			
			if(oper.equals("+"))
			{
			result=num2+num1;
			}

			if(oper.equals("-"))
			{
				result=num1-num2;
			}
			
			if(oper.equals("*"))
			{
				result=num1*num2;
			}
			
			if(oper.equals("/"))
			{
				result=num1/num2;
			}
			resmsg=""+result;
			
			txtBox.setText(resmsg);
			
		}
		
	}
	
	
}
