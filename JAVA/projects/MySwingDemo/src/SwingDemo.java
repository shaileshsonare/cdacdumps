import java.applet.Applet;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class SwingDemo extends Applet implements ActionListener {

	JFrame f;
	JLabel lblNum1,lblNum2,lblRes;
	JTextField txt1,txt2;
	JButton btnAdd,btnSub,btnMul,btnDiv;
	String result;
	
	public void jDisplay()
	{
		f=new JFrame();
		f.setSize(200, 200);
		f.setVisible(true);
		f.setLayout(new FlowLayout());
		
		lblNum1=new JLabel("Num1");
		lblNum2=new JLabel("num2");
		txt1=new JTextField(20);
		txt2=new JTextField(20);
		btnAdd=new JButton("Add");
		btnSub=new JButton("Sub");
		btnMul=new JButton("Mul");
		btnDiv=new JButton("Div");
		lblRes=new JLabel("Result");
		result="";
		
		
		f.add(lblNum1);
		f.add(txt1);
		f.add(lblNum2);
		f.add(txt2);
		btnAdd.addActionListener(this);
		f.add(btnAdd);
		btnSub.addActionListener(this);
		f.add(btnSub);
		btnMul.addActionListener(this);
		f.add(btnMul);
		btnDiv.addActionListener(this);
		f.add(btnDiv);
		f.add(lblRes);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		new SwingDemo().jDisplay();
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource().equals(btnAdd))
		{
			String num1=txt1.getText();
			String num2=txt2.getText();
			int n1=Integer.parseInt(num1);
			int n2=Integer.parseInt(num2);
			Integer sum=n1+n2;
			System.out.println(sum);	
			
			
				lblRes.setText(sum.toString());
		}
		
	}

}
