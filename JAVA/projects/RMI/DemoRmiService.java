import java.rmi.Remote;
import java.rmi.RemoteException;


public interface DemoRmiService extends Remote{

	public int add(int num1,int num2) throws RemoteException;
	
	public int sub(int num1, int num2) throws RemoteException; 
}
