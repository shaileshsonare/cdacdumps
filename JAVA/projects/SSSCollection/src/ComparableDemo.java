import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

class Student implements Comparable<Student>
{
	int SrollNo;

	public Student(int srollNo) {
		super();
		SrollNo = srollNo;
	}

	public Student() {
		super();
	}

	@Override
	public int compareTo(Student s) {
		// TODO Auto-generated method stub
		if(this.SrollNo<s.SrollNo)
			return -1;
		else
			if(this.SrollNo==s.SrollNo)
				return 0;
			else
				return 1;
	}

	@Override
	public String toString() {
		return "Student [SrollNo=" + SrollNo + "]";
	}

}

public class ComparableDemo {

	
	
	
	public static void main(String[] args) {
		ComparableDemo c= new ComparableDemo();
		Set s=new TreeSet();
		s.add(new Student(25));
		s.add(new Student(12));
		System.out.println(s);
		//Iterator
		System.out.println("Iterator...");
		Iterator<Student>it=s.iterator();
		while(it.hasNext())
		{
			Student ss=it.next();
			System.out.println(ss);
		}
	
		
	}

}
