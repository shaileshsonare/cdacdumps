import java.applet.Applet;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class AwtDemo extends Applet implements ActionListener {

	Frame f;
	Label lblNum1,lblNum2,lblRes;
	TextField txt1,txt2;
	Button btnAdd,btnSub,btnMul,btnDiv;
	String result;
	
	
	public void displayf()
	{
		f=new Frame();
		f.setSize(200, 200);
		f.setVisible(true);
		f.setLayout(new FlowLayout());
		
		lblNum1=new Label("Num1");
		lblNum2=new Label("num2");
		txt1=new TextField(20);
		txt2=new TextField(20);
		btnAdd=new Button("Add");
		btnSub=new Button("Sub");
		btnMul=new Button("Mul");
		btnDiv=new Button("Div");
		lblRes=new Label("Result");
		result="";
		
		
		f.add(lblNum1);
		f.add(txt1);
		f.add(lblNum2);
		f.add(txt2);
		btnAdd.addActionListener(this);
		f.add(btnAdd);
		btnSub.addActionListener(this);
		f.add(btnSub);
		btnMul.addActionListener(this);
		f.add(btnMul);
		btnDiv.addActionListener(this);
		f.add(btnDiv);
		f.add(lblRes);
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
			{
				f.dispose();
			}
		});
		
				
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		new AwtDemo().displayf();
	}
	@Override
	public void actionPerformed(ActionEvent ae) {
		// TODO Auto-generated method stub
		if(ae.getSource().equals(btnAdd))
		{
			String num1=txt1.getText();
			String num2=txt2.getText();
			int n1=Integer.parseInt(num1);
			int n2=Integer.parseInt(num2);
			
			Integer sum=n1+n2;
			System.out.println(sum);	
			
			
				lblRes.setText(sum.toString());
		}
		
	}

}
