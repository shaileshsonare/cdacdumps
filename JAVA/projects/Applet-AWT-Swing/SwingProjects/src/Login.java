
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class Login implements ActionListener{

	JFrame jf=null;
	
	JLabel lblUserName;
	JLabel lblPassword;
	
	JTextField txtUserName;
	JTextField txtPassword;
	
	JButton btnSubmit;
	
	public void display()
	{
		jf=new JFrame("Hello World");
		
		txtUserName=new JTextField(20);
		txtPassword=new JTextField(20);
		
		lblUserName = new JLabel("Username");
		lblPassword = new JLabel("Password");
		
		btnSubmit = new JButton("Submit");
		btnSubmit.setText("addition");
		btnSubmit.setName("add");
		
		jf.setSize(new Dimension(400, 500));
		jf.setVisible(true);
		
		jf.setLayout(new FlowLayout());
		
		jf.add(lblUserName);
		jf.add(txtUserName);
		jf.add(lblPassword);
		jf.add(txtPassword);
		jf.add(btnSubmit);
		
		btnSubmit.addActionListener(this);
	}
	
	public static void main(String[] args) {

		new Login().display();
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		// TODO Auto-generated method stub
		
		System.out.println(btnSubmit.getName());
		System.out.println(btnSubmit.getText());
		
		System.out.println(txtUserName.getText());
		System.out.println(txtPassword.getText());
	}

}
