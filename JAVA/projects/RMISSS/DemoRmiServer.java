import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class DemoRmiServer extends UnicastRemoteObject implements DemoRmiService{

	/**
	 * @param args
	 */
	
	public DemoRmiServer() throws RemoteException{
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public int add(int num1, int num2) throws RemoteException {
		// TODO Auto-generated method stub
		return num1+num2+55;
	}

	@Override
	public int sub(int num1, int num2) throws RemoteException {
		// TODO Auto-generated method stub
		return num1 - num2;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		try {
			DemoRmiServer d = new DemoRmiServer();
			Registry reg =LocateRegistry.createRegistry(5525);
			reg.bind("serv1", d);
			//Naming.bind("serv", d);
			System.out.println("server connected with registry...");
		/*} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();*/
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	

}
