
public class Test1 
{
	int y=-20;				  			//instance variable which needs object to access and no need to assign value...
	
	static int z=69;         			//it will be stored in static segment memory....
		
	
	void display1()						//since it is a non-static method, it requires object to call
	{	
		System.out.println("\nthis is a non-static method");
		System.out.println("addition non-static = "+(y+z));		//we can access static as well as non-static variables inside non-static method...
	}
	
	
	static void display2()			   //since it is a static method, it dosen't requires object to call it
	{
		Test1 t2=new Test1();			
		System.out.println("\nthis is a static method....");
		System.out.println("instance variable..."+t2.y);
		System.out.println("static variable..."+z);
		
		System.out.println("addition static method = "+(t2.y+z));
	}
	
	
	public static void main(String[] args) 
	{
		
		Test1 t1=new Test1();
		
		int x=10;		//local variable,,,no need to create object,,,but needs to initialize the value..
		
		System.out.println("First Java Program   ");
		System.out.println("value of x "+x);
		System.out.println("value of y "+t1.y);
		System.out.println("static variable without object is "+z);
		System.out.println("static variable with object is "+t1.z);
		t1.display1();
		
		display2();
		
	}
}
