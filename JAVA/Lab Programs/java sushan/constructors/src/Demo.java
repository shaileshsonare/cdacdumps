
public class Demo {
	
	int x;
	int y;
	
	Demo()
	{
		
		this(444);
		
		System.out.println("Demo Constructor default......"+x);
	}
	
	Demo(int x)
	{
		this(50,100);
		this.x=x;
		System.out.println("Demo Parameterized Constructor...."+this.x);
	}
	
	Demo(int x,int y)
	{
		//this.x=x;
		method1(x);
         this.y=y+x;
         this.x=x+y;
	  
		System.out.println("Demo 2 arguments Constructor....x:"+this.x+ "y:"+this.y);
	}
	
	void method1(int x)
	{
		System.out.println("I m in method 1....."+x);
		method2();
	}
	
	void method2()
	{
		System.out.println("I m in method 2....."+x);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Demo d1=new Demo();
		

	}

}
