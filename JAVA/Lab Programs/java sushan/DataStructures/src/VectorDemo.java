import java.util.Enumeration;
import java.util.Vector;
import java.util.Iterator;

class Student
{
	String name;

	public Student(String name) {
		super();
		this.name = name;
	}
	
	
}


public class VectorDemo {
	
	
	public static void main(String[] args) {
	
		Vector v=new Vector();
		
		v.add(10);
		v.add(20);
		v.add(30);
		v.add(new Student("Siddhant"));
		v.add(new Student("Kasab"));
		v.remove(4);
	
	//	System.out.println(	v.hashcode());
		System.out.println("Initial capacity "+v.capacity());
		System.out.println("Size is  "+v.size());
		
		/*
		for(Object i:v)
		{
			System.out.println((int)i);
		}
		*/
		
	/*	Iterator i=v.iterator();
		
		while(i.hasNext())
		{
			System.out.print(i.hasNext());
			Object obj=i.next();
			
			if(obj instanceof Student)
			{
		//		Student s=(Student)obj;
				System.out.println(((Student)obj).name);
			}
			else
			System.out.println(obj);
		}
		System.out.print(i.hasNext());
*/
	  Enumeration e=v.elements();
	    
	  
	    while(e.hasMoreElements())
	    {
	    	Object o=e.nextElement();
	    	
	    	if(o instanceof Student)
	    	{
	    		 System.out.println(((Student)o).name);
	    	}
	    	else
	    		
	    	{
	    		 System.out.println(o);
	    	}	 
	    	
	    }
	
	}

}

	
	