import java.lang.Comparable;
import java.util.Set;
import java.util.TreeSet;

class Student implements Comparable
{
	
	int rollno;
	String name;
	public Student(int rollno, String name) {
		super();
		this.rollno = rollno;
		this.name = name;
	}
	
	
	
	
	public String toString()
	{
		return "\nRoll NO. "+rollno+" Name "+name;
		
	}

	
	
	@Override
	public int compareTo(Object obj) 
	{
		
		System.out.println("Comparable call zala re zala..........");
		Student s=(Student)obj;
		
		//int no1=this.rollno;
		//int no2=s.rollno;
		
		String s1=this.name;
		String s2=s.name;
		
		//System.out.println("This roll no  "+no1);
		//System.out.println("Object roll no  "+no2);
		
		return s2.compareTo(s1);
		
		
		/*if(no1<no2)
			return -1;
		else
			return (no1==no2?0:1);
	*/	
	}
	
}

public class ComparableDemo {
	
	public static void main(String[] args) {
		
		Set s=new TreeSet();
		int x=10;
		
		s.add(1);	//Integer i=new Integer(1); not like //int i=1;
		s.add(5);
		s.add(2);
		s.add(x);
		
		System.out.println(s);
		
		Set s1=new TreeSet();
		
		s1.add(new Student(5, "ABCailesh"));
		s1.add(new Student(4, "ACDhan"));
		s1.add(new Student(3, "AABiket"));
		
		System.out.println(s1);
		


	}

}
