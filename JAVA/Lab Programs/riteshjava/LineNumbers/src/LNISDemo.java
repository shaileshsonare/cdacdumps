import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.LineNumberInputStream;
import java.util.Scanner;


public class LNISDemo {

	public void readData()
	{
		//LineNumberInputStream lnis=new LineNumberInputStream(new FileInputStream(new File("MyFile.txt")));
		
		try {
			Scanner sc=new Scanner(new File("MyFile.txt"));
			
			while(sc.hasNext())
			{
				System.out.println(sc.nextLine());
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		
		LNISDemo l=new LNISDemo();
		l.readData();

	}

}
