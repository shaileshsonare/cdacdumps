class AccountTransaction
{
	int balance;
	
	public AccountTransaction(int balance) {
		this.balance = balance;
	}
	
	
	public synchronized int deposit(int accno,int amt)
	{
		System.out.println("Deposit");
		
		if (accno>100)
		{
			if (balance>=1500)
			{
				System.out.println("Account No : "+accno);
				System.out.println("Balance : "+balance);
				System.out.println("Amt to Deposit : "+amt);
				balance +=amt;
				notify();
			} else
			   {
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  }	
		}   
		else
			System.out.println("Invalid Account Number");
		return balance;
	}
	
	public int withdraw(int accno,int amt)
	{
		System.out.println("Withdraw");
		if (accno>100)
		{
			if (balance-amt>=500)//
			{
				System.out.println("Account No : "+accno);
				System.out.println("Balance : "+balance);
				System.out.println("Amt to Withdraw : "+amt);
				balance -=amt;//
				notify();
			} else
			   {
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			   }	
		}		
		else
			System.out.println("Invalid Account Number");
		return balance;
	}	
}

class Customer extends Thread
{	
	
	AccountTransaction acctrans;
	int accno;
	int amt;
	int status;
    Thread custThread;
    
	public Customer(AccountTransaction acctrans,int accno,int amt,int status) {
		this.acctrans = acctrans;
		this.accno = accno;
		this.amt = amt;
		this.status = status;		
	    custThread = new Thread(this);
	    custThread.start();	
	}
	
	public void run()
	{
	  switch (status)
	  {
	  	case 1 : 
	  		    int balance = acctrans.deposit(accno, amt);
	  		    System.out.println("Current Balance After Depositing : "+balance);
	  			 break;
	  			 
	  	case 2 : //.....
	  	          synchronized (acctrans) {
	  	        	acctrans.balance= acctrans.withdraw(accno, amt);	
				}
	  	
	  	        System.out.println("Current Balance After Withdrawing : "+acctrans.balance);
			 	 break;
			 	 
	  	/*case 3 : acctrans.fundtransfer(accno, amt);
			 	 break;*/
			 	 
	  	default : System.out.println("Incorrect Option");
			 
	  }
      
    }
}


public class BankingApplication {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AccountTransaction acctrans = new AccountTransaction(2000);
		Customer tirupthy = new Customer(acctrans, 1567, 2500, 1);
		Customer balaji = new Customer(acctrans, 1567, 1500, 2);
	}

}
