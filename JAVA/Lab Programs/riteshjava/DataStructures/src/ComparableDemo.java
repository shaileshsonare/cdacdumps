import java.lang.Comparable;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashSet;

class Person implements Comparable
{
	int rollno;
	
	public Person(int x)
	{
		this.rollno=x;
	}

	public int compareTo(Object obj1) {
		Person obj=(Person)obj1;
		
		if(this.rollno<obj.rollno)
			return 1;
			else
				return (this.rollno==obj.rollno ? 0 : -1);
	}
	
	public String toString()
	{
		return ""+rollno;
	}
}



public class ComparableDemo {

	
	public static void main(String[] args) {
		
		Set s=new TreeSet();
		
		s.add(new Person(4));
		s.add(new Person(8));
		s.add(new Person(1));
		
		System.out.println(s);

	}

}
