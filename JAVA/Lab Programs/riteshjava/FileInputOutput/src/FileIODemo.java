import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class FileIODemo {
	
	File f=new File("MyFile.txt");
	
	public void readFile()
	{
		try {
			Scanner sc=new Scanner(f);
			
			while(sc.hasNext())
			{
				System.out.println(sc.nextLine());
			}
			
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	
	}
	
	public static void main(String[] args) {
	
		FileIODemo fiod=new FileIODemo();
	//	fiod.create();
		fiod.readFile();
	}

}
