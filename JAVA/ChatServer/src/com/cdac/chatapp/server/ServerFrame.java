package com.cdac.chatapp.server;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ServerFrame extends JFrame implements ActionListener, Runnable,KeyListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ServerSocket svr;
	Socket clt;
	DataInputStream din;
	DataOutputStream dout;
	
	JTextArea chatTextArea;
	JScrollPane scrollPane;
	JTextField msgTextField;
	JButton sendButton;
	
	public ServerFrame() {
		initComponents();
		initServer();
	}
	private void initServer() {
		// create server socket
		try{
			svr = new ServerSocket(7654);
		}catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		// accept client connection
		Thread listenerThread = new Thread(new Runnable() {
			public void run() {
				try{
					clt = svr.accept();
					din = new DataInputStream(clt.getInputStream());
					dout = new DataOutputStream(clt.getOutputStream());
					sendButton.setEnabled(true);
					
					Thread readerThread = new Thread(ServerFrame.this);
					readerThread.setDaemon(true);
					readerThread.start();
				}catch (Exception e) {
					// ignore exception
				}
			}
		});
		listenerThread.setDaemon(true);
		listenerThread.start();
	}
	private void initComponents() {
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
		this.setTitle("Chat Server");
		this.setLayout(null);
		
		chatTextArea = new JTextArea();
		chatTextArea.setEditable(false);

		scrollPane = new JScrollPane(chatTextArea);
		scrollPane.setBounds(20, 20, 360, 300);		
		this.add(scrollPane);
		
		msgTextField = new JTextField();
		msgTextField.setBounds(20, 330, 270, 30);
		msgTextField.addKeyListener(this);
		this.add(msgTextField);
		
		sendButton = new JButton();
		sendButton.setBounds(300, 330, 80, 30);
		sendButton.setText("Send");
		sendButton.addActionListener(this);
		sendButton.setEnabled(false);
		this.add(sendButton);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try{
			String msg = "server : " + msgTextField.getText();
			dout.writeUTF(msg);
			chatTextArea.append(msg + "\r\n");
			msgTextField.setText("");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	// read data from data input stream from client
	@Override
	public void run() {
		try {
			while(true) {
				String msg = din.readUTF();
				chatTextArea.append(msg + "\r\n");
			}
		}catch (Exception e) {
			// will raise if client is closed
			this.close();
		}
	}
	
	private void close() {
		try {
			if(dout!=null)
				dout.close();
			if(din!=null)
				din.close();
			if(clt!=null)
				clt.close();
			if(svr!=null)
				svr.close();
		} catch (Exception ex) {
			// ignore exception
		}
		dispose();		
	}
	
	public static void main(String[] args) {
		ServerFrame frm = new ServerFrame();
		frm.setResizable(false);
		frm.setSize(400, 400);
		frm.setVisible(true);
	}
	@Override
	public void keyPressed(KeyEvent ke) {
		//System.out.println(ke.getKeyCode()+"----"+ke.getID());
		if(ke.getKeyCode()==KeyEvent.VK_ENTER)
		{
			try{
				String msg = "server : " + msgTextField.getText();
				dout.writeUTF(msg);
				chatTextArea.append(msg + "\r\n");
				msgTextField.setText("");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
