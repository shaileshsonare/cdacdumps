package com.cdac.chatapp.client;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ClientFrame extends JFrame implements ActionListener, Runnable,KeyListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Socket clt;
	DataInputStream din;
	DataOutputStream dout;
	
	JTextArea chatTextArea;
	JScrollPane scrollPane;
	JTextField msgTextField;
	JButton sendButton;
	
	public ClientFrame() {
		initComponents();
	}
	private void initClient() {
		// create client socket and connect
		try{
			clt = new Socket("127.0.0.1", 7654);
			din = new DataInputStream(clt.getInputStream());
			dout = new DataOutputStream(clt.getOutputStream());

			Thread readerThread = new Thread(this);
			readerThread.setDaemon(true);
			readerThread.start();		
		}catch (Exception e) {
			// exception will be thrown in server not running
			close();
			return;
		}		
	}
	private void initComponents() {
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
		this.setTitle("Chat Client");
		this.setLayout(null);
		
		chatTextArea = new JTextArea();
		chatTextArea.setEditable(false);

		scrollPane = new JScrollPane(chatTextArea);
		scrollPane.setBounds(20, 20, 360, 300);		
		this.add(scrollPane);
		
		msgTextField = new JTextField();
		msgTextField.setBounds(20, 330, 250, 30);
		msgTextField.addKeyListener(this);
		this.add(msgTextField);
		
		sendButton = new JButton();
		sendButton.setBounds(280, 330, 100, 30);
		sendButton.setText("Connect");
		sendButton.addActionListener(this);
		this.add(sendButton);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try{
			JButton btn = (JButton)e.getSource();
			String btnText = btn.getText();
			if(btnText.equalsIgnoreCase("Connect")) {
				initClient();
				btn.setText("Send");
			} else {
				String msg = "client : " + msgTextField.getText();
				dout.writeUTF(msg);
				chatTextArea.append(msg + "\r\n");
				msgTextField.setText("");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	// read data from data input stream from client
	@Override
	public void run() {
		try {
			while(true) {
				String msg = din.readUTF();
				chatTextArea.append(msg + "\r\n");
			}
		}catch (Exception e) {
			// will raise if server is closed
			this.close();
		}
	}
	
	private void close() {
		try {
			if(dout!=null)
				dout.close();
			if(din!=null)
				din.close();
			if(clt!=null)
				clt.close();
		} catch (Exception ex) {
			// ignore exception
		}
		dispose();		
	}
	
	public static void main(String[] args) {
		ClientFrame frm = new ClientFrame();
		frm.setResizable(false);
		frm.setSize(400, 400);
		frm.setVisible(true);
	}
	@Override
	public void keyPressed(KeyEvent ke) {
		if(ke.getKeyCode()==KeyEvent.VK_ENTER)
		{
			try{
				String btnText = sendButton.getText();
				if(btnText.equalsIgnoreCase("Connect")) {
					initClient();
					sendButton.setText("Send");
				} else {
					String msg = "client : " + msgTextField.getText();
					dout.writeUTF(msg);
					chatTextArea.append(msg + "\r\n");
					msgTextField.setText("");
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
