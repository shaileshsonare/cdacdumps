import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;


public class Type4OracleDemo {


	Statement stmt=null;
	PreparedStatement pstmt=null;
	CallableStatement cstmt=null;
	ResultSet rs=null;
	String sql="";
	
	public void connection()
	{
		try(Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521","acts","dac"))
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("Driver loaded.........");
			
						
			/*stmt=conn.createStatement();
			
			sql="select * from employees";
			
			rs=stmt.executeQuery(sql);
			
			while(rs.next())
			{
				System.out.println("Last Name::"+rs.getString("LAST_NAME"));
			}
			*/
			/*sql="select last_name,salary from employees where employee_id=?";
			pstmt=conn.prepareStatement(sql);
			
			pstmt.setInt(1, 100);
			
			rs=pstmt.executeQuery();
			
			while(rs.next())
			{
				System.out.println("Last Name::"+rs.getString("LAST_NAME"));
			}*/
			
			
			cstmt=conn.prepareCall("{call salarys(?,?)}");
			
			cstmt.setInt(1, 10000);
			cstmt.registerOutParameter(2, Types.INTEGER);
			
			boolean flag=cstmt.execute();
			
			System.out.println(flag);
			
			System.out.println(cstmt.getInt(2));
			
			
			
		} 
		
		catch (ClassNotFoundException|SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		
		new Type4OracleDemo().connection();

	}

}
