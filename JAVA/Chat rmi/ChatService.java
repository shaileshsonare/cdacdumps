import java.rmi.Remote;
import java.rmi.RemoteException;


public interface ChatService extends Remote{

	public String add(String msg) throws RemoteException;
	
}