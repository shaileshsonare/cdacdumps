import java.util.LinkedList;
import java.util.*;

class Address {
  private String name;

  private String street;

  private String city;

  private String state;

  private String code;


	Address()
	{}
  Address(String n, String s, String c, String st, String cd) {
    name = n;
    street = s;
    city = c;
    state = st;
    code = cd;
  }

  void setData()
	{
	  Scanner sc=new Scanner(System.in);
	  name=sc.nextLine();
	  street = sc.nextLine();
    city = sc.nextLine();
    state = sc.nextLine();
    code = sc.nextLine();
	  }

  public String toString() {
    return name + " " + street + " " + city + " " + state + " " + code;
  }
}

class MailList {
  public static void main(String args[]) {
    LinkedList<Address> ml = new LinkedList<Address>();
	
	Address a1=new Address();
	a1.setData();

    ml.add(new Address("A", "11 Ave", "U", "IL", "11111"));
    ml.add(new Address("R", "11 Lane", "M", "IL", "22222"));
    ml.add(new Address("T", "8 St", "C", "IL", "33333"));
	ml.add(a1);

    for (Address element : ml)
      System.out.println(element + "\n");

  }
}