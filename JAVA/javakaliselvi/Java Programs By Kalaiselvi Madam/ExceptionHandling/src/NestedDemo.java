
public class NestedDemo {

	public void invoke()
	{
		String s;
		try  //outer
		{
			
			s = "abc";
			//s=null;
			System.out.println(s.length());
			
			int a[];
			try //inner
			{
				
				a = new int[6];
				System.out.println(a[6]);
				
			}catch(StringIndexOutOfBoundsException ne)
			{
				System.out.println("Null Object");
			}
	
			finally
			{
				a = null;
				System.out.println("inner finally");
			}
			
			//outer - try's  catch
			
		}/*catch(ArrayIndexOutOfBoundsException ne)
		{
			System.out.println("Null Object");
		}*/

		catch(NullPointerException ne)
		{
			System.out.println("Null Object");
		}
		/*catch(Exception e)
		{
			System.out.println(e.getMessage());
		}*/
		
		finally
		{
			System.out.println("outer finally");
		}
		
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		new NestedDemo().invoke();

	}

}
