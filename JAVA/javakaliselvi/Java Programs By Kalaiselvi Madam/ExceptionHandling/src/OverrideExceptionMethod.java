import java.io.IOException;

class A
{
	public void m1()throws IOException 
	{
		
	}
	
}


class B extends A
{
	public void m1() 
	{
		
	}
}

public class OverrideExceptionMethod {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new A().m1();

	}

}
