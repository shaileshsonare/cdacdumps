var gradientshadow={}
gradientshadow.depth=6 //Depth of shadow in pixels
gradientshadow.containers=[]
gradientshadow.create=function(){
var a = document.all ? document.all : document.getElementsByTagName('*')
for (var i = 0;i < a.length;i++) {
	if (a[i].className == "shadow") {
		for (var x=0; x<gradientshadow.depth; x++){
			var newSd = document.createElement("DIV")
			newSd.className = "shadow_inner"
			newSd.id="shadow"+gradientshadow.containers.length+"_"+x //Each shadow DIV has an id of "shadowL_X" (L=index of target element, X=index of shadow (depth) 
			if (a[i].getAttribute("rel"))
				newSd.style.background = a[i].getAttribute("rel")
			else
				newSd.style.background = "black" //default shadow color if none specified
			document.body.appendChild(newSd)
		}
	gradientshadow.containers[gradientshadow.containers.length]=a[i]
	}
}
gradientshadow.position()
window.onresize=function(){
	gradientshadow.position()
}
}

gradientshadow.position=function(){
if (gradientshadow.containers.length>0){
	for (var i=0; i<gradientshadow.containers.length; i++){
		for (var x=0; x<gradientshadow.depth; x++){
		var shadowdiv=document.getElementById("shadow"+i+"_"+x)
			shadowdiv.style.width = gradientshadow.containers[i].offsetWidth + "px"
			shadowdiv.style.height = gradientshadow.containers[i].offsetHeight + "px"
			shadowdiv.style.left = gradientshadow.containers[i].offsetLeft + x + "px"
			shadowdiv.style.top = gradientshadow.containers[i].offsetTop + x + "px"
		}
	}
}
}

if (window.addEventListener)
window.addEventListener("load", gradientshadow.create, false)
else if (window.attachEvent)
window.attachEvent("onload", gradientshadow.create)
else if (document.getElementById)
window.onload=gradientshadow.create

function feedback(obj)
{
	window.open('http://www.javabeat.net/articles/feedback.php?article_id='+obj,'P','width=550,height=370,toolbar=no,location=no,directories=no,status=no,menubar=no, scrollbars=yes,resizable=yes,copyhistory=no');
}
function reqArticle(obj)
{
	window.open('http://www.javabeat.net/articles/reqarticle.php?article_id='+obj,'P','width=550,height=370,toolbar=no,location=no,directories=no,status=no,menubar=no, scrollbars=yes,resizable=yes,copyhistory=no');
}
function print(obj)
{
	window.open('http://www.javabeat.net/articles/print.php?article_id='+obj,'P','width=750,height=600,toolbar=no,location=no,directories=no,status=no,menubar=no, scrollbars=yes,resizable=yes,copyhistory=no');
}
function sendEmail(obj)
{
	window.open('http://www.javabeat.net/articles/email.php?article_id='+obj,'P','width=550,height=370,toolbar=no,location=no,directories=no,status=no,menubar=no, scrollbars=yes,resizable=yes,copyhistory=no');
}
function submit(obj)
{
	window.open('http://www.javabeat.net/articles/submit.php?article_id='+obj,'P','width=550,height=170,toolbar=no,location=no,directories=no,status=no,menubar=no, scrollbars=yes,resizable=yes,copyhistory=no');
}

function tips_feedback(obj)
{
	window.open('http://www.javabeat.net/tips/feedback.php?tips_id='+obj,'P','width=550,height=300,toolbar=no,location=no,directories=no,status=no,menubar=no, scrollbars=yes,resizable=yes,copyhistory=no');
}
function tips_reqTips(obj)
{
	window.open('http://www.javabeat.net/tips/reqtips.php?tips_id='+obj,'P','width=550,height=300,toolbar=no,location=no,directories=no,status=no,menubar=no, scrollbars=yes,resizable=yes,copyhistory=no');
}
function tips_print(obj)
{
	window.open('http://www.javabeat.net/tips/print.php?tips_id='+obj,'P','width=750,height=600,toolbar=no,location=no,directories=no,status=no,menubar=no, scrollbars=yes,resizable=yes,copyhistory=no');
}
function tips_sendEmail(obj)
{
	window.open('http://www.javabeat.net/tips/email.php?tips_id='+obj,'P','width=550,height=300,toolbar=no,location=no,directories=no,status=no,menubar=no, scrollbars=yes,resizable=yes,copyhistory=no');
}
function tips_showPdf(obj)
{
	window.open('http://www.javabeat.net/tips/pdf.php?tips_id='+obj,'P','width=750,height=600,toolbar=no,location=no,directories=no,status=no,menubar=no, scrollbars=yes,resizable=yes,copyhistory=no');
}