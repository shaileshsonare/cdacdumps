class A
{
	void display()
	{
		System.out.println("A");
	}
}
class B extends A
{
	void display()
	{
		System.out.println("B");
	}
}

class Test
{
	public static void main(String args[])
	{
		A a=new A();		//object of a
		B b=new B();		//object of b
		a.display();		//a cha display
		b.display();		//b cha display
		A c=new B();		//b cha object
//		B d=new A();		//error
	}
}