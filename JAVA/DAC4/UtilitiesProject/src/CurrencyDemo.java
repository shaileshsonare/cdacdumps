import java.util.Currency;
import java.util.Locale;


public class CurrencyDemo {

	
	public static void main(String[] args) {
	
		Currency c=Currency.getInstance(Locale.UK);
		
		System.out.println(c.getCurrencyCode());
		System.out.println(c.getSymbol());

	}

}
