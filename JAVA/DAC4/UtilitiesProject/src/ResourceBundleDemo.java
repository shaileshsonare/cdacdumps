import java.util.ResourceBundle;


public class ResourceBundleDemo {

	
	public static void main(String[] args) {
	
		ResourceBundle rb=ResourceBundle.getBundle("resource");
		
		System.out.println(rb.getString("username"));
		System.out.println(rb.getString("password"));

	}

}
