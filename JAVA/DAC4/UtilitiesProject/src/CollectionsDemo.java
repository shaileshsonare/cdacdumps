import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class CollectionsDemo {


	public static void main(String[] args) {
		
		List list=new ArrayList();
		
		list.add(10);
		list.add(20);
		list.add("hello");
		
		List l=Collections.unmodifiableList(list);

		System.out.println(l);
		
	}

}
