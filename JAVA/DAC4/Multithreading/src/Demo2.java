
public class Demo2 implements Runnable
{
	int n;
	Demo2(int n)
	{
		this.n=n;
	}
	public void run()
	{
		System.out.println("square of "+n+" is "+n*n);
	}
	public void calCube()
	{
		System.out.println("cube of "+n+" is "+n*n*n);
	}
}
class Square1
{
	public static void main(String[] args) 
	{
		Demo2 d=new Demo2(2);
		
		Thread t=new Thread(d);
		t.start();
	}
}
