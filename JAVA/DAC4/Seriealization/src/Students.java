import java.io.Serializable;


public class Students implements Serializable{
	
	int roll_no;
	String name;
	
	
	public Students(int roll_no, String name) {
		super();
		this.roll_no = roll_no;
		this.name = name;
	}
	
	public String toString()
	{
		return "Roll No. :"+roll_no+"\nName "+name;
	}

}
