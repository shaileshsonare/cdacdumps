import java.util.ArrayList;
import java.util.List;


public class NumbersDemo {

	public static void main(String[] args) {

		List<Number> list=new ArrayList<Number>();
		
		list.add(10);
		list.add(20.25);
		list.add(25.24f);
		
		System.out.println(list);
		
	}

}
