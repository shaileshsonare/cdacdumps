import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class DIOSDemo {
	
	public static void main(String[] args) {
		
		File file=new File("Vinayak.txt");
		
		try
		{
			file.createNewFile();
			DataOutputStream dos=new DataOutputStream(new FileOutputStream(file));
			
			dos.writeChars("20");
			dos.writeChars("Hello India");
			dos.writeChars("Tejpaal");
			dos.write(97);
			System.out.println("Written.......");
			
		}
		catch(FileNotFoundException fnfe)
		{
			System.out.println(fnfe.getMessage());
		}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
		
		
		try
		{
			//file.createNewFile();
			DataInputStream dis=new DataInputStream(new FileInputStream(file));
			
			//dos.writeChars("20");
			//dos.writeChars("Hello India");
			//System.out.println("Written.......");
			
			long i=file.length();
			int l=0;
			while(l<i)
			{
				System.out.print(dis.readChar());
				l++;
			}
			
		}
		catch(FileNotFoundException fnfe)
		{
			System.out.println(fnfe.getMessage());
		}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
		
		
	}

}
