import java.util.Collections;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

class Product implements Comparator 
{
	int id;
	String name;

	
	
	public Product() {
		super();
	}

	public Product(int id,String name) {
		super();
		this.id = id;
		this.name=name;
	}

	@Override
	public int compare(Object obj1, Object obj2) {
		System.out.println("call zaaaaaaaaaliiiiiiii..........");
		
		int id1=((Product)obj1).id;
		int id2=((Product)obj2).id;
		
		System.out.println("OBJ 1 "+id1);
		System.out.println("OBJ 2 "+id2);
		
		return id1-id2;
	}
	

	public String toString()
	{
		return "ID: "+id+ "   Name :"+name;
	}
	
}

public class ComparatorDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		List list1=new ArrayList();
		list1.add(new Product(2, "TV"));
		list1.add(new Product(1, "Mobile"));
		
		Collections.sort(list1);
		
		System.out.println(list1);
		
		
		Set list=new TreeSet(new Product());
		
		list.add(new Product(2, "TV"));
		list.add(new Product(5, "Mobile"));
		list.add(new Product(3, "Uttara"));
		
		System.out.println(list);
		
		
		

	}

}
