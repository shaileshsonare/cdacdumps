/*
 * MedicalTrain.java
 *
 * Created on __DATE__, __TIME__
 */



import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.swing.JFileChooser;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author  __USER__
 */
public class MedicalTrain extends javax.swing.JFrame {

	/** Creates new form MedicalTrain */
	public MedicalTrain() {
		initComponents();
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jTextField1 = new javax.swing.JTextField();
		jButton1 = new javax.swing.JButton();
		/*jMyButton=new javax.swing.JButton();*/
		jLabel1 = new javax.swing.JLabel();
		jButton2 = new javax.swing.JButton();
		jButton3 = new javax.swing.JButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		jTextArea1 = new javax.swing.JTextArea();
		jScrollPane2 = new javax.swing.JScrollPane();
		jTextArea2 = new javax.swing.JTextArea();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		jTextField2 = new javax.swing.JTextField();
		jLabel5 = new javax.swing.JLabel();
		jTextField3 = new javax.swing.JTextField();
		jLabel6 = new javax.swing.JLabel();
		jTextField4 = new javax.swing.JTextField();
		jLabel7 = new javax.swing.JLabel();
		jTextField5 = new javax.swing.JTextField();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jButton1.setText("Browse");
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		jLabel1.setFont(new java.awt.Font("Arial Black", 1, 14));
		jLabel1.setForeground(new java.awt.Color(255, 102, 0));
		jLabel1.setText("Feature Selection Using HFS Algorithm");
		jLabel1.setAlignmentX(10.0F);

		jButton2.setFont(new java.awt.Font("Arial", 0, 10));
		jButton2.setText("Show Specific Result");
		jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton2ActionPerformed(evt);
			}
		});

		jButton3.setFont(new java.awt.Font("Arial", 0, 10));
		jButton3.setText("Show Genral Result");

		jTextArea1.setColumns(20);
		jTextArea1.setRows(5);
		jScrollPane1.setViewportView(jTextArea1);

		jTextArea2.setColumns(20);
		jTextArea2.setRows(5);
		jScrollPane2.setViewportView(jTextArea2);

		jLabel2.setText("Healthy");

		jLabel3.setText("Sick");

		jLabel4.setText("CP (1-4)");

		jLabel5.setText("RestECG(0,1,2)");

		jTextField3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jTextField3ActionPerformed(evt);
			}
		});

		jLabel6.setText("FBS(1,0)");

		jLabel7.setText("Max Heart Rate");

		jTextField5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jTextField5ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGap(75, 75, 75)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING,
												false)
												.addGroup(
														layout.createSequentialGroup()
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addGroup(
																										layout.createParallelGroup(
																												javax.swing.GroupLayout.Alignment.LEADING)
																												.addComponent(
																														jScrollPane1,
																														javax.swing.GroupLayout.PREFERRED_SIZE,
																														156,
																														javax.swing.GroupLayout.PREFERRED_SIZE)
																												.addComponent(
																														jLabel2)
																												/*.addComponent(jMyButton,
																														javax.swing.GroupLayout.PREFERRED_SIZE,
																														156,
																														javax.swing.GroupLayout.PREFERRED_SIZE
																														)
																												*/.addComponent(
																														jButton2,
																														javax.swing.GroupLayout.PREFERRED_SIZE,
																														156,
																														javax.swing.GroupLayout.PREFERRED_SIZE))
																								.addPreferredGap(
																										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																								.addGroup(
																										layout.createParallelGroup(
																												javax.swing.GroupLayout.Alignment.LEADING)
																												.addComponent(
																														jLabel3,
																														javax.swing.GroupLayout.PREFERRED_SIZE,
																														134,
																														javax.swing.GroupLayout.PREFERRED_SIZE)
																												.addComponent(
																														jScrollPane2,
																														javax.swing.GroupLayout.PREFERRED_SIZE,
																														147,
																														javax.swing.GroupLayout.PREFERRED_SIZE)
																												.addComponent(
																														jButton3,
																														javax.swing.GroupLayout.PREFERRED_SIZE,
																														152,
																														javax.swing.GroupLayout.PREFERRED_SIZE)))
																				.addGroup(
																						layout.createSequentialGroup()
																								.addGap(10,
																										10,
																										10)
																								.addGroup(
																										layout.createParallelGroup(
																												javax.swing.GroupLayout.Alignment.LEADING,
																												false)
																												.addComponent(
																														jTextField1,
																														javax.swing.GroupLayout.PREFERRED_SIZE,
																														321,
																														javax.swing.GroupLayout.PREFERRED_SIZE)
																												.addGroup(
																														layout.createSequentialGroup()
																																.addGroup(
																																		layout.createParallelGroup(
																																				javax.swing.GroupLayout.Alignment.LEADING)
																																				.addComponent(
																																						jLabel5)
																																				.addComponent(
																																						jLabel4))
																																.addPreferredGap(
																																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																.addGroup(
																																		layout.createParallelGroup(
																																				javax.swing.GroupLayout.Alignment.TRAILING,
																																				false)
																																				.addComponent(
																																						jTextField2,
																																						javax.swing.GroupLayout.Alignment.LEADING)
																																				.addComponent(
																																						jTextField3,
																																						javax.swing.GroupLayout.Alignment.LEADING,
																																						javax.swing.GroupLayout.DEFAULT_SIZE,
																																						161,
																																						Short.MAX_VALUE))
																																.addPreferredGap(
																																		javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																																		javax.swing.GroupLayout.DEFAULT_SIZE,
																																		Short.MAX_VALUE)
																																.addGroup(
																																		layout.createParallelGroup(
																																				javax.swing.GroupLayout.Alignment.LEADING)
																																				.addComponent(
																																						jLabel7,
																																						javax.swing.GroupLayout.PREFERRED_SIZE,
																																						87,
																																						javax.swing.GroupLayout.PREFERRED_SIZE)
																																				.addComponent(
																																						jLabel6,
																																						javax.swing.GroupLayout.PREFERRED_SIZE,
																																						85,
																																						javax.swing.GroupLayout.PREFERRED_SIZE))))
																								.addGroup(
																										layout.createParallelGroup(
																												javax.swing.GroupLayout.Alignment.LEADING)
																												.addGroup(
																														layout.createSequentialGroup()
																																.addPreferredGap(
																																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																.addComponent(
																																		jTextField5,
																																		javax.swing.GroupLayout.PREFERRED_SIZE,
																																		97,
																																		javax.swing.GroupLayout.PREFERRED_SIZE))
																												.addGroup(
																														layout.createSequentialGroup()
																																.addPreferredGap(
																																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																.addComponent(
																																		jTextField4))
																												.addGroup(
																														layout.createSequentialGroup()
																																.addPreferredGap(
																																		javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																																.addComponent(
																																		jButton1,
																																		javax.swing.GroupLayout.PREFERRED_SIZE,
																																		88,
																																		javax.swing.GroupLayout.PREFERRED_SIZE)))))
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED))
												.addComponent(
														jLabel1,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														382,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addContainerGap(267, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGap(19, 19, 19)
								.addComponent(jLabel1,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										49,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(
														jTextField1,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(
														jButton1,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														21,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel4)
												.addComponent(jLabel6)
												.addComponent(
														jTextField4,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(
														jTextField2,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel5)
												.addComponent(
														jTextField3,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(jLabel7)
												.addComponent(
														jTextField5,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										11, Short.MAX_VALUE)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jButton2)
												.addComponent(jButton3))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(
														javax.swing.GroupLayout.Alignment.TRAILING,
														layout.createSequentialGroup()
																.addComponent(
																		jLabel2)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		jScrollPane1,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.PREFERRED_SIZE))
												.addGroup(
														javax.swing.GroupLayout.Alignment.TRAILING,
														layout.createSequentialGroup()
																.addComponent(
																		jLabel3)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		jScrollPane2,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.PREFERRED_SIZE)))
								.addGap(19, 19, 19)));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		jTextArea1.setText("");
		jTextArea2.setText("");
		try {
			String data = "";
			String[] strarray;
			CheckResult chk1 = new CheckResult();
			FileReader fr = new FileReader(fref);
			BufferedReader br = new BufferedReader(fr);
			while ((data = br.readLine()) != null) {
				strarray = data.split(" ");
				if (chk1.checkSugarLeval(strarray)) {
					jTextArea1.append(strarray[1] + "," + strarray[0] + ","
							+ strarray[5] + "\n");
					System.out
							.println("Healthy People having deatail(age,sex,chest pain type,resting blood pres,cholesteral,fasting blood sugar,resting ecg,exercise induced angina ,oldpeak,slope,number of vessels colored,thal)  "
									+ strarray[0]
									+ ","
									+ strarray[1]
									+ ","
									+ strarray[2]
									+ ","
									+ strarray[3]
									+ ","
									+ strarray[4]
									+ ","
									+ strarray[5]
									+ ","
									+ strarray[6]);

				} else {
					jTextArea2.append(strarray[1] + "," + strarray[0] + ","
							+ strarray[5] + "\n");
					System.out
							.println("Sick  people having deatail(job,age,balance)  "
									+ strarray[1]
									+ ","
									+ strarray[0]
									+ ","
									+ strarray[5]);
				}
			}
			br.close();
		} catch (Exception e) {

		}
	}

	private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {

	}

	private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		jTextArea1.setText("");
		jTextArea2.setText("");
		TestingData testdata = new TestingData();
		int cp = Integer.parseInt(jTextField2.getText());
		int restecg = Integer.parseInt(jTextField3.getText());
		int fbs = Integer.parseInt(jTextField4.getText());
		int maxhrate = Integer.parseInt(jTextField5.getText());
		String test = fbs + "," + restecg + "," + cp + "," + maxhrate;
		//jTextArea1.append("cp"+cp+"restecg"+restecg+"fbs"+fbs+"maxhrate"+maxhrate);
		if (testdata.checkSugarLeval(test)) {
			jTextArea1.setText("Healthy Person\n");
		} else {
			jTextArea2.setText("Sick Person\n");
		}
	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		fchooser = new JFileChooser();
		fchooser.setBounds(200, 200, 300, 300);
		int result = fchooser.showOpenDialog(MedicalTrain.this);
		if (result != JFileChooser.CANCEL_OPTION) {
			fref = fchooser.getSelectedFile();
			jTextField1.setText(fref.getAbsolutePath());
		}

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MedicalTrain().setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	//private javax.swing.JButton jMyButton;
	private javax.swing.JButton jButton1;
	private javax.swing.JButton jButton2;
	private javax.swing.JButton jButton3;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JLabel jLabel7;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JTextArea jTextArea1;
	private javax.swing.JTextArea jTextArea2;
	private javax.swing.JTextField jTextField1;
	private javax.swing.JTextField jTextField2;
	private javax.swing.JTextField jTextField3;
	private javax.swing.JTextField jTextField4;
	private javax.swing.JTextField jTextField5;
	protected JFileChooser fchooser;
	protected File fref;
	// End of variables declaration//GEN-END:variables

}