
public class MedicalPojo 
{
	private int preg,plas,pres,skin,insu,mass,pedi,age,cls;

	public MedicalPojo(int preg, int plas, int pres, int skin, int insu,
			int mass, int pedi, int age, int cls) {
		super();
		this.preg = preg;
		this.plas = plas;
		this.pres = pres;
		this.skin = skin;
		this.insu = insu;
		this.mass = mass;
		this.pedi = pedi;
		this.age = age;
		this.cls=cls;
	}

	public int getPreg() {
		return preg;
	}

	public void setPreg(int preg) {
		this.preg = preg;
	}

	public int getPlas() {
		return plas;
	}

	public void setPlas(int plas) {
		this.plas = plas;
	}

	public int getPres() {
		return pres;
	}

	public void setPres(int pres) {
		this.pres = pres;
	}

	public int getSkin() {
		return skin;
	}

	public void setSkin(int skin) {
		this.skin = skin;
	}

	public int getInsu() {
		return insu;
	}

	public void setInsu(int insu) {
		this.insu = insu;
	}

	public int getMass() {
		return mass;
	}

	public void setMass(int mass) {
		this.mass = mass;
	}

	public int getPedi() {
		return pedi;
	}

	public void setPedi(int pedi) {
		this.pedi = pedi;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getCls() {
		return cls;
	}

	public void setCls(int cls) {
		this.cls = cls;
	}
	
	

}
