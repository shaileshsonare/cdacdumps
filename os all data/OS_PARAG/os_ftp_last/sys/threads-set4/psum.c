#include<pthread.h>

int arr[100],stotal[10];
pthread_t ptarr[10];

void* esum(void* pv)
{
	int *ptr=pv;
	int start=*ptr,end=start+10,i,j;
	j=start/10;
	for(i=start;i<end;i++)
		stotal[j]+=arr[i];
	printf("sum of range:%d-%d is %d\n",
			start,end,stotal[j]);
	pthread_exit(NULL);
}
int main()
{
	//fill array
	int i,k,ftotal=0;
	for(i=0;i<10;i++) {
		k=i*10;//starting index for each sub range
		pthread_create(&ptarr[i],NULL,esum,&k);
		//pthread_join -- no concurrency,sequential
	}
	for(i=0;i<10;i++)
		pthread_join(ptarr[i],NULL);
	for(i=0;i<10;i++)
		ftotal+=stotal[i];
	printf("final total is %d\n",ftotal);
	return 0;
}




