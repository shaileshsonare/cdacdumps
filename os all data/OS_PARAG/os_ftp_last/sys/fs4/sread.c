#include<unistd.h>
#include<fcntl.h>

int main()
{
	int fd,k;
	fd=open("simple.dat",O_RDONLY);
	if(fd<0) {
		perror("open");
		exit(0);
	}
	char buf[100];
	k=read(fd,buf,10);
	buf[k]=0;
	if(k<0)
	{
		perror("read");
		exit(0);
	}
	printf("k=%d,buf=%s\n",k,buf);
	//lseek(fd,4,SEEK_SET); //ret=4
	//lseek(fd,-5,SEEK_CUR);//cur=10,ret=5
	lseek(fd,-7,SEEK_END);  //total=27,ret=20

	k=read(fd,buf,5);
	buf[k]=0;
	printf("k=%d,buf=%s\n",k,buf);

	return 0;
}

