#include<pthread.h>
const int max=20;
void* efun1(void* pv)
{
	int i;
	printf("Thread--A\n");
	for(i=1;i<=max;i++) {
		printf("thread--A--%d\n",i);
		//sleep(1);
	}
	pthread_exit(NULL);
}
void* efun2(void* pv)
{
	int i;
	printf("Thread--B\n");
	for(i=1;i<=max;i++){
		printf("thread--B--%d\n",i);
		//sleep(1);
	}
	pthread_exit(NULL);
}

int main()
{
	int i;
	pthread_t pt1,pt2;

	pthread_create(&pt1,NULL,efun1,NULL);
	pthread_create(&pt2,NULL,efun2,NULL);

	for(i=1;i<=5;i++)
		printf("main--%d\n",i);

	pthread_join(pt1,NULL);
	pthread_join(pt2,NULL);

	printf("main--thank you\n");
	return 0;
}
