#include<unistd.h>
#include<sys/shm.h>

#define KEY 1234

int main()
{
	int shmid;
	shmid=shmget(KEY,256,IPC_CREAT|0666);
	if(shmid<0) {
		perror("shmget");
		exit(0);
	}

	void* pv;
	pv=shmat(shmid,0,0);
	int *ptr=pv;
	char* ps=pv+4;
	*ptr=12;
	strcpy(ps,"Hello SHM\n");
	printf("data written to shm\n");
	shmdt(pv);
	return 0;
}
//printf("int val is %d\n",*ptr);
//printf("String is %s\n",ps);



