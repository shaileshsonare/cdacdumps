#include<unistd.h>
#include<fcntl.h>

int main()
{
	int fd,k;
	fd=open("sample.dat",O_RDONLY);
	if(fd<0) {
		perror("open");
		exit(0);
	}
	char buf[100];
	k=read(fd,buf,5);
	if(k<0) {
		perror("write");
		exit(0);
	}
	buf[k]='\0';
	printf("k=%d,buf=%s\n",k,buf);

	//lseek(fd,8,SEEK_SET);
	//lseek(fd,4,SEEK_CUR);
	lseek(fd,-6,SEEK_END);
	k=read(fd,buf,6);
	//printf("k=%d,buf=%s\n",k,buf);
	write(1,buf,k);
	close(fd);
	return 0;
}

