#include<unistd.h>

int main()
{
	int ret;
	printf("welcome\n");
	ret=fork();
	if(ret<0) {
		perror("fork");
		exit(0);
	}
	if(ret==0) {
		printf("child-pid=%d,ppid=%d\n",
			getpid(),getppid());
		exit(0);
	}
	else {//ret>0
		printf("parent-pid=%d,ppid=%d,ret=%d\n",
			getpid(),getppid(),ret);
		printf("parent-thank you\n");
	}
	return 0;
}

