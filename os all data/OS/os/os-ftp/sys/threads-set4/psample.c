#include<pthread.h>

void* efun1(void* ptr)
{
	int i;
	printf("I am thread-A\n");
	for(i=1;i<=10;i++){
		printf("A--%d\n",i);
		sleep(1);
	}
}
void* efun2(void* ptr)
{
	int i;
	printf("I am thread-B\n");
	for(i=1;i<=10;i++) {
		printf("B--%d\n",i);
		sleep(1);
	}
}
int main()
{
	int i;
	pthread_t pt1,pt2;
	//efun1(NULL);
	//efun2(NULL);
	pthread_create(&pt1,NULL,efun1,NULL);
	pthread_create(&pt2,NULL,efun2,NULL);

	for(i=1;i<=10;i++){
		printf("main--%d\n",i);
		sleep(1);
	}

	return 0;
}
