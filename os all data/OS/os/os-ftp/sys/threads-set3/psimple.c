#include<pthread.h>

const int max=10; //common
pthread_t pt1,pt2,pt3,ptm,pcur;

void* efun(void* pv)
{
	int i; //private
	char* str=pv;

	pcur=pthread_self();
	if(pthread_equal(pt1,pcur))
		printf("pt1:Thread--%s\n",str);
	else
		printf("other threads--%s\n",str);

	for(i=1;i<=max;i++) {
		printf("thread--%s--%d\n",str,i);
	}
	pthread_exit(NULL);
}

int main()
{
	int i;
	ptm=pthread_self();
	pthread_create(&pt1,NULL,efun,"abc");
	pthread_create(&pt2,NULL,efun,"xyz");
	pthread_create(&pt3,NULL,efun,"pqr");

	pthread_join(pt1,NULL);
	pthread_join(pt2,NULL);
	pthread_join(pt3,NULL);

	printf("main--thank you\n");
	return 0;
}
