#include<unistd.h>
#include<sys/msg.h>

#define KEY 1234

int main()
{
	int mid,k;
	mid=msgget(KEY,IPC_CREAT|0666);
	if(mid<0) {
		perror("msgget");
		exit(0);
	}
	char buf[100]={};
	k=msgrcv(mid,buf,40,0,0); //blocking
	//k=msgrcv(mid,buf,40,0,IPC_NOWAIT); //non blocking
	//k=msgrcv(mid,buf,15,0,0); 
	//k=msgrcv(mid,buf,15,0,MSG_NOERROR);//truncate

	if(k<0) {
		perror("msgrcv");
		exit(0);
	}
	buf[k]=0;
	printf("msg received is:%s,k=%d\n",buf,k);
	return 0;
}


