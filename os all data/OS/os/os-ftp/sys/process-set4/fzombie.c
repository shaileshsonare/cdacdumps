#include<unistd.h>

int main()
{
	int ret;
	printf("welcome\n");
	ret=fork();
	if(ret==0) { //child
		printf("child-pid=%d,ppid=%d\n",
				getpid(),getppid());
		printf("child-thank you\n");
		exit(0);
	}
	else{  //ret>0,parent
		printf("parent-pid=%d,ppid=%d,ret=%d\n",
				getpid(),getppid(),ret);
		printf("parent-thank you\n");
		sleep(10);
	}
	return 0;
}
