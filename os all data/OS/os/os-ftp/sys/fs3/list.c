#include<dirent.h>
#include<stdio.h>

int main(int argc,char* argv[])
{
	DIR* pd;
	struct dirent* ps;
	if(argc==2)
		pd=opendir(argv[1]);
	else
		pd=opendir(".");

	while(1) {
		ps=readdir(pd);
		if(ps==NULL) break;
		printf("%s\n",ps->d_name);
	}

	closedir(pd);
}
