
for k in 10 20 30 40 50
do
	if [ $k -eq 20 ]
	then
		continue
	fi
	echo $k
done

for k in `seq 1 10`
do
	if [ -e exam$k ]
	then
		echo "exam$k already exists"
		continue
	fi
	mkdir exam$k
	echo "dir exam$k created successfully"
done
