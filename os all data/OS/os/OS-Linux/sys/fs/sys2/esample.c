#include<unistd.h>
#include<sys/wait.h>

int main()
{
	int ret,k,st;
	ret=fork();
	if(ret==0)
	{
		printf("child-pid=%d,ppid=%d\n",getpid(),
				getppid());
		//some code
		//k=execl("/bin/date","date","+%D::%T",NULL);
	//execl("/usr/bin/cal","cal","15","2015",NULL);
		k=execl("/usr/bin/bc","xyz",NULL);
		//k=execlp("date","date",NULL);
		if(k<0) {
			perror("execl");
			//exit(0);
		}
		printf("child--thank you\n");
	}
	else
	{
		printf("parent-pid=%d,ppid=%d\n",getpid(),
				getppid());
		printf("parent-val of env=%s\n",
				getenv("PATH"));
		waitpid(-1,&st,0);
		printf("exit status=%d\n",WEXITSTATUS(st));
	}
	return 0;
}
