#include<unistd.h>
#include<fcntl.h>

int main()
{
	int fds[2],k,ret;
	//open("sample.dat");
	k=pipe(fds);
	if(k<0)
	{
		perror("pipe");
		exit(0);
	}
	ret=fork();
	if(ret==0) //child,write
	{
		close(fds[0]);
		sleep(5);
		write(fds[1],"ABCDEFGHIJ\n",12);
		
		close(fds[1]);
		exit(0);
	}
	else //parent,read
	{
		char buf[20];
		close(fds[1]);
		printf("parent going to read\n");
		k=read(fds[0],buf,20);
		if(k<0) { perror("read"); exit(0); }
		write(1,buf,k);

		close(fds[0]);
	}
	return 0;
}


