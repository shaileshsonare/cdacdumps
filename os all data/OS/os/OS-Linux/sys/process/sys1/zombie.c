#include<unistd.h>
#include<sys/wait.h>

int main()
{
	int ret,st,*p=NULL;
	ret=fork();
	if(ret==0)
	{
		printf("child-pid=%d,ppid=%d\n",getpid(),getppid());
		exit(0);
	}
	else //ret>0
	{
		printf("parent-ret=%d\n",ret);
		printf("parent-pid=%d,ppid=%d\n",getpid(),getppid());
		sleep(10);
		//waitpid(-1,&st,0);
		/*//waitpid(ret,&st,0);
		printf("parent-thank you\n");
		if(WIFEXITED(st))
		 printf("normal,term status is %d\n",WEXITSTATUS(st));
		else
		 printf("abnormal termination\n");*/
	}
	return 0;
}
