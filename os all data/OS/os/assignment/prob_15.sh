#!/bin/sh

echo "Enter first side of triangle :"
read a
echo "Enter second side of triangle :"
read b
echo "Enter third side of triangle :"
read c
temp1=`echo "$a^2+$b^2" | bc`
temp2=`echo "$c^2" | bc`
if [ $a -eq $b -a $b -eq $c -a $c -eq $a ]
then
	echo "Equilateral Triangle"
elif [ $a -eq $b -o $a -eq $c -o $b -eq $c ]
then
	echo "Isoceles Triangle"
elif [ $temp1 -eq $temp2 ] 
then
	echo "Right Angle Triangle "
else
	echo "Scalene Triangle"
fi

