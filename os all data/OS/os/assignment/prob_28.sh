#!/bin/sh
echo "Enter no. of lines :"
read n
for i in `seq 1 $n`
do
	let temp=$n-$i
	for j in `seq 1 $temp`
	do
		echo -n " "
	done

	let temp_po=$i-1
	temp_p=`echo "11^$temp_po" | bc`

	while [ $temp_p -ne 0 ]
	do
		let p=$temp_p%10
		echo -n "$p "
		let temp_p=$temp_p/10
	done
	echo
done

