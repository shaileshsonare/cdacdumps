#include<unistd.h>
#include<sys/shm.h>

#define KEY 1234

int main()
{
	int shmid;
	shmid=shmget(KEY,128,IPC_CREAT|0666);
	if(shmid<0) {
		perror("shmget");
		exit(0);
	}

	void* pv;
	pv=shmat(shmid,0,0);
	printf("pv=%u\n",pv);

	char *ps=pv+4;
	printf("string is %s\n",ps);
	int *p=pv;
	printf("int val is %d\n",*p);

	shmdt(pv);

	return 0;
}
