#include<unistd.h>

int main()
{
	int x[]={10,20,30,40,50};
	int ret,i;
	ret=fork();
	if(ret==0)
	{
		printf("child process\n");
		for(i=0;i<5;i++)
			printf("child-%d\n",x[i]);
		x[0]+=5;
		sleep(1);
		exit(0);
	}
	else //ret>0
	{
		x[0]++;
		sleep(1);
		for(i=0;i<5;i++)
			printf("parent-%d\n",x[i]);
		printf("parent process\n");
	}
	return 0;
}
