#include<unistd.h>
#include<sched.h>
#include<sys/time.h>
#include<sys/resource.h>

int main()
{
	int k;
	//k=nice(-5);
	//k=nice(5);
	//if(k<0) {
	//	perror("nice");
	//	exit(0);
	//}
	
	int sch=sched_getscheduler(getpid());
	if(sch==SCHED_FIFO)
		printf("fifo\n");
	else if(sch==SCHED_RR)
		printf("round robin\n");
	else
		printf("OTHER\n");

	setpriority(PRIO_PROCESS,PRIO_PROCESS,10);
	printf("prio=%d\n",
	getpriority(PRIO_PROCESS,PRIO_PROCESS));
	getchar();
	return 0;
}
