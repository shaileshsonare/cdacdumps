BEGIN {
	FS="|"
	OFS=":"
	sum=0
}
#/sales/
{
	if($6>5000) {
		print NR,$1,$2,$4,$6
		sum+=$6
	}
}
END {
	print "sum is " sum
}
