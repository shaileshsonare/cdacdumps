#include<unistd.h>

int main()
{
	int ret,st,k;
	printf("welcome\n");
	ret=fork();
	if(ret==0) { //child
		printf("child-pid=%d,ppid=%d\n",
				getpid(),getppid());
		//k=execl("/bin/date","date","+%D", NULL);
	       k=execl("/usr/bin/cal","cal","10","2015",NULL);
		if(k<0) 
			perror("execl");
		printf("child-thank you\n");//reduntant 
			//if execl is successful
	}
	else{  //ret>0,parent
		printf("parent-pid=%d,ppid=%d,ret=%d\n",
				getpid(),getppid(),ret);
		waitpid(-1,&st,0);
		printf("parent-thank you\n");
	}
	return 0;
}
