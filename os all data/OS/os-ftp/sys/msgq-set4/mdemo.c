#include<unistd.h>
#include<sys/msg.h>

#define KEY 1234

int main()
{
	int mid,k,ret;
	mid=msgget(KEY,IPC_CREAT|0666);
	if(mid<0) {
		perror("msgget");
		exit(0);
	}
	ret=fork();
	if(ret==0)
	{
		char buf[100]={};
		k=msgrcv(mid,buf,40,0,0);
		if(k<0) { perror("msgrcv"); exit(0); }
		printf("child:msg is %s\n",buf);
	}
	else {
		char str[]="ABCDEFGHIJKLMNOPQRSTUVWXYZ\n";
		k=msgsnd(mid,str,28,0);
		if(k<0) {
			perror("msgsnd");
			exit(0);
		}
		printf("parent:message sent successfully\n");

		waitpid(-1,&st,0);
		msgctl(mid,IPC_RMID);
	}
	return 0;
}


