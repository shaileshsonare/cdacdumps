#include<unistd.h>
#include<sys/msg.h>

#define KEY 1234

int main()
{
	int mid,k;
	mid=msgget(KEY,IPC_CREAT|0666);
	if(mid<0) {
		perror("msgget");
		exit(0);
	}
	char msg[100];
	k=msgrcv(mid,msg,40,0,0);//blocking call
	//k=msgrcv(mid,msg,20,0,0);
	//k=msgrcv(mid,msg,40,0,IPC_NOWAIT);//no blocking call
	//k=msgrcv(mid,msg,10,0,MSG_NOERROR);//truncate data
			//when requested length is less
	if(k<0) {
		perror("msgrcv");
		exit(0);
	}
	msg[k]='\0';
	printf("msg is : %s,k=%d\n",msg,k);
	return 0;
}

