#include<unistd.h>
#include<fcntl.h>

int main()
{
	int k,ret,fdarr[2];
	char buf[20];
	//int f=open("simple.dat",O_RDONLY);
	k=pipe(fdarr);
	if(k<0) {
		perror("pipe");
		exit(0);
	}
	ret=fork();
	if(ret==0) //child,reading
	{
		close(fdarr[1]);

		k=read(fdarr[0],buf,20);
		//write(1,buf,k);
		printf("child,buf=%s\n",buf);

		close(fdarr[0]);

	}
	else	   //parent,writing
	{
		close(fdarr[0]);

		write(fdarr[1],"Hello FIFO\n",12);
		
		close(fdarr[1]);
	}
	return 0;
}
