#include<pthread.h>

int i;
pthread_mutex_t m1;

void* efun1(void* ptr)
{
	printf("I am thread-A\n");
	pthread_mutex_lock(&m1);
	for(i=1;i<=10;i++){
		printf("A--%d\n",i);
		sleep(1);
		//some access to shared resource
	}
	pthread_mutex_unlock(&m1);
}
void* efun2(void* ptr)
{
	printf("I am thread-B\n");
	pthread_mutex_lock(&m1);
	for(i=1;i<=10;i++) {
		printf("B--%d\n",i);
		sleep(1);
		//some access to shared resource
	}
	pthread_mutex_unlock(&m1);
}
int main()
{
	int i;
	pthread_t pt1,pt2;
	pthread_mutex_init(&m1,NULL);

	pthread_create(&pt1,NULL,efun1,NULL);
	pthread_create(&pt2,NULL,efun2,NULL);

	pthread_join(pt1,NULL);
	pthread_join(pt2,NULL);

	pthread_mutex_destroy(&m1);
	printf("main--thank you\n");


	return 0;
}
