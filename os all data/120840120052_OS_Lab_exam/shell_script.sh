#!/bin/sh
#function for calculating area
area()             
{
set scale=2  #setting the scale as 2 for result in 2 decimal places
pi=`expr 22 \/ 7 | bc`
echo $pi
a=`expr $pi \* $1 \ * $1|bc`
echo Area of circle is $a
}

ch='y'
while [ $ch = y -o $ch = Y ]
do 
	echo " Menu"
	echo "-----------------------------"
	echo 1.Count No. of users logged in
	echo 2.To run a process with a priority lower than default
	echo 3.List all symbolic links available under /bin directory
	echo 4.Calculate area of circle
	echo 5.Exit
	echo Enter your choice:
	read c
case $c in   #case statement to switch to corresponding case as per the users's choice
	1) echo "No. of users logged in:"`who|wc -l`;;
2)echo Enter the process name
	read pro
	if [ ps -f|grep -w "$pro"|cut -d" " -f2 ]
	then
	echo The details of process are as follows:
	`ps $pro`
	echo Process Id of process $pro is:
	pro_id=	ps -f|grep -w "$pro"|cut -d" " -f2   #process id of process with name pro
	nice -n 10 $pro_id  #assign a priority 10 to process with process id pro_id
	else 
		echo Process not found
	fi
       	;;
3)echo List of all symbolic links under /bin diectory is:
	ls -l /bin|grep "^l"
	echo
	echo Count of all symbolic links is:  
	ls -l /bin|grep "^l" | wc -l     #all the symbolic links appears with 
	;;

4) echo "Enter the radius of the circle:"
	read r
area $r  #calling function area with r as argument
	;;
5)exit;;
*)echo Wrong choice
	;;
esac
echo Do you want to continue y/n :
read ch
done



