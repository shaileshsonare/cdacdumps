<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Session Page</title>
<%
	
	session=request.getSession(true);
	Integer id=(Integer)session.getAttribute("jseekerid");
%>
</head>
<body>
<%=id %>
Not a member <a href="../jsps/jseek_login.jsp">Sign up now</a>
<a href="../jsps/seek_home.jsp">Home</a>
<a href="../jsps/logout.jsp">Logout</a>
</body>
</html>