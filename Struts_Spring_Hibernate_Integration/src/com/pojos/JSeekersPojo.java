package com.pojos;

public class JSeekersPojo 
{
	private Integer jseekid;
	private String email;
	private String firstName;
	private String lastName;
	private String userName;
	private String passwd;
	
	public JSeekersPojo() {
		super();
	}

	public JSeekersPojo(Integer jseekid, String email, String firstName,
			String lastName, String userName, String passwd) {
		super();
		this.jseekid = jseekid;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.passwd = passwd;
	}

	public Integer getJseekid() {
		return jseekid;
	}

	public void setJseekid(Integer jseekid) {
		this.jseekid = jseekid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
}
