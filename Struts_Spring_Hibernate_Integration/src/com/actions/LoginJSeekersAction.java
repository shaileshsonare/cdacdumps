package com.actions;

import com.bslogics.JSeekersBsLogic;
import com.pojos.JSeekersPojo;

public class LoginJSeekersAction 
{
	private Integer jseekid;
	private String email;
	private String passwd;
	private String firstName;
	private String lastName;
	private String userName;


	public Integer getJseekid() {
		return jseekid;
	}
	public void setJseekid(Integer jseekid) {
		this.jseekid = jseekid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	public String execute()
	{
		System.out.println(email);
		System.out.println(passwd);
		JSeekersPojo jsp= new JSeekersBsLogic().select(email, passwd);
		setJseekid(jsp.getJseekid());
		setEmail(jsp.getEmail());
		setFirstName(jsp.getFirstName());
		setLastName(jsp.getLastName());
		setUserName(jsp.getUserName());
		setPasswd(jsp.getPasswd());
		return "success";
	}
}
