package com.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.pojos.JSeekersPojo;

public class JSeekersDAO 
{
	private SessionFactory sessionFactory;
	private String status = "failure";
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public String saveUser(JSeekersPojo jsp)
	{
		Session session = getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(jsp);
		System.out.println("data saved ........");
		tx.commit();
		status="success";
		return status;
	}
	
	public JSeekersPojo getUser(String email,String passwd)
	{		
		JSeekersPojo jsp=null;
		Session session=getSessionFactory().openSession();
		Criteria criteria=session.createCriteria(JSeekersPojo.class);
		criteria.add(Restrictions.ilike("email",email))
				.add(Restrictions.ilike("passwd",passwd));
		
		List list=criteria.list();
		Iterator it=list.iterator();
		
		while(it.hasNext())
		{
			jsp=(JSeekersPojo)it.next();
			System.out.println(jsp.getJseekid());
			System.out.println(jsp.getEmail());
			System.out.println(jsp.getFirstName());
			System.out.println(jsp.getLastName());
			System.out.println(jsp.getUserName());
			System.out.println(jsp.getPasswd());
			
		}
		
		return jsp;
	}
}
