import java.util.TreeMap;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;

class Student
{
	int rollno;
	
	public Student(int rno)
	{
		this.rollno=rno;
	}
	
	public String toString()
	{
		return "mcbc "+rollno;
	}
}

public class MapDemo {

	public static void main(String[] args) {
	
		Map map=new TreeMap();
		
		map.put(1, new Student(001));
		map.put(2, new Student(200));
		
		Set s=map.entrySet();
		
		Iterator i=s.iterator();
		
		while(i.hasNext())
		{
			
			Object obj=i.next();
			Map.Entry me=(Map.Entry)obj;
			
			System.out.print(me.getKey());
			System.out.println(me.getValue());
		}
		
	}

}

