import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class DBConnection {

	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	
	public void connect()
	{
		try 
		{
			
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
			System.out.println("Driver Loaded.......");
			 
			conn=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","system","admin");
			
			System.out.println("Connected...");
			
			pstmt=conn.prepareStatement("select * from login");
			
			rs=pstmt.executeQuery();
			
			while(rs.next())
			{
				System.out.println(rs.getString(1));
				System.out.println(rs.getString(2));
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
	}
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new DBConnection().connect();
	}

}
