
class Thread1 extends Thread
{
	public void run()
	{
		for(int i=1; i<50; i++)
		System.out.println("In Thread1......."+i);
	}
}

class Thread2 extends Thread
{
	public void run()
	{
		for(int i=1; i<50; i++)
		System.out.println("In Thread2........"+i);
	}
}


class Thread3 implements Runnable
{
	public void run()
	{
		for(int i=1; i<50; i++)
		System.out.println("In Thread3........"+i);
	}
}


public class MyHomeThread 
{

	public static void main(String[] args) 
	{
		Thread1 t1=new Thread1();
		t1.start();
		
		Thread2 t2=new Thread2();
		t2.start();
		
		Thread3 t3=new Thread3();
		Thread t=new Thread(t3);
		t.start();
		
		for(int i=1; i<50 ; i++)
		System.out.println("in main Thread......"+i);
	}

}
