class Resource
{
	public synchronized void display(String msg)
	{
		System.out.println("[");
		System.out.println(msg);
		System.out.println("]");
	}
}

class MyThreadRes extends Thread
{
	Resource r=new Resource();
	String msg;
	
	public MyThreadRes(Resource r,String msg) {
		
		this.r=r;
		this.msg=msg;
		
	}
	
	public void run()
	{
	
		r.display(msg);
	}
}

public class SynchronizedDemo {

	public static void main(String[] args) {
		
		Resource r1=new Resource();
		
		MyThreadRes t1=new MyThreadRes(r1,"Hey");
		MyThreadRes t2=new MyThreadRes(r1,"How");
		t1.start();
		t2.start();

	}

}
