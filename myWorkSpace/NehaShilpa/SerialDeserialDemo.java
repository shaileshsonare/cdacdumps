import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerialDeserialDemo {

	Students s[]=new Students[3];
	
	File f=new File("StudentsInfo.dat");
	FileOutputStream fos=null;
	FileInputStream fis=null;
	ObjectOutputStream oos=null;
	ObjectInputStream ois=null;
	
	
	public void writeObjectStudents()
	{
		
		try
		{

			s[0]=new Students(1,"SSS");
			s[1]=new Students(2,"Sid");
			s[2]=new Students(3,"Vkt");
			f.createNewFile();
			fos=new FileOutputStream(f);
			oos=new ObjectOutputStream(fos);
			for(int i=0;i<s.length;i++)
			{
			oos.writeObject(s[i]);
			}
			/*oos.writeObject(s[1]);
			oos.writeObject(s[2]);*/
			System.out.println("Data Written to the file........");
		}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
		
	}
	
	public void readObjectStudents()
	{
		try
		{
			fis=new FileInputStream(f);
			ois=new ObjectInputStream(fis);
			Students s1=(Students)ois.readObject();
			System.out.println(s1);
			Students s2=(Students)ois.readObject();
			System.out.println(s2);
			Students s3=(Students)ois.readObject();
			System.out.println(s3);
			
			
		}
		catch(ClassNotFoundException cnfe)
		{
			System.out.println(cnfe.getMessage());
		}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
		
	}
	public static void main(String[] args) {
		// 
		
		SerialDeserialDemo sdd=new SerialDeserialDemo();
		sdd.writeObjectStudents();
		sdd.readObjectStudents();

	}

}
