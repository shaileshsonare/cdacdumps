<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.apache.struts2.views.jsp.TagUtils"%>  
<%@ page import="com.opensymphony.xwork2.ognl.OgnlValueStack"%>    
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="../Style/default.css" rel="stylesheet" type="text/css">
<title>Job Providers Home Page</title>
<s:property value="jseekid" />
<%
Integer jproviderid=null;
OgnlValueStack valStack = (OgnlValueStack)TagUtils.getStack(pageContext);  
jproviderid=(Integer)valStack.findValue("jprovider_id");   
session=request.getSession(true);
session.setAttribute("jproviderid", jproviderid);

session=request.getSession(true);
Integer id=(Integer)session.getAttribute("jproviderid");
%>
</head>
<body>

Welcome ${userName}
	<a href="../jsps/logout.jsp">Logout</a>
	<div id="header">
	<h1 style="margin:50px 0px 5px 20px">IndiaJobs.com</h1>
</div>


<div id="content">

<table>
	<tr>
		<td style="width:300px"></td>
		<td align="center" width="400">
			<div id="jseekers_info">
				<table border="0" cellpadding="5" cellspacing="5">
					<tr><td>Id<td>${jprovider_id}
					<tr><td>Person Name<td>${person_name}
					<tr><td>Email<td>${email}
					<tr><td>Password<td>${passwd}
					<tr><td>Phone Number<td>${stdcode}-${phone}
					<tr><td>Job Title<td>${job_title}
					<tr><td>Company<td>${corp_name}
					<tr><td>Industry<td>${industry}
					<tr><td>Company Type<td>${company_type}
					<tr><td>Mobile<td>${mobile}
					<tr><td>Address<td>${address}
					<tr><td>City<td>${city}
					<tr><td>State<td>${state}
					<tr><td>Pincode/Zip Code<td>${zip}
					<tr><td>Country<td>${country}
				</table>
			</div>	
		</td>
		<td style="width:300px"></td>
	</tr>
</table>

</div><!-- END OF CONTENT HERE -->

<div id="footer">

copyright &copy; 2012 | indiajobs.com | Powered By: 4SUV Technologies
</div>

</body>
</html>