<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.apache.struts2.views.jsp.TagUtils"%>  
<%@ page import="com.opensymphony.xwork2.ognl.OgnlValueStack"%>    
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Job Seekers Home Page</title>
<link href="../Style/default.css" rel="stylesheet" type="text/css">
<s:property value="jseekid" />
<%
	
Integer jseekerid=null;
OgnlValueStack valStack = (OgnlValueStack)TagUtils.getStack(pageContext);  
jseekerid=(Integer)valStack.findValue("jseekid");   
session=request.getSession(true);
session.setAttribute("jseekerid", jseekerid);

session=request.getSession(true);
Integer id=(Integer)session.getAttribute("jseekerid");
%>
</head>
<body>
	
	Welcome ${userName}
	<a href="../jsps/create_account.jsp">Sign up now</a>
	<a href="../jsps/jseek_home.jsp">Home</a>
	<a href="../jsps/testpageforsession.jsp">Job Posts</a>
	<a href="../jsps/logout.jsp">Logout</a>
	<div id="header">
	<h1 style="margin:50px 0px 5px 20px">IndiaJobs.com</h1>
</div>


<div id="content" style="height:400px">

<table>
	<tr>
		<td style="width:300px"></td>
		<td align="center" width="400">
			<div id="jseekers_info">
				<table border="0" cellpadding="5" cellspacing="5">
					<tr><td>Id<td>${jseekid}
					<tr><td>Email id<td>${email}
					<tr><td>Password<td>${passwd}
					<tr><td>First name<td>${firstName}
					<tr><td>Last name<td>${lastName}
					<tr><td>User name<td>${userName}
				</table>
			</div>	
		</td>
		<td style="width:300px"></td>
	</tr>
</table>

</div><!-- END OF CONTENT HERE -->

<div id="footer">

copyright &copy; 2012 | indiajobs.com | Powered By: 4SUV Technologies
</div>
	
</body>
</html>