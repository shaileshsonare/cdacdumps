<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Job Provider Login</title>
<link href="../Style/default.css" rel="stylesheet" type="text/css">
</head>
<body>
	Not a member <a href="../jsps/register_jprovider.jsp">Sign up now</a>
	<div id="header">
	<h1 style="margin:50px 0px 5px 20px">IndiaJobs.com</h1>
</div>


<div id="content" style="height:400px">

<table>
	<tr>
		<td style="width:300px"></td>
		<td align="center" width="400">
			<div >
				<s:form action="jp_logins" id="login_form">
					<div>
					<s:label><h3 style="text-align: center;">Job Provider Login Form</h3></s:label>
					<s:textfield name="email" label="*Email address "></s:textfield>
					<s:password name="passwd" label="*Choose a password "></s:password> 
					<s:submit value="Login"></s:submit>
					</div>
				</s:form>
				</div><!-- END OF REGISTRATION FORM HERE-->
		</td>
		<td style="width:300px"></td>
	</tr>
</table>

</div><!-- END OF CONTENT HERE -->



<div id="footer">
copyright &copy; 2012 | indiajobs.com | Powered By: 4SUV Technologies
</div>
</body>
</html>