class ResourceDataa
{
	String msg1 = "(";
	String msg2 = ")";
	
	public  void access(String msg)
	{
	  	System.out.println("{");
	  /*	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	  	System.out.println(msg);
	  	System.out.println("}");	
	}
		
}


class ThreadPrint implements Runnable
{
	ResourceDataa resobj;
	String msg;
	Thread t;
	
	
	public ThreadPrint(ResourceDataa resobj,String data)
	{
		this.resobj = resobj;
		msg = data;
		t = new Thread(this);
		t.start();
	}
	
	
	public void run()
	{
		System.out.println("Name : "+t.getName());
		
		
		synchronized (resobj) {
			System.out.println(resobj.msg1);
			System.out.println(this.msg);
			System.out.println(resobj.msg2);
		}
		
	}	
}

public class SynchronizationDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ResourceDataa resobj = new ResourceDataa();
		ThreadPrint t1 = new ThreadPrint(resobj,"Hai");
		ThreadPrint t2 = new ThreadPrint(resobj,"How");
		ThreadPrint t3 = new ThreadPrint(resobj,"are");
		ThreadPrint t4 = new ThreadPrint(resobj,"You");
	}
}