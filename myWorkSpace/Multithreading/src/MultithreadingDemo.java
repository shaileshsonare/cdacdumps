class Thread11 implements Runnable
{
   public void run()
   {
	   for (int i=100; i<=110; i++)
	   {
		 if (i==105)
		  {
			 System.out.println("I am going to sleep........");
		   try
		   {
			   Thread.sleep(10);
		   }catch(InterruptedException ie)
		   {
			   System.out.println("Interrupted..........");
		   }
	   }
	   else
		   System.out.println("Thread 1 : "+i);
   }
 }
}

class Thread22 extends Thread
{
   public void run()
   {
	   for (int i=111; i<=120; i++)
	   {
		if (i==113)   
		{
			System.out.println("Thread22 is yielding the resource.......");
			yield();
		}
		else
		   System.out.println("Thread 2 : "+i);
	   }
   }
}



public class MultithreadingDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		// TODO Auto-generated method stub
		
		
		//Runnable Interface
		Thread11 t11 = new Thread11();
		//Passing the runnable obj as arg
		Thread t1 = new Thread(t11,"ThreadOne");
		
		Thread22 t2 = new Thread22();
			
		t1.start();
		t2.start();
		
		for (int i=501; i<510;i++)
		
		System.out.println("Main Thread.........."+ i);
		System.out.println("Thread1.isAlive() : " + t1.isAlive());
		System.out.println("Thread2.isAlive() : " + t2.isAlive());
		System.out.println("Thread.currentThread().isAlive() : " + Thread.currentThread().isAlive());
		System.out.println("Thread 11 Name : " + t1.getName());
		System.out.println("Thread 22 Name : " + t2.getName());
		System.out.println("Main Thread Name : " + Thread.currentThread().getName());
		System.out.println("Thread 11 ID : " + t1.getId());
		System.out.println("Thread 22 ID : " + t2.getId());
		System.out.println("Main Thread ID : " + Thread.currentThread().getId());
		try {
			t1.join();
			t2.join();
			Thread.currentThread().join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Thread1.isAlive() : " + t1.isAlive());
		System.out.println("Thread2.isAlive() : " + t2.isAlive());
		System.out.println("Thread.currentThread().isAlive() : " + Thread.currentThread().isAlive());
		
		
		
		
		

	}

}
