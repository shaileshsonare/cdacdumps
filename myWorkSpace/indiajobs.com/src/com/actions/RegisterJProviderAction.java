package com.actions;

import com.bslogics.JProviderBsLogic;

import com.pojos.JProviderPojo;


public class RegisterJProviderAction {

	private Integer jprovider_id;// number(10) primary key,
	private String person_name;// varchar2(30) not null,
	private String email;// varchar2(30) unique,
	private String passwd;// varchar2(20) not null,
	private Integer stdcode;// number(5),
	private Integer phone;// number(10),
	private String job_title;// varchar2(30),
	private String corp_name;// varchar2(50),
	private String industry;// varchar2(50),
	private String company_type;// varchar2(50),
	private Integer mobile;// number(12),
	private String address;// varchar2(100),
	private String city;// varchar2(20),
	private String state;// varchar2(30),
	private Integer zip;// varchar2(20),
	private String country;// varchar2(20)
	
	
	public RegisterJProviderAction() {
		super();
	}
	public RegisterJProviderAction(Integer jprovider_id, String person_name,
			String email, String passwd, Integer stdcode, Integer phone,
			String job_title, String corp_name, String industry,
			String company_type, Integer mobile, String address, String city,
			String state, Integer zip, String country) {
		super();
		this.jprovider_id = jprovider_id;
		this.person_name = person_name;
		this.email = email;
		this.passwd = passwd;
		this.stdcode = stdcode;
		this.phone = phone;
		this.job_title = job_title;
		this.corp_name = corp_name;
		this.industry = industry;
		this.company_type = company_type;
		this.mobile = mobile;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.country = country;
	}
	public Integer getJprovider_id() {
		return jprovider_id;
	}
	public void setJprovider_id(Integer jprovider_id) {
		this.jprovider_id = jprovider_id;
	}
	public String getPerson_name() {
		return person_name;
	}
	public void setPerson_name(String person_name) {
		this.person_name = person_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public Integer getStdcode() {
		return stdcode;
	}
	public void setStdcode(Integer stdcode) {
		this.stdcode = stdcode;
	}
	public Integer getPhone() {
		return phone;
	}
	public void setPhone(Integer phone) {
		this.phone = phone;
	}
	public String getJob_title() {
		return job_title;
	}
	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}
	public String getCorp_name() {
		return corp_name;
	}
	public void setCorp_name(String corp_name) {
		this.corp_name = corp_name;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getCompany_type() {
		return company_type;
	}
	public void setCompany_type(String company_type) {
		this.company_type = company_type;
	}
	public Integer getMobile() {
		return mobile;
	}
	public void setMobile(Integer mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Integer getZip() {
		return zip;
	}
	public void setZip(Integer zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String execute()
	{
		System.out.println(jprovider_id);
		
		JProviderPojo jpp=new JProviderPojo(jprovider_id, person_name, email, passwd, stdcode, phone, job_title, corp_name, industry, company_type, mobile, address, city, state, zip, country);
		
		return new JProviderBsLogic().insert(jpp);
	}
}
