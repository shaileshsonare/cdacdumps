package com.bslogics;

import org.springframework.context.ApplicationContext;

import com.dao.JSeekersDAO;
import com.dbconn.DBConnection;
import com.pojos.JSeekersPojo;

public class JSeekersBsLogic 
{
	ApplicationContext context;
	JSeekersDAO rjsdao;
	
	public void getBean()
	{
		System.out.println("Alo JSeekers Bs Logic madhe...");
		context=new DBConnection().getContext();
		rjsdao=(JSeekersDAO)context.getBean("registerjsdao");
	}
	
	public String insert(JSeekersPojo jsp)
	{
		getBean();
		return rjsdao.saveUser(jsp);
	}
	
	public JSeekersPojo select(String email,String passwd)
	{
		getBean();
		return rjsdao.getUser(email, passwd);
	}
}
