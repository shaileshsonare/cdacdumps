class testthread extends Thread  // implements Runnable
{
	Thread t1;
	String name; 
	public testthread(String name)
	{
		this.name = name;
		System.out.println("cons called."+name);
		t1 = new Thread(this);
		t1.start();
	}
	
	public void run()
	{		
		System.out.println("run method called."+name);
		for(int i=1;i<=10;i++)
		{
			System.out.println("hello"+i);
			try{
				System.out.println("Before Suspend : "+t1.isAlive());
				t1.suspend();
				System.out.println("After Suspend : "+t1.isAlive());
			}catch(Exception e)
			{
				System.out.println(e);
			}
		}
	}
	public static void main(String arg[])
	{
		testthread tt=null;
		tt = new testthread("1");
		tt.t1.resume();
		testthread tt1 = new testthread("2");
		tt1.t1.resume();		
		System.out.println("I am here ");
		testthread tt2 = new testthread("3");
		tt2.t1.resume();		

	}
}