class Accounts
{
	int balance;
	int accno;
	
	public synchronized int deposit(int damt)
	{
		balance=damt;
		return damt;
	}
	
	public int withdraw(int wamt)
	{
		return wamt;
	}
	
	
}

class Thread11 implements Runnable
{
	Accounts aobj;
	public Thread11(Accounts a)
	{
		this.aobj=a;
		new Thread(this).start();
	}
	public void run()
	{
		aobj.deposit(2000);
		System.out.println("run of thread.."+aobj.balance);
		
	}
	
	public void display()
	{
		System.out.println("balance is "+aobj.balance);
	}
}


public class BankingDemo {

	
	public static void main(String[] args) {
		
		Accounts a1=new Accounts();
		Thread11 t11=new Thread11(a1);
		
		
		t11.display();

	}

}
