class Resource
{
	String msg1="[";
	String msg2="]";
	
	public synchronized void display(String msg)
	{
		System.out.println(msg1);
		System.out.println(msg);
		System.out.println(msg2);
	}
}

class Thread111 extends Thread
{
	Resource r;
	String msg;
	public Thread111(Resource r,String msg)
	{
		this.r=r;
		this.msg=msg;
		start();
	}
	
	public void run()
	{
		System.out.println("hello World");
		r.display(msg);
	}
}


public class MulThread {
	

	public static void main(String[] args) {
		
		Resource r1=new Resource();
		
		Thread111 t1=new Thread111(r1,"Hey");
		Thread111 t2=new Thread111(r1,"How");
		

	}

}
