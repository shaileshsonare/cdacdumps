import java.io.Serializable;


public class Person implements Serializable{

	String name;
	int age;
	String address;
	
	public Person() {
		super();
	}

	public Person(String name, int age, String address) {
		super();
		this.name = name;
		this.age = age;
		this.address = address;
	}
	
}
