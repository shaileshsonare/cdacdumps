import java.io.Serializable;


public class Account implements Serializable{

	int account_number;
	int balance;
	
	
	public Account() {
		// TODO Auto-generated constructor stub
	}


	public Account(int account_number, int balance) {
		super();
		this.account_number = account_number;
		this.balance = balance;
	}
	
	
	public synchronized int deposit(int deposit_amount)
	{
		balance=balance+deposit_amount;
		return deposit_amount;
	}
	
	public int withdraw(int withdraw_amount)
	{
		balance=balance-withdraw_amount;
		return withdraw_amount;
	}
	
	public void show_balance()
	{
		System.out.println("Current balance is :: "+balance);
	}

}
