
class ResourceData  //Resource
{   	public synchronized void display(String msg)
	{
	
			System.out.println("{");
			System.out.println(msg);
			System.out.println("}");		
	}
}


class ThreadDemo extends Thread
{
	ResourceData res;
	String msg;
	
	public ThreadDemo(ResourceData res,String data) {
		this.res = res;
	    msg = data;
	    start();
	}
	
	
	public void run()
	{
		res.display(msg);
	}
	
}

public class Synch {
	
	
	public static void main(String[] args) {
		ResourceData obj = new ResourceData();
		ThreadDemo t1 = new ThreadDemo(obj,"Welcome");
		ThreadDemo t2 = new ThreadDemo(obj,"To");
		ThreadDemo t3 = new ThreadDemo(obj,"Synchronized");
		ThreadDemo t4 = new ThreadDemo(obj,"Method");
	}

}
