using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/* READ UNCOMMITTED: 
 * Specifies that statements can read rows that have been modified by 
 * other transactions but not yet committed.
 * When this option is set, it is possible to read uncommitted 
 * modifications, which are called dirty reads. 
 * Values in the data can be changed and rows can appear or
 * disappear in the data set before the end of the transaction.
 * 
 
 * READ COMMITTED
 * Specifies that statements cannot read data that has been modified but not committed by other transactions.
 * This prevents dirty reads. 
 * Data can be changed by other transactions between individual statements within the current transaction, 
 * resulting in nonrepeatable reads or phantom data.
 
 * REPEATABLE READ
 * Specifies that statements cannot read data that has been modified but not yet committed by other transactions 
 * and that no other transactions can modify data that has been read by the current transaction until the 
 * current transaction completes.
 
 * SNAPSHOT
 * Specifies that data read by any statement in a transaction will be the transactionally consistent version of
 * the data that existed at the start of the transaction. The transaction can only recognize data modifications 
 * that were committed before the start of the transaction. 
 * Data modifications made by other transactions after the start of the current transaction are not visible to 
 * statements executing in the current transaction. 
 * The effect is as if the statements in a transaction get a snapshot of the committed data as it existed at the 
 * start of the transaction.
 * 
 * SERIALIZABLE
 *  pecifies the following:

    Statements cannot read data that has been modified but not yet committed by other transactions.

    No other transactions can modify data that has been read by the current transaction until the current transaction completes.

    Other transactions cannot insert new rows with key values that would fall in the range of keys read by any statements in the current transaction until the current transaction completes.

 * Range locks are placed in the range of key values that match the search conditions of each statement executed 
 * in a transaction. 
 * This blocks other transactions from updating or inserting any rows that would qualify for any of the statements 
 * executed by the current transaction. This means that if any of the statements in a transaction are executed a
 * second time, they will read the same set of rows. The range locks are held until the transaction completes.
 */


namespace Language_Reference.ADO.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Biblio"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlConnection connection2 = new SqlConnection(connectionString);

            SqlTransaction publishersTransaction1, publishersTransaction2;

            connection1.Open();
            publishersTransaction1 = connection1.BeginTransaction();

            connection2.Open();
            publishersTransaction2 = 
                connection2.BeginTransaction(IsolationLevel.ReadUncommitted);
            
            SqlCommand publisherCommand1 = connection1.CreateCommand();
            publisherCommand1.CommandType = CommandType.Text;
            publisherCommand1.Transaction = publishersTransaction1;

            SqlCommand publisherCommand2 = connection2.CreateCommand();
            publisherCommand2.CommandType = CommandType.Text;
            publisherCommand2.Transaction = publishersTransaction2;

            SqlDataReader publishersReader;

            try
            {
                publisherCommand1.CommandText = 
                    "INSERT INTO Publishers(Name) VALUES('AAA')";
                publisherCommand1.ExecuteNonQuery();
                publisherCommand1.CommandText = 
                    "INSERT INTO Publishers(Name) VALUES('BBB')";
                publisherCommand1.ExecuteNonQuery();

                publisherCommand2.CommandText = 
                    "SELECT TOP 2 PubID FROM Publishers ORDER BY PubID DESC";
                publishersReader = publisherCommand2.ExecuteReader();

                Console.WriteLine("Last 2 Publishers - Transaction is pending");
                while (publishersReader.Read())
                {
                    Console.WriteLine(publishersReader[0]);
                }
                publishersReader.Close();

                publishersTransaction1.Rollback();

                Console.WriteLine();

                publishersReader = publisherCommand2.ExecuteReader();

                Console.WriteLine("Last 2 Publishers - Transaction rolled back");
                while (publishersReader.Read())
                {
                    Console.WriteLine(publishersReader[0]);
                }
                publishersReader.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                publishersTransaction1.Rollback();
            }

            connection1.Close();
        }
    }
}