using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Language_Reference.ADO.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.
                ConnectionStrings["Biblio"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand authorCommand = connection.CreateCommand();
            authorCommand.CommandType = CommandType.Text;
            authorCommand.CommandText = 
                        "SELECT Au_ID, Author FROM Authors";

            DataSet biblioDataSet = new DataSet();
            SqlDataAdapter authorsAdapter = 
                            new SqlDataAdapter(authorCommand);
            authorsAdapter.Fill(biblioDataSet, "Authors");

            SqlCommandBuilder authorCommandBuilder = 
                                     new SqlCommandBuilder(authorsAdapter);
            //authorsAdapter.UpdateCommand = authorCommandBuilder.GetUpdateCommand();

            Console.WriteLine("Author name before change: {0}",
                biblioDataSet.Tables["Authors"].Rows[0]["Author"]);

            biblioDataSet.Tables["Authors"].Rows[0]["Author"] = "Jacobb, Rusell";
            authorsAdapter.Update(biblioDataSet, "Authors");
            
            Console.WriteLine("Author name after change: {0}",
                biblioDataSet.Tables["Authors"].Rows[0]["Author"]);
        }
    }
}