using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Language_Reference.ADO.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            DataTable authors = new DataTable("Authors");
            authors.Columns.Add("Au_ID", typeof(int));
            authors.Columns.Add("Author", typeof(string));
            authors.Columns.Add("Year Born", typeof(short));
            
            DataSet biblioDataSet = new DataSet();
            biblioDataSet.Tables.Add(authors);
            
            string connectionString = ConfigurationManager.
                ConnectionStrings["Biblio"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            SqlCommand authorsCommand = connection.CreateCommand();
            authorsCommand.CommandType = CommandType.Text;
            authorsCommand.CommandText = 
                            "SELECT Au_ID, Author FROM Authors";

            SqlDataReader authorsDataReader = authorsCommand.ExecuteReader();
            biblioDataSet.Tables["Authors"].Load(authorsDataReader);

            foreach (DataRow theRow in biblioDataSet.Tables["Authors"].Rows)
            {
                Console.WriteLine(theRow["Au_ID"] + "\t" + theRow["Author"]);
            }

            connection.Close();
        }
    }
}