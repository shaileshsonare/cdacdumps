using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Language_Reference.ADO.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Biblio"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand publishersCommand = connection.CreateCommand();
            publishersCommand.CommandType = CommandType.Text;
            publishersCommand.CommandText = "SELECT * FROM Publishers";

            DataSet biblioDataSet = new DataSet();
            SqlDataAdapter publishersAdapter = new SqlDataAdapter(publishersCommand);
            publishersAdapter.Fill(biblioDataSet, "Publishers");

            DataRow publisherRow;

            publisherRow = biblioDataSet.Tables["Publishers"].NewRow();
            publisherRow["Name"] = "AAA";
            biblioDataSet.Tables["Publishers"].Rows.Add(publisherRow);

            publisherRow = biblioDataSet.Tables["Publishers"].NewRow();
            publisherRow["Name"] = "BBB";
            biblioDataSet.Tables["Publishers"].Rows.Add(publisherRow);

            SqlCommandBuilder publishersCommandBuilder = new SqlCommandBuilder(publishersAdapter);
            publishersAdapter.InsertCommand = publishersCommandBuilder.GetInsertCommand();

            connection.Open();
            SqlTransaction publishersTransaction = connection.BeginTransaction();
            publishersAdapter.InsertCommand.Transaction = publishersTransaction;

            bool throwException = false;

            try
            {
                publishersAdapter.Update(biblioDataSet, "Publishers");
                if (throwException)
                    throw new InvalidExpressionException();
                publishersTransaction.Commit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                publishersTransaction.Rollback();
            }

            connection.Close();
        }
    }
}