﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace FileHandlings
{
    [AttributeUsage(AttributeTargets.All,AllowMultiple=true)]
    class CommentAttribute : Attribute
    {
        public string Comment
        {
            get;
            set;
        }

        public CommentAttribute(string Comment)
        {
            this.Comment = Comment;
        }
    }

    [Comment("Person ")]
    [Comment("Dress kharab zalyawar bhetnaar")]
    public class Person : ICloneable 
    {
        int age;
        public int Age
        {
            set 
            {
                age = value;
            }
            get 
            {
                return age;
            }
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person();
            p.Age = 25;
            Console.WriteLine(p.Age);
            Type t = typeof(Person);

            CommentAttribute[] comments = t.GetCustomAttributes(typeof(CommentAttribute), false) as CommentAttribute[];

            if (comments == null) return;

            foreach (CommentAttribute item in comments)
            {
                Console.WriteLine(item.Comment);
            }
        }
    }
}
