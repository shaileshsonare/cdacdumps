﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Generics1
{
    class Student
    {
        public int rollno;
        public string name;
        public Student()
        {
            rollno = 1;
            name = "vss";
        }
        public Student(int r, string n)
        {
            this.rollno = r;
            this.name = n;
        }

        public override string ToString()
        {
            return "RollNo::" + rollno + "Name::" + name;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> list = new List<Student>();

            list.Add(new Student(5 ,"Shailesh"));

            list.Add(new Student(9,"Sonu"));

            list.Add(new Student(7,"Monu"));
            
            IEnumerator ie=list.GetEnumerator();

            while (ie.MoveNext())
            {
                Console.WriteLine(ie.Current);
            }

           
        }
    }
}
