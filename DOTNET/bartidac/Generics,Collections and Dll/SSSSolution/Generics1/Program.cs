﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Generics1
{
    class Numbers<T> where T:struct
    {
        T var1;
        public Numbers(T x)
        {
            //var1 = default(T);
            var1 = x;
            Console.WriteLine(var1);
        }
    }

    class GenrClass<T> where T : class
    {
        T[] array;
        int ni;
        public GenrClass(int dim)
        {
            array=new T[dim];
            ni = 0;
        }
        public void Add(T objct)
        {
            if (ni < array.Length)
            {
                array[ni] = objct;
                ni++;
            }
        }
        public T this[int index]
        {
            get
            {
                return array[index];
            }
        }
    }

    class Person
    {
        int age;
        public Person()
        {
            age = 25;
        }
        public Person(int age)
        {
            this.age = age;
        }
        public override string ToString()
        {
            return "" + age;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Numbers<int> num = new Numbers<int>(2);
            Numbers<float> num2 = new Numbers<float>(3.45f);
            Numbers<double> num3 = new Numbers<double>(4.12345);
            Numbers<char> num4 = new Numbers<char>('a');

            GenrClass<Person> gc = new GenrClass<Person>(5);
            gc.Add(new Person(10));
            gc.Add(new Person(20));
            gc.Add(new Person(30));
            gc.Add(new Person(40));
            gc.Add(new Person(50));


            for (int i = 0; i < 5; i++)
            {
                   Console.WriteLine(gc[i]);    
            }
           
            
        }
    }
}
