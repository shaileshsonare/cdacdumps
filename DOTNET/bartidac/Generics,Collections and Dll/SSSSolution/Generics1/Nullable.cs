﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Generics1
{
    class Program
    {
        static void Main(string[] args)
        {
            int? n = null;
            
            int m = n ?? 5;
            n = 2147483647;

            

            Console.WriteLine(m);
            Console.WriteLine(n + 5);
        }
    }
}
