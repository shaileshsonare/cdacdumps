using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Language_Reference
{
    class Program
    {
        [System.Runtime.InteropServices.DllImport("User32")]
        public extern static Boolean MessageBeep(uint uType);

        const uint MB_ICONASTERISK = 0x00000040;

        static void Main(string[] args)
        {
            MessageBeep(MB_ICONASTERISK);
            MessageBeep(MB_ICONASTERISK);
            MessageBeep(MB_ICONASTERISK);
        }
    }
}