using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Language_Reference
{
    class Program
    {
        static void threadProc1()
        {
            for (int i = 100; i < 400; i++)
                Console.WriteLine(i);
        }

        static void threadProc2()
        {
            for (int i = 500; i < 800; i++)
                Console.WriteLine(i);
        }

        static void Main(string[] args)
        {
            ThreadStart entrypoint;

            entrypoint = new ThreadStart(threadProc1);
            Thread thread1 = new Thread(entrypoint);

            entrypoint = new ThreadStart(threadProc2);
            Thread thread2 = new Thread(entrypoint);

            thread1.Start();
            thread2.Start();
        }
    }
}