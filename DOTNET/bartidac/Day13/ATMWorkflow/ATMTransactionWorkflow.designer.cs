﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace ATMWorkflow
{
    partial class ATMTransactionWorkflow
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        [System.CodeDom.Compiler.GeneratedCode("", "")]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.Activities.CodeCondition codecondition1 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition2 = new System.Workflow.Activities.CodeCondition();
            this.promtFailuerCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.delayActivity2 = new System.Workflow.Activities.DelayActivity();
            this.disburseCashCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.ifElseBranchActivity2 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifTransactionSuccessfull = new System.Workflow.Activities.IfElseBranchActivity();
            this.welcomeCodeActivity2 = new System.Workflow.Activities.CodeActivity();
            this.delayActivity3 = new System.Workflow.Activities.DelayActivity();
            this.logTransactionCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.ifElseActivity1 = new System.Workflow.Activities.IfElseActivity();
            this.performTransactionCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.gatherInformationCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.cancellationHandlerActivity1 = new System.Workflow.ComponentModel.CancellationHandlerActivity();
            this.sequenceActivity1 = new System.Workflow.Activities.SequenceActivity();
            this.delayActivity4 = new System.Workflow.Activities.DelayActivity();
            this.shutDownCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.whileActivity = new System.Workflow.Activities.WhileActivity();
            this.welcomeMessageCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.delayActivity1 = new System.Workflow.Activities.DelayActivity();
            this.initializeCodeActivity = new System.Workflow.Activities.CodeActivity();
            // 
            // promtFailuerCodeActivity
            // 
            this.promtFailuerCodeActivity.Name = "promtFailuerCodeActivity";
            this.promtFailuerCodeActivity.ExecuteCode += new System.EventHandler(this.PromptFailuer);
            // 
            // delayActivity2
            // 
            this.delayActivity2.Name = "delayActivity2";
            this.delayActivity2.TimeoutDuration = System.TimeSpan.Parse("00:00:05");
            // 
            // disburseCashCodeActivity
            // 
            this.disburseCashCodeActivity.Name = "disburseCashCodeActivity";
            this.disburseCashCodeActivity.ExecuteCode += new System.EventHandler(this.DisburseCash);
            // 
            // ifElseBranchActivity2
            // 
            this.ifElseBranchActivity2.Activities.Add(this.promtFailuerCodeActivity);
            this.ifElseBranchActivity2.Name = "ifElseBranchActivity2";
            // 
            // ifTransactionSuccessfull
            // 
            this.ifTransactionSuccessfull.Activities.Add(this.disburseCashCodeActivity);
            this.ifTransactionSuccessfull.Activities.Add(this.delayActivity2);
            codecondition1.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IsTransactionSuccessfull);
            this.ifTransactionSuccessfull.Condition = codecondition1;
            this.ifTransactionSuccessfull.Name = "ifTransactionSuccessfull";
            // 
            // welcomeCodeActivity2
            // 
            this.welcomeCodeActivity2.Name = "welcomeCodeActivity2";
            this.welcomeCodeActivity2.ExecuteCode += new System.EventHandler(this.WelcomeMessage);
            // 
            // delayActivity3
            // 
            this.delayActivity3.Name = "delayActivity3";
            this.delayActivity3.TimeoutDuration = System.TimeSpan.Parse("00:00:05");
            // 
            // logTransactionCodeActivity
            // 
            this.logTransactionCodeActivity.Name = "logTransactionCodeActivity";
            this.logTransactionCodeActivity.ExecuteCode += new System.EventHandler(this.LogTransaction);
            // 
            // ifElseActivity1
            // 
            this.ifElseActivity1.Activities.Add(this.ifTransactionSuccessfull);
            this.ifElseActivity1.Activities.Add(this.ifElseBranchActivity2);
            this.ifElseActivity1.Name = "ifElseActivity1";
            // 
            // performTransactionCodeActivity
            // 
            this.performTransactionCodeActivity.Name = "performTransactionCodeActivity";
            this.performTransactionCodeActivity.ExecuteCode += new System.EventHandler(this.PerformTransaction);
            // 
            // gatherInformationCodeActivity
            // 
            this.gatherInformationCodeActivity.Name = "gatherInformationCodeActivity";
            this.gatherInformationCodeActivity.ExecuteCode += new System.EventHandler(this.GatherInformation);
            // 
            // cancellationHandlerActivity1
            // 
            this.cancellationHandlerActivity1.Name = "cancellationHandlerActivity1";
            // 
            // sequenceActivity1
            // 
            this.sequenceActivity1.Activities.Add(this.gatherInformationCodeActivity);
            this.sequenceActivity1.Activities.Add(this.performTransactionCodeActivity);
            this.sequenceActivity1.Activities.Add(this.ifElseActivity1);
            this.sequenceActivity1.Activities.Add(this.logTransactionCodeActivity);
            this.sequenceActivity1.Activities.Add(this.delayActivity3);
            this.sequenceActivity1.Activities.Add(this.welcomeCodeActivity2);
            this.sequenceActivity1.Name = "sequenceActivity1";
            // 
            // delayActivity4
            // 
            this.delayActivity4.Name = "delayActivity4";
            this.delayActivity4.TimeoutDuration = System.TimeSpan.Parse("00:00:05");
            // 
            // shutDownCodeActivity
            // 
            this.shutDownCodeActivity.Name = "shutDownCodeActivity";
            this.shutDownCodeActivity.ExecuteCode += new System.EventHandler(this.ShutDown);
            // 
            // whileActivity
            // 
            this.whileActivity.Activities.Add(this.sequenceActivity1);
            this.whileActivity.Activities.Add(this.cancellationHandlerActivity1);
            codecondition2.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IsCardInserted);
            this.whileActivity.Condition = codecondition2;
            this.whileActivity.Name = "whileActivity";
            // 
            // welcomeMessageCodeActivity
            // 
            this.welcomeMessageCodeActivity.Name = "welcomeMessageCodeActivity";
            this.welcomeMessageCodeActivity.ExecuteCode += new System.EventHandler(this.WelcomeMessage);
            // 
            // delayActivity1
            // 
            this.delayActivity1.Name = "delayActivity1";
            this.delayActivity1.TimeoutDuration = System.TimeSpan.Parse("00:00:05");
            // 
            // initializeCodeActivity
            // 
            this.initializeCodeActivity.Name = "initializeCodeActivity";
            this.initializeCodeActivity.ExecuteCode += new System.EventHandler(this.Initialize);
            // 
            // ATMTransactionWorkflow
            // 
            this.Activities.Add(this.initializeCodeActivity);
            this.Activities.Add(this.delayActivity1);
            this.Activities.Add(this.welcomeMessageCodeActivity);
            this.Activities.Add(this.whileActivity);
            this.Activities.Add(this.shutDownCodeActivity);
            this.Activities.Add(this.delayActivity4);
            this.Name = "ATMTransactionWorkflow";
            this.CanModifyActivities = false;

        }

        #endregion

        private DelayActivity delayActivity4;

        private CodeActivity shutDownCodeActivity;

        private CodeActivity welcomeCodeActivity2;

        private DelayActivity delayActivity3;

        private CodeActivity initializeCodeActivity;

        private DelayActivity delayActivity2;

        private CancellationHandlerActivity cancellationHandlerActivity1;

        private SequenceActivity sequenceActivity1;

        private WhileActivity whileActivity;

        private CodeActivity welcomeMessageCodeActivity;

        private CodeActivity promtFailuerCodeActivity;

        private CodeActivity logTransactionCodeActivity;

        private DelayActivity delayActivity1;

        private CodeActivity disburseCashCodeActivity;

        private IfElseBranchActivity ifElseBranchActivity2;

        private IfElseBranchActivity ifTransactionSuccessfull;

        private IfElseActivity ifElseActivity1;

        private CodeActivity performTransactionCodeActivity;

        private CodeActivity gatherInformationCodeActivity;


























    }
}
