﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ICICITransactionService
{
    [ServiceContract]
    public interface IDebitTransactionService
    {
        [OperationContract]
        double Debit(int accountNo, double amount);
    }
}