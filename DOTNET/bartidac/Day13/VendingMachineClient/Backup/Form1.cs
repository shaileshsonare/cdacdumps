﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Workflow.Activities;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using VendingMachine;
using System.Diagnostics;

namespace VendingMachineClient
{
    public partial class Form1 : Form, IMixerController, IHotWaterController, IMakeTea, IStateController, ITeaContainerController
    {
        private WorkflowRuntime workflowRuntime = null;
        private WorkflowInstance workflowInstance = null;
        private ExternalDataExchangeService exchangeService = null;

        public Form1()
        {
            InitializeComponent();
            this.workflowRuntime = new WorkflowRuntime();
            this.exchangeService = new ExternalDataExchangeService();
            this.workflowRuntime.AddService(this.exchangeService);
            this.exchangeService.AddService(this);
            this.workflowRuntime.StartRuntime();

            Type vendingMachineWorkflow = typeof(VendingMachineWorkflow);
            workflowInstance = this.workflowRuntime.CreateWorkflow(vendingMachineWorkflow);
            workflowInstance.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            makeTea(null, new ExternalDataEventArgs(workflowInstance.InstanceId));
        }

        #region IMixerController Members

        void IMixerController.Start()
        {
            Debug.WriteLine("Mixer Started");
        }

        void IMixerController.Stop()
        {
            Debug.WriteLine("Mixer Stopped");
        }

        #endregion

        #region IHotWaterController Members

        void IHotWaterController.CloseValve()
        {
            Debug.WriteLine("Hot Water Valve Closed");
        }

        void IHotWaterController.OpenValve()
        {
            Debug.WriteLine("Hot Water Valve Opened");
        }

        #endregion

        #region IMakeTea Members

        private event EventHandler<ExternalDataEventArgs> makeTea; 
        public event EventHandler<ExternalDataEventArgs> MakeTea
        {
            add
            {
                makeTea += value;
            }

            remove
            {
                makeTea -= value;
            }

        }

        #endregion

        #region IStateController Members

        private delegate void UpdateStatusDelegate(string state);

        public void UpdateStatus(string state)
        {
            Debug.WriteLine("State:" + state);
        }

        #endregion

        #region ITeaContainerController Members

        void ITeaContainerController.CloseValve()
        {
            Debug.WriteLine("Tea Controller Valve Closed");
        }

        void ITeaContainerController.OpenValve()
        {
            Debug.WriteLine("Tea Controller Valve Opened");
        }

        #endregion
    }
}
