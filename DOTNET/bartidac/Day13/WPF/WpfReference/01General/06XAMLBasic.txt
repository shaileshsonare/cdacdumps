﻿class MyObject
{
	public int SomeProerpty
	{
		set;
		get;
	}
}


// C# version
MyObject obj = new MyObject();
obj.SomeProperty = 1;

<!-- XAML version -->
<MyObject SomeProperty='1' />

----

// C# version
using Samples;

MyObject obj = new MyObject();
obj.SomeProperty = 1;

<!-- XAML version -->
<MyObject xmlns='clr-namespace:Samples' SomeProperty='1' />

----

// C# version
csc /r:samples.dll test.cs
using Samples;

MyObject obj = new MyObject();
obj.SomeProperty = 1;

<!-- XAML version -->
<MyObject xmlns='clr-namespace:Samples;assembly=samples.dll' SomeProperty='1' />

----

<MyObject xmlns='clr-namespace:Samples;assembly=samples.dll'>
	<MyObject.SomeProperty>
		1
	</MyObject.SomeProperty>
</MyObject>

----

<!-- option 1: import by CLR namespace -->
<Window xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml' 
xmlns='clr-namespace:System.Windows;assembly=presentationframework.dll'>
</Window>


<!-- option 2: import by URI -->
<Window xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'
xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'>
</Window>