﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace WpfReference
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Application application = new Application();

            Window window = new Window();
            window.Title = "WPF Basic Window";

            application.Run(window);
        }
    }
}
