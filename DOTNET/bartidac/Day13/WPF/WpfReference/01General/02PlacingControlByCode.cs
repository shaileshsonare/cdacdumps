﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ConsoleApplication1
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application application = new Application();

            Window window = new Window();
            window.Title = "WPF Basic Window";

            Grid grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            window.Content = grid;

            Button button1 = new Button();
            button1.Content = "Button1";
            Grid.SetRow(button1, 0);
            Grid.SetColumn(button1, 0);

            Button button2 = new Button();
            button2.Content = "Button2";
            Grid.SetRow(button2, 0);
            Grid.SetColumn(button2, 1);

            Button button3 = new Button();
            button3.Content = "Button3";
            Grid.SetRow(button3, 1);
            Grid.SetColumn(button3, 0);

            Button button4 = new Button();
            button4.Content = "Button4";
            Grid.SetRow(button4, 1);
            Grid.SetColumn(button4, 1);

            grid.Children.Add(button1);
            grid.Children.Add(button2);
            grid.Children.Add(button3);
            grid.Children.Add(button4);

            application.Run(window);
        }
    }
}
