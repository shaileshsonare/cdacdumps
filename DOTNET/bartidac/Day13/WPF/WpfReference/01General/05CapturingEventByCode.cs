﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace WpfReference
{
    class Window1 : Window
    {
        public Grid grid;

        public Window1()
        {
            grid = new Grid();
            this.Content = grid;
        }
    }
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application application = new Application();

            Window1 window = new Window1();
            window.Title = "WPF Basic Window";

            window.grid.RowDefinitions.Add(new RowDefinition());
            window.grid.RowDefinitions.Add(new RowDefinition());
            window.grid.ColumnDefinitions.Add(new ColumnDefinition());
            window.grid.ColumnDefinitions.Add(new ColumnDefinition());

            Button button1 = new Button();
            button1.Content = "Button1";
            Grid.SetRow(button1, 0);
            Grid.SetColumn(button1, 0);
            button1.Click += new RoutedEventHandler(button_Click);

            Button button2 = new Button();
            button2.Content = "Button2";
            Grid.SetRow(button2, 0);
            Grid.SetColumn(button2, 1);
            button2.Click += new RoutedEventHandler(button_Click);

            Button button3 = new Button();
            button3.Content = "Button3";
            Grid.SetRow(button3, 1);
            Grid.SetColumn(button3, 0);
            button3.Click += new RoutedEventHandler(button_Click);

            Button button4 = new Button();
            button4.Content = "Button4";
            Grid.SetRow(button4, 1);
            Grid.SetColumn(button4, 1);
            button4.Click += new RoutedEventHandler(button_Click);

            window.grid.Children.Add(button1);
            window.grid.Children.Add(button2);
            window.grid.Children.Add(button3);
            window.grid.Children.Add(button4);

            application.Run(window);
        }

        static void button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(((Button)sender).Content + " Clicked");
        }
    }
}
