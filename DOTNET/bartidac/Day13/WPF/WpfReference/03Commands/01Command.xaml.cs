﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfReference
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();

            InputBinding ib = new InputBinding(MyAppCommands.OpenCommand,
                new KeyGesture(Key.O, ModifierKeys.Control | ModifierKeys.Shift));
            this.InputBindings.Add(ib);

            CommandBinding cb = new CommandBinding(MyAppCommands.OpenCommand);
            cb.Executed += new ExecutedRoutedEventHandler(cb_Executed);
            this.CommandBindings.Add(cb);
        }

        void cb_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show(TextBox1.Text + " loaded successfully.");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
