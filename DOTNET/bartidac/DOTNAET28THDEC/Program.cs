﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ConsoleApplication
{
    class DataSource
    {
        public delegate void NotifyDelegate(int data);
        public NotifyDelegate Update;

        public DataSource() { data = 1; }

        public void Change(int value)
        {
            data = value;
            if (Update != null)
                Update(data);
        }
        int data;
    }

    class DataConsumer1
    {
        public DataConsumer1(DataSource datasource)
        {
            ds = datasource;
            ds.Update = Notify;
        }
        public void Notify(int data)
        {
            Console.WriteLine("DataConsumern1:{0}", data);
        }

        public void Modify(int v)
        {
            ds.Change(v);
        }

        DataSource ds;
    }

    class DataConsumer2
    {
        public DataConsumer2(DataSource datasource)
        {
            datasource.Update = Render;
        }

        public void Render(int value)
        {
            Console.WriteLine("DataConsumern2:{0}", value);
        }
    }



    class Program
    {
        static void Main()
        {
            DataSource ds = new DataSource();
            DataConsumer1 consumer1 = new DataConsumer1(ds);
            consumer1.Modify(2);
        }
    }
}
