﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ConsoleApplication
{
    interface IGraphics2DDraw
    {
        void Draw();
    }

    interface IGraphics3DDraw
    {
        void Draw();
    }


    class Cube : IGraphics2DDraw, IGraphics3DDraw
    {
        void IGraphics2DDraw.Draw()
        {
            Console.WriteLine("2DDraw");
        }

        void IGraphics3DDraw.Draw()
        {
            Console.WriteLine("3DDraw");
        }
    }


    class Program
    {
        static void Main()
        {
            Cube cube = new Cube();

            IGraphics2DDraw g2d = cube;

            IGraphics3DDraw g3d = g2d as IGraphics3DDraw;

            g2d.Draw();
            g3d.Draw();
        }
    }
}
