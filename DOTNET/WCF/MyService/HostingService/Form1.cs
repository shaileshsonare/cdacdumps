﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace HostingService
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Uri adr = new Uri("http://localhost:8732/Design_Time_Addresses/MyService/Math/");
            ServiceHost host = new ServiceHost(typeof(MyService.Math), adr);
            host.AddServiceEndpoint(typeof(MyService.IMath), new WSHttpBinding(), adr);

            ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
            smb.HttpGetEnabled = true;
            host.Description.Behaviors.Add(smb);
            host.Open();

            MessageBox.Show("host is running");
        }
    }
}
