using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Runtime.Serialization.Formatters.Soap;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Language_Reference.ADO.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Biblio"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand authorCommand = connection.CreateCommand();
            authorCommand.CommandType = CommandType.Text;
            authorCommand.CommandText = "SELECT Au_ID, Author FROM Authors";

            DataSet biblioDataSet = new DataSet();
            SqlDataAdapter authorsAdapter = new SqlDataAdapter(authorCommand);
            authorsAdapter.Fill(biblioDataSet, "Authors");

            Console.WriteLine("Serializing Graph to Binary Stream...");
            Stream s = File.Open("MyDS.bin", FileMode.Create, FileAccess.ReadWrite);
            BinaryFormatter sf = new BinaryFormatter();
            sf.Serialize(s, biblioDataSet);
            s.Close();
            Console.WriteLine("Serialization Complete.");

            Console.WriteLine("Deserializing Graph from Binary Stream...");
            Stream r = File.Open("MyDS.bin", FileMode.Open, FileAccess.Read);
            BinaryFormatter sf2 = new BinaryFormatter();
            DataSet biblioDataSet2 = (DataSet)sf2.Deserialize(r);
            r.Close();
            Console.WriteLine("Deserializing Complete...");

            Console.WriteLine("After deserialization BiblioDataSet2 contains {0} Authors.",
                biblioDataSet2.Tables["Authors"].Rows.Count);

            for (int i = 0; i < 10; i++)
            {
                DataRow authorRow = biblioDataSet.Tables["Authors"].Rows[i];
                Console.WriteLine(authorRow["Au_ID"] + "\t" + authorRow["Author"]);
            }
        }
    }
}