using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Language_Reference.ADO.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.
                ConnectionStrings["Biblio"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);

            SqlTransaction publishersTransaction;
            connection.Open();
            publishersTransaction = connection.BeginTransaction();

            SqlCommand publisherCommand1 = connection.CreateCommand();
            publisherCommand1.CommandType = CommandType.Text;
            publisherCommand1.CommandText = 
                                    "INSERT INTO Publishers(Name) VALUES('AAA')";
            publisherCommand1.Transaction = publishersTransaction;

            SqlCommand publisherCommand2 = connection.CreateCommand();
            publisherCommand2.CommandType = CommandType.Text;
            publisherCommand2.CommandText = 
                                     "INSERT INTO Publishers(Name) VALUES('BBB')";
            publisherCommand2.Transaction = publishersTransaction;

            bool throwException = true;

            try
            {
                publisherCommand1.ExecuteNonQuery();
                publisherCommand2.ExecuteNonQuery();
                if (throwException)
                    throw new InvalidExpressionException();
                publishersTransaction.Commit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                publishersTransaction.Rollback();
            }

            connection.Close();
        }
    }
}