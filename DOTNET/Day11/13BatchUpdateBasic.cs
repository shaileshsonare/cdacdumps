using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Language_Reference.ADO.NET
{
    class Program
    {
        private static int countUpdating;
        private static int countUpdated;

        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.
                                ConnectionStrings["Biblio"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand authorCommand = connection.CreateCommand();
            authorCommand.CommandType = CommandType.Text;
            authorCommand.CommandText =
                    @"SELECT Au_ID, Author, 
                            [Year Born] FROM Authors WHERE Au_ID < 1000";

            DataSet biblioDataSet = new DataSet();
            SqlDataAdapter authorsAdapter = 
                                new SqlDataAdapter(authorCommand);
            authorsAdapter.Fill(biblioDataSet, "Authors");

            SqlCommandBuilder authorCommandBuilder = 
                                new SqlCommandBuilder(authorsAdapter);
            authorsAdapter.UpdateCommand = authorCommandBuilder.GetUpdateCommand();

            Random r = new Random();
            foreach (DataRow authorRow in biblioDataSet.Tables["Authors"].Rows)
            {
                authorRow["Year Born"] = 1950 + r.Next() % 20;
            }

            authorsAdapter.RowUpdated += 
                new SqlRowUpdatedEventHandler(authorsAdapter_RowUpdated);

            authorsAdapter.RowUpdating +=
                new SqlRowUpdatingEventHandler(authorsAdapter_RowUpdating);

            authorsAdapter.UpdateBatchSize = 3;
            authorsAdapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
            authorsAdapter.Update(biblioDataSet, "Authors");

            Console.WriteLine("Updating count {0}, Updated count {1}",
                                            countUpdating, countUpdated);
        }

        static void authorsAdapter_RowUpdating(object sender, SqlRowUpdatingEventArgs e)
        {
            countUpdating++;
        }

        static void authorsAdapter_RowUpdated(object sender, SqlRowUpdatedEventArgs e)
        {
            countUpdated++;
        }
    }
}