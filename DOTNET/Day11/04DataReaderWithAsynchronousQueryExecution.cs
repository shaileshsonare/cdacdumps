using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;

namespace Language_Reference.ADO.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Biblio"].ConnectionString;
            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlConnection connection2 = new SqlConnection(connectionString);
            SqlConnection connection3 = new SqlConnection(connectionString);

            string commandText1 = 
               @"WAITFOR DELAY '0:0:01'; SELECT * FROM Authors WHERE Au_ID = 5632";
            string commandText2 = @"WAITFOR DELAY '0:0:05'; SELECT * FROM Authors WHERE Au_ID = 5632";
            string commandText3 = @"WAITFOR DELAY '0:0:10'; SELECT * FROM Authors WHERE Au_ID = 5632";

            try
            {
                connection1.Open();
                SqlCommand command1 = new SqlCommand(commandText1, connection1);
                IAsyncResult result1 = command1.BeginExecuteReader();
                WaitHandle waitHandle1 = result1.AsyncWaitHandle;

                connection2.Open();
                SqlCommand command2 = new SqlCommand(commandText2, connection2);
                IAsyncResult result2 = command2.BeginExecuteReader();
                WaitHandle waitHandle2 = result2.AsyncWaitHandle;

                connection3.Open();
                SqlCommand command3 = new SqlCommand(commandText3, connection3);
                IAsyncResult result3 = command3.BeginExecuteReader();
                WaitHandle waitHandle3 = result3.AsyncWaitHandle;

                WaitHandle[] waitHandles = { waitHandle1, waitHandle2,
                                                                waitHandle3 };

                int index;
                for (int countWaits = 0; countWaits <= 2; countWaits++)
                {
                    index = WaitHandle.WaitAny(waitHandles, 60000, false);

                    switch (index)
                    {
                        case 0:
                            SqlDataReader reader1;
                            reader1 = command1.EndExecuteReader(result1);
                            if (reader1.Read())
                            {
                                Console.WriteLine("Query 1 Completed " + 
                                          System.DateTime.Now.ToLongTimeString());
                            }
                            reader1.Close();
                            break;
                        case 1:
                            SqlDataReader reader2;
                            reader2 = command2.EndExecuteReader(result2);
                            if (reader2.Read())
                            {
                                Console.WriteLine("Query 2 Completed " + 
                                    System.DateTime.Now.ToLongTimeString());
                            }
                            reader2.Close();
                            break;
                        case 2:
                            SqlDataReader reader3;
                            reader3 = command3.EndExecuteReader(result3);
                            if (reader3.Read())
                            {
                                Console.WriteLine("Query 3 Completed " + 
                                    System.DateTime.Now.ToLongTimeString());
                            }
                            reader3.Close();
                            break;
                        case WaitHandle.WaitTimeout:
                            throw new Exception("Timeout");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            connection1.Close();
            connection2.Close();
            connection3.Close();
        }
    }
}