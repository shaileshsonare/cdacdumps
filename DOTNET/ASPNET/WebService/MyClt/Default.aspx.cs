﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calci;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_Click(object sender, EventArgs e)
    {
        int num1 = int.Parse(txt1.Text);
        int num2 = int.Parse(txt2.Text);
        CalcService obj = new CalcService();
        int res = obj.Sum(num1, num2);
        txt3.Text = res.ToString();
    }
}