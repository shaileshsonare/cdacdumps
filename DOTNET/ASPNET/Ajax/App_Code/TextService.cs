﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for TextService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class TextService : System.Web.Services.WebService {

    public TextService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [System.Web.Services.WebMethod]
    public string[] GetCompletionList(string prefixText, int count)
    {
        // db connection and retrieve appropriate texts to show to user
        String[] arr = { "mumbai", "pune", "karad", "delhi", "banglore" };
        return arr;
    }
    
}
