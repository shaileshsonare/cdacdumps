﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="page4.aspx.cs" Inherits="page4" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                <asp:AutoCompleteExtender ID="TextBox1_AutoCompleteExtender" runat="server" 
                    DelimiterCharacters="" Enabled="True" MinimumPrefixLength="2" 
                    ServiceMethod="GetCompletionList" ServicePath="TextService.asmx" 
                    TargetControlID="TextBox1">
                </asp:AutoCompleteExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </div>
    </form>
</body>
</html>
