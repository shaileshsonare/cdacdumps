﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace CalcLib
{
    [ServiceContract]
    public interface ICalcService
    {
        [OperationContract]
        int Sum(int num1, int num2);
    }
}
