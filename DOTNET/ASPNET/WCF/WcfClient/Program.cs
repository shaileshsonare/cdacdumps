﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WcfClient.Calci;

namespace WcfClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Client Started");
            CalcServiceClient client = new CalcServiceClient();
            int res = client.Sum(12, 4);
            Console.WriteLine("Result : " + res);
            client.Close();
        }
    }
}
