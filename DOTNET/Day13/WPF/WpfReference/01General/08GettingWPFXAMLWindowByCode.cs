﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

// Include 07GettingWPFWindow.xaml in build.
namespace WpfReference
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Application application = new Application();

            Window1 window = new Window1();

            application.Run(window);
        }
    }
}
