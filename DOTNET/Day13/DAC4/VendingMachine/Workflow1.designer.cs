﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace VendingMachine
{
    partial class Workflow1
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        [System.CodeDom.Compiler.GeneratedCode("", "")]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding1 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding2 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            this.setStateActivity3 = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity8 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity7 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.setStateActivity2 = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity6 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity5 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity4 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity3 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity2 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.setStateActivity1 = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity1 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.handleExternalEventActivity1 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.stateInitializationActivity3 = new System.Workflow.Activities.StateInitializationActivity();
            this.stateInitializationActivity2 = new System.Workflow.Activities.StateInitializationActivity();
            this.stateInitializationActivity1 = new System.Workflow.Activities.StateInitializationActivity();
            this.makeTea = new System.Workflow.Activities.EventDrivenActivity();
            this.dispensingStateActivity = new System.Workflow.Activities.StateActivity();
            this.preparingTeaStateActivity = new System.Workflow.Activities.StateActivity();
            this.readyStateActivity = new System.Workflow.Activities.StateActivity();
            // 
            // setStateActivity3
            // 
            this.setStateActivity3.Name = "setStateActivity3";
            this.setStateActivity3.TargetStateName = "readyStateActivity";
            // 
            // callExternalMethodActivity8
            // 
            this.callExternalMethodActivity8.InterfaceType = typeof(VendingMachine.IHotWaterController);
            this.callExternalMethodActivity8.MethodName = "CloseValve";
            this.callExternalMethodActivity8.Name = "callExternalMethodActivity8";
            // 
            // callExternalMethodActivity7
            // 
            this.callExternalMethodActivity7.InterfaceType = typeof(VendingMachine.IMixerController);
            this.callExternalMethodActivity7.MethodName = "Stop";
            this.callExternalMethodActivity7.Name = "callExternalMethodActivity7";
            // 
            // setStateActivity2
            // 
            this.setStateActivity2.Name = "setStateActivity2";
            this.setStateActivity2.TargetStateName = "dispensingStateActivity";
            // 
            // callExternalMethodActivity6
            // 
            this.callExternalMethodActivity6.InterfaceType = typeof(VendingMachine.IStateController);
            this.callExternalMethodActivity6.MethodName = "UpdateStatus";
            this.callExternalMethodActivity6.Name = "callExternalMethodActivity6";
            workflowparameterbinding1.ParameterName = "strate";
            workflowparameterbinding1.Value = "Dispensing Tea";
            this.callExternalMethodActivity6.ParameterBindings.Add(workflowparameterbinding1);
            // 
            // callExternalMethodActivity5
            // 
            this.callExternalMethodActivity5.InterfaceType = typeof(VendingMachine.ITeaContainerController);
            this.callExternalMethodActivity5.MethodName = "CloseValve";
            this.callExternalMethodActivity5.Name = "callExternalMethodActivity5";
            // 
            // callExternalMethodActivity4
            // 
            this.callExternalMethodActivity4.InterfaceType = typeof(VendingMachine.IMixerController);
            this.callExternalMethodActivity4.MethodName = "Start";
            this.callExternalMethodActivity4.Name = "callExternalMethodActivity4";
            // 
            // callExternalMethodActivity3
            // 
            this.callExternalMethodActivity3.InterfaceType = typeof(VendingMachine.IHotWaterController);
            this.callExternalMethodActivity3.MethodName = "OpenValve";
            this.callExternalMethodActivity3.Name = "callExternalMethodActivity3";
            // 
            // callExternalMethodActivity2
            // 
            this.callExternalMethodActivity2.InterfaceType = typeof(VendingMachine.ITeaContainerController);
            this.callExternalMethodActivity2.MethodName = "OpenValve";
            this.callExternalMethodActivity2.Name = "callExternalMethodActivity2";
            // 
            // setStateActivity1
            // 
            this.setStateActivity1.Name = "setStateActivity1";
            this.setStateActivity1.TargetStateName = "preparingTeaStateActivity";
            // 
            // callExternalMethodActivity1
            // 
            this.callExternalMethodActivity1.InterfaceType = typeof(VendingMachine.IStateController);
            this.callExternalMethodActivity1.MethodName = "UpdateStatus";
            this.callExternalMethodActivity1.Name = "callExternalMethodActivity1";
            workflowparameterbinding2.ParameterName = "strate";
            workflowparameterbinding2.Value = "Preparing Tea";
            this.callExternalMethodActivity1.ParameterBindings.Add(workflowparameterbinding2);
            // 
            // handleExternalEventActivity1
            // 
            this.handleExternalEventActivity1.EventName = "MakeTea";
            this.handleExternalEventActivity1.InterfaceType = typeof(VendingMachine.IMakeTea);
            this.handleExternalEventActivity1.Name = "handleExternalEventActivity1";
            // 
            // stateInitializationActivity3
            // 
            this.stateInitializationActivity3.Activities.Add(this.callExternalMethodActivity7);
            this.stateInitializationActivity3.Activities.Add(this.callExternalMethodActivity8);
            this.stateInitializationActivity3.Activities.Add(this.setStateActivity3);
            this.stateInitializationActivity3.Name = "stateInitializationActivity3";
            // 
            // stateInitializationActivity2
            // 
            this.stateInitializationActivity2.Activities.Add(this.callExternalMethodActivity2);
            this.stateInitializationActivity2.Activities.Add(this.callExternalMethodActivity3);
            this.stateInitializationActivity2.Activities.Add(this.callExternalMethodActivity4);
            this.stateInitializationActivity2.Activities.Add(this.callExternalMethodActivity5);
            this.stateInitializationActivity2.Activities.Add(this.callExternalMethodActivity6);
            this.stateInitializationActivity2.Activities.Add(this.setStateActivity2);
            this.stateInitializationActivity2.Name = "stateInitializationActivity2";
            // 
            // stateInitializationActivity1
            // 
            this.stateInitializationActivity1.Name = "stateInitializationActivity1";
            // 
            // makeTea
            // 
            this.makeTea.Activities.Add(this.handleExternalEventActivity1);
            this.makeTea.Activities.Add(this.callExternalMethodActivity1);
            this.makeTea.Activities.Add(this.setStateActivity1);
            this.makeTea.Name = "makeTea";
            // 
            // dispensingStateActivity
            // 
            this.dispensingStateActivity.Activities.Add(this.stateInitializationActivity3);
            this.dispensingStateActivity.Name = "dispensingStateActivity";
            // 
            // preparingTeaStateActivity
            // 
            this.preparingTeaStateActivity.Activities.Add(this.stateInitializationActivity2);
            this.preparingTeaStateActivity.Name = "preparingTeaStateActivity";
            // 
            // readyStateActivity
            // 
            this.readyStateActivity.Activities.Add(this.makeTea);
            this.readyStateActivity.Activities.Add(this.stateInitializationActivity1);
            this.readyStateActivity.Name = "readyStateActivity";
            // 
            // Workflow1
            // 
            this.Activities.Add(this.readyStateActivity);
            this.Activities.Add(this.preparingTeaStateActivity);
            this.Activities.Add(this.dispensingStateActivity);
            this.CompletedStateName = null;
            this.DynamicUpdateCondition = null;
            this.InitialStateName = "readyStateActivity";
            this.Name = "Workflow1";
            this.CanModifyActivities = false;

        }

        #endregion

        private HandleExternalEventActivity handleExternalEventActivity1;

        private EventDrivenActivity makeTea;

        private StateActivity dispensingStateActivity;

        private StateActivity preparingTeaStateActivity;

        private SetStateActivity setStateActivity3;

        private CallExternalMethodActivity callExternalMethodActivity8;

        private CallExternalMethodActivity callExternalMethodActivity7;

        private SetStateActivity setStateActivity2;

        private CallExternalMethodActivity callExternalMethodActivity6;

        private CallExternalMethodActivity callExternalMethodActivity5;

        private CallExternalMethodActivity callExternalMethodActivity4;

        private CallExternalMethodActivity callExternalMethodActivity3;

        private CallExternalMethodActivity callExternalMethodActivity2;

        private SetStateActivity setStateActivity1;

        private CallExternalMethodActivity callExternalMethodActivity1;

        private StateInitializationActivity stateInitializationActivity3;

        private StateInitializationActivity stateInitializationActivity2;

        private StateInitializationActivity stateInitializationActivity1;

        private StateActivity readyStateActivity;




    }
}
