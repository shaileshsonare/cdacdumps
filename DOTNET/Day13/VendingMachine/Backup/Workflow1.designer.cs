﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace VendingMachine
{
    partial class VendingMachineWorkflow
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding1 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding2 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            System.Workflow.ComponentModel.WorkflowParameterBinding workflowparameterbinding3 = new System.Workflow.ComponentModel.WorkflowParameterBinding();
            this.setStateActivity3 = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity7 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity6 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.setStateActivity2 = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity8 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity5 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity4 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity3 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity2 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.callExternalMethodActivity9 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.setStateActivity1 = new System.Workflow.Activities.SetStateActivity();
            this.callExternalMethodActivity1 = new System.Workflow.Activities.CallExternalMethodActivity();
            this.handleExternalEventActivity1 = new System.Workflow.Activities.HandleExternalEventActivity();
            this.stateInitializationActivity2 = new System.Workflow.Activities.StateInitializationActivity();
            this.stateInitializationActivity1 = new System.Workflow.Activities.StateInitializationActivity();
            this.stateInitializationActivity3 = new System.Workflow.Activities.StateInitializationActivity();
            this.makeTea = new System.Workflow.Activities.EventDrivenActivity();
            this.DispensingState = new System.Workflow.Activities.StateActivity();
            this.PreparingTeaState = new System.Workflow.Activities.StateActivity();
            this.ReadyState = new System.Workflow.Activities.StateActivity();
            // 
            // setStateActivity3
            // 
            this.setStateActivity3.Name = "setStateActivity3";
            this.setStateActivity3.TargetStateName = "ReadyState";
            // 
            // callExternalMethodActivity7
            // 
            this.callExternalMethodActivity7.InterfaceType = typeof(VendingMachine.IHotWaterController);
            this.callExternalMethodActivity7.MethodName = "CloseValve";
            this.callExternalMethodActivity7.Name = "callExternalMethodActivity7";
            // 
            // callExternalMethodActivity6
            // 
            this.callExternalMethodActivity6.InterfaceType = typeof(VendingMachine.IMixerController);
            this.callExternalMethodActivity6.MethodName = "Stop";
            this.callExternalMethodActivity6.Name = "callExternalMethodActivity6";
            // 
            // setStateActivity2
            // 
            this.setStateActivity2.Name = "setStateActivity2";
            this.setStateActivity2.TargetStateName = "DispensingState";
            // 
            // callExternalMethodActivity8
            // 
            this.callExternalMethodActivity8.InterfaceType = typeof(VendingMachine.IStateController);
            this.callExternalMethodActivity8.MethodName = "UpdateStatus";
            this.callExternalMethodActivity8.Name = "callExternalMethodActivity8";
            workflowparameterbinding1.ParameterName = "strate";
            workflowparameterbinding1.Value = "Dispensing";
            this.callExternalMethodActivity8.ParameterBindings.Add(workflowparameterbinding1);
            // 
            // callExternalMethodActivity5
            // 
            this.callExternalMethodActivity5.InterfaceType = typeof(VendingMachine.ITeaContainerController);
            this.callExternalMethodActivity5.MethodName = "CloseValve";
            this.callExternalMethodActivity5.Name = "callExternalMethodActivity5";
            // 
            // callExternalMethodActivity4
            // 
            this.callExternalMethodActivity4.InterfaceType = typeof(VendingMachine.IMixerController);
            this.callExternalMethodActivity4.MethodName = "Start";
            this.callExternalMethodActivity4.Name = "callExternalMethodActivity4";
            // 
            // callExternalMethodActivity3
            // 
            this.callExternalMethodActivity3.InterfaceType = typeof(VendingMachine.IHotWaterController);
            this.callExternalMethodActivity3.MethodName = "OpenValve";
            this.callExternalMethodActivity3.Name = "callExternalMethodActivity3";
            // 
            // callExternalMethodActivity2
            // 
            this.callExternalMethodActivity2.InterfaceType = typeof(VendingMachine.ITeaContainerController);
            this.callExternalMethodActivity2.MethodName = "OpenValve";
            this.callExternalMethodActivity2.Name = "callExternalMethodActivity2";
            // 
            // callExternalMethodActivity9
            // 
            this.callExternalMethodActivity9.InterfaceType = typeof(VendingMachine.IStateController);
            this.callExternalMethodActivity9.MethodName = "UpdateStatus";
            this.callExternalMethodActivity9.Name = "callExternalMethodActivity9";
            workflowparameterbinding2.ParameterName = "strate";
            workflowparameterbinding2.Value = "Ready";
            this.callExternalMethodActivity9.ParameterBindings.Add(workflowparameterbinding2);
            // 
            // setStateActivity1
            // 
            this.setStateActivity1.Name = "setStateActivity1";
            this.setStateActivity1.TargetStateName = "PreparingTeaState";
            // 
            // callExternalMethodActivity1
            // 
            this.callExternalMethodActivity1.InterfaceType = typeof(VendingMachine.IStateController);
            this.callExternalMethodActivity1.MethodName = "UpdateStatus";
            this.callExternalMethodActivity1.Name = "callExternalMethodActivity1";
            workflowparameterbinding3.ParameterName = "strate";
            workflowparameterbinding3.Value = "Preparing Tea";
            this.callExternalMethodActivity1.ParameterBindings.Add(workflowparameterbinding3);
            // 
            // handleExternalEventActivity1
            // 
            this.handleExternalEventActivity1.EventName = "MakeTea";
            this.handleExternalEventActivity1.InterfaceType = typeof(VendingMachine.IMakeTea);
            this.handleExternalEventActivity1.Name = "handleExternalEventActivity1";
            // 
            // stateInitializationActivity2
            // 
            this.stateInitializationActivity2.Activities.Add(this.callExternalMethodActivity6);
            this.stateInitializationActivity2.Activities.Add(this.callExternalMethodActivity7);
            this.stateInitializationActivity2.Activities.Add(this.setStateActivity3);
            this.stateInitializationActivity2.Name = "stateInitializationActivity2";
            // 
            // stateInitializationActivity1
            // 
            this.stateInitializationActivity1.Activities.Add(this.callExternalMethodActivity2);
            this.stateInitializationActivity1.Activities.Add(this.callExternalMethodActivity3);
            this.stateInitializationActivity1.Activities.Add(this.callExternalMethodActivity4);
            this.stateInitializationActivity1.Activities.Add(this.callExternalMethodActivity5);
            this.stateInitializationActivity1.Activities.Add(this.callExternalMethodActivity8);
            this.stateInitializationActivity1.Activities.Add(this.setStateActivity2);
            this.stateInitializationActivity1.Name = "stateInitializationActivity1";
            // 
            // stateInitializationActivity3
            // 
            this.stateInitializationActivity3.Activities.Add(this.callExternalMethodActivity9);
            this.stateInitializationActivity3.Name = "stateInitializationActivity3";
            // 
            // makeTea
            // 
            this.makeTea.Activities.Add(this.handleExternalEventActivity1);
            this.makeTea.Activities.Add(this.callExternalMethodActivity1);
            this.makeTea.Activities.Add(this.setStateActivity1);
            this.makeTea.Name = "makeTea";
            // 
            // DispensingState
            // 
            this.DispensingState.Activities.Add(this.stateInitializationActivity2);
            this.DispensingState.Name = "DispensingState";
            // 
            // PreparingTeaState
            // 
            this.PreparingTeaState.Activities.Add(this.stateInitializationActivity1);
            this.PreparingTeaState.Name = "PreparingTeaState";
            // 
            // ReadyState
            // 
            this.ReadyState.Activities.Add(this.makeTea);
            this.ReadyState.Activities.Add(this.stateInitializationActivity3);
            this.ReadyState.Name = "ReadyState";
            // 
            // VendingMachineWorkflow
            // 
            this.Activities.Add(this.ReadyState);
            this.Activities.Add(this.PreparingTeaState);
            this.Activities.Add(this.DispensingState);
            this.CompletedStateName = null;
            this.DynamicUpdateCondition = null;
            this.InitialStateName = "ReadyState";
            this.Name = "VendingMachineWorkflow";
            this.CanModifyActivities = false;

        }

        #endregion

        private EventDrivenActivity makeTea;
        private SetStateActivity setStateActivity1;
        private HandleExternalEventActivity handleExternalEventActivity1;
        private StateActivity PreparingTeaState;
        private CallExternalMethodActivity callExternalMethodActivity1;
        private CallExternalMethodActivity callExternalMethodActivity2;
        private StateInitializationActivity stateInitializationActivity1;
        private CallExternalMethodActivity callExternalMethodActivity5;
        private CallExternalMethodActivity callExternalMethodActivity4;
        private CallExternalMethodActivity callExternalMethodActivity3;
        private SetStateActivity setStateActivity2;
        private CallExternalMethodActivity callExternalMethodActivity8;
        private StateActivity DispensingState;
        private CallExternalMethodActivity callExternalMethodActivity7;
        private CallExternalMethodActivity callExternalMethodActivity6;
        private StateInitializationActivity stateInitializationActivity2;
        private SetStateActivity setStateActivity3;
        private CallExternalMethodActivity callExternalMethodActivity9;
        private StateInitializationActivity stateInitializationActivity3;
        private StateActivity ReadyState;



































    }
}
