﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;
using MyMath = IntegerMath.Math;

namespace ConsoleApplication
{
    class Program
    {
        static void Main()
        {
            MyMath m = new MyMath();
            int n = m.Add(2, 3);
            Console.WriteLine(n);
            Console.ReadKey();
        }
    }
}