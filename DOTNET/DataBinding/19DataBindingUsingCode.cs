﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace Language_Reference.ADO.NET
{
    public partial class DataBindingForm : Form
    {
        private DataSet biblioDS;
        private String connectionString;
        private DataViewManager biblioDSView;

        private SqlConnection connection;
        private SqlDataAdapter publishersDataAdapter;

        private SqlDataAdapter authorsDataAdapter;
        public DataBindingForm()
        {
            //Create Components
            InitializeComponent();

            // Setup DB-Connection
            connectionString = ConfigurationManager.ConnectionStrings["Biblio"].ConnectionString;
            connection = new SqlConnection(connectionString);

            // Create the DataSet
            biblioDS = new DataSet("Biblio");

            // Fill the Dataset with Publishers
            publishersDataAdapter = new SqlDataAdapter("SELECT * FROM Publishers",
                                                                       connection);

            publishersDataAdapter.Fill(biblioDS, "Publishers");

            DataColumn[] primaryKey = new DataColumn[1];
            primaryKey[0] = biblioDS.Tables["Publishers"].Columns["PubID"];
            biblioDS.Tables["Publishers"].PrimaryKey = primaryKey;

            SqlCommandBuilder publishersCommandBuilder = 
                            new SqlCommandBuilder(publishersDataAdapter);
            //publishersDataAdapter.UpdateCommand = publishersCommandBuilder.GetUpdateCommand();

            biblioDSView = biblioDS.DefaultViewManager;
            
            //biblioDSView.DataViewSettings["Publishers"].RowFilter =
            //                                                "PubID < 10";
            //DataView dv = biblioDSView.CreateDataView(biblioDS.Tables[0]);
            //publishersComboBox.DataSource = dv;
            //publishersComboBox.DisplayMember = "Name";
            //publishersComboBox.ValueMember = "PubID";

            publishersComboBox.DataSource = biblioDSView;
            publishersComboBox.DisplayMember = "Publishers.Name";
            publishersComboBox.ValueMember = "Publishers.PubID";

            companyNameTextBox.DataBindings.Add("Text", biblioDSView,
                                                    "Publishers.Company Name");

            addressTextBox.DataBindings.Add("Text", biblioDSView,
                                                    "Publishers.Address");
            
            cityTextBox.DataBindings.Add("Text", biblioDSView,
                                                    "Publishers.City");

            stateTextBox.DataBindings.Add("Text", biblioDSView,
                                                    "Publishers.State");

            zipTextBox.DataBindings.Add("Text", biblioDSView,
                                                    "Publishers.Zip");

            telephoneTextBox.DataBindings.Add("Text", biblioDSView,
                                                    "Publishers.Telephone");

            faxTextBox.DataBindings.Add("Text", biblioDSView, "Publishers.Fax");

            commentsTextBox.DataBindings.Add("Text", biblioDSView,
                                                    "Publishers.Comments");

            publishersDataAdapter.RowUpdated += new SqlRowUpdatedEventHandler(publishersDataAdapter_RowUpdated);
        }

        void publishersDataAdapter_RowUpdated(object sender, SqlRowUpdatedEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {
                DataColumn[] pk = e.Row.Table.PrimaryKey;
                if (pk != null && pk.Length > 0)
                {
                    DataColumn primaryKeyColumn = pk[0];

                    SqlCommand cmd = new SqlCommand(
                        "SELECT @@IDENTITY", connection); 

                    int id = Convert.ToInt32(cmd.ExecuteScalar());
                   
                    e.Row[primaryKeyColumn] = id;
                }
            }
        }

        private void firstButton_Click(object sender, EventArgs e)
        {
            this.BindingContext[biblioDSView, "Publishers"].Position = 0;
        }

        private void previousButton_Click(object sender, EventArgs e)
        {
            if (this.BindingContext[biblioDSView, "Publishers"].Position > 0)
            {
                this.BindingContext[biblioDSView, "Publishers"].Position--;
            }
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext
                [biblioDSView, "Publishers"];
            if (cm.Position < cm.Count)
            {
                cm.Position++;
            }
         }

        private void lastButton_Click(object sender, EventArgs e)
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext
                [biblioDSView, "Publishers"];
            cm.Position = cm.Count - 1;
        }

        private void savePublisherButton_Click(object sender, EventArgs e)
        {
            CurrencyManager cm = this.BindingContext[biblioDSView, "Publishers"] 
                            as CurrencyManager;
            (cm.Current as DataRowView).Row["Name"] = publishersComboBox.Text;
            cm.EndCurrentEdit();
            publishersDataAdapter.Update(biblioDS, "Publishers");
        }

        private void addPublisherButton_Click(object sender, EventArgs e)
        {
            CurrencyManager cm = this.BindingContext[biblioDSView, "Publishers"] as 
                                                                    CurrencyManager;
            
            cm.AddNew();
            (cm.Current as DataRowView).Row["PubID"] = -1;
        }

        private void deletePublisherButton_Click(object sender, EventArgs e)
        {
            CurrencyManager cm = this.BindingContext[biblioDSView, "Publishers"] as CurrencyManager;
            cm.EndCurrentEdit();
            try
            {
                ((DataRowView)cm.Current).Delete();
                publishersDataAdapter.Update(biblioDS, "Publishers");
            }
            catch (DBConcurrencyException ex)
            {
                ex.Row.AcceptChanges();
            }
        }

        private void cancelPublisherButton_Click(object sender, EventArgs e)
        {
            CurrencyManager cm = this.BindingContext[biblioDSView, "Publishers"] as CurrencyManager;
            cm.CancelCurrentEdit();
        }
    }
}
