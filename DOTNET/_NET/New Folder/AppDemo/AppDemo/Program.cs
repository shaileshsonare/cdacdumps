﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using calc;
using System.Reflection;

namespace AppDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Assembly asm = Assembly.LoadFrom(@"F:\CDAC\. NET\programs\calc\calc\bin\Debug\calc.dll");
            Type[] types = asm.GetTypes();
            Console.WriteLine("The types in assembly are:");
            Console.WriteLine("-===================================");
            foreach (Type t in types)
            {
                Console.WriteLine(t.Name);
                MethodInfo[] methods = t.GetMethods();
                Console.WriteLine("The methods in class {0} are:", t.Name);
                Console.WriteLine("-===================================");


                foreach (MethodInfo m in methods)
                {
                    Console.WriteLine(m.Name);
                }



                Console.WriteLine("enter the method u want to call");
                MethodInfo method = t.GetMethod(Console.ReadLine());
                object obj = asm.CreateInstance(t.FullName);
                object res = t.InvokeMember(method.Name, BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance | BindingFlags.InvokeMethod, null, obj, null);
                Console.WriteLine("result=" + res);
            }

            Console.ReadLine();
        }
    }
}
