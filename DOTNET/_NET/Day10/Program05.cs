using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Language_Reference
{
    class Program
    {
        static Mutex mtx;

        static void threadProc()
        {
            mtx.WaitOne(Timeout.Infinite, true);
            for (int i = 100; i < 170; i++)
            {
                Thread.Sleep(100);
                Console.WriteLine("Thread1:number={0}", i);
            }
            mtx.ReleaseMutex();
        }

        static void Main(string[] args)
        {
            Thread thread1;

            mtx = new Mutex(false, "MyMutex");
            ThreadStart entrypoint = new ThreadStart(threadProc);
            thread1 = new Thread(entrypoint);
            thread1.Name = "thread1";

            thread1.Start();
        }
    }
}