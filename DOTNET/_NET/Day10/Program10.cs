using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

//Multi Module Second File
// csc /out:x.netmodule /t:module Program09.cs

// csc /addmodule:x.netmodule /out:main.netmodule 
//     /t:module program10.cs

// al x.netmodule main.netmodule 
//      main:ConsoleApplication.Program.Main
//      /out:multimodule.exe /t:exe

namespace ConsoleApplication
{
    class Program
    {
        public static void Main(string[] args)
        {
            X x = new X();
            x.M();
        }
    }
}