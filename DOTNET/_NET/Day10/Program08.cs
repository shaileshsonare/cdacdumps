using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

//Single Module
//csc /out:singlemodule.exe program08.cs

namespace ConsoleApplication
{
    class X
    {
        public void M()
        {
            Console.WriteLine("From X.M");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            X x = new X();
            x.M();
        }
    }
}