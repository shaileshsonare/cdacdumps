﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ConsoleApplication
{
    abstract class Persist
    {
        public abstract void Load();
        public abstract void Save();
    }

    class Button : Persist
    {
        public virtual void Draw()
        {
            Console.WriteLine("From Button.Draw");
        }

        public override void Load()
        {
            Console.WriteLine("Obect loaded");
        }

        public override void Save()
        {
            Console.WriteLine("object saved");
        }
    }

    class ImageButton : Button
    {
        public override void Draw()
        {
            base.Draw();
            Console.WriteLine("From ImageButton.DrawImage");
        }
    }

    class Program
    {
        static void Main()
        {
            ImageButton ib = new ImageButton();
            ib.Draw();

            Button b = ib;
            b.Draw();
            b.Save();
        }
    }
}
