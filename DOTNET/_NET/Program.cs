﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ConsoleApplication
{
    class Button
    {
        public void Draw()
        {
            Console.WriteLine("From Button.Draw");
        }
    }

    class ImageButton : Button
    {
    }

    class Program
    {
        static void Main()
        {
            ImageButton ib = new ImageButton();
            ib.Draw();
        }
    }
}
