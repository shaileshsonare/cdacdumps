﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ConsoleApplication
{
    interface IPersist
    {
        void Load();
        void Save();
    }

    class Slide : IPersist
    {

        public void Load()
        {
            Console.WriteLine("Slide Loaded");
        }

        public void Save()
        {
            Console.WriteLine("Slide Saved");
        }
    }

    class Program
    {
        static void Main()
        {
            IPersist persist = new Slide();
            persist.Load();
            persist.Save();
        }
    }
}
