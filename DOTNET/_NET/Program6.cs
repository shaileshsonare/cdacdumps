﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

//Sealed class
namespace ConsoleApplication
{
    class Base
    {
    }

    sealed class Derived : Base
    {
    }

    class MoreDerived : Derived
    {
    }

    class Program
    {
        static void Main()
        {
        }
    }
}
