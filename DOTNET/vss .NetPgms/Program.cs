﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace outparameter
{
    class Program
    {
        static void funout(out int i, out int j)
        {
            i = 8;
            j = 9;
            Console.WriteLine("x={0}   y={1}", i, j);
            i = i - j; // a=10 ,b=80 
            j = i + j; //b=90 a=10
            i = j - i;
            Console.WriteLine("x={0}   y={1}", i, j);
        }

    static void funref(ref int x, ref int y) //needs to be init

      {
          
          Console.WriteLine("in Fun Ref");
          Console.WriteLine("x={0}   y={1}", x, y);
          Console.WriteLine("swap");
          int t = x;  
          x = y;
          y = t;
          Console.WriteLine("x={0}   y={1}", x, y);

          
      }
        static void Main(string[] args)
        {

            int i = 90, j = 80;
            Console.WriteLine("i={0}   j={1}", i, j);
            funref(ref i, ref j);
            Console.WriteLine("i={0}   j={1}", i, j);

            Console.WriteLine("OUT PARAMETER");
            int x, y;
            x = 66;
            y = 90;

            funout(out x, out y);


            Console.ReadKey();

        }
    }
}
