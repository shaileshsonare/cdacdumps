﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace outparameter
{
    class Complex
    {
        int r,i;
        public Complex()
        {
            this.r = 1;
            this.i = 1;
        }
       public void display()
       {
           Console.WriteLine(r + "  " + i);
       }

       public static Complex operator +(Complex c1, Complex c2)
       {
           Complex t = new Complex();
           t.r = c1.r + c2.r;
           t.i = c1.i + c2.i;
           return t;
       }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Complex c1 = new Complex();
            Complex c2 = new Complex();
            Complex c3;
            c3 = c1 + c2;
            c1.display();
            c2.display();
            c3.display();
           
        }
    }
}
