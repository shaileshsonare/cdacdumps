using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TypeCheck
{
    class User
    {
        string password;
        float pi;
        
        public User()
        {
            pi = 3.14f;
        }
       
        public string Password
        {
            set
            {
                password = value;
            }
            private get
            {
                return password;
            }
        }
        public float PI
        {
            get
            {
                return pi;
            }
        }
       


        public string getPassword()
        {
           return password;
        }
       
    }
    class Program
    {
        static void Main(string[] args)
        {
            User u = new User();
            u.Password="24";
            Console.WriteLine(u.getPassword());
            Console.WriteLine(u.PI);   
        }
    }
}
