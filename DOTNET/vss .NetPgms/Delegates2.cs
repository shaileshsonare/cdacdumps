﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace outparameter
{
    class DataSource 
    {
        public delegate void NotifyDeligate(int x); //creating data type for function pointer
        public NotifyDeligate update;               //creating function pointer variable for storing address of function

        int data;
        public DataSource()
        {
            data = 1;
        }
        public DataSource(int x)
        {
            data = x;
        }
        public void Display()
        {
            Console.WriteLine("Datasource Data = "+data);
        }
        public void Change(int value)
        {
            data = value;
            if (update != null)
                update(data);       //calling the Notify function using function pointer means deligate
        }
    }

    class DataConsumer1
    {
        DataSource ds;
        public DataConsumer1(DataSource ds)
        {
            this.ds = ds;
            ds.update += Notify;
        }

        public void Notify(int data)
        {
            Console.WriteLine("Value of Notify {0}", data);
        }

        public void Modify(int value)
        {
            ds.Change(value);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            DataSource ds = new DataSource();
            ds.Display();
            ds.Change(5);
            ds.Display();

            DataConsumer1 dc1 = new DataConsumer1(ds);
            dc1.Modify(25);
            ds.Display();
        }
    }
}
