﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ConsoleApplication
{
    class Real
    {
        public Real(int n)
        {
            this.n = n;
        }

        int n;
    }
    class Complex
    {
        int r;
        int i;

        public Complex(int r, int i)
        {
            this.r = r;
            this.i = i;
        }

        public static Complex operator +(Complex c1, Complex c2)
        {
            Complex t = new Complex(0, 0);
            t.r = c1.r + c2.r;
            t.i = c1.i + c2.i;
            return t;
        }

        public static explicit operator Real(Complex c)
        {
            return new Real(c.r);
        }
    }

    class Program
    {
        static void Main()
        {
            Complex c1 = new Complex(1, 1), c2 = new Complex(2, 2), c3 = new Complex(0, 0);
            c3 = c1 + c2;
            Real r = new Real(0);

            r = (Real) c3 ;
        }
    }
}
