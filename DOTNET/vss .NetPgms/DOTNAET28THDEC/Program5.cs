﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ConsoleApplication
{
    class DataSource
    {
        public delegate void NotifyDelegate(int data);

        private NotifyDelegate _update;
        public event NotifyDelegate Update
        {
            add
            {
                _update += new NotifyDelegate(value);
            }

            remove
            {
                _update -= new NotifyDelegate(value);
            }

        }

        public DataSource() { data = 1; }

        public void Change(int value)
        {
            data = value;
            if (_update != null)
                _update(data);
        }
        int data;
    }

    class DataConsumer1
    {
        public DataConsumer1(DataSource datasource)
        {
            ds = datasource;
            ds.Update += new DataSource.NotifyDelegate(ds_Update);
        }

        void ds_Update(int data)
        {
            Console.WriteLine("DataConsumern1:{0}", data);
        }

        public void Modify(int v)
        {
            ds.Change(v);
        }

        DataSource ds;
    }

    class DataConsumer2
    {
        public DataConsumer2(DataSource datasource)
        {
            datasource.Update += new DataSource.NotifyDelegate(datasource_Update);
        }

        void datasource_Update(int value)
        {
            Console.WriteLine("DataConsumern2:{0}", value);
        }

    }

    class Program
    {
        static void Main()
        {
            DataSource ds = new DataSource();
            DataConsumer2 consumer2 = new DataConsumer2(ds);
            DataConsumer1 consumer1 = new DataConsumer1(ds);
            consumer1.Modify(2);
        }
    }
}
