﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication4
{
    class Base
    {
        private int y;
        protected int x;
        public int z;
        public Base()
        {
            x = y = z = 99;
        }
        public void fun1()
        {
            Console.WriteLine("I am in fun1...."+x+y+z);
        }
    }

    class Derived : Base
    {
        new public void fun1()
        {
            Console.WriteLine("I am in fun1"+x+z);
        }
    }

    class DirivedDerived : Derived
    {
        public DirivedDerived()
        {
            Console.WriteLine(""+x+z);
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Derived d = new Derived();
            Base b = d;
            d.fun1();
            b.fun1();

            DirivedDerived dd = new DirivedDerived();
            dd.fun1();
        } 
    }
}
