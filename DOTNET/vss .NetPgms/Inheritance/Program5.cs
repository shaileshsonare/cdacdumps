﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication4
{

    interface IInterface
    {
        void function();
    }

    class Test:IInterface
    {
        public void function()
        {
            Console.WriteLine("My India is Great");
        }
    }

    class Demo : IInterface
    {
        public void function()
        {
            Console.WriteLine("I am in demo");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Test t = new Test();
            Demo d = new Demo();
            IInterface i;
            i = t;
            i.function();
            i = d;
            i.function();
        }
    }
}
