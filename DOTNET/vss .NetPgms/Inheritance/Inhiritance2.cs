﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication4
{
    class Base
    {
        public Base()
        {
            Console.WriteLine("Base Constructor....");
        }

        public void Display()
        {
            Console.WriteLine("I m in Base display()");
        }
    }

    class Derived : Base
    {
        public Derived()
        {
            Console.WriteLine("Derived Constructor....");
        }
         new public void Display()
        {
            Console.WriteLine("I m in Derived display()");
        }
    }

    
    
    class Program
    {
        static void Main(string[] args)
        {
            Base b = new Base();  
            Derived d = new Derived();
        }
    }
}
