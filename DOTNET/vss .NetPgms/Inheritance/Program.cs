﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication4
{
    class Base
    {
        int x;
        public Base()
        {
            x = 10;
        }
        public virtual void fun1()
        {
            Console.WriteLine(x);
        }
    }

    class Derived : Base
    {
        int y;
        
        public override void fun1()
        {
            base.fun1();
            //fun1();
            Console.WriteLine(y);
        }
    }

    
    
    class Program
    {
        static void Main(string[] args)
        {
         //   Console.WriteLine("I m sorry "+(100+40+3));

            Base b = new Base();
            Derived d = new Derived();

            b.fun1();
            d.fun1();
            b.fun1();

            

        }
    }
}
