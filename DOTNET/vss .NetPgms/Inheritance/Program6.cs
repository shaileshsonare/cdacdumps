﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication4
{

    interface IInterface1
    {
        void function();
    }

    interface IInterface2
    {
        void function();
    }
    class Demo
    {
        public Demo()
        {
            Console.WriteLine("I am in demo constructor");
        }
        public void fun1()
        {
            Console.WriteLine("kyu fullon ke ude ude rang khil gaye......");
        }
    }
    class Test:Demo,IInterface1,IInterface2
    {
        void IInterface1.function()
        {
            Console.WriteLine("Hello WOrld");
        }

        void IInterface2.function()
        {
            Console.WriteLine("Bye World...");
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Test t = new Test();

            IInterface1 ii1 = t;
            ii1.function();

            IInterface2 ii2 = t;
            ii2.function();
            t.fun1();
        }
    }
}
