﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace outparameter
{
    class DataSource
    {
        public delegate void NotifyDelegate(int data);
        public NotifyDelegate Update;
        int data;
        public DataSource()
        {
            data = 1;
         }
        public void change(int data)
        {
            this.data = data;
            if (Update != null)
            Update(data);
        }
    }
   
    class DataConsumer1
    {
        DataSource ds;
        
        public DataConsumer1(DataSource ds)
        {
            this.ds=ds;
            this.ds.Update += (int data) =>
            {
                Console.WriteLine(data);
            }
            ;
        }

        //public void Notify(int x)
        //{
        //    Console.WriteLine("Notified by Data Source:Consumer 1::"+x);
        //}

        public void Modify(int v)
        {
            ds.change(v);
        }
    }
    class DataConsumer2
    {
       
        public DataConsumer2(DataSource ds)
        {
            ds.Update += (int x)  =>
            {
                Console.WriteLine(x);
            }
            ;
        }
        //public void Notify111(int x)
        //{
        //    Console.WriteLine("Notify by Data Source :Consumer 2::"+x);
        //}

    }

      class Program
    {
        static void Main(string[] args)
        {
            DataSource ds = new DataSource();
            DataConsumer2 d2 = new DataConsumer2(ds);
            DataConsumer1 d1 = new DataConsumer1(ds);
            d1.Modify(20);
           
        }
    }
}
