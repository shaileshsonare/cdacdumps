using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TypeCheck
{
    class InvalidAgeException : Exception
    {
        public InvalidAgeException() : base("INvalid age...."){}
    }
    class Person
    {
        private int age;
        public int Age
        {
        //    set;
        //    get;
        //}
            set
            {
                if (value < 0) throw new InvalidAgeException();
                age = value;
               
            }
            get
            {
                return age;
            }
        }

        public void display()
        {
            Console.WriteLine(age);
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person();
            p.Age = 22;
            p.display();
            Console.WriteLine(p.Age);
            p.Age = -11;
            Console.WriteLine(p.Age);
        }
    }
}
