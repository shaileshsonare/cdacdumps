﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication4
{
 
    class Program
    {
        public int Add(int n1=1,int n2=0,int n3=0)
        {
            int sum = 0;
            sum = n1+n2+n3;
            return sum;
        }
        

        static void Main(string[] args)
        {
            Console.WriteLine(new Program().Add());
            Console.WriteLine(new Program().Add(1));
            Console.WriteLine(new Program().Add(1, 2));
           Console.WriteLine(new Program().Add(1, 2, 3));
        }
    }
}