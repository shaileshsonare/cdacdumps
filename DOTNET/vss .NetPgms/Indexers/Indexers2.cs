﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace outparameter
{
    class Number
    {
        int[,] arr;

        public Number(int r)
        {
            arr = new int[r,r];
            int cnt = 1;
            for (int i = 0; i < r; i++)
            {
                for (int j = 0; j < r; j++)
                {
                    arr[i, j] = cnt++;
                }
            }
        }

        public int this[int r,int c]
        {
            set
            {
                arr[r,c] = value;
            }
            get
            {
                return arr[r,c];
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Number n1 = new Number(2);
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Console.Write(n1[i, j]+" ");                   
                }
                Console.WriteLine();
            }
            
        }
    }
}
