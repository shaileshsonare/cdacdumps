package slsb_dao_impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import rev_entities.MoviePOJO;
import slsb_dao.MovieDAO;

@Stateless(mappedName="movie_tester")
public class MovieDAOImpl implements MovieDAO {
	
	//unit name --- persistence unit name as it appears in 
	//persistence.xml
	@PersistenceContext(unitName="new_pu")
	private EntityManager em;


	@SuppressWarnings("unchecked")
	@Override
	
	//how to tell EJBC to invoke follo method in Tx scope?
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<MoviePOJO> getMovies() {
		
		return em.createQuery("select m from MoviePOJO m").getResultList();
	} // persitence ctx is closed auto by EJBC

}
