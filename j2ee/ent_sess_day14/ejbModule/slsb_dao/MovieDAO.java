package slsb_dao;

import java.util.List;

import javax.ejb.Remote;

import rev_entities.MoviePOJO;

@Remote
public interface MovieDAO {
	List<MoviePOJO> getMovies();

}
