package dao;

import java.sql.*;
import java.util.ArrayList;

import java.io.Serializable;

import pojos.BillPOJO;

public class BillDAO implements Serializable {
	// D.M
	private Connection cn;
	private PreparedStatement pst1, pst2, pst3;

	// constr
	public BillDAO() throws Exception {
		// load drvr cls
		Class.forName("com.mysql.jdbc.Driver");
		// get cn --- fixed
		cn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/testjdbc", "root", "root");
		// pst
		String sql = "select mob_no from billing";
		pst1 = cn.prepareStatement(sql);
		sql = "select * from billing where mob_no = ?";
		pst2 = cn.prepareStatement(sql);
		sql = "update billing set plan=? , call_duration=? , total_bill=? where mob_no=?";
		pst3 = cn.prepareStatement(sql);
		System.out.println("init dao succeeded......");
	}

	public void cleanup() throws Exception {
		if (pst1 != null)
			pst1.close();
		if (pst2 != null)
			pst2.close();
		if (pst3 != null)
			pst3.close();
		if (cn != null)
			cn.close();
	}

	public ArrayList<String> getCellNos() throws Exception {
		ArrayList<String> l1 = new ArrayList<>();
		try (ResultSet rst = pst1.executeQuery()) {
			while (rst.next())
				l1.add(rst.getString(1));
		}
		return l1;
	}

	public BillPOJO getCellDetails(String mobNo) throws Exception {
		ResultSet rst = null;
		try {
			// /set in param --- cell no
			pst2.setString(1, mobNo);
			rst = pst2.executeQuery();
			if (rst.next())
				return new BillPOJO(mobNo, rst.getString(2), rst.getString(3),
						rst.getInt(4), rst.getDouble(5));
		} finally {
			if (rst != null)
				rst.close();
		}
		return null;
	}

	public String updateCellDetails(BillPOJO b) throws Exception{
		String sts = "Failed";
		// set in params
		pst3.setString(1, b.getPlan());
		pst3.setInt(2,b.getCallDuration());
		pst3.setDouble(3, b.getBillAmt());
		pst3.setString(4,b.getMobileNO());
		int updateCnt=pst3.executeUpdate();
		if (updateCnt == 1)
			sts="success";
		return sts;
	}

}
