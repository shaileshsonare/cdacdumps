package com.app.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.app.model.Contact;
import com.app.service.ContactService;

@Controller
@RequestMapping("/contacts")
// specifies base url pattern
public class LoginController {
	@Autowired
	@Qualifier("contact_service")
	private ContactService service;

	@RequestMapping("/validate")
	public String showForm(Model m) {
		System.out.println("in show form");
		// create empty model inst & add it to req scope
		// m.addAttribute("contact", new Contact());
		m.addAttribute(new Contact());// rq.setattribute("contact",new
										// Contact());
		return "login";
	}

	@RequestMapping(value = "/validate", method = RequestMethod.POST)
	public String processForm(Contact c, BindingResult res, HttpSession hs) {
		// Contact c --- c=rq.getAttribute("contact")
		// BindingResult --- stores P.L errors --- conversion & validation
		System.out.println("in process  form " + c);
		// if P.L errs --- take user back to i/p page
		if (res.hasErrors())
			return "login";// p.l errs
		Contact validatedContact = service.validateContact(c);
		if (validatedContact != null) {
			// how to add valid contact in sess scope?
			hs.setAttribute("contact_dtls", validatedContact);
			return "user_ok";
		}
		return "user_err";//B.L error
	}

}
