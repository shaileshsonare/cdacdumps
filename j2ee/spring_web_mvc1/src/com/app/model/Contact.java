package com.app.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Contact implements Serializable {
	private String email;
	private String password;
	private String name;
	@DateTimeFormat(pattern="dd-MMM-yyyy")
	private Date regDate;
	private double subAmt;
	public Contact() {
		// TODO Auto-generated constructor stub
	}
	public Contact(String email, String password, String name, Date regDate,
			double subAmt) {
		super();
		this.email = email;
		this.password = password;
		this.name = name;
		this.regDate = regDate;
		this.subAmt = subAmt;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getRegDate() {
		return regDate;
	}
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	public double getSubAmt() {
		return subAmt;
	}
	public void setSubAmt(double subAmt) {
		this.subAmt = subAmt;
	}
	@Override
	public String toString() {
		return "Contact [email=" + email + ", name=" + name + ", regDate="
				+ regDate + ", subAmt=" + subAmt + "]";
	}
	

}
