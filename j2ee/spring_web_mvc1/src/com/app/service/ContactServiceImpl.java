package com.app.service;

import java.text.SimpleDateFormat;
import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.app.model.Contact;

@Service("contact_service")
public class ContactServiceImpl implements ContactService {
	private HashMap<String, Contact> contacts;
	private SimpleDateFormat sdf;

	public ContactServiceImpl() {
		try {
		System.out.println("in constr " + getClass().getName());
		sdf = new SimpleDateFormat("dd-MMM-yyyy");
		contacts = new HashMap<>();
		contacts.put("a@gmail",
				new Contact("a@gmail", "1234", "a", sdf.parse("1-Jan-2013"),
						350));
		contacts.put("b@gmail",
				new Contact("b@gmail", "12345", "b", sdf.parse("10-Mar-2011"),
						350));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Contact validateContact(Contact c) {
		String em=c.getEmail();
		String pa=c.getPassword();
		if (contacts.containsKey(em))
			if (contacts.get(em).getPassword().equals(pa))
				return contacts.get(em);
		return null;
	}

}
