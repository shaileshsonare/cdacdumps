package com.app.service;

import com.app.model.Contact;

public interface ContactService {
	Contact validateContact(Contact c);

}
