package spring_tester;

import intf.HelloBeanIntf;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloBeanTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// start SC
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"spring-config1.xml");
		System.out.println("ctx loaded.....");
		//get bean ref & exec B.L
		HelloBeanIntf ref=(HelloBeanIntf) ctx.getBean("hello_bean");
		ref.show();
		
		HelloBeanIntf ref2=(HelloBeanIntf) ctx.getBean("hello_bean");
		ref2.show();
		System.out.println(ref==ref2?"same":"different");
		//to auto invoke destroy method ---close appln ctx
		((ClassPathXmlApplicationContext)ctx).close();
		

	}

}
