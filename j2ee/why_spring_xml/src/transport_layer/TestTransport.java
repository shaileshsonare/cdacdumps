package transport_layer;

import java.util.Arrays;

public class TestTransport implements Transport {

	@Override
	public void communicateWithBank(byte[] dataPkt) {
		System.out.println("sent data over Test layer "
				+ Arrays.toString(dataPkt));

	}

}
