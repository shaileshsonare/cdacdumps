package atm;

import transport_layer.TestTransport;
import transport_layer.Transport;

//dependent obj --- atmimpl
//dependency --- transport layer
//any way -- even when test--- http --- atmimpl should be un-changed
public class ATMImpl implements ATMIntf {
	private Transport transport;

	@Override
	public void withdraw(double amt) {
		StringBuilder sb = new StringBuilder("Withdrawing " + amt);
		// send this info to the bank

		transport.communicateWithBank(sb.toString().getBytes());

	}

	@Override
	public void deposit(double amt) {
		StringBuilder sb = new StringBuilder("Depositing " + amt);
		// send this info to the bank

		transport.communicateWithBank(sb.toString().getBytes());

	}

	public void setTransport(Transport transport) {
		this.transport = transport;
	}

}
