package tester;

import transport_layer.HttpTransport;
import transport_layer.SOAPtransport;
import atm.ATMImpl;
import atm.ATMIntf;

public class TestBank {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//create atm inst
		ATMIntf atm=new ATMImpl();
		//supply dependencies to the dependent obj -- wiring 
		((ATMImpl)atm).setTransport(new SOAPtransport());
		//test B.L -- w(500)
		atm.withdraw(500);
		

	}

}
