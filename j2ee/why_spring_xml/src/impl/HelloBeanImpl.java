package impl;

import intf.HelloBeanIntf;

public class HelloBeanImpl implements HelloBeanIntf {
	private String message;
	private double subAmt;
	
	
	public HelloBeanImpl(String mesage,double subAmt) {
		System.out.println("in constr hello bean");
		this.message=mesage;
		this.subAmt=subAmt;
	}
	
	public void myInit()
	{
		System.out.println("in myInit");
	}
	public void myDestroy()
	{
		System.out.println("in myDestroy");
	}


	@Override
	public void show() {
		System.out.println("from show : "+message);
		System.out.println("sub Amt "+subAmt);

	}

}
