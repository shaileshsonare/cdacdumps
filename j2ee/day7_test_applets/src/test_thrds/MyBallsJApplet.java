package test_thrds;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JApplet;
import javax.swing.JPanel;

public class MyBallsJApplet extends JApplet {
	public static final int X_START = 50;
	public static final int BALL_DIMS = 30;
	public static final int NO_OF_BALLS = 3;
	public static final int Y_DEC = 10;
	private MyPanel p1;

	Ball ball1,ball2,ball3;
	int width, height, panelHeight;
	DrawBalls d;
	//volatile --- java keyword, applicable at data member.
	//typically used in multi-threaded scenario only ---
	//when multiple thrds can access the data member.
	//Use --- to specify to the compiler -- that data var. is
	//used by multiple thrds concurrently -- so dont apply
	//any optimizations. , its guaranteed to give most recent value. i.e exit flag MUST be chked in
	//every loop iteration
	
	volatile boolean exit;

	@Override
	public void init() {
		p1 = new MyPanel();
		p1.setBackground(Color.lightGray);
		add(p1);

		width = height = BALL_DIMS;


	}

	@Override
	public void start() {
		exit = false;
		d = new DrawBalls();
		System.out.println("strt " + p1.getSize().height);
		panelHeight = p1.getSize().height;

		ball1= new Ball(X_START,100, d);
	
		BallMovement b1 = new BallMovement(ball1);
		ball2 = new Ball(X_START+150,250, d);
		
		BallMovement b2 = new BallMovement(ball2);
		ball3 = new Ball(X_START+300,400, d);
	
		BallMovement b3 = new BallMovement(ball3);
	}

	@Override
	public void stop() {

	
		d.stop();

	}

	@Override
	public void destroy() {
		d.stop();

	}

	class MyPanel extends JPanel {
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;

			g2.setColor(Color.RED);
			g2.fill(ball1.getShape());
		
			g2.setColor(Color.GREEN);
			g2.fill(ball2.getShape());
		
			g2.setColor(Color.BLUE);
			g2.fill(ball3.getShape());
		
		}
	}

	class DrawBalls {
		private int ballsReached;

		synchronized void movementComplete() throws Exception {
			ballsReached++;
			System.out.println("reachedTop " + ballsReached);
			while (ballsReached < 3) {
				System.out.println("Blocked "
						+ Thread.currentThread().getName());
				wait();
				return;
			}
			// all 3 balls reached top
			ballsReached = 0;
			notifyAll();
		}

		synchronized void stop() {
			exit = true;
			notifyAll();
		}
	}

	class Ball {
		private int x,y;
		int dly;
		DrawBalls d;

		public Ball(int x,int dly, DrawBalls d) {
			this.x=x;
			this.dly = dly;
			this.d = d;
			y = panelHeight - height;
			
		}

		public void move() throws Exception {
			
			y -= Y_DEC;
			if (y > height / 2) {
				p1.repaint();
				Thread.sleep(dly);

			} else {
				System.out.println("complete "
						+ Thread.currentThread().getName());
				d.movementComplete(); // this thrd will get blocked
				// if balls reached cnter != 3
				System.out.println("un-blocked  "
						+ Thread.currentThread().getName());
				y = panelHeight - height;
			}

		}
		public Ellipse2D getShape()
		{
			return new Ellipse2D.Double(x, y, width, height);
		}
	}

	class BallMovement extends Thread {
		private Ball b;
		private Random r1 = new Random();

		public BallMovement(Ball b) {
			this.b = b;
			start();

		}

		public void run() {
			
			
			System.out.println("in thrd strt "+ Thread.currentThread().getName());
			
			try {
				// exit flag set by applet's stop & destroy methods
				while (!exit) {
					b.move();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("thrd over " + Thread.currentThread().getName());

		}
	}

}
