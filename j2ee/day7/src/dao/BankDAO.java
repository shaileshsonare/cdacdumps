package dao;

import java.sql.*;
import java.util.ArrayList;

import java.io.Serializable;

import pojos.BankAcctPOJO;
import pojos.BankUserPOJO;

public class BankDAO implements Serializable {
	private Connection cn;
	private PreparedStatement pst, pst2;

	public BankDAO() throws Exception {
		// load drvr cls
		Class.forName("com.mysql.jdbc.Driver");
		// get cn --- fixed
		cn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/testjdbc", "root", "root");
		// pst
		String sql = "select id from bank_users where name=? and password=?";
		pst = cn.prepareStatement(sql);
		sql = "select a.ac_no,a.type,a.bal from bank_users u,bank_accounts a where u.id=? and u.id=a.id";
		pst2 = cn.prepareStatement(sql);

	}

	public void cleanup() throws Exception {
		if (pst2 != null)
			pst2.close();
		if (pst != null)
			pst.close();
		if (cn != null)
			cn.close();
	}

	public BankUserPOJO validateUser(String nm, String pass) throws Exception {
		ResultSet rst = null, rst2 = null;
		try {
			// set in
			pst.setString(1, nm);
			pst.setString(2, pass);
			rst = pst.executeQuery();
			if (rst.next()) {
				BankUserPOJO b = new BankUserPOJO(rst.getInt(1), nm, pass);
				//set IN param
				pst2.setInt(1, b.getUserId());
				// exec query to get acct info
				rst2 = pst2.executeQuery();
				// empty AL
				ArrayList<BankAcctPOJO> l1 = new ArrayList<>();
				while (rst2.next())
					l1.add(new BankAcctPOJO(rst2.getInt(1), rst2.getString(2),
							rst2.getDouble(3)));
				b.setAccts(l1);
				System.out.println("from DAO "+b +" accts "+b.getAccts());
				return b;

			}

		} finally {
			if (rst2 != null)
				rst2.close();
			if (rst != null)
				rst.close();
		}
		return null;

	}

}
