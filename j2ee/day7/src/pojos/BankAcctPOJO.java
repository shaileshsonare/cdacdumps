package pojos;

import java.io.Serializable;

public class BankAcctPOJO implements Serializable {
	//props 
	private int acctId;
	private String type;
	private double balance;
	public BankAcctPOJO() {
		// TODO Auto-generated constructor stub
	}
	public BankAcctPOJO(int acctId, String type, double balance) {
		super();
		this.acctId = acctId;
		this.type = type;
		this.balance = balance;
	}
	public int getAcctId() {
		return acctId;
	}
	public void setAcctId(int acctId) {
		this.acctId = acctId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Account Info  [acctId=" + acctId + ", type=" + type
				+ ", balance=" + balance + "]";
	}
	

}
