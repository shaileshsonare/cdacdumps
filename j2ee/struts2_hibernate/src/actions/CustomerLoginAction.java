package actions;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import rev_pojos.CustomerPOJO;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import dao.CustomerDAO;

@Namespace(value = "/hiber")
public class CustomerLoginAction extends ActionSupport implements
		ModelDriven<CustomerPOJO> {

	// d.m
	private CustomerPOJO c;
	private CustomerPOJO details;
	
	public CustomerLoginAction() {
		System.out.println("in la constr");
		 c= new CustomerPOJO();
	}

	@Override
	public CustomerPOJO getModel() {
		System.out.println("in get model");
		return c;
	}
	//pass thro action to displ login form
	@Action(value="show_login_form",results={@Result(location="login.jsp")})
	public String showForm() throws Exception
	{
		System.out.println("in show form");
		return SUCCESS;
	}

	@Override
	@Action(value = "login", results = { @Result(location = "login_ok.jsp"),
			@Result(name = "error", location = "login_err.jsp"),
			@Result(name = "input", location = "login.jsp") })
	public String execute() throws Exception {
		System.out.println("in exec "+c.getEmail()+"  "+c.getPassword());
		// invoke dao layer method --- cust pojo is null ---err , ow success
		details = new CustomerDAO().validateCustomer(c.getEmail(),
				c.getPassword());
		System.out.println("details "+details);
		if (details != null)
		return SUCCESS;
		return ERROR;
	}

	public CustomerPOJO getC() {
		return c;
	}

	public CustomerPOJO getDetails() {
		return details;
	}
	

}
