package dao;

import org.hibernate.*;

import rev_pojos.CustomerPOJO;
import utils.HibernateUtils;

public class CustomerDAO {
	// CRUD --- validateCustomer
	public CustomerPOJO validateCustomer(String email, String pass)
			throws Exception {
		String hql = "select c from CustomerPOJO c where c.email = :e and c.password =:p";
		Session sess = HibernateUtils.getSession();
		Transaction tx = sess.beginTransaction();

		try {
			CustomerPOJO c = (CustomerPOJO) sess.createQuery(hql)
					.setParameter("e", email).setParameter("p", pass)
					.uniqueResult();
			tx.commit();
			return c;//either customer pojo if validated ow null
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();

			throw e;
		} finally {
			if (sess != null)
				sess.close();
		}

	}
}
