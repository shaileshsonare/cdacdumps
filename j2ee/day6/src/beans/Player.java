package beans;

public class Player {
	private String name;
	private String place;
	private String game;

	public Player() {
		name = "  ";
		place = " ";
		game = " ";
	}

	public void setName(String a) {
		name = a;
	}

	public void setPlace(String b) {
		place = b;
	}

	public void setGame(String c) {
		game = c;
	}

	public String getName() {
		return name;
	}

	public String getPlace() {
		return place;
	}

	public String getGame() {
		return game;
	}

}
