package pojos;

import java.io.Serializable;
import java.util.ArrayList;



public class BankUserPOJO implements Serializable {
	private int userId;
	private String name, password;
	private ArrayList<BankAcctPOJO> accts;

	public BankUserPOJO() {
		// TODO Auto-generated constructor stub
	}

	public BankUserPOJO(int userId, String name, String password) {
		super();
		this.userId = userId;
		this.name = name;
		this.password = password;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

	public ArrayList<BankAcctPOJO> getAccts() {
		return accts;
	}

	public void setAccts(ArrayList<BankAcctPOJO> accts) {
		this.accts = accts;
	}

	@Override
	public String toString() {
		return "BankUser Details  [userId=" + userId + ", name=" + name + "]";
	}
	
}
