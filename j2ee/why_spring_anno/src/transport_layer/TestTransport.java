package transport_layer;

import java.util.Arrays;

import org.springframework.stereotype.Component;

@Component("test")
public class TestTransport implements Transport {

	@Override
	public void communicateWithBank(byte[] dataPkt) {
		System.out.println("sent data over Test layer "
				+ Arrays.toString(dataPkt));

	}

}
