package transport_layer;

import java.util.Scanner;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component("input")
public class TransportSelector implements ApplicationContextAware{
	
	private Scanner sc;
	private ApplicationContext ctx;
	public TransportSelector() {
		System.out.println("in trasport sel constr");
		sc=new Scanner(System.in);
	}
	public Transport supplyTransportLayer()
	{
		System.out.println("Enter Transport layer --- http|test|soap");
		return (Transport) ctx.getBean(sc.next());
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		System.out.println("in set ctx ");
		ctx=arg0;
		
	}
	

}
