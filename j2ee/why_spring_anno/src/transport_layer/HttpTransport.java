package transport_layer;

import java.util.Arrays;

import org.springframework.stereotype.Component;

@Component("http")
public class HttpTransport implements Transport {

	@Override
	public void communicateWithBank(byte[] dataPkt) {
		System.out.println("sent data over HTTP layer "
				+ Arrays.toString(dataPkt));

	}

}
