package atm;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import transport_layer.TestTransport;
import transport_layer.Transport;

@Component("my_atm")
public class ATMImpl implements ATMIntf {
/*	@Autowired//auto wiring by type
	@Qualifier("soap")// auto wiring by name 
	dis-advantage --- injecting dependency --- static manner -- so replacing the same
	with SPEL syntax
	
*/
	@Value("#{input.supplyTransportLayer()}")
	private Transport transport;

	@Override
	public void withdraw(double amt) {
		StringBuilder sb = new StringBuilder("Withdrawing " + amt);
		// send this info to the bank

		transport.communicateWithBank(sb.toString().getBytes());

	}

	@Override
	public void deposit(double amt) {
		StringBuilder sb = new StringBuilder("Depositing " + amt);
		// send this info to the bank

		transport.communicateWithBank(sb.toString().getBytes());

	}


}
