package atm;

public interface ATMIntf {
	void withdraw(double amt);
	void deposit(double amt);

}
