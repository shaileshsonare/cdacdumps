package calc;

import javax.ejb.Remote;

@Remote
//to specify to EJBC -- methods declared in foll i/f 
//should be made available to remote(outside current JVM) clients
public interface Calculator {
	//B.M declarations 
	double add(double d1,double d2);
	//added so that ejb clnt can invoke the same @ time of http clnt's log out
	void doLogOut();

}
