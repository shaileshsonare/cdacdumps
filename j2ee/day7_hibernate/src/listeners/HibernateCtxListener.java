package listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import utils.HibernateUtils;

/**
 * Application Lifecycle Listener implementation class HibernateCtxListener
 *
 */
@WebListener
public class HibernateCtxListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public HibernateCtxListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0) {
        //to load singleton inst of SF
    	System.out.println("in ctx inited ");
    	HibernateUtils.getFactory();
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0) {
        // close SF
    	HibernateUtils.getFactory().close();
    }
	
}
