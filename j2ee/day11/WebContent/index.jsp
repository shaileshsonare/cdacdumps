<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<s:a action="show_login_form" namespace="basic">Display Login Form</s:a><br/>
	<s:a action="show_validate_form" namespace="test">Test Prog Validations</s:a><br/>
	<s:a action="show_validate_form1" namespace="test">Test Prog Validations Using Prop Files</s:a>
	<s:a action="show_register_form" namespace="test">Test XML Validations Using Prop Files</s:a>
</body>
</html>