package actions;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.util.ServletContextAware;

import pojos.UserPOJO;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class LoginAction extends ActionSupport implements
		ModelDriven<UserPOJO>, SessionAware, ServletContextAware {
	private UserPOJO u;
	private Map<String, Object> sessMap;
	private ServletContext ctx;

	public LoginAction() {
		System.out.println("in la constr....");
		u = new UserPOJO();
	}

	@Override
	public void setServletContext(ServletContext arg0) {
		// who invokes it --- ctx interceptor
		// calls setServletCtx --- to pass ctx obj to action class
		System.out.println("in set ctx ");

		ctx = arg0;
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		// who invokes --- session interceptor
		// call setSess --- to pass session scoped attr map to action class
		System.out.println("in set sess ");
		sessMap = arg0;
	}

	@Override
	public UserPOJO getModel() {
		// called by MDI --- before params
		System.out.println("in get model");
		return u;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String execute() throws Exception {
		// get hm from ctx scope
		HashMap<String, UserPOJO> users = (HashMap<String, UserPOJO>) ctx
				.getAttribute("all_users");
		// get rq params
		String em = u.getEmail();
		String pass = u.getPass();
		System.out.println("in exec " + em + " " + pass + " " + users);
		// perform validations
		if (users.containsKey(em))
			if (users.get(em).getPass().equals(pass)) {
				// in succ --- store user dtls in session scope & ret succ
				sessMap.put("user_dtls", users.get(em));
				return SUCCESS;
			}

		// ow ret err
		return ERROR;
	}

	public String doLogout() throws Exception {
		System.out.println("in do-logout");
		// invalidate sess.
		// sessMap -- i/f ref ponting to SessionMap cls inst.
		// SessionMap has method --- invalidate --- which internally calls HS's
		// invalidate
		((SessionMap<String, Object>) sessMap).invalidate();
		return SUCCESS;
	}

}
