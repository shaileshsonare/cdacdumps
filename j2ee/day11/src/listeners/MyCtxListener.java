package listeners;

import java.util.HashMap;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import pojos.UserPOJO;

/**
 * Application Lifecycle Listener implementation class MyCtxListener
 *
 */
@WebListener
public class MyCtxListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public MyCtxListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0) {
     
       //create n populate HM & add it to appln /ctx scope
       HashMap<String, UserPOJO> hm=new HashMap<>();
       hm.put("a@gmail", new UserPOJO("a@gmail", "123", 350));
       hm.put("b@gmail", new UserPOJO("b@gmail", "1234", 550));
       arg0.getServletContext().setAttribute("all_users", hm);
       System.out.println("in ctx inited "+hm);
       
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
    }
	
}
