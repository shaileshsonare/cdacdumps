package pojos;

import java.util.Date;

public class EmpPOJO {
	private int empId;
	private String name,address;
	private double sal;
	private String deptId;
	private Date joinDate;
	public EmpPOJO() {
		// TODO Auto-generated constructor stub
	}
	public EmpPOJO(int empId, String name, String address, double sal,
			String deptId, Date joinDate) {
		super();
		this.empId = empId;
		this.name = name;
		this.address = address;
		this.sal = sal;
		this.deptId = deptId;
		this.joinDate = joinDate;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public double getSal() {
		return sal;
	}
	public void setSal(double sal) {
		this.sal = sal;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	@Override
	public String toString() {
		return "EmpPOJO [empId=" + empId + ", name=" + name + ", address="
				+ address + ", sal=" + sal + ", deptId=" + deptId
				+ ", joinDate=" + joinDate + "]";
	}
	

}
