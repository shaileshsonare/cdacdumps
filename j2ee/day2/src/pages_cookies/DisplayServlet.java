package pages_cookies;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DisplayServlet
 */
@WebServlet("/display")
public class DisplayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			//get cookie array
			String sts="Emp Not found......";
			Cookie[] cookies=request.getCookies();
			//chk if non empty --- chk if contains cookie by emp_dtls---if yes display
			//ow err mesg
			if (cookies != null)
				for (Cookie c : cookies) {
					if (c.getName().equals("emp_dtls"))
						sts=c.getValue();
					break;
				}
			pw.print("<h3>"+sts+"</h3>");
						
			
	
		}
	}

}
