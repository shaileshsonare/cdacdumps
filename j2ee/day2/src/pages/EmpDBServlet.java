package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pojos.EmpPOJO;

import java.sql.*;

/**
 * Servlet implementation class EmpDBServlet
 */
@WebServlet("/emp_db")
public class EmpDBServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection cn;
	private PreparedStatement pst;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		try {
			// load drvr class
			Class.forName("com.mysql.jdbc.Driver");
			// get db cn
			cn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/testjdbc", "root", "root");

			String sql = "select * from my_emp where empid = ?";
			// pst
			pst = cn.prepareStatement(sql);
		} catch (Exception e) {
			throw new ServletException("err in init", e);
		}

	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
			if (pst != null)
				pst.close();
			if (cn != null)
				cn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			int id = Integer.parseInt(request.getParameter("emp_id"));
			// set in param
			pst.setInt(1, id);
			// execute query --- rst --- if success
			ResultSet rst = pst.executeQuery();
			if (rst.next()) {
				// create emppojo --- by using db table dtls.
				EmpPOJO e1 = new EmpPOJO(id, rst.getString(2),
						rst.getString(3), rst.getDouble(4), rst.getString(5),
						rst.getDate(6));
				pw.print("Emp Dtls " + e1);
			} else
				// if err ---retry link
				pw.print("Invalid Emp ID, " + id
						+ "Pls <a href='emp_info.html'>Retry</a>");
		} catch (Exception e) {
			throw new ServletException("err in servcing req", e);
		}
	}

}
