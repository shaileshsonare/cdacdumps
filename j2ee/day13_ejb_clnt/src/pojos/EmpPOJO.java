package pojos;

import java.io.Serializable;
import java.util.Date;

public class EmpPOJO implements Serializable {
	private int empId;
	private String name,adr;
	private double salary;
	private String deptId;
	private Date joinDate;
	public EmpPOJO() {
		// TODO Auto-generated constructor stub
	}
	public EmpPOJO(int empId, String name, String adr, double salary,
			String deptId, Date joinDate) {
		super();
		this.empId = empId;
		this.name = name;
		this.adr = adr;
		this.salary = salary;
		this.deptId = deptId;
		this.joinDate = joinDate;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAdr() {
		return adr;
	}
	public void setAdr(String adr) {
		this.adr = adr;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	@Override
	public String toString() {
		return "Emp Details [empId=" + empId + ", name=" + name + ", adr=" + adr
				+ ", salary=" + salary + ", deptId=" + deptId + ", joinDate="
				+ joinDate + "]";
	}
	

}
