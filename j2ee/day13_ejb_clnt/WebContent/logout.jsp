<%@page import="calc.Calculator"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		Calculator ref1 = (Calculator) session.getAttribute("rem_ref");
		if (ref1 != null)
			ref1.doLogOut();
		session.invalidate();
	%>
	<h3>U r being logged out.....</h3>
	<a href="index.jsp">Start again</a>
</body>
</html>