<%@page import="calc.Calculator"%>
<%@page import="javax.naming.InitialContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GlassFish JSP Page</title>
</head>
<body>
	<form>
		Enter Num 1 <input type="text" name="num1" /><br /> Enter Num 2 <input
			type="text" name="num2" /><br /> <input type="submit" name="btn"
			value="Test SLSB" />
	</form>
	<c:if test="${param.btn == 'Test SLSB' }">
		<%
			//perform lookup  only once per clnt
				Calculator ref = (Calculator) session.getAttribute("rem_ref");
				if (ref == null) {
					InitialContext ctx = new InitialContext();
					ref = (Calculator) ctx.lookup("my_slsb");
					session.setAttribute("rem_ref", ref);
					System.out.println("New user ref =" + ref);
				}
				else
					System.out.println("old user ");
				double d1 = Double.parseDouble(request.getParameter("num1"));
				double d2 = Double.parseDouble(request.getParameter("num2"));
				//perform RMI on SLSB via layers.
				out.print("Result again = " + ref.add(d1, d2));
		%>
	</c:if>
	<br/>
	<c:url var="url" value="logout.jsp"/>
	<a href="${url}">Log Out </a>
</body>
</html>
