<%@page import="emp.EmpService"%>
<%@page import="calc.Calculator"%>
<%@page import="javax.naming.InitialContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GlassFish JSP Page</title>
</head>
<body>
	<form>
		Enter Emp ID  <input type="text" name="emp_id" /><br />
		 <input type="submit" name="btn"
			value="Display Emp Details" />
	</form>
	<c:if test="${param.btn == 'Display Emp Details' }">
		<%
		
				
				
					InitialContext ctx = new InitialContext();
					EmpService  ref = (EmpService) ctx.lookup("emp_slsb123");
					
					System.out.println("New user ref =" + ref);
				int id = Integer.parseInt(request.getParameter("emp_id"));
			
				//perform RMI on EMP SLSB via layers.
				out.print("Emp Info " + ref.getEmpDetails(id));
		%>
	</c:if>
	<br/>
	<c:url var="url" value="logout.jsp"/>
	<a href="${url}">Log Out </a>
</body>
</html>
