Day 1

1. Create web-appln w/o IDE ---test html content , followed by Servlet ---with annotations & once w/o ---with xml tags.
2. Enter IDE --- create web appln & test the same(with .html & servlet)

3.  Create a web application with IDE . Write HTML form for accepting following details from the Customer/User
a.	Customer Name(textbox)
b.	Email(textbox)
c.	Password(password field)
d.	O.S Used(Drop down single selection list) 
e.	Technology Used (multiple-selectable check boxes) 
f.	Problem Description(textarea)
g.	Register Me : submit button for sending these details to the CustomerServlet.

Display the accepted customer details using  dyn. Content(try both get & post methods & understand the difference)

Day 2

1. Finish Login assignment as discussed in theory session. --- validate user from LoginServlet --- if validations succeed --- navigate user to next page(1st with link & then with sendRedirect) --- Use 1st cookie API , understand disadvantages & then replace cookie api by  HttpSession API
2. Try signup assignment after finishing login part.


Day 3
1. Create Online Book Shop utility as discussed in theory session from login till logout servlet.

Day 4
For JSP basics
0. MUST look at translated servlet pages & understand basics of JSP life cycle. Read life-cycle from readme_jsp

1. Create login form(login.html) -- accepting email & password -- submit the same to login.jsp.
Perform HashMap based authentication(i.e currently replace DB table by HashMap) --- upon successful validation --- display user dtls from the same page , o.w give Retry link to take user to login form.

2. Above flow remains the same for error branch. For success branch -- add user dtls to session scope, give Next link to dtls page(details.jsp) & display User dtls from dtls page.(with or w/o cookies)

3. Above flow remains the same for error branch. For success branch -- add user dtls to session scope, use send re-direct to navigate user to dtls page(details.jsp) & display User dtls from dtls page.

4.Above flow remains the same for error branch. For success branch -- add user dtls to appropriate scope, use RD's forward scenarion to navigate user to dtls page(details.jsp) & display User dtls from dtls page.

5. From above --- replace forward scenario by include' scenario & test the same.(Identify diff between include & forward)
6. If time permits --- replace HashMap by users DB table & confirm above observations.


Day 5
1. Import day5 project , set up DB using DDLs , test & understand JSP using JBs.
2. Replace LoginServlet by login.jsp (using BookShopBean) , BookShopDAO & BookPOJO,UserPOJO unchanged.

3.  Replace BooksMainServlet by BooksMain.jsp (Try & reduce scriptlets) If time permits --- replace LogoutServlet by logout.jsp

Day 6 
1. Go through complete day6 --- for testing Bank scenario . Go through JSTL syntax & test it.
2. Write your own HttpSession listener & test the same
3. Go thrugh steps of creating custom tag --- & create simple custom tag which send hello to clnt browser.
4. Complete BookShopCart assignment .

Day 7
1. Go through complete day7 along with logout handler custom tags
2. Test dynamic downloading of applets
3. Create fresh dynamic web project & test using Hiebrnate API --- "create" functionality











