<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h3>Via EL Syntax </h3>
<h3>Hello, ${param.u_name} </h3> 
<h3>Prefs are , ${param.u_prefs} </h3>
<%
 //add user info to session scope
 session.setAttribute("user_name",request.getParameter("u_name"));
session.setAttribute("user_prefs",request.getParameter("u_prefs"));
String encodeURL=response.encodeURL("next.jsp");
%>
<a href="<%= encodeURL%>">Next</a>
</body>
</html>