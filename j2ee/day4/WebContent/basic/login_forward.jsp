<%@page import="pojos.UserPOJO"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<%!HashMap<String, UserPOJO> users;

	public void jspInit() {
		System.out.println("in jsp init");
		users = new HashMap<String, UserPOJO>();//empty HM
		users.put("a@gmail", new UserPOJO("a@gmail", "123", "abc123", 350));
		users.put("b@gmail", new UserPOJO("b@gmail", "1234", "abc1234", 550));
		users.put("c@gmail", new UserPOJO("c@gmail", "12345", "abc12345", 850));
	}%>
<body>
	<%
		String email = request.getParameter("u_email");
		String pass = request.getParameter("u_pass");
		UserPOJO u = null;
		if (users.containsKey(email))
			if (users.get(email).getPassword().equals(pass)) {
				u = users.get(email);
				out.print("From 1st page......");
			//	out.flush();
				//add user pojo dtls to req scope
				request.setAttribute("user_dtls", u);
				RequestDispatcher rd=request.getRequestDispatcher("display.jsp");
				if (rd != null)
					rd.forward(request, response);
				else
					System.out.println("rd err");
	
	
	
		} else {
	%>
	Invalid login , Pls
	<a href="login.html">Retry</a>
	<%
		}
	%>
</body>
<%!
public void jspDestroy()
{
	System.out.println("in jsp destroy");
	users=null;
}
%>
</html>