package vku;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionDB {
   
	Connection conn=null;
	InitialContext context;
	DataSource ds;
	
	public Connection connect()
	{
		try 
		{
			context=new InitialContext();
			ds=(DataSource)context.lookup("java:comp/env/jdbc/Test");
			conn=ds.getConnection();
			
		}
		catch (NamingException e) 
		{
		// TODO Auto-generated catch block
		e.printStackTrace();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return conn;
	}
}
