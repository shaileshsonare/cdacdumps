<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="ua"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Page</title>
</head>
<body>
	<ua:form action="myaction">
		
		
		<ua:textfield name="username" label="User Name"></ua:textfield>
		<ua:password name="password" label="Password"> </ua:password>
		<ua:submit value="Login"></ua:submit>
		
		<%-- <ua:if test="hasActionErrors()">
			<ua:actionerror/>
		</ua:if>
		
		<ua:if test="hasActionMessage()">
			<ua:actionmessage/>
		</ua:if> --%>
	
	</ua:form>

</body>
</html>