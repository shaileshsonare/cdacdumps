import com.opensymphony.xwork2.ActionSupport;


public class LoginAction extends ActionSupport {

	private String username;
	private String password;
	public LoginAction()
	{
		
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String execute() throws Exception {
		Pojo1 obj=new Pojo1();
		obj.setPassword(getPassword());
		obj.setUsername(getUsername());
		Login login=new Login();
		return login.insert(obj);
	}
	
}
