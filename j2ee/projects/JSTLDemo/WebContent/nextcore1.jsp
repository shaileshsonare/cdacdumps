<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!-- <form name="form1" ac method="post"> -->

Parameter Value : <c:out value='${data}'/>
<br/>
<c:set var="choice" value="one"/>

<c:choose>
<c:when test="${choice eq 'one' }"> first </c:when>
<c:when test="${choice eq 'two' }"> second </c:when>
<c:when test="${choice eq 'three' }"> third </c:when>
<c:otherwise>otherwise</c:otherwise>

</c:choose>
<!-- </form> -->
</body>
</html>