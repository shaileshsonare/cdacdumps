<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib  prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
 <form method="post">
<products>
    
       <product>
          <id>01</id>
          <name>LCD</name>
       </product>
    
          <product>
             <id>02</id>
             <name>LAPPY</name> 
          </product>    
     
             <product>
               <id>03</id>
               <name>DESKTOP</name>
             </product>
    
            <product>
              <id>04</id>
              <name>AC</name>
            </product>
    
</products>
</form>

 <c:if test="$pageContext.request.method=='POST'">

<x:parse xml="${url}" var="doc"/>
<x:forEach  select="$doc/products/product">
<x:out select="id"/>
<br/>
<x:out select="name"/>
<br/>
</x:forEach>
</c:if>
</body>
</html>