<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form name="form1" action="nextcore1.jsp" method="get">
Welcome &nbsp;&nbsp;
<c:out value="${param.pin}"></c:out>

<c:set var="data" value="java" scope="request"></c:set>
<br/> Data setted : 
<c:out value="${data}"></c:out>
<br/>
Conditional EL :
<c:if test="${data eq 'java'}" >
Welcome ${data}
</c:if>
<br/>
Remove : 
<%-- <c:remove var="data"/> --%>

<br/>
<input type="submit"/>
</form>
</body>
</html>