<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<center><h2>FUNCTION TAGS IN JSTL</h2></center>
 <!-- <!-- String x = "testing"; -->
  <!-- String[] a = { "xx", "yy", "zz" }; -->
  <c:set var="string1" value="JAVA PROGRAMS" > </c:set>
  <c:set var="string2" value="PROGRAMS" ></c:set>
  <c:set var="string3" value="JAVA" > </c:set>
  <c:set var="string4" value="y*r*e*t"></c:set>
   
  STRING1: ${string1}<br/>
  STRING2: ${string2}<br/>
 
  Length of STRING1 is : ${fn:length(string1)}<br/>
  STARTS WITH:${fn:startsWith(string1,"J")}<br/>
  ENDS WITH:  ${fn:endsWith(string2,"u")}<br/>
  ENDS WITH:  ${fn:endsWith(string2,"S")}<br/>
  INDEX OF :  ${fn:indexOf(string1,"V")}<br/>
  SPLIT :     <c:out value="${fn:split(string4,'*')}"/><br/>
  SUBSTRING:  ${fn:substring(string2,2,4)}<br/>
  REPLACE:    ${fn:replace(string4,"*","i")}<br/>
  SUBSTRING AFTER: PROGRAMS ${fn:substringAfter(string2,"R")}<br/>
  SUBSTRING BEFORE: ${fn:substringBefore(string2,"M")}<br/>
 
</body>
</html>