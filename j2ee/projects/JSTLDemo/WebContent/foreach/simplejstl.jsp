<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*" %>
<html>
<body>
<strong>
Movie String array List:</strong>

<br><br>
<table>
<!-- 
****************************************************************************************
the items attribute is the object which is set as Attribute in the servlet the step attribute is to skip as the value assigned to it. 
*****************************************************************************************
--->

<c:forEach var="movie" items="${movieList}" varStatus="loopcount" step="2" >
<tr>
	<td>SiNo:${loopcount.count}</td>
	<td>${movie}</td>
</tr>
</c:forEach>
<tr><td>

</table>
</body>
</html>
