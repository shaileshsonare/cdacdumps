SPRING FRAMEWORK 3.0 M1 (December 2008)
---------------------------------------
http://www.springframework.org

1. INTRODUCTION
---------------

This is the first milestone of Spring 3.0 which is scheduled for final release in Q2 2009.
This release updates the entire codebase for Java 5+ and introduces EL and REST support.

2. RELEASE NOTES
----------------

This release comes without reference documentation. For the time being, please consult
the provided javadoc for details on the new features.

3. DISTRIBUTION JAR FILES
-------------------------

The Spring Framework module jar files can be found in the 'dist' directory. Note that
this release does not contain a 'spring.jar' file anymore. Furthermore, the jar file
names follow bundle repository conventions now.

4. GETTING STARTED
------------------

Check out the provided PetClinic sample application. It has been partially updated
for Spring 3.0 already.
