package com.beans;

import org.springframework.core.io.ClassPathResource;
import org.springframework.beans.factory.xml.XmlBeanFactory;


public class InvokeBean {

	static void invokeBean()
	{
		
		ClassPathResource resource = new ClassPathResource("beans.xml");
		XmlBeanFactory beanFactory = new XmlBeanFactory(resource);
		ConstructorInjection spring =(ConstructorInjection) beanFactory.getBean("helloSpring");
		System.out.println(spring);	
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		invokeBean();

	}

}
