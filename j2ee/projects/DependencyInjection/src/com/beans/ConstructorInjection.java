package com.beans;

public class ConstructorInjection {
	
	
	private int id;
	private String name;
	
	//overload
	
	//constructor 1
	public ConstructorInjection(int id, String name) {
		super();
		System.out.println("Constructor - 1");
		this.id = id;
		this.name = name;
	}
	
	//constructor 2
	public ConstructorInjection(String name, int id) {
		super();
		System.out.println("Constructor - 2");
		this.id = id;
		this.name = name;
	}
	
	
	public String toString()
	{
		return "Id : "+id+"\nName : "+name;
	}
	

}
