package com.dao;

import javax.sql.DataSource;
import java.sql.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class EmpDAOImplementation implements EmpDAOInterface{

	private JdbcTemplate jdbcTemplate;
	
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	
	public void insertEmp(EmpBean empBean) {
		String query ="insert into emp(empno,ename,sal,deptname) values(?,?,?,?)";
		jdbcTemplate.update(query, new Object[]{Integer.valueOf(empBean.getEmpno()),empBean.getEname(),empBean.getSal(),empBean.getDeptname()});
        System.out.println("Row Added............");   		
	}
	
	public EmpBean selectEmp(int empno) {
		String query = "SELECT * FROM emp where empno=?";
		/**
		 * Implement the RowMapper call back interface
		 */
		EmpBean	empBean	=(EmpBean) jdbcTemplate.queryForObject(query, new Object[]{
				Integer.valueOf(empno)},new RowMapper() {
			public Object mapRow(ResultSet resultSet, int rowNum)
					throws SQLException {
				return new EmpBean(
						resultSet.getInt("empno"),
						resultSet.getString("ename"), 
						resultSet.getInt("sal"), 
						resultSet.getString("deptname"));
			
			}
		});
		return empBean;
	}
	
	public EmpBean[] selectEm() {
		String query = "SELECT * FROM emp";
		/**
		 * Implement the RowMapper call back interface
		 */
		
		//jdbcTemplate.queryForObject
		EmpBean[]	empBean	=(EmpBean[]) jdbcTemplate.queryForObject(query,new RowMapper() {
			public Object mapRow(ResultSet resultSet, int rowNum)
					throws SQLException {
				return new EmpBean(
						resultSet.getInt("empno"),
						resultSet.getString("ename"), 
						resultSet.getInt("sal"), 
						resultSet.getString("deptname"));
			
			}
			
		});
		System.out.println(empBean.length);
		return empBean;
	}	
}
