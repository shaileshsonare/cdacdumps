package com.dao;

public interface EmpDAOUpdateInterface {
	
	public void updateEmp(EmpBean bean);
	
	public void deleteEmp(EmpBean empBean);
	
}
