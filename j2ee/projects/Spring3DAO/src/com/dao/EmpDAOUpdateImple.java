package com.dao;

import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

public class EmpDAOUpdateImple implements EmpDAOUpdateInterface{

	private JdbcTemplate jdbcTemplate;
	
	public void setDataSource(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource); 
	}
	
	@Override
	public void updateEmp(EmpBean empBean) {
		String query ="update emp set sal = ? where empno= ?";
		jdbcTemplate.update(query, new Object[]{Integer.valueOf(empBean.getSal()),Integer.valueOf(empBean.getEmpno())});
	}

	@Override
	public void  deleteEmp(EmpBean empBean) {
		String query ="delete from emp where empno= ?";
		jdbcTemplate.update(query, new Object[]{Integer.valueOf(empBean.getEmpno())});	
	}

	/**
	 * @return the jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	/**
	 * @param jdbcTemplate the jdbcTemplate to set
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
