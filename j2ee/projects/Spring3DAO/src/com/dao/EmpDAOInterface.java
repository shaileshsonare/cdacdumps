
package com.dao;

public interface EmpDAOInterface {
	
	public void insertEmp(EmpBean empBean);

	public EmpBean selectEmp(int empno);
	
	public EmpBean[] selectEm();

}