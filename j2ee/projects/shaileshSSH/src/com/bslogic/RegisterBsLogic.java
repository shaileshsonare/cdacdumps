package com.bslogic;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dao.RegisterDAO;
import com.pojo.RegisterPojo;

public class RegisterBsLogic 
{
	
	public String insert(RegisterPojo rp)
	{
		ApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");
		
		RegisterDAO rdao=(RegisterDAO)context.getBean("logindao");
		
		return rdao.saveUser(rp);
	}
}
