import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class OurServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/*Connection conn =null;*/ 
			
    public OurServlet() 
    {
        super();   
    }
		
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String usname=request.getParameter("userid");
		String password=request.getParameter("pwd");
		String sql = "select * from login where username=?";
		String unm,pwd;
		
		Connection conn =new DBConnection().connect();
		PreparedStatement pstat;
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		boolean flag=true;
		RequestDispatcher rd=null;
		
		/*out.close();*/
		try 
		{
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, usname);
			
			ResultSet rs = pstat.executeQuery();
			
			if(rs.next())
			{
			while(flag)
			{
				if(password.equals(rs.getString(2)))
				{
					out.print("Username "+ rs.getString(1) );
					out.print(" password "+ rs.getString(2));
					response.sendRedirect("home.html");
					//rd=request.getRequestDispatcher("/home.html");
					//rd.forward(request, response);
					flag=false;
				}
				else
				{
					out.print("invalid  password");
					rd=request.getRequestDispatcher("/Ourhtml.html");
					rd.include(request, response);
					flag=false;
				}
			}
		}
			else
			{
				out.print("invalid  username/password");
				rd=request.getRequestDispatcher("/Ourhtml.html");
				rd.include(request, response);
			}
		} 
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	
	}

}
