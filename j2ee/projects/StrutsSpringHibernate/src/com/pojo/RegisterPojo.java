package com.pojo;

public class RegisterPojo 
{
	private String username;
	private String password;
	
	public RegisterPojo(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	public RegisterPojo() {
		super();
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
