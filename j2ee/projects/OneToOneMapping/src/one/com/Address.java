package one.com;

public class Address 
{
	int aid;
	String city;
	Student student;
	
	public Address(){}
	
	public Address(int aid,String city)
	{
		this.aid=aid;
		this.city=city;
	//	this.student=student;
	}
	
	
	public int getAid() {
		return aid;
	}
	public void setAid(int aid) {
		this.aid = aid;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	
}
