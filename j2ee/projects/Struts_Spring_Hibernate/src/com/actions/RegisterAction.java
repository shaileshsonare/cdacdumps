package com.actions;

import com.bslogic.RegisterBsLogic;

public class RegisterAction 
{
	private String username;
	private String password;
		
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public RegisterAction(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public RegisterAction() {
		super();
	}

	public String execute()
	{
		System.out.println("Action Username : "+username);
		System.out.println("Action Password : "+password);
		new RegisterBsLogic().getValues(username, password);
		return "success";
	}
}
