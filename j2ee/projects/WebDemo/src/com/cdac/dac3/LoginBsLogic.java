package com.cdac.dac3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginBsLogic {
	
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	boolean flag = false;
	public boolean loginValidate(String uid,String pwd)
	{
		try
		{
		
		DBConnection dbconn = new DBConnection();
		conn = dbconn.connect();
		String sql = "select userid from login where userid=? and password=?";
		
		pstmt = conn.prepareStatement(sql);
		
		pstmt.setString(1, uid);
		pstmt.setString(2, pwd);
		rs = pstmt.executeQuery();
		
		if(rs.next())
			flag = true;
	}
	catch(SQLException e)
	{
		e.printStackTrace();
	}
	finally
	{
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		return flag;
		
	}
	

}
