package com.san.dao;

import com.san.model.Department;
import com.san.model.Employee;
 
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

class Onetomany
{
	public static void main(String[] args) 
	{
		
		//setting the department values
		Department department =new Department();
		department.setDid(2);
		department.setDname("hr");
		
		//setting the employee values..
		Employee employee=new Employee();
		employee.setEid(238);
		employee.setEname("nasas");
		employee.setDeparment(department);
		
		/*
		employee.setEid(20);
		employee.setEname("sdhsdf");
		employee.setDeparment(department);
		
		
		employee.setEid(30);
		employee.setEname("sdidhya");
		employee.setDeparment(department);
		
		employee.setEid(40);
		employee.setEname("sdidhyasdaf");
		employee.setDeparment(department);
	*/
		
		System.out.println("creating session factory..");
		SessionFactory sessionfactory=new Configuration().configure().buildSessionFactory();
		System.out.println("created the configuration....");
		Session session=sessionfactory.openSession();
		System.out.println("created the session...");
		Transaction transaction=session.beginTransaction();
	
		//session.save(department);
		session.save(employee);
	
		transaction.commit();
		System.out.println("commited transaction....");
		session.close();
	}
}
