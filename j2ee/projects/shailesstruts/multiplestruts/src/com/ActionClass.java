package com;

import com.opensymphony.xwork2.ActionSupport;

public class ActionClass extends ActionSupport
{
	private String message;
	
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String execute()
	{
		return "SUCCESS";
	}
	
	public String add()
	{
		setMessage(getText("user.rqd"));
		return "success";
	}
	
	public String edit()
	{
		return "success";
	}
}
