

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MyServlet2
 */
public class MyServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		HttpSession session=request.getSession();
		
		String name=session.getAttribute("uname").toString();
		PrintWriter out=response.getWriter();		
		out.println("Hello2......."+name);
		out.println("byeeeeeee");
		
		
		out.println(
				"<html>" +
				"<body>" +
				"<form name=\"form2\" action=\"myservlet3\" method=\"post\">" +
				"Click next" +
				"<input type=\"submit\" value=\"next\"" +
				"</form>" +
				"</body>" +
				"</html>"
				);
		
		session.invalidate();
		
	}

}
