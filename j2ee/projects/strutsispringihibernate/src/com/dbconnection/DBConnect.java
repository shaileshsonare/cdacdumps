package com.dbconnection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.DAO.LoginDAO;

public class DBConnect {
	
	public LoginDAO getLoginDAO()
	{
		ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
		 LoginDAO logdao= (LoginDAO)context.getBean("logindao");
		 return logdao;
	}

}
