Objective --- 
JMS sender as servlet --- take mesg from clnt (thin clnt) --- using JMS API -- servelet will send the mesg to JMS dest(Queue)

JMS receiver --- asynch receiver ---- Mesg drive bean

Types of JMS sender- servelet/JSP/Java bean, SLSB,SFSB or JMS based java appln
Types of JMS receivers --- JMS based appln(synch/asynch) OR MDB


Steps for creating JMS sender
0. Create 2 administered objs --- JMS conn factory & JMS dest. -- using admin tool(asadmin) or admin console.

Setting up of JMS on Glassfish server console
1. Create JMS resource (Connection factory)

create-jms-resource --restype javax.jms.ConnectionFactory  jms/new_factory

2. Create JMS resource (Queue type end point = JMS destination in PTP domain)
create-jms-resource --restype javax.jms.Queue --property Name=PhysicalQueue1 jms/newQueue

3. Use D.I --- to inject JMS conn factory  & JMS (Q) dest into servlet.

4. From init() --- create JMS conn inst from CF
5. from doGet/doPost --- per req ---- create JMS session --- from JMS session --- create jms sender & jms text msg--text contents will be sent to MDB via JMS dest.

Steps for creating asynch receiver in form of Mesage Driven Bean
1. Create MDB class (within EJB project)--- which implements listener i/f ---- javax.jms.MessageListener i/f
2. Implement method 
public void onMessage(Message m) 
3. After supplying (using D.I) JMS dest details ---i.e information on --- from where to read messages ---
EJBC will auto invoke onMessage(...) of MDB & supply the mesg.














