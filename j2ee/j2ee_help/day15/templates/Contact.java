package com.app.domain;

import java.io.Serializable;
import java.util.Date;



import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;


import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

public class Contact implements Serializable {
	@NotEmpty
	@Email
	@Length(min=3,max=15)
	private String email;
	
	@NotEmpty
	@Length(min=5,max=10)
	private String password;
	
	@NotEmpty
	private String name;
	
	@NotNull
	@Past
	@DateTimeFormat(pattern="dd-MMM-yyyy")
	private Date regDate;
	
	@Range(min=100,max=1000)
	private double regAmt;
	
	public Contact() {
		// TODO Auto-generated constructor stub
	}

	public Contact(String email, String password, String name, Date regDate,
			double regAmt) {
		super();
		this.email = email;
		this.password = password;
		this.name = name;
		this.regDate = regDate;
		this.regAmt = regAmt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		System.out.println("date =" +regDate);
		this.regDate = regDate;
	}

	public double getRegAmt() {
		System.out.println("in get reg amt");
		return regAmt;
	}

	public void setRegAmt(double regAmt) {
		this.regAmt = regAmt;
	}

	@Override
	public String toString() {
		return "Contact [email=" + email + ", password=" + password + ", name="
				+ name + ", regAmt=" + regAmt + "]";
	}
	

}
