informal steps for testing aop.
1. Create java project (core java)-- test_aop
2. add external jars (as its core java project) --- must include aspectrt & aspectjweaver to enable anno support.
3. create spring xml config file -- add bean,context,aop namespace 
4. To  Enable the use of the @AspectJ style of Spring AOP. 

 <aop:aspectj-autoproxy/>, 
for auto scanning of beans(components) supply -- comma separated list of packages to scan
<context:component-scan base-package="com.aop" />
For internal anno(@Resource,@AutoWired.....  etc)
<context:annotation-config />


5. Start with simple CustomerService --- i/f -- addCustomer, listCustomers
impl class -- just s.op -- in B.L of addCustomer
6. Identify -- what is it that u want to intercept --- let's say only addCustomer has to be intercepted before its exec.
7. Create a class -- annotate it with @Aspect(to tell that its aspect) & for auto-scanning @Component
7.1
Define one or multiple pointcuts. -- Spring has point-cuts only for method executions.
anno @PointCut("execution (* com.aop.service.*.add*(..))")
public void test() {}

This is a method level annotation ---applied to any public method, void returning . can also have args & no method definition.
7.2 Use this point defined for any of the advices --- before,after,after returning,after throwing,around .
syntax
@Before(value = "test()")
public void logBefore(JoinPoint p) {
		System.out.println("in before advice args " + Arrays.toString(p.getArgs()) + " kind " + p.getKind() + " sign " +p.getSignature()+" target "+ p.getTarget());
	}
JoinPoint API
getArgs ---rets Object[] of args
getKind rets --kind of join point i.e = method execution
getSignature --- rets complete method signature --which is being advised.
getTarget -- rets the target object -- being advised.


Regarding ---  Around Advice

    Around Advice is combination of both Before and After Advice.
    In a single Advice it is possible to implement both Before and After services.
    Around Advice is not given by spring framework and it is from Open Source implementation called AOP alliance.
    Around Advice can be used by any framework which supports AOP.
    To create Around Advice, our class should implement an interface called MethodInterceptor. or use annotation 
    In Around Advice, we implement Before and After Advice in a single method called invoke(), in order to separate Before an After services to execute business logic, in the middle we call proceed() method.
    Around Advice can access the return value of business method and it can modify the value and it can return a different value back to the client, as return type is Object, but in the After Advice its not possible right, as its return type is void.

special features on @Around method
Three things are special about this signature:

    The return type is java.lang.Object; the other advice types have return type void.
    The first argument is of type org.aspectj.lang.ProceedingJoinPoint.
    The method declares java.lang.Throwable in its throws clause.

Another interesting thing to notice is the call to the proceed() method on the ProceedingJoinPoint object. 

eg  syntax ---
//method annotated with Pointcut 
//meaning -- applies to all method executions -- with ANY return type,any pkg name,class name , method name=run & must have a string arg.
 @Pointcut("execution(* *.run(String))")
	    public void markerNew() {}

	 
@Around("markerNew()")
	public void logAround(ProceedingJoinPoint joinPoint) {
	    System.out.println("#####Before method invokation.#####: " + joinPoint.toShortString());
	    try {
	        joinPoint.proceed();
	    } catch (Throwable e) {
	        e.printStackTrace();
	    }
	    System.out.println("#####After method invokation.#####: " + joinPoint.toShortString());	}



