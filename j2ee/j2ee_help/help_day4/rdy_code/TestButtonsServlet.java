package ex;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestButtonsServlet
 */
@WebServlet("/test_buttons")
public class TestButtonsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=null;
		
		try {
			response.setContentType("text/html");
			pw=response.getWriter();
			if (request.getParameter("command").equals("Login"))
				pw.print("in login");
			else
				pw.print("in register");
		} finally {
			
		}
	}

}
