<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<s:url var="url" action="hello" namespace="/basic"/>
<a href="${url}">Test Struts</a><br/>
<s:url id="url" action="log_complaint" namespace="/basic"/>
<%--using OGNL syntax to pass dyn expr value to href --%>
<s:a href="%{url}">Register Your Complaint</s:a>
<s:url var="url" action="validate" namespace="/validations" /><br/>
<s:a href="%{url}">Start Validations</s:a><br/>

<s:url var="url" action="start_upload" namespace="/upload" /><br/>
<s:a href="%{url}">Test File Upload</s:a><br/>
	
</body>
</html>