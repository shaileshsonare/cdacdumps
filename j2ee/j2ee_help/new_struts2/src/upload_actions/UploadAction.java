package upload_actions;

import java.io.File;

import com.opensymphony.xwork2.ActionSupport;

public class UploadAction extends ActionSupport {
	// d.m --props --- req param created by file upload interceptor
	private File abc;
	private String abcFileName, abcContentType;
	// param name coming from action tag
	private String uploadFolder;

	public String getAbcFileName() {
		return abcFileName;
	}

	public void setAbcFileName(String abcFileName) {
		this.abcFileName = abcFileName;
	}

	public String getAbcContentType() {
		return abcContentType;
	}

	public void setAbcContentType(String abcContentType) {
		this.abcContentType = abcContentType;
	}

	public void setAbc(File abc) {
		this.abc = abc;
	}

	public void setUploadFolder(String uploadFolder) {
		this.uploadFolder = uploadFolder;
	}

	@Override
	public String execute() throws Exception {
		// disp the dtls about uploaded content -- path,size
		System.out.println("Path : " + abc.getAbsolutePath() + "  " + " size "
				+ abc.length());
		// transfer uploaded contents from tmp folder to specified folder
		File dest = new File(uploadFolder, abcFileName);
		if (abc.renameTo(dest))
			// ret success or input
			return SUCCESS;
		return INPUT;
	}

}
