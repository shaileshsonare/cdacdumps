web-appln security
(web server : Tomcat 6.x/7.x) 
Refer : D:\Apache Tomcat 7.0.5\webapps\docs\ssl-howto.html
0. Generate keystore
keytool -genkey -alias tomcat -keyalg RSA
def. password : changeit
It generates keystore in user's home dir.

Actually tried : steps for generating keystore.
1. From cmd prompt ---(from any folder)
keytool -genkey -alias my_ks -keyalg RSA -keystore c:\my_ks
2. To view the keystore ---
keytool -list -keystore c:\my_ks
	
Steps 
1.understand security model 
separate the secured contents from non-secure contents.
If user is acessing non-secured resources : none of the steps will be executed.
For secured resources : 
For authentication : web-container refers to security realms(security configuration : consists of user names, user passwords & roles---> a logical role.) : saved in <Servers>/tomcat-users.xml
2. Authentication(logging -in) : identifying the client----basic authentication : uses very simple data encoding techniques. easily possible for the snoopers to read & modify data. so add security by adding data confidentiality using SSL protocol. Browser pops up a form for login.

This default can be replaced by a custom form.
Trigger : Client sending URL rq to any of the secured resource.
 custom form based authentication.
prog jobs : 1. write login form.(.html/.jsp)
2. action ="j_security_check", method=post
user name : j_username, passowrd : j_password
If successful login : authorization begins.
If failed : redirect him to login error page.

3.Authorization : controlling access to the web-pages depending upon the user roles.
Deployer jobs : add the desired roles to the list & enable http method viz get,post etc.
WC : in the run-time , it cheks authenticated user's role & then grants access to doGet/doPost method.(declarative way of authorization)
If security needs are still further fine -grained : (means : u want to give access only to particular part of doGet or doPost then) : HttpServletRequest method : is UserInRole(...)

4. Data Confidentiality : NONE=> no privacy, CONFIDENTIAL=> over SSL

Implementation steps.(ref project --- secure_web)
5. Creating users & mapping them to security roles -- for Tomcat its in 
Ecipse IDE --- servers---tomcat-users.xml . Add following

<role rolename="bank_mgr"/>
<role rolename="bank_user"/>
<user password="rama123" roles="bank_mgr" username="rama"/> <user password="ketan123" roles="bank_user" username="ketan"/>


6.creating security constraints via web.xml(DD) settings :
ref web.xml in ---  \secure_web\WebContent\WEB-INF\web.xml

7. adding login-config via login form.
8. enabling roles for methods access
9 .SSL ---

10. 
From Eclipse IDE --- servers --server.xml --- uncomment SSL
<!-- Define a SSL HTTP/1.1 Connector on port 8443 This connector uses the 	JSSE configuration, when using APR, the connector should be using the OpenSSL 	style configuration described in the APR documentation -->
<Connector SSLEnabled="true" clientAuth="false" keystoreFile="c:\my_ks" keystorePass="changeit" maxThreads="150" port="8443" protocol="HTTP/1.1" scheme="https" secure="true" sslProtocol="TLS"/>

<!-- Define an AJP 1.3 Connector on port 8009 -->
<Connector port="8009" protocol="AJP/1.3" redirectPort="8443"/>

Above will start : HTTPs on port no 8443

12. In web.xml of ur web-appln
<transport-guarantee>CONFIDENTIAL</transport-guarantee>(then only ssl will be enabled)

11. As this keystore & certificate is locally generated ---

First time Firefox or any browser : asks for confirmation ---- so add exception.
To view existing certificates
(Firefox--advanced--encryption---view certificates)



Updates for Apache 7.0.25 

---- works for this also (http to https also works properly) in same manner
web -appln created ---   <help>\secure_web.
its server .xml as a ref copied in --- G:\j2ee_help\latest
its tomcat-users.xml is also there.

<Connector connectionTimeout="20000" port="9090" protocol="HTTP/1.1"
			redirectPort="8443" />
		<!-- A "Connector" using the shared thread pool -->
		<!-- <Connector executor="tomcatThreadPool" port="8080" protocol="HTTP/1.1" 
			connectionTimeout="20000" redirectPort="8443" /> -->
		<!-- Define a SSL HTTP/1.1 Connector on port 8443 This connector uses the 
			JSSE configuration, when using APR, the connector should be using the OpenSSL 
			style configuration described in the APR documentation -->
		<!--  uncommented for enabling SSL-->
		<Connector SSLEnabled="true" clientAuth="false"
			keystoreFile="c:/tomcat.keystore" keystorePass="changeit" maxThreads="150"
			port="8443" protocol="HTTP/1.1" scheme="https" secure="true"
			sslProtocol="TLS" /> 

		<!-- Define an AJP 1.3 Connector on port 8009 -->
		<Connector port="8009" protocol="AJP/1.3" redirectPort="8443" />


