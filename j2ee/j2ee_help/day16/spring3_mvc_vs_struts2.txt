Common point between Struts 2 & Spring 3 web mvc ---- Both follow MVC pattern (Model II architectue) --Front Controller
1.Struts 2 ---  Front Controller --- SPE Filter 
Spring 3  Front Controller --- DispatcherServlet

2. Struts business actions replaced by Controller classes in spring
3. In struts 2 -- B.Actions are inst per clnt req.
In spring 3 --- If scope is default(singleton) --- Spring controllers will be inst once per ctx loading 
4. In S2 -- SPEF consults ActionMapping to find out mapping between url & name of the action class(eg --- /basic/hello.action ---- HelloAction class) 
Sp3 --- D.S consults Handler mapping ---to getmapping  between url & name of controller class & its req. handling method name.
eg URL --- http://localhost:9090/spring_web_mvc1/hello