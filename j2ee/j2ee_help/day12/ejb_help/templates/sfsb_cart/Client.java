


import java.util.*;
import javax.naming.*; 
import sfsb.*;
 
public class Client
{
   public static void main(String[] args) throws Exception
   {
      Properties properties = new Properties();
      properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
      properties.put(Context.PROVIDER_URL, "jnp://localhost:1099");
      
     InitialContext ctx = new InitialContext(properties);
     ShoppingCart cart = (ShoppingCart) ctx.lookup("ShoppingCartBean/remote");

      System.out.println("Buying 1 memory stick");
      cart.buy("Memory stick", 1);
      System.out.println("Buying another two memory sticks");
      cart.buy("Memory stick", 2);

      System.out.println("Buying a laptop");
      cart.buy("Laptop", 1);

      System.out.println("Print cart:");
      HashMap<String, Integer> fullCart = cart.getCartContents();
      for (String product : fullCart.keySet())
      {
         System.out.println(fullCart.get(product) + "     " + product);
      }

      System.out.println("Checkout");
      cart.checkout();

      System.out.println("Should throw an object not found exception by invoking on cart after @Remove method");
      try
      {
		  System.out.println("Buying a laptop");
      cart.buy("Laptop", 1);

               System.out.println("Print cart:");
      fullCart = cart.getCartContents();
      for (String product : fullCart.keySet())
      {
         System.out.println(fullCart.get(product) + "     " + product);
      }

      }
      catch (Exception e)
      {
         System.out.println("Successfully caught no such object exception."+e.getMessage());
      }


   }
}
