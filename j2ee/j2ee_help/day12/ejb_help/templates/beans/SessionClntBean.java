package beans;

import javax.naming.*;
import p1.CalcRemote;

public class SessionClntBean {
	private InitialContext ctx;
	private CalcRemote ref;
	// props of JB
	private String action;
	private double num1, num2;
	private String out;

	public SessionClntBean() throws Exception {
		ctx = new InitialContext();
		ref = (CalcRemote) ctx.lookup("my_calc");
	}

	public String getAction() {
		if (ref != null && action.equals("add"))
			return "" + ref.add(num1, num2);
		return "";
	}

	public void setNum1(double num1) {
		this.num1 = num1;
	}

	public void setNum2(double num2) {
		this.num2 = num2;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getOut() {
		if(ref != null)
			ref.logout();
		ref=null;
		return "";
	}

}
