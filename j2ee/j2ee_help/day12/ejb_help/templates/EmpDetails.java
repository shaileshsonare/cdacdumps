package slsb_emp;

import java.sql.*;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import exc.EmpNotFoundException;

/**
 * Session Bean implementation class EmpDetails
 */
@Stateless(mappedName = "emp_dtls")
public class EmpDetails implements EmpDetailsRemote, EmpDetailsLocal {
	@Resource(mappedName="java:ora_pool")
	DataSource ds;
    /**
     * Default constructor. 
     */
    public EmpDetails() {
        // TODO Auto-generated constructor stub
    }
    
    @PostConstruct
    public void init()
    {
    	System.out.println("in postCon "+getClass().getName());
    	
    }

	@Override
	public String getEmpDtls(int id) throws EmpNotFoundException {
		// TODO Auto-generated method stub
		String result=null;
		Connection cn=null;
		try {
			cn=(Connection) ds.getConnection();
			String sql="select * from my_emp where id=?";
			PreparedStatement pst=cn.prepareStatement(sql);
			pst.setInt(1,id);
			ResultSet rst=pst.executeQuery();
			if (rst.next())
				result="Emp Dtls : Name "+rst.getString(2)+" sal "+rst.getDouble(6);
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				cn.close();
				return result;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
		
	}
    

}
