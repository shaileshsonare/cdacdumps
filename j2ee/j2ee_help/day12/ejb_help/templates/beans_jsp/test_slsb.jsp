<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Session Bean Tester</title>
</head>
<jsp:useBean id="test" class="beans.SessionClntBean" scope="session"/>
<body>

<form action="test_slsb.jsp">Enter Number 1 <input type="text"
	name="num1" /><br />
Enter Number 2 <input type="text" name="num2" /><br />
<input type="submit" name="action" value="add" /><br />
</form>

<c:if test="${param.action eq 'add'}">
<jsp:setProperty name="test" property="*" />
${test.action}
</c:if>
<a href="test_slsb.jsp?num1=${param.num1}&num2=${param.num2}&action=add">Refresh </a></h3>
<a href="logout.jsp">LogOut</a>
<br />


</body>

</html>