1. @Entity : class level
2. @Table(name="upper cased table name") : class level
For Named queries : static queries : query syntax (JPQL/EJB QL) does not change, but IN params can still be suppiled
@NamedQueries({@NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a"), @NamedQuery(name = "Account.findByBal", query = "SELECT a FROM Account a WHERE a.bal > :bal")})


3. @Id : can be field level or method level.
4. @GeneratedValue(strategy=GenerationType.AUTO) --> id generator supplied by persistence  provider(app srvr's ) & not by DB
5. @Column(name="upper-cased  col name of prim key colmn") --> not mandatory if same names
3,4,5 : applicable to Entity id property
6.@Column(name="upper-cased  col name") : for rest of prop to col names mapping(optional)	
7. For SLSB Facade Bean : for injecting(making it availbale) the EntityManager 
@PersistenceContext(unitName="PU name used in persistence.xml")
private Entitymanager mgr;


8.
Using the Named queries from Facade bean :
Query q= em.createNamedQuery("Account.findByBal"); q.setParameter("bal", d1);
& then getResultList()/getSingleResult

8. For extended persistence ctx :
Add this anno. in SFSB class for EntityManager
@PersistenceContext(unitName="my_jpa",type=PersistenceContextType.EXTENDED)
EntityManager em;
9. Some query samples
EntityManager em;
    9.1 
List<Person>list=em.createQuery("from Person").getResultList(); 
9.2 To get single result
Person p = (Person) em.createQuery("from Person").getSingleResult(); 
9.3 Find entity by its prim key
Person p = em.find(Person.class, personKey);
