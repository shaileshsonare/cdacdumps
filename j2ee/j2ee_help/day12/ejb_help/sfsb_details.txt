Thick clnts use RMI/IIOP (Internet based Inter ORB protocol --- ORB Object request broker  ----- protocol is similar to JRMP --- but more flexible . Uses JNDI port for connectivity --- For JBoss ---1099
If u have changed port nos ---  use changed port nos)

SFSB ---- less performant (less optimized / less favourite) than SLSB.
Represent 1 clnt (thick/thin) in EJBC
state of the clnt ----is represented by data members of SFSB
EJBC can auto persist the state of SFSB ---till ??????????????

Pre-requisite --SFSB is deployed with its JNDI name entry existing in JNDI tree of app srvr.

Desc of SFSB life-cycle
1. Thin clnt---via JSP/Servlet (using HTTP/s protocol) or Thick clnt
via RMI/IIOP(skipping WC completely) --- performs JNDI lookup.
2. In EC --- EJBObj, EJBObjStub is created---stub get dyn dowaloaded to clnt --- clnt stores the stub in B.I ref.
3. Unlike SLSB --- Step 2 will definitely result into --- 
3.1 EC will inst SFSB. 
3.2 If dependencies are required ---- they will be auto injected by EJBC into SFSB.---optional 
3.3 EJBC invokes the method anno. with @PostConstruct---optional
4. After step 3 ---- SFSB inst enters into Method rdy stage.---rdy for servcing clnts.
Details ---Cache ---SFSB in this stage --- maintains Clnt state ----In this state only , it can server clnts.

5. Triggers ---for SFSB
5.1 Trigger for Doesn't Exist----> M.R. stage ----- clnt performs JNDI look-up.
5.2 Trigger for  M.R. stage -----> Doesn't Exist ----- clnt invokes a method anno. with @Remove.(This implies ---- method with @Remove anno method must be declared in B.I)

6. Transition from Active (Meth rdy) ---- Passive 
Triggers ---- Type of Cache.
NRU --- Not recently used --- Lazy passivation---  reqd no of beans exceed cache max size.  
LRU ---Least recently used --- Eager passivation --- any bean inst exceed idle tmout .

In either of above cases ---SFSB inst will be chosen for passivation.

7. What is passivation ?(transition from active---->)passive
7.1 EJBC invokes meth annotated with @PrePassaivate --- to release the non-java resources(eg --- close i/o strms, closing DB cn,closing of EntityMgr,....) ---optional
7.2 Mandatory --- serialization---- EJBC extracts state of obj(SFSB) & stores it on secondary storage(Def --- File/ optional ---DB)
7.3 SFSB is passivated.
7.4 MOST IMP -----Its the connection bet ejb obj & SFSB inst ---which is broke at the time of passivation. . BUT EJB obj maintains a link  to where state of clnt is stored --- so that it can load the same state at the time of activation.

8. What is activation ?= de-serialization=un-marshalling
EJBC invokes activation --- if clnt invokes a method on EJB object ---whose bean inst is passivated.

8.1 Loads the state back into ANY bean inst from the passive stage .
8.2 Invokes method anno. with @PostActivate ---if any
8.3 SFSB returns to the active stage & continues to serve the clnt.

9 Transition from passive ---> does not exist.
If any bean inst in passive stage exceed ---passive state tmout ---- EJBC will garbage collect it & sends it to Doesn't exist state.


Implementation Objective ---- Shopping cart implemented using SFSB.
Client --- thick clnt --- Text console based application.
Server side steps ---
1.Identify B.L --- declare the same in POJI.
(cart_sfsb\ShoppingCart.java)
To support -- thick clnts --- POJI must be anno with @Remote.

2. Imple B.L --in POJO(SFSB based)
JNDI name in app srvr inde manner
eg --- my_ear/ShoppingCartBean/remote
Syntax --- name of EAR project/EJB class name/remote ---thick clnts.
If ur POJI is annotated with ---- @Local
Syntax --- name of EAR project/EJB class name/local ---thick clnts.

Example of ready-imple of Primary services ---
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
--- At the time of deployment --- EJBC reads these anno --- & creates EJB Obj class --- In run tm ---clnt invokes a method on B. I ref---- stub ----EJB Obj --- it must begin a NEW transaction ----then Method invocation reaches SFSB ----if meth rets success --tx is committed or ow. rolled back.


3. Deploy EJB --- R-click on ejb project ---Run on server---If its embedded in EAR --EAR itlself will get deployed.


Thick Appln clnts steps --follow from readme_ejb0

















