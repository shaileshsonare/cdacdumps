dyn web proj---copy jars--hibernate.cfg.xml ---src (edit)--either write anno pojos or create rdymade rev eng pojos ---utils ---listener--JSP 
1. create -- 
get sess ---begin tx ---sess.save(b) --- entity is persistent --- tx.commit() --sess.close()

save() -- rets Serializable ID , persist() --- rets void (insert)

2. retrieve --- by ID
2.1 
get sess ---begin tx --sess.get(BookPOJO.class,101) ---- rets persistent POJO ref if id found or null ow. ----tx.commit() --sess.close()
get --- rets data always --in eager manner
2.2 
get sess ---begin tx --sess.load(BookPOJO.class,101) ---- rets persistent POJO ref if id found or throws HibernateExc  ----tx.commit() / tx.rollback() --sess.close()
load ---lazy fetching policy

3. retrieve by HQL
String hql="select u from UserPOJO u where u.email=:em and u.pass=:pa";
open sess--begin tx -- UserPOJO u= (UserPOJO) sess.createQuery(hql).setParameter("em",email).setParameter("pa",password).uniqueResult()
rets --- null or not null

4. update ---- hql (select) --- selected persistent pojos --- update obj state
OR hql --- update query syntax --- executeUpdate()

5. delete  ---- hql (select) --- selected persistent pojos --- deleting pojos 
OR hql --- delete query syntax --- executeUpdate()

6. hql -- insert query syntax is also supported BUT its NOT JPA compliant.
JPQL(Java persistence query) doesn't support it.

7. Associations ---- bi-dir one----many
write in bankUserPOJO
@OneToMany (mappedBy="bankUser)
private List<BankAccounts> accts;


write in BankAccounts ---
@ManyToOne
@JoinColumn(name="user_id") ---- user_id --- FK in accts table

1st URL pattern
http://localhost:9090/mvc/    ---rq1



http://localhost:9090/mvc/controller?.....&command=Login    ---rq2


http://localhost:9090/mvc/controller?emailID=cc%40gmail&pass=123&command=Login












what is scope of hibernate session or persistence context ? 
Duration from sf.openSession ------ sess.close()
For persistent POJO/Entity --- Hibernate maintains auto. synchronization between state of object & state of DB(update/select/insert queries generated auto)
For Detached POJO --- no such synch is maintained.