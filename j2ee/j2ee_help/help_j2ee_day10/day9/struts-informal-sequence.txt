<package name="basic" namespace="/basic" extends="struts-default">
<action name="hello">
 <result>struts_welcome.jsp</result>
</action>
</package>

1. root tag <struts> ........</struts>
Single  struts.xml --- can consist of multiple pkges
2.Each pkg --- mandatory attribute --- name ---unique within struts.xml --- identifier of the pkg.

optional attrs 
extends="struts-default" => to tell Struts frmwork --- to apply default settings --- how to inherit def configuration from struts-default.xml 
3. Each package can consist of ----collection of Business actions
4. Struts controller(Filter) --- search for matching action name ---last part of URL -.action suffix --- eg : action name =hello --- if not found error is reted to clnt --
if matching action name is found --- S.C (struts controller) will chk --- for class attribute ---yes or no --- not found ----S.C loc/load/insts ActionSupport class --- invokes method ----public String execute() ---- rets "success" as outcome ----control is reted to S.C---chks if any matching result exists --tries to match outcome with result name --- upon finding match -----invokes RequestDispatcher rd=req.getRequestDispatcher("welcome.jsp")---rd.forward(rq,rs) ----view layer ---generates dyn resp & rets it thro chain of interceptors to the clnt browser.

<result name="success" type="dispatcher">welcome.jsp</result>


namespace="/basic" ----namespace appears in URL --- after name of the web appln

URL ---- to print welcome msg from struts
http://localhost:9090/day9/basic/hello.action






