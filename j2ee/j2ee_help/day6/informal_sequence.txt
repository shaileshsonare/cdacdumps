why JBs? 
clean sep ----JSP ---- P.L dyn resp gen, nav logic , WC --- req processing , JB --- B.L & stores clnt's conv state, re-usable comp,easy conv bet rep params --- props of JB
eg --- email,pass,name,subAmt,age,regDate(String --> prim type auto performed by WC, BUT String ---> Date, GregorianCalendar,Calendar or any UDT --- must be performed by prog --- in setter method of JB)
what is JB specs? --- pub,pkged , def constr , private D.M ---props of JB(non-static , non tx) --- per prop --- setter --- public void setPropName(Type val),  to acces the prop from JSP --- getter --- public Type getPropName()
+ B.L --- public 
JSP actions Vs JSP direcives --- 
JSP using JB related actions 
JB ---- server side obj / attribute
JB's id , name---- attr name
JB inst --- attr value
to add it in any scope --- scope attr of useBean
<jsp:useBean id="user" class="beans.UserBean" scope="session"/> ---- P --- constr of JB
---sess.getAttr("user") --- null/not null 
null-----loc/load/inst----u1 (Userbean u1=new Userbean()) --- sess.setAtribute("user",u1)


<jsp:setproperty name="user" property="*"/> --- P --- setters of JB
WC invokes ONLY matching setters(rq param names matching with JB prop names)

<jsp:getProperty name="user" property="subAmt"/> ---- P --- getter of JB
out.print((UserBean)session.getAttribute("user")).getSubAmt().toString())
OR EL 
${sessionScope.user.subAmt}
  BL --- can't be invoked from JSP w/o using a scriptlet ---- simple soln --- re-name B.L methods as getter
eg --- public String getStatus() --- B.L to validate user

From JSP --- ${user.status}

JSP 's RD related actions
<jsp:forward page="${user.status}.jsp" />

JSP's implicit objs ---- session,request,out....pageContext----javax.servlet.jsp.PageContext(abstract class) ---represents entire page env--- all impl objs are available to PC (eg --- ${pageContext.exception.message} ----- 
+ set / get attribute in page scope
HOW ? 
<% // how to add an attr to page scope?
pageContext.setAtrribute(name,val);

%>
// how to get an attr from page scope?
<%= pageContext.getAttribute(name) %>
EL --- 
${name}


Objective  --- links --- view a/c summary -- for multiple a/cs & logout , session listener
POJO --- BankUserPOJO ----id,name,pass , ArrayList<BankAcctPOJO> accts
BankAcctPOJO ---- acct id,type, bal






Reminders TODO ---
debugger, serialization readme(refer to readme) , getSession, init overloaded , how to pass jsp init  params

API of HttpServletRequest 
HttpSession getSession() --- new user ----WC creates new HttpSession, if old user ---alrdy created HttpSession obj is reted to the caller.

Vs 
HttpSession getSession(boolean create)
create -- true => if new user WC creates new HttpSession, if old user ---alrdy created HttpSession obj is reted to the caller.

create - false => if old user ---alrdy created HttpSession obj is reted to the caller.
BUT new user => HttpSession obj is not created (this is used to disable session tracking)

similar in JSP --- session="false"

javax.servlet.Servlet ---- i/f
public void init() throws SE
---ServletConfig sc=getServletConfig();......
Vs
public void init(ServletConfig sc) throws SE
--- super.init(sc);
sc.getInitParam......

 how to pass jsp init  params?
shown in day6 's web.xml

How to modify session expiration tmout?
1. Prog --- API ---HttpSession
public void setMaxInactiveInterval(int secs) ---throws IllegalStateExc ---- if HS is not yet created.
2. Via web.xml ----

What if user doesn't vist logout page & resources(eg DB cns) must be closed?
Soln --- Use HttpSessionListener
Steps 
1. Create Listener class ---- by implementing javax.servlet.http.HttpSessionListener i/f
2. For registering --- either annotate it with 
@WebListener OR by an entry of <listener><listener-class> F.Q listener cls name</listener-class>
</listener> 
3. Implement 2 methods 
public void sessionCreated(HttpSessionEvent hse)
public void sessionDestroyed(HttpSessionEvent hse)

4. How to get HttpSession --- from HttpSessionEvent?
HttpSession getSession();





































JB Vs POJO ----
1. POJO setter / getter conventions not mandatory, BUT JB --- if u dont follow conventions --- props will not get/set(reflected by WC)
2. POJO -- represents typically Class equivalent of DB table (class --- Table/s, prps -- table cols, POJO cls inst --- rows of table)
JB --- used in web scenario -- represents clnt's converted, validated conversational state & supplies B.L







