Revise 
JavaBeans --- why ---- clean separation between P.L & B.L ---JSP --- navigation logic, P.L
JB s--- B.L , re-usability,easy coversion from String(request params) ---> Java data types (primitive)
eg --- email,pass,subAmt,age,regDate ---req params-- string
JB -- email(String),pass(String),age(int),subAmt(double),regDate(Date)---Prog
Rules --- public,pkged , Serializable(to add bean to session scope),properties --- private, non-static,non transient --- per prop --- setter--- public void setPropName(Type val), getter---public Type getPropName()-- otherwise properties are not set or retrieved by WC
B.L --- public methods.

JSP using JavaBean actions? ---
Javabean --- attr 
JB id --- attr name
JB inst -- attr value
can be added to any of the scopes ---page...application

<jsp:useBean id="user" class="bean.UserBean" scope="session"/> ----P
WC --- session.getAttribute("user")---- null or not-null
null---- loc/load/inst---adds bean inst in the given scope ----session.setAttribute("user",u1)  --- rets
not-null-----rets

<jsp:setProperty name="user" property="*"/>
---WC invokes ONLY MATCHING setters(rq param names matching with bean prop names)

<jsp:getProperty name="user" property="subAmt"/> ----
out.write((UserBean) session.getAttribute("user")).getSubAmt().toString());
OR 
${sessionScope.user.subAmt}


B.L  ---public String validateUser() {......}----re-name the method as pseudo getter---
public String getStatus(){...} 
EL --- ${sessionScope.user.status}

JSP using Request dispatcher actions
<jsp:forward page="dispatcher URI" />
eg --- <jsp:forward page="${sessionScope.user.status}.jsp"/>


How to modify session expiration tmout?
1. Programmatic  --- API ---HttpSession
public void setMaxInactiveInterval(int secs) ---throws IllegalStateExc ---- if HS is not yet created.
2. Via web.xml ----

What if user doesn't visit logout page & resources(eg DB cns) must be closed?
Soln --- Use HttpSessionListener
Steps 
1. Create Listener class ---- by implementing javax.servlet.http.HttpSessionListener i/f
2. For registering --- either annotate it with 
@WebListener OR by an entry of <listener><listener-class> F.Q listener cls name</listener-class>
</listener> 
3. Implement 2 methods 
public void sessionCreated(HttpSessionEvent hse)
public void sessionDestroyed(HttpSessionEvent hse)

4. How to get HttpSession --- from HttpSessionEvent?
HttpSession getSession();

Current objective --- clean up the resources ---even when user doesn't reach logout page
from sessionDestroyed --- 
HttpSessionEvent --- HS ---BankBean ---logout----dao's cleanup








