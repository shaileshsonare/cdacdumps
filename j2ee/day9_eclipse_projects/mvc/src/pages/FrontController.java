package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FrontController
 */
@WebServlet("/")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//check command & forward user accordingly
		String cmd=request.getParameter("command");
		if (cmd == null)
		{
			RequestDispatcher rd=request.getRequestDispatcher("login.jsp");
			if (rd != null) 
				rd.forward(request, response);
			else
				System.out.println("rd not created for JSP. ....");
			return;
		}
		if (cmd.equals("Login"))
		{
			RequestDispatcher rd=request.getRequestDispatcher("login_servlet");
			if (rd != null)
				rd.forward(request, response);
			else
				System.out.println("rd not created for servlet.....");
			return;
		}
	}

}
