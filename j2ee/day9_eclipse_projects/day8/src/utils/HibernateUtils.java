package utils;

import org.hibernate.*;
import org.hibernate.cfg.*;
import org.hibernate.service.*;

public class HibernateUtils {
	private static SessionFactory sf;
	
	static {
		// create empty cfg, load settings from hibernate.cfg.xml
		Configuration cfg = new Configuration().configure();
		// create service reg ---- empty Builder inst ---apply settings --- map
		// ---hib.cfg.xml props
		ServiceRegistry reg = new ServiceRegistryBuilder().applySettings(
				cfg.getProperties()).buildServiceRegistry();
		sf=cfg.buildSessionFactory(reg);// hib cfg is loaded ---cn pool is rdy
		System.out.println("sf created ");
	}
	public static SessionFactory getSf() {
		return sf;
	}
	public static Session getSession() {
		return sf.openSession();//cn is pooled out & wrapped in HS
	}
	

}
