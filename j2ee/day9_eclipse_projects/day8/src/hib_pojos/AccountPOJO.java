package hib_pojos;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="my_bank_accts")
public class AccountPOJO implements Serializable {
	private String type;
	private double balance;
	public AccountPOJO() {
		System.out.println("in def ap");
	}
	public AccountPOJO(String type, double balance) {
		super();
		this.type = type;
		this.balance = balance;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	

}
