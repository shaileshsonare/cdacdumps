package hib_pojos;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="my_users1")
public class UserPOJO implements Serializable {
	@Id
	private String email;
	private String name;
	private String pass;
	public UserPOJO() {
		// TODO Auto-generated constructor stub
	}
	public UserPOJO(String email, String name, String pass) {
		super();
		this.email = email;
		this.name = name;
		this.pass = pass;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	@Override
	public String toString() {
		return "UserPOJO [email=" + email + ", name=" + name + ", pass=" + pass
				+ "]";
	}
	
	
}
