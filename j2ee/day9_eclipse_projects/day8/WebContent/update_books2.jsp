<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="hib_pojos.BookPOJO"%>
<%@page import="utils.HibernateUtils"%>
<%@page import="org.hibernate.*"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<%!SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");%>
<body>
	<center>
		<form>
			Enter Publish Date <input type="text" name="f1" /><br /> Enter
			Discount <input type="text" name="f2" /><br /> <input type="submit"
				name="btn" value="Update Books" /><br />
		</form>


		<br /> <br />
		<c:if test="${param.btn == 'Update Books'}">
			<%
				String sts = "Books Not Updated";
					Date d1 = sdf.parse(request.getParameter("f1"));
					double disc = Double.parseDouble(request.getParameter("f2"));
					Session hs = null;
					Transaction tx = null;
					List<BookPOJO> books = null;
					try {
						hs = HibernateUtils.getSession();
						tx = hs.beginTransaction();
						//CRUD
						//from session ---query - ret all old books
						String hql = "update BookPOJO b set b.price=b.price - :disc where b.publicationDate < :dt";
						//set In params --exec it ---get rdy made populated book list
						int updateCount = hs.createQuery(hql)
								.setParameter("disc", disc).setParameter("dt", d1)
								.executeUpdate();
						sts = "Updated Books successfully!!!!";

						/* 		for (BookPOJO b : books)
									b.setPrice(b.getPrice()-disc);//updating state of selected pojos
						 */tx.commit();
					} catch (Exception e) {
						if (tx != null)
							tx.rollback();
					} finally {
						if (hs != null)
							hs.close();
					}
			%>
			<h3><%=sts%></h3>
		</c:if>
	</center>
</body>
</html>