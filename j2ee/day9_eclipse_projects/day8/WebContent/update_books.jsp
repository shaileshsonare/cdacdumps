<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="hib_pojos.BookPOJO"%>
<%@page import="utils.HibernateUtils"%>
<%@page import="org.hibernate.*"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<%!SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");%>
<body>
	<center>
		<form>
			Enter Publish Date <input type="text" name="f1" /><br /> Enter
			Discount <input type="text" name="f2" /><br /> <input type="submit"
				name="btn" value="Update Books" /><br />
		</form>


		<br /> <br />
		<c:if test="${param.btn == 'Update Books'}">
			<%
				Date d1 = sdf.parse(request.getParameter("f1"));
					double disc = Double.parseDouble(request.getParameter("f2"));
					Session hs = null;
					Transaction tx = null;
					List<BookPOJO> books = null;
					try {
						hs = HibernateUtils.getSession();
						tx = hs.beginTransaction();
						//CRUD
						//from session ---query - ret all old books
						String hql = "select b from BookPOJO b where b.publicationDate < :dt";
						//set In params --exec it ---get rdy made populated book list
						books=hs.createQuery(hql).setParameter("dt", d1).list();
						for (BookPOJO b : books)
							b.setPrice(b.getPrice()-disc);//updating state of selected pojos
						tx.commit();
					} catch (Exception e) {
						if (tx != null)
							tx.rollback();
					} finally {
						if (hs != null)
							hs.close();
					}
					pageContext.setAttribute("bk_list", books);
			%>
			<h3>Updated Book List</h3>
			<c:forEach var="bk" items="${bk_list}">
   Book ID ${bk.bookId} Book Title ${bk.bookTitle} Book Price ${bk.price} Book Author ${bk.author} Pub Date  ${bk.publicationDate}<br />
			</c:forEach>
		</c:if>
	</center>
</body>
</html>