package com.app.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.MovieDao;

import rev_pojos.MovieNewPOJO;

@Service("movie_service")
public class MovieServiceImpl implements MovieService {
	
	@Resource(name="movie_dao")
	private MovieDao dao;

	@Override
	@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
	public List<MovieNewPOJO> getMovies() {
		// TODO Auto-generated method stub
		return dao.getAllMovies();
	}

}
