package com.app.dao;

import java.util.List;

import rev_pojos.MovieNewPOJO;

public interface MovieDao {

	List<MovieNewPOJO> getAllMovies();
}
