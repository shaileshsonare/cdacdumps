package com.app.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import rev_pojos.MovieNewPOJO;

@Repository("movie_dao")
public class MovieDaoImpl implements MovieDao {

	@Resource(name = "sessionFactory")
	private SessionFactory factory;

	@SuppressWarnings("unchecked")
	@Override
	public List<MovieNewPOJO> getAllMovies() {
		// TODO Auto-generated method stub
		return factory.getCurrentSession()
				.createQuery("select m from MovieNewPOJO m").list();
	}

}
