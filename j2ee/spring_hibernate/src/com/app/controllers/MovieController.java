package com.app.controllers;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.service.MovieService;

@Controller
@RequestMapping("/movies")
public class MovieController {
	
	@Resource(name="movie_service")
	private MovieService service;
	@RequestMapping("/list")
	public String listMovies(Model m)
	{
		m.addAttribute("movie_list",service.getMovies());
		return "list";
	}

}
