package rev_pojos;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the movies database table.
 * 
 */
@Entity
@Table(name="movies")
public class MovieNewPOJO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String category;

	private String language;

	private String name;

	private double price;

    @Temporal( TemporalType.DATE)
	@Column(name="release_date")
	private Date releaseDate;

    public MovieNewPOJO() {
    	System.out.println("in movie constr.....");
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getReleaseDate() {
		return this.releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Override
	public String toString() {
		return "MovieNewPOJO [id=" + id + ", category=" + category
				+ ", language=" + language + ", name=" + name + ", price="
				+ price + ", releaseDate=" + releaseDate + "]";
	}
	

}