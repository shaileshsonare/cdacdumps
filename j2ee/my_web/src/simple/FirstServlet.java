package simple;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.Date;
import javax.servlet.annotation.*;

//annotation meant for WC --- to tell whatever follows is Http Servlet --- invoke its life cycle --- make it available at /hello
@WebServlet("/hello")
public class  FirstServlet extends HttpServlet
{
	@Override
		public void doGet(HttpServletRequest rq,HttpServletResponse rs) throws ServletException,IOException
	{
		//set content type
		rs.setContentType("text/html");
		//open data strm to write resp to clnt
		try(PrintWriter pw=rs.getWriter()) {
			//generate hello with changing time stamp 
			//send the same to clnt
			pw.print("<h3> Welcome 2 Servlets .... @"+new Date()+"</h3>");
		}
	}
}
