package calc;

import javax.ejb.Remote;

@Remote
// to inform EJBC -- following is rem business i/f fro session bean
// -- remote clnt can invoke these methods
public interface Calculator {
	double add(double d1, double d2);
	void doLogOut();
}
