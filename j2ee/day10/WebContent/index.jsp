<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<a href="basic/hello123.action">Test Struts</a><br>
<s:url var="url" action="show_login_form" namespace="basic"/>
<a href="${url}">Simple Login </a><br/>

<s:url var="url" action="show_reg_form" namespace="basic"/>
<a href="${url}">Simple Registration </a><br/>

<s:url var="url" action="show_login1_form" namespace="basic"/>
<a href="${url}">Simple Login with MDI</a><br/>


</body>
</html>