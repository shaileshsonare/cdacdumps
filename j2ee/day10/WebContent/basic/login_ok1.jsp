<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="/struts-tags"  prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h3>Login Successful with MDI<br/>
Hello ${param.email} <br/>
via OGNL <s:property value="email"/><br/>
via OGNL <s:property value="u"/><br/>
via OGNL again <s:property value="details"/>
</h3>

</body>
</html>