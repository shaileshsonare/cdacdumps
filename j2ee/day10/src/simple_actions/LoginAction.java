package simple_actions;

import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport {
	private String email, pass;

	public LoginAction() {

		System.out.println("in login action constr");
	}

	@Override
	public String execute() throws Exception {
		System.out.println("in exec");
		if (email.equals("a@gmail") && pass.equals("123"))
			return SUCCESS;
		return ERROR;
	}

	public String getEmail() {
		System.out.println("in get email");
		return email;
	}

	public void setEmail(String email) {
		System.out.println("in set email");
		this.email = email;
	}

	public void setPass(String pass) {
		System.out.println("in set pass");
		this.pass = pass;
	}

}
