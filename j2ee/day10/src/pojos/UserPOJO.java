package pojos;

public class UserPOJO {
	private String email,pass;
	private double subAmt;
	public UserPOJO() {
		System.out.println("in user pojo constr");
	}
	public UserPOJO(String email, String pass, double subAmt) {
		super();
		this.email = email;
		this.pass = pass;
		this.subAmt = subAmt;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public double getSubAmt() {
		return subAmt;
	}
	public void setSubAmt(double subAmt) {
		this.subAmt = subAmt;
	}
	@Override
	public String toString() {
		return "UserPOJO [email=" + email + ", subAmt=" + subAmt + "]";
	}
	
	

}
