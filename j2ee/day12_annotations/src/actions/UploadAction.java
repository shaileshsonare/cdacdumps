package actions;

import java.io.File;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;

@Namespace("/test")
public class UploadAction extends ActionSupport {
	// props
	private File abc;
	private String abcFileName, abcContentType, uploadDest;

	public String getAbcFileName() {
		return abcFileName;
	}

	public void setAbcFileName(String abcFileName) {
		this.abcFileName = abcFileName;
	}

	public String getAbcContentType() {
		return abcContentType;
	}

	public void setAbcContentType(String abcContentType) {
		this.abcContentType = abcContentType;
	}

	public void setAbc(File abc) {
		this.abc = abc;
	}

	public void setUploadDest(String uploadDest) {
		this.uploadDest = uploadDest;
	}

	@Override
	@Action(value = "upload", results = {
			@Result(location = "upload_succ.jsp"),
			@Result(name = "error", location = "upload_err.jsp") }, params = {
			"uploadDest", "e:/uploaded_contents/" })
	public String execute() throws Exception {
		// file upload interceptor has alrdy uploaded contents --- tmp folder
		// so simply store them in specified folder --- sepcified by action
		// parameter.
		System.out.println("in anno action " + abc.getAbsolutePath());
		File dest = new File(uploadDest, abcFileName);
		boolean sts = abc.renameTo(dest);
		if (sts)
			return SUCCESS;
		return ERROR;
	}

}
