package actions;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import com.opensymphony.xwork2.ActionSupport;

@Namespace("/test")
public class ShowFormAction extends ActionSupport {
	
	@Action(value="showForm",results={@Result(location="upload.jsp")})
	public String execute() throws Exception
	{
		System.out.println("in exec of show form");
		return SUCCESS;
	}
	

}
