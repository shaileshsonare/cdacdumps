<%@page import="cust_exc.BrowserNotSupportedException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="err_handler.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<%!String supportedBrowser = "FireFox";%>
<body>
	<%
		String selectedBrowser = request.getParameter("browser");
		if (selectedBrowser.equals(supportedBrowser)) {
	%>
	<%@ include file="correct.jsp"%>
	<%
		} else
			throw new BrowserNotSupportedException(
					"Sorry, Your selection not yet supported"
							+ selectedBrowser);
	%>
</body>
</html>