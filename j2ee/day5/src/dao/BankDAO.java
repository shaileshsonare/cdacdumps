package dao;

import java.sql.*;

import java.io.Serializable;

import pojos.BankUserPOJO;

public class BankDAO implements Serializable {
	private Connection cn;
	private PreparedStatement pst;

	public BankDAO() throws Exception {
		// load drvr cls
		Class.forName("com.mysql.jdbc.Driver");
		// get cn --- fixed
		cn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/testjdbc", "root", "root");
		// pst
		String sql = "select id from bank_users where name=? and password=?";
		pst = cn.prepareStatement(sql);

	}
	public void cleanup() throws Exception {
		if (pst != null)
			pst.close();
		if (cn != null)
			cn.close();
	}
	public BankUserPOJO validateUser(String nm,String pass) throws Exception
	{
		ResultSet rst=null;
		try {
			//set in
			pst.setString(1,nm);
			pst.setString(2,pass);
			rst=pst.executeQuery();
			if(rst.next())
				return new BankUserPOJO(rst.getInt(1),nm,pass);
			
		} finally {
			if (rst != null)
				rst.close();
		}
		return null;
				
	}
}
