package beans;

import java.io.Serializable;
import java.util.HashMap;

import pojos.UserPOJO;

public class UserBean implements Serializable {
	//props --- reflect state of clnt ---- clnt sent params
	private String userEmail,userPass;
	//internal prop --meant for validation
	private HashMap<String, UserPOJO> users;
	//for displaying validated user dtls to clnt
	private UserPOJO userDetails;
	//def Constr
	public UserBean() {
		System.out.println("in user bean constr");
		//empty hm
		users=new HashMap<>();
		//fill it
		users.put("a@gmail", new UserPOJO("a@gmail","123", "abc", 345));
		users.put("b@gmail", new UserPOJO("b@gmail","1234", "abc1", 500));
		users.put("c@gmail", new UserPOJO("c@gmail","1235", "abc2", 200));
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserPass() {
		return userPass;
	}
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
	public HashMap<String, UserPOJO> getUsers() {
		return users;
	}
	public void setUsers(HashMap<String, UserPOJO> users) {
		this.users = users;
	}
	public UserPOJO getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(UserPOJO userDetails) {
		this.userDetails = userDetails;
	}
	public String getStatus()
	{
		String sts="user_err";
		if (users.containsKey(userEmail))
			if(users.get(userEmail).getPassword().equals(userPass)){
				sts="user_valid";
				userDetails=users.get(userEmail);
			}
		return sts;
	}
	

}
