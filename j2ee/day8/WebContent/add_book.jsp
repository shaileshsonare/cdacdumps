<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="org.hibernate.*,hib_pojos.BookPOJO,utils.HibernateUtils"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<%!
    SimpleDateFormat sdf=new SimpleDateFormat("dd-MMM-yyyy");
%>
<body>
<%
//create transient BookPOJO 
    Date d1=sdf.parse(request.getParameter("f4"));
	BookPOJO b1 = new BookPOJO(request.getParameter("f1"),
				request.getParameter("f2"), Double.parseDouble(request
						.getParameter("f3")),d1);
	Session hibSession=null;
	Transaction tx=null;
	try {
		//from sf --- get session
		hibSession=HibernateUtils.getSession();
		//begin tx
		tx=hibSession.beginTransaction();
		//CRUD work
		out.print("Book added with ID "+hibSession.save(b1));
		tx.commit();
	} catch (Exception e)
	{
		System.out.println("err in add bk "+e);
		if (tx != null)
			tx.rollback();
	} finally {
		if (hibSession != null)
			hibSession.close();
	}

   
%>
</body>
</html>