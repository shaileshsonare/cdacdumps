<%@page import="java.util.List"%>
<%@page import="hib_pojos.BookPOJO"%>
<%@page import="utils.HibernateUtils"%>
<%@page import="org.hibernate.*"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<form>
			Enter Book Author <input type="text" name="f1" /><br /> Enter Upper
			Price Limit <input type="text" name="f2" /><br /> <input
				type="submit" name="btn" value="Get Book Details By Author N Price" /><br />
		</form>


		<br /> <br />
		<c:if test="${param.btn == 'Get Book Details By Author N Price'}">
			<%
				String author1 = request.getParameter("f1");
					double price1 = Double.parseDouble(request.getParameter("f2"));
					Session hs = null;
					Transaction tx = null;
					List<BookPOJO> books = null;
					try {
						hs = HibernateUtils.getSession();
						tx = hs.beginTransaction();
						//CRUD
						//from session ---query - set In params --exec it ---get rdy made populated book list
						String hql = "select b from BookPOJO b where b.author = :au and b.price < :pr";
						books = hs.createQuery(hql).setParameter("au", author1)
								.setParameter("pr", price1).list();
						tx.commit();
					} catch (Exception e) {
						if (tx != null)
							tx.rollback();
					} finally {
						if (hs != null)
							hs.close();
					}
					pageContext.setAttribute("bk_list", books);
			%>
			<h3>Selected Book List</h3>
			<c:forEach var="bk" items="${bk_list}">
   Book ID ${bk.bookId} Book Title ${bk.bookTitle} Book Price ${bk.price} Book Author ${bk.author} Pub Date  ${bk.publicationDate}<br />
			</c:forEach>
		</c:if>
	</center>
</body>
</html>