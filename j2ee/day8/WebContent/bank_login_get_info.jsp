<%@page import="rev_pojos.BankUserPOJO"%>
<%@page import="rev_pojos.BankAccountPOJO"%>
<%@page import="java.util.List"%>

<%@page import="utils.HibernateUtils"%>
<%@page import="org.hibernate.*"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<form>
			Enter Bank User Name <input type="text" name="f1" /><br />
			Enter Password <input type="password" name="f2" /><br /> <input
				type="submit" name="btn" value="Login" /><br />
		</form>


		<br /> <br />
		<c:if test="${param.btn == 'Login'}">
			<%
				String nm1 = request.getParameter("f1");
			String pass = request.getParameter("f2");
				
					Session hs = null;
					Transaction tx = null;
					List<BankAccountPOJO> accts = null;
					BankUserPOJO b=null;
					try {
						hs = HibernateUtils.getSession();
						tx = hs.beginTransaction();
						//CRUD
						//from session ---query - set In params --exec it ---get rdy made populated book list
						String hql = "select b from BankUserPOJO b where b.name = :nm and b.password = :pa";
						b=(BankUserPOJO)hs.createQuery(hql).setParameter("nm",nm1).setParameter("pa",pass).uniqueResult();
						accts=b.getBankAccounts();
					 	System.out.println("no of accts "+accts.size()); 
						tx.commit();
					} catch (Exception e) {
						if (tx != null)
							tx.rollback();
					} finally {
						if (hs != null)
							hs.close();
					}
					pageContext.setAttribute("acct_list", accts);
			%>
			<h3>Account Summary</h3>
			<c:forEach var="ac" items="${acct_list}">
   Acct ID ${ac.acNo} Acct Type  ${ac.type} Acct Balance ${ac.balance}<br/>
			</c:forEach>
		</c:if>
	</center>
</body>
</html>