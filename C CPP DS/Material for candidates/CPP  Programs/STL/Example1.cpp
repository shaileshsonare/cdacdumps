#include<iostream>
#include<algorithm>
#include<functional>
#include<vector>
#include<string>
#include<list>
using namespace std;

class Employee
{
	string name;
	int age;
public:
	Employee() : age(0) {}
	Employee(string nm,int a) : name(nm), age(a) {}
	int getAge()
	{
		return age;
	}
	string getName()
	{
		return name;
	}
	friend ostream& operator<<(ostream& stream,const Employee& e)
	{
		stream<<endl<<e.name<<"\t"<<e.age;		
		return stream;
	}
	bool operator>(const Employee& e)
	{
		return age > e.age;
	}
};

class EmpCriteria	// My unary function object
{
	int age;
public:
	EmpCriteria(int a) : age(a) {}
	Employee* operator()(Employee *e)
	{
		return (e->getAge()>age) ? e : 0;
	}
};

void display(vector<Employee*>::iterator F,vector<Employee*>::iterator L)
{
	for(;F!=L;++F)
		if(*F!=NULL)
			cout<<**F<<"\t";
	cout<<endl;
}

void main()
{
	vector<Employee*> v1;

	v1.push_back(new Employee("ABC",25));
	v1.push_back(new Employee("ABC",23));
	v1.push_back(new Employee("ABC",22));
	v1.push_back(new Employee("ABC",21));
	v1.push_back(new Employee("ABC",20));
	v1.push_back(new Employee("ABC",28));
	v1.push_back(new Employee("ABC",30));
	v1.push_back(new Employee("ABC",31));

	vector<Employee*> v2(v1.size());

	int age;
	cout<<"Enter age criteria : ";
	cin>>age;

	transform(v1.begin(),v1.end(),v2.begin(),EmpCriteria(age));

	display(v1.begin(),v1.end());	
	display(v2.begin(),v2.end());
}