#include<iostream>
#include<algorithm>
#include<functional>
#include<vector>
#include<list>
using namespace std;

// class Allocator

template<class T>
class Allocator
{
	typedef T value;
	typedef T& reference;
	typedef const T& const_reference;
	typedef T* pointer;
	typedef const T* const_pointer;
public:
	void allocate(pointer &p,int size)
	{
		p = new T[size];
	}
	void deallocator(pointer p,int size)
	{
		// ignore size

		delete[] p;
	}
	void construct(reference target,value source)
	{
		target = source;
	}
};

// partial specialization for <char*>

void Allocator<char*>::allocate(pointer &p,int size)
{
	p = new T[size];
	for(int i=0;i<size;++i)
		p[i] = new char[20];
}
void Allocator<char*>::deallocator(pointer p,int size)
{
	// use size

	for(int i=0;i<size;++i)
		delete[] p[i];
	delete[] p;
}
void Allocator<char*>::construct(reference target,value source)
{
	strcpy(target,source);
}

// class Array

template<class T,class A = Allocator<T> >
class Array
{
	T *arr;
	int size;
	A allocator;
public:
	Array()	: arr(0), size(0) {}
	Array(int sz,T initVal=T()) : size(sz) 
	{
		allocator.allocate(arr,size);
		for(int i=0;i<size;++i)
			allocator.construct(arr[i],initVal);
	}
	Array(int sz,T *x) : size(sz)
	{
		allocator.allocate(arr,size);
		for(int i=0;i<size;++i)
			allocator.construct(arr[i],x[i]);
	}
	Array(const Array<T,A>& a) : size(a.size)
	{
		allocator.allocate(arr,size);
		for(int i=0;i<size;++i)
			allocator.construct(arr[i],a[i]);
	}
	Array<T,A>& operator=(const Array<T,A>& a) 
	{
		if(this==&a)
			;
		else
		{
			if(size) this->~Array();

			size = a.size;
			allocator.allocate(arr,size);

			for(int i=0;i<size;++i)
				allocator.construct(arr[i],a[i]);
		}
		return *this;
	}
	~Array() 
	{
		allocator.deallocator(arr,size);
		arr = 0;
		size = 0;
	}
	T& operator[](size_t index)
	{
		return arr[index];
	}
	const T& operator[](size_t index) const
	{
		return arr[index];
	}
	friend ostream& operator<<(ostream& stream,const Array<T,A>& a)
	{
		for(int i=0;i<a.size;++i)
			stream<<a[i]<<"\t";
		stream<<endl;
		return stream;
	}
	friend istream& operator>>(istream& stream,Array<T,A>& a)
	{
		for(int i=0;i<a.size;++i)
			stream>>a[i];
		return stream;
	}
	T* begin()
	{
		return arr;
	}
	T* end()
	{
		return arr+size;
	}
};

// function objects

template<class T>
struct Plus
{
	T operator()(T arg1,T arg2)
	{
		return arg1 + arg2;
	}
};

template<class T>
struct Minus
{
	T operator()(T arg1,T arg2)
	{
		return arg1 - arg2;
	}
};

template<class T>
struct Greater		// Binary predicate
{
	bool operator()(T arg1,T arg2)
	{
		return arg1 > arg2;
	}
};

// unary function objects

template<class T>
struct Negate
{
	T operator()(T arg)
	{
		return -arg;
	}
};

template<class T>
struct Logical_Not			// Unary predicate
{
	bool operator()(T arg)
	{
		return !arg;
	}
};

template<class T>
class CplxOpn
{
	T x;
	T y;
public:
	CplxOpn(T _x=T(),T _y=T()) : x(_x), y(_y) {}
	T operator()(T arg1,T arg2)
	{
		return x*arg1 + y*arg2;
	}
};

int MyOpn(int arg1,int arg2)
{
	return arg1 * 10 + arg2 * 20;
}

// unary transform

template<class II,class OI,class UF>
OI Transform(II F1,II L1,OI F2,UF op)
{
	for(;F1!=L1;++F1,++F2)
		*F2 = op(*F1);
	return F2;
}

// binary transform

template<class II1,class II2,class OI,class BF>
OI Transform(II1 F1,II1 L1,II2 F2,OI F3,BF op)
{
	for(;F1!=L1;++F1,++F2,++F3)
		*F3 = op(*F1,*F2);
	return F3;
}

template<class T>
void display(T F,T L)
{
	for(;F!=L;++F)
		cout<<*F<<"\t";
	cout<<endl;
}

void main()
{
	Array<int> a1(5);
	vector<int> a2(5);
	list<int> a3(7);

	for(int i=0;i<5;++i)
	{
		a1[i] = rand() % (i+1);
		a2[i] = i+1;
	}

	Transform(a2.begin()+1,a2.end()-1,a1.begin(),Transform(a1.begin()+1,a1.end(),a2.begin(),a3.begin(),Plus<int>()),Minus<int>());

	list<int> a4(7);

	Transform(a3.begin(),a3.end(),a4.begin(),Negate<int>());

	display(a1.begin(),a1.end());
	display(a2.begin(),a2.end());
	display(a3.begin(),a3.end());
	display(a4.begin(),a4.end());
}