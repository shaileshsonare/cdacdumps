#include<iostream>
#include<string>
using namespace std;

template<class T>
struct Plus
{
	T execute(T arg1,T arg2)
	{
		return arg1 + arg2;
	}
};

template<class T>
struct Minus
{
	T execute(T arg1,T arg2)
	{
		return arg1 - arg2;
	}
};

template<class T>
class CplxOpn
{
	T x;
	T y;
public:
	CplxOpn(T _x=T(),T _y=T()) : x(_x), y(_y) {}
	T execute(T arg1,T arg2)
	{
		return x*arg1 + y*arg2;
	}
};

int MyOpn(int arg1,int arg2)
{
	return arg1 * 10 + arg2 * 20;
}

template<class T,class BF>
void Transform(T *F1,T *L1,T *F2,T *F3,BF op)
{
	for(;F1!=L1;++F1,++F2,++F3)
		*F3 = op.execute(*F1,*F2);
}

template<class T>
void display(T *F,T *L)
{
	for(;F!=L;++F)
		cout<<*F<<"\t";
	cout<<endl;
}

void main()
{
	int a[] = {1,5,9,6,3};
	int b[] = {2,7,5,20,1};
	int c[] = {0,0,0,0,0,0,0,0,0,0};

	Transform(a+1,a+5,b+0,c+0,CplxOpn<int>(10,20));

	display(a,a+5);
	display(b,b+5);
	display(c,c+5);
}