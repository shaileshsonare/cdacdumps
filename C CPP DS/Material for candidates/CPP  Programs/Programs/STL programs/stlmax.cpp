#include <iostream>
using std::cout;
using std::endl;

#include <algorithm>

int main()
{
   cout << "\nThe maximum of 'G' and 'Z' is: " << std::max( 'G', 'Z' );
   cout << "\nThe maximum of 12 and 7 is: " << std::max( 12, 7 );
   cout << endl; 
   return 0;
}

 /* 

The maximum of 'G' and 'Z' is: Z
The maximum of 12 and 7 is: 12

 */       