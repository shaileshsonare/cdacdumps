#include <iostream>
#include <algorithm>
#include <functional>
#include <iterator>
using namespace std;

int main()
{
    int coll[] = { 5, 6, 2, 4, 1, 3 };

    // sort beginning with the first element
    sort (coll, coll+6);
    
    // print all elements
    copy (coll, coll+6,
          ostream_iterator<int>(cout," "));
  //  cout <<coll<< endl;
}

/* 
1 2 3 4 5 6

 */        