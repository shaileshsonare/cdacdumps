
#include <iostream>
#include <set>
#include <string>

using namespace std;

int main( ) {

   set<string> setStr;
   string s = "B";

   setStr.insert(s);
   s = "S";
   setStr.insert(s);
   s = "R";
   setStr.insert(s);
   s = "H";
   setStr.insert(s);

   for (set<string>::const_iterator p = setStr.begin( );p != setStr.end( ); ++p)
      cout << *p << endl;
}