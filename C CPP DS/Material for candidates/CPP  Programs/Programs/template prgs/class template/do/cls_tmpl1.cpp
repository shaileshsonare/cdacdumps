/*class templates*/

#include <iostream.h>

template <class T>
class MyClass {
    T value1, value2;
  public:
    MyClass (T first, T second){
        value1=first; 
        value2=second;
    }
    T getmax ()
    {
      T retval;
      retval = value1>value2 ? value1 : value2;
      return retval;
    }
    
};

int main () {
  MyClass <int> myobject (100, 75);
  
  cout << myobject.getmax();
  
  return 0;
}