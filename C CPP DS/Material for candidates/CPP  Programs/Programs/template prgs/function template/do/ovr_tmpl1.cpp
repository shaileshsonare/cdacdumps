Overload generic method and non-generic method
#include<iostream.h>
#include<string.h>
template<class T>
T min(T a,T b)
{
    cout << "generic";
       return (a<b?a:b);
}

char *min(char *a,char *b)
{
    cout << "not generic";
       return (strcmp(a,b)<0?a:b);
}

int main()
{
  double a=3.56,b=8.23;
  char s1[]="Hello",s2[]="Good";
  cout<<min(a,b)<<endl;
  cout<<min(s1,s2)<<endl;
}