#include<iostream>
using namespace std;

#define ADD(x,y) x+y

template<class T>
T add(const T& arg1,const T& arg2)
{
	return arg1 + arg2;
}

void main()
{
	int a=10;
	int b = 20;

	float x = 12.3f;
	float y = 21.5f;

	cout<<add(a,b)<<endl;
	cout<<ADD(x,y)<<endl;

	char *s1 = "IBM";
	char *s2 = "Gurgoan";

	cout<<add(s1,s2)<<endl;
//	cout<<ADD(s1,s2)<<endl;
}