#include<iostream>
#include<string>
#include<vector>
using namespace std;

template<class T>
class Array
{
	T *arr;
	int size;
public:
	Array()	: arr(0), size(0) {}
	Array(int sz,T initVal=T()) : size(sz) 
	{
		arr = new T[size];
		for(int i=0;i<size;++i)
			arr[i] = initVal;
	}
	Array(int sz,T *x) : size(sz)
	{
		arr = new T[size];
		for(int i=0;i<size;++i)
			arr[i] = x[i];
	}
	Array(const Array<T>& a) : size(a.size)
	{
		arr = new T[size];
		for(int i=0;i<size;++i)
			arr[i] = a[i];
	}
	Array<T>& operator=(const Array<T>& a) 
	{
		if(this==&a)
			;
		else
		{
			if(size) this->~Array();

			size = a.size;
			arr = new T[size];

			for(int i=0;i<size;++i)
				arr[i] = a[i];
		}
		return *this;
	}
	~Array() 
	{
		delete[] arr;
		arr = 0;
		size = 0;
	}
	T& operator[](size_t index)
	{
		return arr[index];
	}
	const T& operator[](size_t index) const
	{
		return arr[index];
	}
	friend ostream& operator<<(ostream& stream,const Array<T>& a)
	{
		for(int i=0;i<a.size;++i)
			stream<<a[i]<<"\t";
		stream<<endl;
		return stream;
	}
	friend istream& operator>>(istream& stream,Array<T>& a)
	{
		for(int i=0;i<a.size;++i)
			stream>>a[i];
		return stream;
	}
};

void main()
{
	int x[] = {12,34,32,45,65};

	Array<int> a1;
	Array<int> a2(5);
	Array<int> a3(5,10);
	Array<int> a4(5,x);
	Array<int> a5(a4);

	a1 = a5;

	cout<<a1<<a2<<a3<<a4<<a5;

	a1[0] = 100;
	cout<<a1[0]<<endl;	

	cin>>a1;
	cout<<a1;
}