#include<iostream>
#include<string>
#include<typeinfo>
using namespace std;

template<class T>
class Array
{
	T *arr;
	int size;
public:
	Array()	: arr(0), size(0) {}
	Array(int sz,T initVal=T()) : size(sz) 
	{
		arr = new T[size];
		for(int i=0;i<size;++i)
			arr[i] = initVal;
	}
	Array(int sz,T *x) : size(sz)
	{
		arr = new T[size];

		for(int i=0;i<size;++i)
			arr[i] = x[i];
	}
	Array(const Array<T>& a) : size(a.size)
	{
		arr = new T[size];
		for(int i=0;i<size;++i)
			arr[i] = a[i];
	}
	Array<T>& operator=(const Array<T>& a) 
	{
		if(this==&a)
			;
		else
		{
			if(size) this->~Array();

			size = a.size;
			arr = new T[size];

			for(int i=0;i<size;++i)
				arr[i] = a[i];
		}
		return *this;
	}
	~Array() 
	{
		delete[] arr;
		arr = 0;
		size = 0;
	}
	T& operator[](size_t index)
	{
		return arr[index];
	}
	const T& operator[](size_t index) const
	{
		return arr[index];
	}
	friend ostream& operator<<(ostream& stream,const Array<T>& a)
	{
		for(int i=0;i<a.size;++i)
			stream<<a[i]<<"\t";
		stream<<endl;
		return stream;
	}
	friend istream& operator>>(istream& stream,Array<T>& a)
	{
		for(int i=0;i<a.size;++i)
			stream>>a[i];
		return stream;
	}
};

// <char*> specialization

class Array<char*>
{
	T *arr;
	int size;
public:
	Array()	: arr(0), size(0) {}
	Array(int sz,T initVal=T()) : size(sz) 
	{
		arr = new T[size];
		for(int i=0;i<size;++i)
		{
			arr[i] = new char[strlen(initVal)+1];	
			strcpy(arr[i],initVal);
		}
	}
	Array(int sz,T *x) : size(sz)
	{
		arr = new T[size];
		for(int i=0;i<size;++i)
		{
				arr[i] = new char[strlen(x[i])+1];	
				strcpy(arr[i],x[i]);
		}		
	}
	Array(const Array<T>& a) : size(a.size)
	{
		arr = new T[size];
		for(int i=0;i<size;++i)
		{
			arr[i] = new char[strlen(a[i])+1];	
			strcpy(arr[i],a[i]);
		}
	}
	Array<T>& operator=(const Array<T>& a) 
	{
		if(this==&a)
			;
		else
		{
			if(size) this->~Array();

			size = a.size;
			arr = new T[size];
			for(int i=0;i<size;++i)
			{
				arr[i] = new char[strlen(a[i])+1];	
				strcpy(arr[i],a[i]);
			}
		}
		return *this;
	}
	~Array() 
	{
		for(int i=0;i<size;++i)
			delete arr[i];
		delete[] arr;
		arr = 0;
		size = 0;
	}
	T& operator[](size_t index)
	{
		return arr[index];
	}
	const T& operator[](size_t index) const
	{
		return arr[index];
	}
	friend ostream& operator<<(ostream& stream,const Array<T>& a)
	{
		for(int i=0;i<a.size;++i)
			stream<<a[i]<<"\t";
		stream<<endl;
		return stream;
	}
	friend istream& operator>>(istream& stream,Array<T>& a)
	{
		for(int i=0;i<a.size;++i)
			stream>>a[i];
		return stream;
	}
};

void main()
{
	int x[] = {1,2,3};

	Array<int> a1(3,x);
	cout<<a1;

	char* xStr[] = {"ABC","XYZ","PQR"};

	Array<char*> a2(3,xStr);
	cin>>a2;
	cout<<a2;

}