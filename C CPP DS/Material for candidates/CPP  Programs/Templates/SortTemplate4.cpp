#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

class Employee
{
	string name;
	int age;
public:
	Employee() : age(0) {}
	Employee(string nm,int a) : name(nm), age(a) {}
	friend ostream& operator<<(ostream& stream,const Employee& e)
	{
		stream<<endl<<e.name<<"\t"<<e.age;		
		return stream;
	}
	bool operator>(const Employee& e)
	{
		return age > e.age;
	}
};

template<class T>
void display(T *arr,int size)
{
	for(int i=0;i<size;++i)
		cout<<arr[i]<<"\t";
	cout<<endl;
}

template<class T>
void SwapValues(T &a,T &b)
{
	T temp = a;
	a = b;
	b = temp;
}

template<class T>
void Sort(T *arr,int size)
{
	// selection sort...
	for(int i=0;i<size-1;++i)
		for(int j=i+1;j<size;++j)
			if(compare(arr[i],arr[j]))
				SwapValues(arr[i],arr[j]);
}

template<class T>
bool compare(T& arg1,T& arg2)
{
	return arg1 > arg2;
}

// <char*> specialization

bool compare(char* &arg1,char* &arg2)
{
	return strcmp(arg1,arg2)>0;
}

void main()
{
	Employee x[] = {Employee("ABC",23),Employee("XYZ",19),Employee("PQR",22)};

	display(x,3);

	Sort(x,3);

	display(x,3);	
}