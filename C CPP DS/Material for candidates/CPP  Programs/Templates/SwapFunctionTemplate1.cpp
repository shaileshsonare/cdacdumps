#include<iostream>
using namespace std;

template<class T>
void SWAP(T &a,T& b)
{
	T temp = a;	
	a = b;		
	b = temp;
}

void main()
{
	int a=10;
	int b = 20;

	float x = 12.3f;
	float y = 21.5f;

	cout<<a<<"\t"<<b<<endl;
	SWAP(a,b);
	cout<<a<<"\t"<<b<<endl;

	cout<<x<<"\t"<<y<<endl;
	SWAP(x,y);
	cout<<x<<"\t"<<y<<endl;
}