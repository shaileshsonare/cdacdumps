#include<iostream>
#include<string>
using namespace std;

template<class T>
void display(T *arr,int size)
{
	for(int i=0;i<size;++i)
		cout<<arr[i]<<"\t";
	cout<<endl;
}

template<class T>
void SwapValues(T &a,T &b)
{
	T temp = a;
	a = b;
	b = temp;
}

template<class T>
void Sort(T *arr,int size)
{
	// selection sort...
	for(int i=0;i<size-1;++i)
		for(int j=i+1;j<size;++j)
			if(arr[i]>arr[j])
				SwapValues(arr[i],arr[j]);
}

// <char*> specialization

void Sort(char **arr,int size)
{
	// selection sort...
	for(int i=0;i<size-1;++i)
		for(int j=i+1;j<size;++j)
			if(strcmp(arr[i],arr[j])>0)
				SwapValues(arr[i],arr[j]);
}

void main()
{
	int x[] = {5,4,3,2,1};

	display(x,5);

	Sort(x,5);

	display(x,5);

	char *s[] = {"IBM","Global","India","Services","Gurgoan"};

	display(s,5);

	Sort(s,5);

	display(s,5);
}