#include<iostream>
#include<string>
#include<typeinfo>
using namespace std;

class Base
{
	int *x;
public:
	Base()
	{
		cout<<"Base()"<<endl;
		x = new int[100];
	}
	virtual ~Base() = 0
	{
		cout<<"~Base()"<<endl;
		delete[] x;
	}
};

class Derived : public Base 
{
	int *y;
public:
	Derived()
	{
		cout<<"Derived()"<<endl;
		y = new int[100];
	}
	~Derived()
	{
		cout<<"~Derived()"<<endl;
		delete[] y;
	}
};

void main()
{
	Base *b = new Derived;
	delete b;
}