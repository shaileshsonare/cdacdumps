#include<iostream>
#include<string>
using namespace std;

int Plus(int arg1,int arg2)
{
	return arg1 + arg2;
}

int Minus(int arg1,int arg2)
{
	return arg1 - arg2;
}

int Multiplies(int arg1,int arg2)
{
	return arg1 * arg2;
}

int Divides(int arg1,int arg2)
{
	return arg1 / arg2;
}

int MyOpn(int arg1,int arg2)
{
	return arg1 * 10 + arg2 * 20;
}

void Transform(int *F1,int *L1,int *F2,int *F3,int (*op)(int arg1,int arg2))
{
	for(;F1!=L1;++F1,++F2,++F3)
		*F3 = op(*F1,*F2);
}

void display(int *F,int *L)
{
	for(;F!=L;++F)
		cout<<*F<<"\t";
	cout<<endl;
}

void main()
{
	int a[] = {1,5,9,6,3};
	int b[] = {2,7,5,20,1};
	int c[] = {0,0,0,0,0,0,0,0,0,0};

	Transform(a+1,a+5,b+0,c+0,MyOpn);

	display(a,a+5);
	display(b,b+5);
	display(c,c+5);
}