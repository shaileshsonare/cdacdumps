#include<iostream>
#include<algorithm>
#include<functional>
#include<vector>
#include<string>
#include<list>
using namespace std;

#define BY_AGE 1

class Employee
{
	string name;
	int age;
public:
	Employee() : age(0) {}
	Employee(string nm,int a) : name(nm), age(a) {}
	int getAge()
	{
		return age;
	}
	string getName()
	{
		return name;
	}
	friend ostream& operator<<(ostream& stream,const Employee& e)
	{
		stream<<endl<<e.name<<"\t"<<e.age;		
		return stream;
	}
	bool operator>(const Employee& e)
	{
		return age > e.age;
	}
};

class EmpCriteria	// My unary function object
{
	int age;
public:
	EmpCriteria(int a) : age(a) {}
	Employee* operator()(Employee *e)
	{
		return (e->getAge()>age) ? e : 0;
	}
};

template<class II>
void display(II F,II L)
{
	for(;F!=L;++F)
		if(*F!=NULL)
			cout<<**F<<"\t";
	cout<<endl;
}

template<class T>
class DBMgr
{
public:
	static void getAllEmployees(T container)
	{
		// fires the SQL and returns a collection of Employees

		// example...

		container->push_back(new Employee("ABC",25));
		container->push_back(new Employee("ABC",23));
		container->push_back(new Employee("ABC",22));
		container->push_back(new Employee("ABC",21));
		container->push_back(new Employee("ABC",20));
		container->push_back(new Employee("ABC",28));
		container->push_back(new Employee("ABC",30));
		container->push_back(new Employee("ABC",31));
	}
};

template<class II,class OI>
class BusinessCriteriaMgr
{
public:
	static void getByCriteria(II F1,II L1,OI F2,int type,int val)
	{
		if(type==BY_AGE)
			transform(F1,L1,F2,EmpCriteria(val));
	}
};

void main()
{
	vector<Employee*> *v1 = new vector<Employee*>;

	DBMgr<vector<Employee*>*>::getAllEmployees(v1);

	int age;
	cout<<"Enter age criteria : ";
	cin>>age;

	list<Employee*> *v2 = new list<Employee*>(v1->size());
	
	BusinessCriteriaMgr<vector<Employee*>::iterator,list<Employee*>::iterator>::getByCriteria(v1->begin(),v1->end(),v2->begin(),BY_AGE,age);

	display(v1->begin(),v1->end());	
	display(v2->begin(),v2->end());
}