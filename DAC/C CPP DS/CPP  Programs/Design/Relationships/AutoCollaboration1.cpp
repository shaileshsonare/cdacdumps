#include<iostream>
using namespace std;

class A
{
	A *a;
public:
	A() : a(0) {}
	A(A *_a) : a(_a) {	}
	virtual void f1()
	{
		cout<<"A.f1()"<<endl;
		a->f2();
	}
	virtual void f2()
	{
		cout<<"A.f2()"<<endl;
	}
};

class A1 : public A
{
public:
	A1() {}
	A1(A *_a) : A(_a) {	}
	void f1()
	{
		cout<<"A1.f1()"<<endl;
		A::f1();
	}
	void f2()
	{
		cout<<"A1.f2()"<<endl;
	}
};

class A2 : public A
{
public:
	A2() {}
	A2(A *_a) : A(_a) {	}
	void f1()
	{
		cout<<"A2.f1()"<<endl;
		A::f1();
	}
	void f2()
	{
		cout<<"A2.f2()"<<endl;
	}
};

void main()
{
	A *a1 = new A(new A);		// A has A
	cout<<"A has A"<<endl;
	a1->f1();
	
	A1 *a2 = new A1(new A);		// A1 has A
	cout<<"\nA1 has A"<<endl;
	a2->f1();

	A *a3 = new A(new A1);		// A has A1
	cout<<"\nA has A1"<<endl;
	a3->f1();

	A1 *a4 = new A1(new A2);	// A1 has A2
	cout<<"\nA1 has A2"<<endl;
	a4->f1();

	A2 *a5 = new A2(new A1);	// A2 has A1
	cout<<"\nA2 has A1"<<endl;
	a5->f1();
}