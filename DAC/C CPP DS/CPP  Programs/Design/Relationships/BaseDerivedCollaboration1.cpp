#include<iostream>
using namespace std;

class A
{
public:
};

class A1 : public A
{
	A *a;
public:
	A1() {}
	A1(A *_a) : a(_a) {	}
};

class A2 : public A
{
public:
};

void main()
{
	A1 *a1 = new A1(new A1);		// A1 has A1

	A1 *a2 = new A1(new A);		// A1 has A

	A1 *a3 = new A1(new A2);		// A1 has A2
}