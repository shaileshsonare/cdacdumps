#include<iostream>
using namespace std;

class A
{
	int x[100];	
public:
	A()
	{
		cout<<"A()"<<this<<endl;
	}
	A(A& a)
	{
		cout<<"A(A&)"<<this<<endl;
	}

	~A()
	{
		cout<<"~A()"<<this<<endl;
	}

};

A f1(A obj)
{
	A a1;
	return a1;
}

void main()
{
	A a1,a2;
	a2 = f1(a1);
}