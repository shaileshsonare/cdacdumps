#include<iostream>
using namespace std;

class A
{
	int x[100];	
public:
	A()
	{
		cout<<"A()"<<endl;
	}
	A(A& a)
	{
		cout<<"A(A&)"<<endl;
	}
	~A()
	{
		cout<<"~A()"<<endl;
	}
	void operator=(const A& a)
	{
		cout<<"operator="<<endl;
	}	
};

A& f1(A &obj)
{
	A a1;
	return obj;
}

void main()
{
	A a1,a2;
	a2 = f1(a1);	// a2 = temp;		// a2.operator=(temp);
}