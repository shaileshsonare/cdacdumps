#include<iostream>
using namespace std;

class A
{	
public:
	A()
	{
		cout<<"A()"<<endl;
	}
	A(const A& a)
	{
		cout<<"A(A&)"<<endl;
	}
	A& operator=(const A& a)
	{
		cout<<"operator=(A)"<<endl;
		return *this;
	}
	~A()
	{
		cout<<"~A()"<<endl;
	}
};

class B
{	
public:
	B()
	{
		cout<<"B()"<<endl;
	}
	B(const B& b)
	{
		cout<<"B(B&)"<<endl;
	}
	B& operator=(const B& b)
	{
		cout<<"operator=(B)"<<endl;
		return *this;
	}
	~B()
	{
		cout<<"~B()"<<endl;
	}
};

class C : public B, public A
{	
public:
	C()
	{
		cout<<"C()"<<endl;
	}
	C(const C& c)
	{
		cout<<"C(C&)"<<endl;
	}
	C& operator=(const C& c)
	{
		cout<<"operator=(C)"<<endl;
		return *this;
	}
	~C()
	{
		cout<<"~C()"<<endl;
	}
};

void main()
{
	C c1;
}