#include <iostream>
using namespace std;

class BaseClass {
  int i;
protected:
  int j;
public:
  int k;
  void setInt(int a) { 
     i = a; 
  }
  int getInt() { 
     return i; 
  }
};


class DerivedClass : protected BaseClass {  // Inherit BaseClass as protected.
public:
  void setj(int a) { 
     j = a; 
  } 
  void setk(int a) { 
     k = a; 
  } 
  int getj() { 
     return j; 
  }
  int getk() { 
     return k; 
  }
};

int main()
{
  DerivedClass ob;

  ob.setk(10);
  cout << ob.getk() << ' ';
  ob.setj(12);
  cout << ob.getj() << ' ';

  return 0;
}