#include <iostream>
using namespace std;

class BaseClass1 {
  int a;
public:
  BaseClass1(int x) { 
     a = x; 
  }
  int geta() { 
     return a; 
  }
};


class BaseClass2 {
  int b;
public:
  BaseClass2(int x) 
  {
    b = x;
  }
  int getb() { 
    return b; 
  }
};


class DerivedClass : public BaseClass1, public BaseClass2 { 
  int c;
public:
  DerivedClass(int x, int y, int z) : BaseClass1(z), BaseClass2(y){
    c = x;
  }

  void show() {
    cout << geta() << ' ' << getb() << ' ';
    cout << c << '\n';
  }
};

int main()
{
  DerivedClass object(1, 2, 3);

  object.show();

  return 0;
}
