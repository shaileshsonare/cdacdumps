#include <iostream>
#include <string.h>
using namespace std;
class Curly 
{
 public:
   Curly(char *msg) { strcpy(message, msg); };
   void show_message(void) { cout << message << endl; };
   friend class Moe;
   void show_moe(class Moe moe);
 private:
   char message[256];
 };


class Moe 
{
 public:
   Moe(char *msg) { strcpy(message, msg); };
   void show_message(void) { cout << message << endl; };
   friend class Curly;
   void show_curly(class Curly curly);
 private:
   char message[256];
 };


void Curly::show_moe(class Moe moe) { cout << moe.message << endl; };

void Moe::show_curly(class Curly curly) { cout << curly.message << endl; };

int main(void)
{
   class Moe moe("nuck...");
   class Curly curly("whoop...");
 
   moe.show_message();
   moe.show_curly(curly);
   curly.show_message();
   curly.show_moe(moe);
}