#include<iostream.h>
#include<conio.h>

class A
{
 int a;
 int b;
public:
 A(int x,int y)
 {
  a=x;b=y;
  cout<<"\n\n A is constructed :";
  cout<<"\n a = "<<a;
  cout<<"\n b = "<<b;
  };
};

class B
{
 int c;
 int d;
public:
 B(int x,int y)
 {
  c=x;d=y;
  cout<<"\n\n B is constructed :";
  cout<<"\n c = "<<c;
  cout<<"\n d = "<<d;
  };
};
class C: public B,private A
{
 int k;
 int j;
public:
 C(int p1,int p2,int p3,int p4,int p5,int p6):
 A(p1,p2),B(p3,p4),k(p5+p6),j(k)
 {
  cout<<"\n\n C is constructed :";
  cout<<"\n k = "<<k;
  cout<<"\n j = "<<j;
  };
};

void main()
{
clrscr();
C obj(1,2,3,4,5,6);

getch();
}