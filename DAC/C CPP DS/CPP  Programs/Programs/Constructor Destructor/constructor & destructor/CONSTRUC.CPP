#include<iostream.h>
#include<conio.h>
#include<string.h>

class employee
{
 
private:
	 int id;
	 char name[15];
	 int salary;
 public :
	 employee();
	 employee(int,char[],int);
	 employee(employee&);
	 void putdata();
};

//member function definition
void employee::putdata()
 {
	 cout<<"\n\nid : "<<id<<" name : "<<" salary :"<<salary;
 }

employee::employee()
{
	id=0;
	strcpy(name,"");
	salary=0;
}

employee::employee(int id,char name[],int salary)
{
	this->id=id;
	strcpy(this->name,name);
	this->salary=salary;
}
employee::employee(employee &x)
{
 	id=x.id;
	strcpy(name,x.name);
	salary=x.salary;
}
void main()
{
	clrscr();
	employee e1;
	e1.putdata();
	employee e2(2,"xyz",5000);
	e2.putdata();
	employee e3(e2);       //copy constructor
	e3.putdata();
	getch();
}




