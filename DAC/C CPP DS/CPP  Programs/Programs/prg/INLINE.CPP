/*
Write a C++ program using class to calculate square and cube of 
given number using inline function.
*/

#include<iostream.h>
#include<conio.h>

inline float square(float x)
 {
  return(x*x);
 }
inline float cube(float x)
{
 return(x*x*x);
}
void main()
{
 float num;
 clrscr();
 cout<<"\n Enter the number : ";
 cin>>num;
 cout<<"\nSquare of number is : "<<square(num)<<endl;
 cout<<"\n\n Enter the number : ";
 cin>>num;
 cout<<"\nCube of number is : "<<cube(num)<<endl;
 getch();
}
/*

 Enter the number : 4                                                           
                                                                                
Square of number is : 16                                                        
                                                                                
                                                                                
 Enter the number : 5                                                           
                                                                                
Cube of number is : 125                                                         
                                                                                
*/
                                                                                
                                                                                
                                                                                

                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                


