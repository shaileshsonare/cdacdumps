/*Create two classes dist1(meters, centimeters) and dist2 (feet ,inches ). Accept two distances from the user, one in meters and centimeters and the other in feet and inches. Find the sum and difference of the two distances. Display the result in both, meters and centimeters as well as feet and inches (Use friend Function)*/
#include<sam.h>
class dist2;
class dist1
{
	float m,cm;
	public:
	void accept(void);
	void display(void);
	friend void add(dist1 ,dist2);
};
void dist1::accept()
{
	cout<<"\nEnter Distance In Meters And Centimeters:-";
	cin>>m>>cm;
}
void dist1::display()
{
	cout<<"\nDistance Is :-"<<m<<"."<<cm;
}
class dist2
{
	float f,in;
	public:
	void accept(void);
	void display(void);
	friend void add(dist1 ,dist2 );
};
void dist2::accept()
{
	cout<<"\n Enter Distance In Feets And Inches:-";
	cin>>f>>in;
}
void dist2::display()
{
	cout<<"\n Distance Is:-"<<f<<"."<<in;
}
void add(dist1 Q,dist2 B)
{
	float a,b,c;
	a=(Q.m*100)+Q.cm;    //Conversion in centimeter
	b=(B.f*30.48)+(B.in*2.54);       //Conversion in centimeter
	c=a+b;
	cout<<"\n \n Distance In Centimeters:-";
	cout<<c;
	cout<<"\n\n Distance In meters:-";
	cout<<(c/100);
	cout<<"\n\n Distance In Feets:-";
	cout<<(c/30.48);
	cout<<"\n\nDistance In Inches:-";
	cout<<(c/2.54);
}
void main()
{
	dist1 D;
	dist2 E;
	clrscr();
	cout<<"\n Enter First Distance :-";
	D.accept();
	cout<<"\n Enter Second Distance:-";
	E.accept();
	cout<<"\n Addition Is:-";
	add(D,E);
	getch();
}