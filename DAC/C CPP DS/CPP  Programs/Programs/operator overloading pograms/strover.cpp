/*Create a class Cstring as follows
     class Cstring
	{
		char *ptr;
		int len;
		public :
		Cstring (char * str); // constructor
		Cstring (Cstring &obj); // Constructor
	};
	overload the following operators 
	operator		example		meaning
                = 		s2 = s1		Assigns contents of s1 to s2	
	 +		s3 = s1 + s2	Concatenates s2 to s1 and stores the result in s3 */
#include<sam.h>
#include<string.h>
#define size 50
class Cstring
{
	char *ptr;
	int len;
	public:
	Cstring(char *str='\0')
	{
		len=strlen(ptr);
		ptr= new char[size];
		strcpy(ptr,str);
	}
	Cstring(Cstring &S1)
	{
		len=strlen(ptr);
		ptr=new char[len];
		strcpy(ptr,S1.ptr);
	}
	void accept();
	void display();
	int length();
	Cstring operator =(Cstring);
	Cstring operator +(Cstring);
};
void Cstring::accept()
{
	cout<<"\nEnter String:-";
	cin.getline(ptr,size);
}
void Cstring::display()
{
	cout<<"\n String Is:-";
	cout<<ptr;
}
Cstring Cstring::operator =(Cstring X)
{
	strcpy(ptr,X.ptr);
	return *this;
}
Cstring Cstring::operator +(Cstring Z)
{
	Cstring W;
	strcpy(W.ptr,ptr);
//	display();
	strcat(W.ptr,Z.ptr);
//	display();
	return W;
}
void main()
{
	Cstring A,B,C;
	clrscr();
	cout<<"\n Enter First Object:-";
	A.accept();
	cout<<"\n Entered Object Is:-";
	A.display();
	cout<<"\n Assigned Object Is:-";
	B=A;
	B.display();
	cout<<"\n Addition Of Both Objects Is:-";
	C=A+B;
	C.display();
	getch();
}