#include <iostream>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

int main()
{
  vector<int> v(5);
  bool found;

  for (int i = 0; i < 5; ++i)
     v[i] = i;

  found = binary_search(v.begin(), v.end(), 3);
  cout << found << " ";

  // Try searching for a value that's not present:
  found = binary_search (v.begin(), v.end(), 9);
  cout << found;

  return 0;
}

/* 
1 0
 */  