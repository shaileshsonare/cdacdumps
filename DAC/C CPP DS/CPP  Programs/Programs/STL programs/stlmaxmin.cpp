#include <numeric>
#include <vector>
#include <iostream>

using namespace std;

bool norm_less_than( const vector<double>& a,const vector<double>& b );

int main( )
{
   const double data1[] = { 1.77, 3.33, -0.44 };
   const double data2[] = { -1.99, -2.11, 4.33 };

   vector<double> v1( data1, data1+sizeof( data1 )/sizeof( double ) );
   vector<double> v2( data2, data2+sizeof( data2 )/sizeof( double ) );

   const vector<double>& min_vector = min( v1, v2, norm_less_than );
   const vector<double>& max_vector = max( v1, v2, norm_less_than );

}

// returns true if the norm of a is < norm of b, false otherwise
inline
bool norm_less_than( const vector<double>& a,const vector<double>& b )
{
   return inner_product( a.begin(), a.end(), a.begin(), 0.0 )
        < inner_product( b.begin(), b.end(), b.begin(), 0.0 );
}