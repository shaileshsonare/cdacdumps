#include<iostream>
#include<string>
using namespace std;

class TaskManager
{
	TaskManager() {}
	static TaskManager* instance;
public:
	static TaskManager* getInstance()
	{
		if(instance==0)
			instance = new TaskManager;
		return instance;
	}
};

TaskManager* TaskManager::instance;

// many clients

void client1()
{
	TaskManager *tm = TaskManager::getInstance();
	cout<<tm<<endl;
}

void client2()
{
	TaskManager *tm = TaskManager::getInstance();
	cout<<tm<<endl;
}

void main()
{
	TaskManager *tm = TaskManager::getInstance();
	cout<<tm<<endl;

	client1();

	client2();
}