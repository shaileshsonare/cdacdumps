#include<iostream>
#include<string>
using namespace std;

template<class T>
T add(const T& arg1,const T& arg2)
{
	return arg1 + arg2;
}

void main()
{
	int a=10;
	int b = 20;

	float x = 12.3f;
	float y = 21.5f;

	// implicit instantiation

	cout<<add(a,b)<<endl;

	char *s1 = "IBM";
	char *s2 = "Gurgoan";

	cout<<add<string>(s1,s2)<<endl;
}