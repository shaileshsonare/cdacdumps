#include<iostream>
#include<string>
using namespace std;

class A
{
	int x;
	class B
	{
		int y;
	public:
		void show()
		{
			cout<<"B : "<<&y<<endl;
		}
	};
public:
	void demo()
	{
		cout<<"A.x : "<<&x<<endl;
		B b;
		b.show();
	}
};

void main()
{
	A a1;
	a1.demo();
}