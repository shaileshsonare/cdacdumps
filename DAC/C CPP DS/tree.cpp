#include<iostream>
using namespace std;
class node
{
  public:
         int data;
	 node *left;
	 node *right;
};

class tree
{
  node *root,*p,*q;
	public:
                  tree()
		  { root=NULL;
	            p =NULL;
		    q = NULL;}

		  int insert(int data)
		  {
			  node *n= new node;
			  n->data = data;
			  n->left = NULL;
			  n->right =NULL;

			  if(root == NULL)
			  {
				  root = n;
				  return 1;
			  }

			 p = q = root;
			while(p->data!=data && q!=NULL)
			{
				p = q;

				if(p->data < data)
					q=q->right ;
				else
					q=q->left;
			}

			if(p->data == data)
			{
				cout<<"Duplicate !!!\n";
				return 1;
			}
			 if(p->data < data)
				p->right = n;
			else
				p->left = n;
		  }

		  void display()
		  {
			  cout<<"Preorder BST\t::\t";
			preorder(root);
			cout<<endl;
			  cout<<"Inorder BST\t::\t";
		  	inorder(root);
			cout<<endl;
			  cout<<"Postorder BST\t::\t";
			postorder(root);
				cout<<endl;
		  	
		   }	
		   
		   void inorder(node *root)
		       {
			       if(root!=NULL)
			       {
		  			inorder(root->left);
					cout<<" "<<root->data;
			  		inorder(root->right);
			       }
			  }
		
		   void preorder(node *root)
		       {
			       if(root!=NULL)
			       {	cout<<" "<<root->data;
		  			preorder(root->left);
					preorder(root->right);

			       }
		       }
			       
		   void postorder(node *root)
		       {
			       if(root!=NULL)
			       {
		  			postorder(root->left);
			  		postorder(root->right);
					cout<<" "<<root->data;
			       }
		       }

		   void remove(int data)
		   {
			   if(root==NULL)
			   {
				   cout<<"Empty ";
					return;
			   }

			   node *r,*p,*q;

			   while(p->data!=data && q!=NULL)
			   {
				   r=p;
				   if(p->data<data)
					   q=q->right;
				   else
					   q=q->left;
			   }

			   if(p->data==data)
			   {
				   if(r->data==data)
				   {
					   if(r->data==data)
						   rdelete(r->right);
					   else
						   rdelete(r->left);
				   }
			   }
			
			   
		   }

		   void rdelete(node *p)
		   {
			  node *temp,*q,*current;

			  if(p->left==NULL && p->right==NULL)
			  {
				  temp=p;
				  p=NULL;
				  delete temp;
				  return;
			  }

			  if(p->left==NULL)
			  {
				  temp=p;
				  p=p->right;
				  delete temp;
			  }

			  if(p->right==NULL)
			  {
				  temp=p;
				  p=p->left;
				  delete temp;
			  }

			  p=current->right;
			  q=NULL;

			  while(p->left!=NULL)
			  {
				  q=p;
				  p=p->left;
			  }

			  current->data=p->data;

			  if(p==current->right)
				  current->right=p->right;
			  else
				  p->left=p->right;
			  delete p;

		   }
};

int main(void)
{
	tree t;
	t.insert(50);
	t.insert(25);
	t.insert(26);
	t.insert(69);
	t.insert(62);
	t.insert(61);
	t.insert(59);
	t.insert(98);
	t.insert(92);
	t.insert(90);
	t.insert(88);
	t.insert(89);	
	t.insert(100);
	t.remove(90);
	t.display();
	return 1;
}

																