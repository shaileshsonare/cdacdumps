#include<iostream>
using namespace std;

struct IA
{
	virtual void x()=0;
	virtual void y()=0;
};

class A1 : public IA
{
public:
	void x()
	{
		cout<<"Inside A1.x()"<<endl;
	}
	void y()
	{
		cout<<"Inside A1.y()"<<endl;
	}
};

class A2 : public IA
{
public:
	void x()
	{
		cout<<"Inside A2.x()"<<endl;
	}
	void y()
	{
		cout<<"Inside A2.y()"<<endl;
	}
};

class B : public A1
{
public:	
	void x()
	{
		cout<<"Accessing through B.x()"<<endl;
		A1::x();
	}
	void y()
	{
		cout<<"Accessing through B.y()"<<endl;
		A1::y();
	}
};

void client(IA *a)
{
	a->x();
	a->y();
}

void main()
{
	B *b = new B;
	client(b);
}