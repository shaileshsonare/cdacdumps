#include<iostream>
using namespace std;

class A
{	
public:
	A()
	{
		cout<<"A()"<<endl;
	}
	A(const A& a)
	{
		cout<<"A(A&)"<<endl;
	}
	A& operator=(const A& a)
	{
		cout<<"operator=(A)"<<endl;
		return *this;
	}
	~A()
	{
		cout<<"~A()"<<endl;
	}
};

class B
{	
public:
	B()
	{
		cout<<"B()"<<endl;
	}
	B(const B& b)
	{
		cout<<"B(B&)"<<endl;
	}
	B& operator=(const B& b)
	{
		cout<<"operator=(B)"<<endl;
		return *this;
	}
	~B()
	{
		cout<<"~B()"<<endl;
	}
};

class C
{	
	A a;
	B b;
public:
	C()
	{
		cout<<"C()"<<endl;
	}
	C(A& _a,B& _b) : a(_a), b(_b)
	{
		cout<<"C(A& a,B& b)"<<endl;
	}
	C(const C& c)
	{
		cout<<"C(C&)"<<endl;
	}
	C& operator=(const C& c)
	{
		cout<<"operator=(C)"<<endl;
		return *this;
	}
	~C()
	{
		cout<<"~C()"<<endl;
	}
};

void main()
{
	A a1;
	B b1;

	cout<<"Enter the block"<<endl;
	{
		C c1(a1,b1);
	}
	cout<<"Exit the block"<<endl;
}