#include<iostream>
#include<vector>
using namespace std;

class A
{
	int *x;	
	int size;
public:
	A() : x(0), size(0) {	}
	A(int sz) : x(0), size(sz)
	{
		x = new int[size];
		for(int i=0;i<size;++i)
			x[i] = i+1;
	}
	A(const A& a) : size(a.size)
	{
		x = new int[size];
		for(int i=0;i<size;++i)
			x[i] = a.x[i];
	}
	~A()
	{
		delete[] x;
	}
	A& operator=(const A& a)
	{
		if(this==&a)
			;
		else
		{
			if(size!=a.size)
			{
				delete[] x;
				size = a.size;
				x = new int[size];
			}

			for(int i=0;i<size;++i)
				x[i] = a.x[i];
		}

		return *this;
	}	
	int& operator[](int index)
	{
		cout<<"int&"<<endl;
		return x[index];
	}
	const int& operator[](int index) const
	{
		return x[index];
	}
};

void f1(const A& obj)
{
	//obj[0] = 200;
	cout<<obj[0]<<endl;
}

void main()
{
	A a1(5);
	
	//a1[0] = 100;
	//cout<<a1[0]<<endl;

	f1(a1);

	//cout<<a1[0]<<endl;
}