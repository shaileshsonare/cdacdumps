#include <iostream>
using namespace std;

void myFunction(int test)
{
  try{
    if(test==0)  // throw int
       throw test;   
    if(test==1)  // throw char
       throw 'a';    
    if(test==2)  // throw double
       throw 123.23; 
  } catch(int i) { // catch an int exception
    cout << "Caught " << i << '\n';
  } catch(...) {   // catch all other exceptions
    cout << "Caught One!\n";
  }
}

int main()
{
  cout << "start\n";

  myFunction(0);
  myFunction(1);
  myFunction(2);

  cout << "end";

  return 0;
}