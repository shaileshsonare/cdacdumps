#include <iostream>
using namespace std;


void myFunction(int test)
{
  try{
    if(test) 
      throw test;
  }
  catch(int i) {
    cout << "Caught One!  Ex. #: " << i << '\n';
  }
}

int main()
{
  cout << "start\n";

  myFunction(1);
  myFunction(2);
  myFunction(0);
  myFunction(-3);

  cout << "end";

  return 0;
}
