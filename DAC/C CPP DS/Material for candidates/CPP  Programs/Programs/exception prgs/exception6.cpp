#include <iostream>
using namespace std;

int main()
{
  cout << "Start\n";

  try { 
    cout << "Inside try block\n";
    cout << "Still inside try block\n";
  }catch (int i) { // catch an error
    cout << "Caught an exception and value is: ";
    cout << i << endl;
  }

  cout << "End";

  return 0;
}