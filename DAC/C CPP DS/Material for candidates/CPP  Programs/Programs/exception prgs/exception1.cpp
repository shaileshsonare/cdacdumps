/*Step 1: Start the program.
Step 2: Declare the variables a,b,c.
Step 3: Read the values a,b,c,.
Step 4: Inside the try block check the condition.
            a. if(a-b!=0) then calculate the value of d and display.
            b. otherwise throw the exception.
Step 5: Catch the exception and display the appropriate message.
Step 6: Stop the program.
PROGRAM:*/

#include<iostream.h>
#include<conio.h>
void main()
{
   int a,b,c;
   float  d;
   //clrscr();
   cout<<"Enter the value of a:";
   cin>>a;
   cout<<"Enter the value of b:";
   cin>>b;
   cout<<"Enter the value of c:";
   cin>>c;
   
   try
   {
              if((a-b)!=0)
              {
                 d=c/(a-b);
                 cout<<"Result is:"<<d;
              }
              else
              {
                 throw(a-b);
              }
   }
 
   catch(int i)
   {
              cout<<"Answer is infinite because a-b is:"<<i;
   }
  
   getch();
}