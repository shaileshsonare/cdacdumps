/*pointer to 2d array */
#include<conio.h>
#include<stdio.h>

void main()
{
	int a[2][3];
	int *p1,i,j;
	clrscr();
	printf("\nEnter 6 Integers :");
	for(i=0;i<2;i++)
	{
		for(j=0;j<3;j++)
		{
			printf("\n%d %d number : ",i,j);
			scanf("%d",&a[i][j]);
		}
	}

	p1=&a[0][0];
	for(i=0;i<6;i++)
	{
		printf("\n%d",*(p1+i));
	}
}/*end of main */
