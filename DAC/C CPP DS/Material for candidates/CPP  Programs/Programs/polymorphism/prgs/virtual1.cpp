#include <iostream>
using namespace std;

class BaseClass {
public:
  virtual void virtualFunction() {
    cout << "Base\n";
  }
};

class DerivedClass1 : public BaseClass {
public:
  void virtualFunction() {
    cout << "First derivation\n";
  }
};

class DerivedClass2 : public DerivedClass1 {
};

int main()
{
  BaseClass baseObject;
  BaseClass *p;
  DerivedClass1 derivedObject1;
  DerivedClass2 derivedObject2;

  p = &baseObject;
  p->virtualFunction();

  p = &derivedObject1;
  p->virtualFunction();

  p = &derivedObject2;
  p->virtualFunction();
  return 0;
}