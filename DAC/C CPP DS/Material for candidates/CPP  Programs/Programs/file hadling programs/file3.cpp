  
#include <iostream>
#include <stdlib.h>
#include <fstream>
using namespace std;
int main(int argc, char *argv[])
 {
   char buffer[1];
   
   //ifstream input(argv[1], ios::in);
   
   
   ifstream input("input.txt", ios::in); 
   if (input.fail())
    {
      cout << "Error opening the file ";
      exit(1);
    }

   //ofstream output(argv[2], ios::out);
   ofstream output("output.txt", ios::out);
   if (output.fail())
    {
      cout << "Error opening the file " ;
      exit(1);
    }
 
 
   do {
     input.read(buffer, sizeof(buffer));
     if (input.good())
       output.write(buffer, sizeof(buffer));
   } while (! input.eof());

   input.close();
   output.close(); 
   return 1;
}
  