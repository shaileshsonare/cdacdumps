#include <iostream>
#include <fstream>
using namespace std;

int main(int argc, char *argv[])
{
  /*if(argc != 2) {
    cout << "Usage: Display <filename>\n";
    return 1;
  }*/

  //ifstream in(argv[ 1 ]);                 // input

  ifstream in("input.txt");                 // input


  if(!in) {
    cout << "Cannot open input file.\n";
    return 1;
  }

  char str[ 255 ];

  while(in) {
    in.getline(str, 255);                // delim defaults to '\n'
    if(in) 
       cout << str << endl;
  }

  in.close();

  return 0;
}