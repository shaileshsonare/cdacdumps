#include <iostream>
using namespace std;

class BaseClass {
protected:
  int i, j; // private to BaseClass, but accessible to DerivedClass
public:
  void set(int a, int b) { 
     i = a; 
     j = b; 
  }
  void show() { 
     cout << i << " " << j << endl; 
  }
};

class DerivedClass : public BaseClass {
  int k;
public:
  // DerivedClass may access BaseClass's i and j
  void setk() { 
     k = i*j; 
  }

  void showk() { 
     cout << k << endl; 
  }
};

int main()
{
  DerivedClass ob;

  ob.set(2, 3);
  ob.show();   

  ob.setk();
  ob.showk();

  return 0;
}