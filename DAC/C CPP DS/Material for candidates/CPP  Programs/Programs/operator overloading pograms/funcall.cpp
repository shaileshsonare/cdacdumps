#include <iostream.h>



class three_d {
private
  int x, y, z;
public:
  three_d() { x = y = z = 0; }
  
  three_d(int i, int j, int k) { x = i; y = j; z = k; }

  three_d operator()(three_d obj);
  three_d operator()(int a, int b, int c);

  friend ostream &operator<<(ostream &strm, three_d op);
};

three_d three_d::operator()(three_d obj)
{
  three_d temp;

  temp.x = (x + obj.x) / 2;
  temp.y = (y + obj.y) / 2;
  temp.z = (z + obj.z) / 2;

  return temp;
}

three_d three_d::operator()(int a, int b, int c)
{
  three_d temp;

  temp.x = x + a;
  temp.y = y + b;
  temp.z = z + c;

  return temp;
}

ostream &operator<<(ostream &strm, three_d op) {
  strm << op.x << ", " << op.y << ", " << op.z << endl;
  return strm;
}

int main()
{
  three_d objA(1, 2, 3), objB(10, 10, 10), objC;

  cout << "This is objA: " << objA;
  cout << "This is objB: " << objB;

  objC = objA(objB);
  cout << "objA(objB): " << objC;

  objC = objA(10, 20, 30);
  cout << "objA(10, 20, 30): " << objC;

  objC = objA(objB(100, 200, 300));
  cout << "objA(objB(100, 200, 300)): " << objC;

  return 0;
}
  
    
  