#include<iostream>
using namespace std;

void f1(int* const p)
{
	cout<<*p<<endl;		//Read
	*p = 100;			//Write
	p = new int(50);	//Allocate
	delete p;			//Deallocate
}
void main()
{
	int *a = new int(50);
	f1(a);
}